<?php

Route::get('/boss_login', 'Admin\AdminController@getAdminLogin')->name('adminLogin');
Route::post('/login_admin', 'Admin\AdminController@postAdminLogin')->name('postadminLogin');

Auth::routes();
Route::prefix('/')->group(function () {
    Route::get('/dashboard', 'Admin\DashboardController@getAdminDashboard')->name('adminDashboard')->middleware('auth');
    Route::get('/logout', 'Admin\AdminController@getAdminLogout')->name('adminLogout')->middleware('auth');
//    Route::get('/testing', 'Admin\DashboardController@getTest')->name('test')->middleware('auth');
   
    //khusus manager
    Route::get('/add-crew', 'Admin\ManagerController@getAddCrew')->name('add_crew')->middleware('auth');
    Route::post('/new-crew', 'Admin\ManagerController@postAddCrew')->name('new_crew')->middleware('auth');
    Route::get('/view-crew', 'Admin\ManagerController@getViewCrew')->name('view_crew')->middleware('auth');
    Route::get('/edit-crew/{id}', 'Admin\ManagerController@getEditCrew')->name('edit_crew')->middleware('auth');
    Route::post('/edit-crew/{id}', 'Admin\ManagerController@postEditCrew')->name('post_edit_crew')->middleware('auth');
    Route::get('/rm-crew/{id}', 'Admin\ManagerController@getDeleteCrew')->name('delete_crew')->middleware('auth');
    Route::get('/my-profile', 'Admin\ManagerController@getMyProfile')->name('my_profile')->middleware('auth');
    Route::get('/listupgrade-crew', 'Admin\ManagerController@getListUpgradeCrew')->name('list_upgrade_crew')->middleware('auth');
    Route::get('/upgrade-crew/{id}', 'Admin\ManagerController@getUpgradeCrew')->name('upgrade_crew')->middleware('auth');
    Route::post('/upgrade/{id}', 'Admin\ManagerController@postUpgradeCrew')->middleware('auth');
    Route::get('/m/struktur/{id}', 'Admin\ManagerController@getMStructure')->name('m_str')->middleware('auth');
    
    Route::get('/m/data-sales', 'Admin\ManagerController@getDataPenjualan')->name('data_penjualan')->middleware('auth');
    Route::get('/m/input-sale', 'Admin\ManagerController@getInputPenjualan')->name('input_penjualan')->middleware('auth');
    Route::post('/m/input-sale', 'Admin\ManagerController@postInputPenjualan')->name('post_input_penjualan')->middleware('auth');
//    Route::get('/m/edit-sale/{id}', 'Admin\ManagerController@getEditPenjualan')->name('edit_sale')->middleware('auth');
//    Route::get('/m/rm-sale/{id}', 'Admin\ManagerController@getDeletePenjualan')->name('rm_sale')->middleware('auth');
//    Route::post('/m/action-sale/{type}/{id}', 'Admin\ManagerController@postActionPenjualan')->middleware('auth');
    Route::get('/m/data-sales-safra', 'Admin\ManagerController@getDataPenjualanSafra')->name('data_penjualan_safra')->middleware('auth');
    Route::get('/m/input-sale-safra', 'Admin\ManagerController@getInputPenjualanSafra')->name('input_penjualan_safra')->middleware('auth');
    Route::post('/m/input-sale-safra', 'Admin\ManagerController@postInputPenjualanSafra')->middleware('auth');
    
    Route::get('/m/history/stock', 'Admin\ManagerController@getHistoryStock')->name('history_stock')->middleware('auth');
    Route::get('/m/stock-barang', 'Admin\ManagerController@getStockBarang')->name('stock_barang')->middleware('auth');
    Route::get('/m/add/input-stock/{purchase_id}', 'Admin\ManagerController@getInputStock')->name('input_stock')->middleware('auth');
    Route::post('/m/add/input-stock/{purchase_id}', 'Admin\ManagerController@postInputStock')->name('post_input_stock')->middleware('auth');
    
    Route::get('/myreport', 'Admin\ManagerController@getMyReports')->name('my_reports')->middleware('auth');
    
    Route::get('/m/myglobal', 'Admin\ManagerController@getMyGlobal')->name('my_global')->middleware('auth');
    Route::get('/m/global-manager/{type}', 'Admin\ManagerController@getMyGlobalManager')->name('my_global_manager')->middleware('auth');
    Route::get('/m/global-asmen', 'Admin\ManagerController@getMyGlobalAsmen')->name('my_global_asmen')->middleware('auth');
    Route::get('/m/global-tld', 'Admin\ManagerController@getMyGlobalTld')->name('my_global_tld')->middleware('auth');
    Route::get('/m/global-crew/{type}', 'Admin\ManagerController@getMyGlobalCrew')->name('my_global_crew')->middleware('auth');
    Route::get('/m/global-stock/{type}', 'Admin\ManagerController@getMyGlobalStock')->name('my_global_stock')->middleware('auth');
    
    Route::get('/m/detail-sales', 'Admin\ManagerController@getManagerDetailSales')->name('manager_detail_sales')->middleware('auth');
    Route::post('/m/detail-sales', 'Admin\ManagerController@postManagerDetailSales')->middleware('auth');
    
    Route::get('/m/report-crew', 'Admin\ManagerController@getMReportCrew')->name('m_report_crew')->middleware('auth');
    Route::post('/m/report-crew', 'Admin\ManagerController@postMReportCrew')->middleware('auth');
    
    Route::get('/m/tree/structure', 'Admin\ManagerController@getTree')->name('m_mySponsorTree')->middleware('auth');
    
    Route::get('/mqb/order-barang', 'Admin\ManagerController@getOrderBarang')->name('order_barang')->middleware('auth');
    Route::get('/mqb/list-order-barang', 'Admin\ManagerController@getListOrderStock')->name('list_order_barang')->middleware('auth');
    Route::post('/mqb/order-barang', 'Admin\ManagerController@postOrderInputStock')->middleware('auth');
    Route::get('/mqb/detail/order-barang/{order_id}', 'Admin\ManagerController@getMQBOrderBarangId')->name('mqb_order_barang_id')->middleware('auth');
//    Route::post('/mqb/terima-barang', 'Admin\ManagerController@postTerimaBarang')->middleware('auth');
    Route::get('/mqb/confirm-barang/{order_id}/{item_id}', 'Admin\ManagerController@getMQBConfirmBarang')->name('mqb_confirm_barang_id')->middleware('auth');
    Route::post('/mqb/terima-barang/{order_id}/{item_id}', 'Admin\ManagerController@postTerimaBarangPerItem')->middleware('auth');
    
    Route::get('/mqb/customers', 'Admin\ManagerController@getMQBListCustomer')->name('mqb_list_customer')->middleware('auth');
    Route::post('/mqb/customers', 'Admin\ManagerController@postMQBListCustomer')->middleware('auth');
    Route::get('/mqb/input-customers', 'Admin\ManagerController@getMQBInputCustomer')->name('mqb_input_customer')->middleware('auth');
    Route::post('/mqb/input-customers', 'Admin\ManagerController@postMQBInputCustomer')->middleware('auth');
    
    Route::get('/m/absen', 'Admin\ManagerController@getManagerListAbsen')->name('list_absen')->middleware('auth');
    Route::get('/m/absen-detail/{user_id}', 'Admin\ManagerController@getManagerAbsenDetail')->name('absen_detail')->middleware('auth');
    Route::get('/m/absen-create', 'Admin\ManagerController@getManagerAbsenCreate')->name('create_absen')->middleware('auth');
    Route::post('/m/absen', 'Admin\ManagerController@postManagerAbsen')->middleware('auth');
    
     Route::get('/m/input/pengeluaran', 'Admin\ManagerController@getManagerCreatePengeluaran')->name('create_pengeluaran')->middleware('auth');
     Route::post('/m/input/pengeluaran', 'Admin\ManagerController@postManagerPengeluaran')->middleware('auth');
    
    Route::get('/m/transfers', 'Admin\ManagerController@getManagerListTransfer')->name('m_transfer')->middleware('auth');
    Route::get('/m/transfer/{id}/{tpye}', 'Admin\ManagerController@getManagerTransferById')->name('m_transfer_id')->middleware('auth');
    Route::post('/m/confirm/transfer', 'Admin\ManagerController@postManagerConfirmTransfer')->middleware('auth');
    
    Route::get('/m/bonus/{id}', 'Admin\ManagerController@getManagerBonusCrew')->name('m_bonus_crew')->middleware('auth');
    Route::get('/m/my-bonus', 'Admin\ManagerController@getManagerMyBonus')->name('m_my_bonus')->middleware('auth');
    
    Route::get('/manager/transfers', 'Admin\ManagerController@getUpperManagerTransfer')->name('u_m_transfer')->middleware('auth');
    
    Route::get('/mqb/input/pengeluaran', 'Admin\ManagerController@getMQBCreatePengeluaran')->name('mqb_create_pengeluaran')->middleware('auth');
    Route::post('/mqb/input/pengeluaran', 'Admin\ManagerController@postMQBPengeluaran')->middleware('auth');
    Route::get('/mqb/list/pengeluaran', 'Admin\ManagerController@getMQBListPengeluaran')->name('mqb_list_pengeluaran')->middleware('auth');
    
    Route::get('/m/contents', 'Admin\ManagerController@getManagerContents')->name('mgr_content')->middleware('auth');
    Route::get('/m/content/{id}', 'Admin\ManagerController@getManagerContent')->name('mgr_content')->middleware('auth');
    
    Route::get('/m/list/bonus/manager', 'Admin\ManagerController@getManagerListBonus')->name('mgr_list_bonus')->middleware('auth');
    
    Route::get('/m/down-structures', 'Admin\ManagerController@getManagerDownOrganisasi')->name('m_down_structures')->middleware('auth');
    Route::get('/m/tree/top-structure', 'Admin\ManagerController@getManagerTreeOrganisasi')->middleware('auth');
//    Route::get('/m/org/bonus/{id}', 'Admin\ManagerController@getManagerBonusOrganisasi')->middleware('auth');
//    Route::get('/m/org/global-sales/{id}', 'Admin\ManagerController@getManagerGlobalSalesOrganisasi')->middleware('auth');
//    Route::get('/m/org/global-stock/{id}', 'Admin\ManagerController@getManagerGlobalStockOrganisasi')->middleware('auth');
    Route::get('/org/dashboard/{id}', 'Admin\OrganisasiController@getDashboard')->middleware('auth');
    Route::get('/org/data-sales/{id}', 'Admin\OrganisasiController@getDataSales')->middleware('auth');
    Route::get('/org/data-stock/{id}', 'Admin\OrganisasiController@getDataStock')->middleware('auth');
    Route::get('/org/data-transfer/{id}', 'Admin\OrganisasiController@getDataTransfer')->middleware('auth');
    Route::get('/org/global/detail-sales/{id}', 'Admin\OrganisasiController@getGlobalDetailSales')->middleware('auth');
    Route::post('/org/global/detail-sales/{id}', 'Admin\OrganisasiController@postGlobalDetailSales')->middleware('auth');
    Route::get('/org/data-transfer/{id}', 'Admin\OrganisasiController@getDataTransfer')->middleware('auth');
    Route::get('/org/data-crew/{id}', 'Admin\OrganisasiController@getDataCrew')->middleware('auth');
    Route::get('/org/tree-structure/{id}', 'Admin\OrganisasiController@getTreeStucture')->middleware('auth');
    Route::get('/org/data-absen/{id}', 'Admin\OrganisasiController@getDataAbsen')->middleware('auth');
    Route::get('/org/myreport/{id}', 'Admin\OrganisasiController@getOrganisasiMyReport')->middleware('auth');
    Route::get('/org/report-crew/{id}', 'Admin\OrganisasiController@getOrganisasiMReportCrew')->name('org_report_crew')->middleware('auth');
    Route::post('/org/report-crew/{id}', 'Admin\OrganisasiController@postOrganisasiMReportCrew')->middleware('auth');
    Route::get('/org/myglobal/{id}', 'Admin\OrganisasiController@getOrgMyGlobal')->middleware('auth');
    Route::get('/org/global/{id}/{type}', 'Admin\OrganisasiController@getOrgMyGlobalType')->middleware('auth');
    Route::get('/org/mystock/{id}', 'Admin\OrganisasiController@getOrgMyStock')->middleware('auth');
    Route::get('/org/stock/{id}/{type}', 'Admin\OrganisasiController@getOrgMyStockType')->middleware('auth');
    Route::get('/m/org/global/sales', 'Admin\OrganisasiController@getOrgGlobalSalesDownline')->middleware('auth');
    
    
    
    //khusus admin
    Route::get('/add-manager', 'Admin\MasterAdminController@getAddManager')->name('add_manager')->middleware('auth');
    Route::post('/new-manager', 'Admin\MasterAdminController@postAddManager')->name('new_manager')->middleware('auth');
    Route::get('/view-managers', 'Admin\MasterAdminController@getViewManager')->name('view_managers')->middleware('auth');
    Route::get('/view-manager/{id}', 'Admin\MasterAdminController@getViewDetailManager')->name('detail_manager')->middleware('auth');
    Route::post('/update-manager/{id}', 'Admin\MasterAdminController@postUpdateProfileManager')->middleware('auth');
    Route::get('/nonaktif-manager/{id}', 'Admin\MasterAdminController@getNonaktifManager')->middleware('auth');
    
    Route::get('/add-cabang', 'Admin\MasterAdminController@getAddCabang')->name('add_cabang')->middleware('auth');
    Route::post('/new-cabang', 'Admin\MasterAdminController@postAddCabang')->name('new_cabang')->middleware('auth');
    Route::get('/view-cabangs', 'Admin\MasterAdminController@getViewCabangs')->name('view_cabangs')->middleware('auth');
    Route::get('/view-cabang/{id}', 'Admin\MasterAdminController@getViewDetailCabang')->name('view_cabang')->middleware('auth');
    Route::post('/edit-cabang/{id}', 'Admin\MasterAdminController@postEditCabang')->name('edit_cabang')->middleware('auth');
    Route::get('/rm-cabang/{id}', 'Admin\MasterAdminController@getDeleteCabang')->name('delete_cabang')->middleware('auth');
    
    Route::get('/all-purchase', 'Admin\MasterAdminController@getAllPurchase')->name('all_purchase')->middleware('auth');
    Route::get('/add-purchase', 'Admin\MasterAdminController@getAddPurchase')->name('add_purchase')->middleware('auth');
    Route::post('/add-purchase', 'Admin\MasterAdminController@postAddPurchase')->name('post_add_purchase')->middleware('auth');
    Route::get('/edit-purchase/{id}', 'Admin\MasterAdminController@getEditPurchase')->name('edit_purchase')->middleware('auth');
    Route::post('/edit-purchase/{id}', 'Admin\MasterAdminController@postEditPurchase')->name('_postedit_purchase')->middleware('auth');
    Route::get('/status-purchase/{id}', 'Admin\MasterAdminController@getStatusPurchase')->name('rm_purchase')->middleware('auth');
    
    Route::get('/admin-report', 'Admin\MasterAdminController@getAdminReport')->name('admin_report')->middleware('auth');
    Route::get('/adm/report/stock', 'Admin\MasterAdminController@getAdminReportStock')->name('admin_report_stock')->middleware('auth');
    Route::post('/adm/report/stock-cabang', 'Admin\MasterAdminController@postAdminReportStockCabang')->name('admin_report_stock_cbg')->middleware('auth');
    Route::get('/adm/detail/stock/{id}', 'Admin\MasterAdminController@getAdminDetailStock')->name('admin_detail_stock')->middleware('auth');
    Route::get('/adm/edit/stock/{id}/{date}', 'Admin\MasterAdminController@getAdminEditStock')->middleware('auth');
    Route::post('/adm/edit/stock/{id}/{date}', 'Admin\MasterAdminController@postAdminEditStock')->middleware('auth');
    Route::get('/adm/rm/stock/{id}/{date}', 'Admin\MasterAdminController@getAdminRemoveStock')->middleware('auth');
    Route::post('/adm/rm/stock/{id}/{date}', 'Admin\MasterAdminController@postAdminRemoveStock')->middleware('auth');
    Route::get('/adm/add-stock/{cabid}', 'Admin\MasterAdminController@getAdmStockBarang')->name('adm_add_stock')->middleware('auth');
    Route::get('/adm/add/stock/{pid}/{cabid}', 'Admin\MasterAdminController@getAdmAddStockBarang')->name('adm_input_stock')->middleware('auth');
    Route::post('/adm/add/stock/{pid}/{cabid}', 'Admin\MasterAdminController@postAdmAddStockBarang')->middleware('auth');
    Route::get('/adm/add-stock/{cabid}/{tgl}', 'Admin\MasterAdminController@getAdmStockBarangTgl')->name('adm_add_stock_tgl')->middleware('auth');
    Route::get('/adm/add/stock/{pid}/{cabid}/{tgl}', 'Admin\MasterAdminController@getAdmAddStockBarangTgl')->name('adm_input_stock_tgl')->middleware('auth');
    Route::post('/adm/add/stock/{pid}/{cabid}/{tgl}', 'Admin\MasterAdminController@postAdmAddStockBarangTgl')->middleware('auth');
    
    Route::get('/adm/data-sales', 'Admin\MasterAdminController@getAdminDataSales')->name('data_sales_admin')->middleware('auth');
    Route::get('/adm/view-sales/{invo}/{uid}', 'Admin\MasterAdminController@getAdminViewSalesPerorangan')->name('view_sales_admin')->middleware('auth');
    Route::get('/adm/detail-view-sales/{invo}/{uid}', 'Admin\MasterAdminController@getAdminDetailViewSalesPerorangan')->name('view_sales_admin')->middleware('auth');
    Route::get('/adm/edit-sale/{id}/{uid}', 'Admin\MasterAdminController@getEditPenjualan')->name('edit_sale')->middleware('auth');
    Route::get('/adm/rm-sale/{id}/{uid}', 'Admin\MasterAdminController@getDeletePenjualan')->name('rm_sale')->middleware('auth');
    Route::post('/adm/action-sale/{type}/{id}/{uid}', 'Admin\MasterAdminController@postActionPenjualan')->middleware('auth');
    
    Route::get('/adm/global-manager/{type}', 'Admin\MasterAdminController@getGlobalManager')->name('global_manager')->middleware('auth');
    Route::get('/adm/global-asmen', 'Admin\MasterAdminController@getGlobalAsmen')->name('global_manager')->middleware('auth');
    Route::get('/adm/global-tld', 'Admin\MasterAdminController@getGlobalTLD')->name('global_tld')->middleware('auth');
    
    Route::get('/adm/add-down/{id}', 'Admin\MasterAdminController@getAddDownline')->name('add_downline')->middleware('auth');
    Route::post('/adm/add-down/{id}', 'Admin\MasterAdminController@postAddDownline')->middleware('auth');
    Route::get('/adm/view-down/{id}', 'Admin\MasterAdminController@getViewDownlineManager')->name('view_down_m')->middleware('auth');
    Route::get('/adm/view-structure/{id}', 'Admin\MasterAdminController@getViewStructureManager')->name('view_structure_m')->middleware('auth');
    
    Route::get('/adm/report-crew', 'Admin\MasterAdminController@getReportCrew')->name('adm_report_crew')->middleware('auth');
    Route::post('/adm/report-crew', 'Admin\MasterAdminController@postReportCrew')->middleware('auth');
    
    Route::get('/adm/add-upgrade/{id}/{mid}', 'Admin\MasterAdminController@getAddUpgrade')->name('upgrade_m')->middleware('auth');
    Route::post('/adm/upgrade/{id}/{mid}', 'Admin\MasterAdminController@postAddUpgrade')->middleware('auth');
    
    Route::get('/adm/top-cabang', 'Admin\MasterAdminController@getTopCabang')->name('top_cabang')->middleware('auth');
    Route::post('/adm/top-cabang', 'Admin\MasterAdminController@postTopCabang')->middleware('auth');
    
    Route::get('/adm/manager/detail-sales', 'Admin\MasterAdminController@getManagerDetailSales')->name('manager_detail_sales')->middleware('auth');
    Route::post('/adm/manager/detail-sales', 'Admin\MasterAdminController@postManagerDetailSales')->middleware('auth');
    Route::get('/adm/manager/detail-sales-safra', 'Admin\MasterAdminController@getManagerDetailSalesSafra')->name('manager_detail_sales_safra')->middleware('auth');
    Route::post('/adm/manager/detail-sales-safra', 'Admin\MasterAdminController@postManagerDetailSalesSafra')->middleware('auth');
    Route::get('/adm/all/sales-safra', 'Admin\MasterAdminController@getAllSalesSafra')->name('all_sales_safra')->middleware('auth');
    Route::post('/adm/sales-safra', 'Admin\MasterAdminController@postSalesSafra')->middleware('auth');
    Route::get('/adm/report/stock-safra', 'Admin\MasterAdminController@getReportStockSafra')->name('adm_report_stock_safra')->middleware('auth');
    Route::post('/adm/report/stock-safra-cabang', 'Admin\MasterAdminController@postReportStockSafraCabang')->middleware('auth');
    
    Route::get('/adm/testing', 'Admin\MasterAdminController@getTesting')->middleware('auth');
    
    Route::get('/adm/add-admin', 'Admin\MasterAdminController@getAddAdmin')->name('add_admin')->middleware('auth');
    Route::post('/adm/add-admin', 'Admin\MasterAdminController@postAddAdmin')->middleware('auth');
    Route::get('/adm/list-admin', 'Admin\MasterAdminController@getListAdmin')->name('list_admin')->middleware('auth');
    Route::get('/adm/edit-admin/{id}', 'Admin\MasterAdminController@getEditAdmin')->name('edit_admin')->middleware('auth');
    Route::post('/adm/edit-admin/{id}', 'Admin\MasterAdminController@postEditAdmin')->middleware('auth');
    
    Route::get('/adm/edit-crew/{id}/{mid}', 'Admin\MasterAdminController@getAdminEditCrew')->name('adm_edit_crew')->middleware('auth');
    Route::post('/adm/edit-crew/{id}/{mid}', 'Admin\MasterAdminController@postAdminEditCrew')->middleware('auth');
    
    Route::get('/adm/rm-crew/{id}/{mid}', 'Admin\MasterAdminController@getAdminRemoveCrew')->name('adm_edit_crew')->middleware('auth');
    Route::post('/adm/rm-crew/{id}/{mid}', 'Admin\MasterAdminController@postAdminRemoveCrew')->middleware('auth');
    
    Route::get('/adm/pindah-crew', 'Admin\MasterAdminController@getPindahStruktur')->name('adm_pindah')->middleware('auth');
    Route::post('/adm/pindah-crew', 'Admin\MasterAdminController@postPindahStruktur')->middleware('auth');
    
    Route::get('/view-crew-cabang', 'Admin\MasterAdminController@getViewCrewCabang')->name('view_crew_cabang')->middleware('auth');
    Route::get('/adm/edit-date/{id}', 'Admin\MasterAdminController@getEditDateActive')->middleware('auth');
    Route::post('/adm/edit-date/{id}', 'Admin\MasterAdminController@postEditDateActive')->middleware('auth');
    
    Route::get('/adm/list-order-barang', 'Admin\MasterAdminController@getDaftarOrderBarang')->name('adm_list_order_barang')->middleware('auth');
    Route::get('/adm/order-barang/{order_id}', 'Admin\MasterAdminController@getOrderBarang')->name('adm_order_barang')->middleware('auth');
    Route::post('/adm/order-barang', 'Admin\MasterAdminController@postKirimOrderBarang')->middleware('auth');
    Route::get('/adm/edit/order-barang/{order_id}/{item_id}', 'Admin\MasterAdminController@getEditOrderItemBarang')->name('adm_edit_order_item_barang')->middleware('auth');
    Route::post('/adm/edit/order-barang/{order_id}/{item_id}', 'Admin\MasterAdminController@postEditOrderItemBarang')->middleware('auth');
    Route::get('/adm/rm/order/{order_id}/{item_id}', 'Admin\MasterAdminController@getDeleteOrderItemBarang')->name('adm_rm_order_item_barang')->middleware('auth');
    Route::post('/adm/rm/order/{order_id}/{item_id}', 'Admin\MasterAdminController@postDeleteOrderItemBarang')->middleware('auth');
    Route::get('/adm/order-invoice/{order_id}', 'Admin\MasterAdminController@getOrderInvoice')->name('adm_order_invoice')->middleware('auth');
    
    Route::get('/adm/absen', 'Admin\MasterAdminController@getAdminListAbsen')->name('adm_list_absen')->middleware('auth');
    Route::post('/adm/absen', 'Admin\MasterAdminController@postAdminListAbsen')->middleware('auth');
    Route::get('/adm/absen-detail/{user_id}', 'Admin\MasterAdminController@getAdminDetailAbsen')->middleware('auth');
    
    Route::get('/adm/out/category', 'Admin\MasterAdminController@getAdminListCategoryOut')->name('adm_out_category')->middleware('auth');
    Route::post('/adm/out/category', 'Admin\MasterAdminController@postCategoryOut')->middleware('auth');
    Route::post('/adm/out/category/action/{type}/{id}', 'Admin\MasterAdminController@postCategoryOutByType')->middleware('auth');
    
    Route::get('/adm/transfers', 'Admin\MasterAdminController@getAdminListTransfer')->name('adm_transfer')->middleware('auth');
    Route::get('/adm/transfer/{id}', 'Admin\MasterAdminController@getAdminTransferById')->name('adm_transfer_id')->middleware('auth');
    Route::post('/adm/confirm/transfer', 'Admin\MasterAdminController@postAdminConfirmTransfer')->middleware('auth');
    Route::post('/adm/edit/pengeluaran', 'Admin\MasterAdminController@postAdminEditPengeluaran')->middleware('auth');
    Route::post('/adm/remove/pengeluaran', 'Admin\MasterAdminController@postAdminRemovePengeluaran')->middleware('auth');
    Route::post('/adm/search/data-transfer', 'Admin\MasterAdminController@postAdminSearchTransfer')->middleware('auth');
    
    Route::get('/adm/list/pengeluaran', 'Admin\MasterAdminController@getAdminListPengeluaran')->name('adm_list_pengeluaran')->middleware('auth');
    
    Route::get('/adm/cabang/transfers', 'Admin\MasterAdminController@getAdminTransferPerCabang')->name('adm_transfer_cabang')->middleware('auth');
    Route::post('/adm/cabang/transfers', 'Admin\MasterAdminController@postAdminTransferPerCabang')->middleware('auth');
    Route::get('/adm/transfers/cbg/{id}', 'Admin\MasterAdminController@getAdminTransferByCabangId')->name('adm_transfer_cabang_id')->middleware('auth');
    
    Route::get('/adm/list/mqb-pengeluaran', 'Admin\MasterAdminController@getAdminListMQBPengeluaran')->name('adm_list_mqb_pengeluaran')->middleware('auth');
    
    Route::get('/adm/setting/bonus', 'Admin\MasterAdminController@getAdminSettingBonus')->name('adm_setting_bonus')->middleware('auth');
    Route::get('/adm/edit/setting/bonus', 'Admin\MasterAdminController@getAdminEditSettingBonus')->name('adm_edit_setting_bonus')->middleware('auth');
    Route::post('/adm/setting/bonus', 'Admin\MasterAdminController@postAdminSettingBonus')->middleware('auth');
    
    Route::get('/adm/setting/app', 'Admin\MasterAdminController@getAdminSettingApp')->name('adm_setting_app')->middleware('auth');
    Route::get('/adm/edit/setting/app', 'Admin\MasterAdminController@getAdminEditSettingApp')->name('adm_edit_setting_app')->middleware('auth');
    Route::post('/adm/setting/app', 'Admin\MasterAdminController@postAdminSettingApp')->middleware('auth');
    
    Route::get('/adm/bonus/{id}', 'Admin\MasterAdminController@getManagerBonusCrew')->name('adm_bonus_crew')->middleware('auth');
    Route::get('/adm/report/bonus-crew', 'Admin\MasterAdminController@getReportCrewBonus')->name('adm_report_bonus_crew')->middleware('auth');
    Route::post('/adm/report/bonus-crew', 'Admin\MasterAdminController@postReportCrewBonus')->middleware('auth');
    Route::get('/adm/report/omset', 'Admin\MasterAdminController@getReportOmsetBonus')->name('adm_report_bonus_omset')->middleware('auth');
    Route::post('/adm/report/omset', 'Admin\MasterAdminController@postReportOmsetBonus')->middleware('auth');
    
    Route::get('/adm-cs/customers', 'Admin\MasterAdminController@getAdminCSCustomers')->name('adm_cs_customers')->middleware('auth');
    Route::post('/adm-cs/customers', 'Admin\MasterAdminController@postAdminCSCustomers')->middleware('auth');
    
    Route::get('/adm/top-structures', 'Admin\MasterAdminController@getAdminTopStructures')->name('adm_top_structures')->middleware('auth');
    Route::post('/adm/create/top-structure', 'Admin\MasterAdminController@postAdminCreateTopStructure')->middleware('auth');
    Route::post('/adm/rm/top-structure', 'Admin\MasterAdminController@postAdminRemoveTopStructure')->middleware('auth');
    Route::get('/adm/down-structures/{id}', 'Admin\MasterAdminController@getAdminDownStructures')->name('adm_down_structures')->middleware('auth');
    Route::post('/adm/create/down-structure', 'Admin\MasterAdminController@postAdminCreateDownStructure')->middleware('auth');
    Route::post('/adm/rm/down-structure', 'Admin\MasterAdminController@postAdminRemoveStructure')->middleware('auth');
    Route::get('/adm/tree/top-structure/{id}', 'Admin\MasterAdminController@getViewStructureOrganisasi')->middleware('auth');
    
    Route::get('/adm/contents', 'Admin\MasterAdminController@getAdminContents')->name('adm_contents')->middleware('auth');
    Route::get('/adm/create/content', 'Admin\MasterAdminController@getAdminCreateContents')->name('adm_create_content')->middleware('auth');
    Route::post('/adm/create/content', 'Admin\MasterAdminController@postAdminCreateContents')->middleware('auth');
    Route::get('/adm/edit/content/{id}', 'Admin\MasterAdminController@getAdminEditContents')->name('adm_edit_content')->middleware('auth');
    Route::post('/adm/edit/content', 'Admin\MasterAdminController@postAdminEditContents')->middleware('auth');
    Route::post('/adm/rm/content', 'Admin\MasterAdminController@postAdminRemoveContents')->middleware('auth');
    
    Route::get('/adm/passwd', 'Admin\MasterAdminController@getAdminChangePassword')->name('adm_password')->middleware('auth');
    Route::post('/adm/passwd', 'Admin\MasterAdminController@postAdminChangePassword')->middleware('auth');
    Route::get('/adm/list/bonus/manager', 'Admin\MasterAdminController@getAdminListBanusManager')->name('adm_list_bonus_manager')->middleware('auth');
    
    Route::get('/adm/product-sales/{type}', 'Admin\MasterAdminController@getDetailProductSales')->name('manager_detail_sales')->middleware('auth');
    Route::post('/adm/product-sales/{type}', 'Admin\MasterAdminController@postDetailProductSales')->middleware('auth');
    
    //Ajax Admin
    Route::get('/search-type/{type}/{cabId}', 'Admin\MasterAdminController@getSearchType')->name('search_type')->middleware('auth');
    Route::get('/adm/search-sp/{type}/{cabId}/{id}', 'Admin\AjaxAdminController@getSearchSponsor')->middleware('auth');
    Route::get('/adm/search-manager/{cabId}', 'Admin\AjaxAdminController@getSearchManager')->middleware('auth');
    Route::get('/adm/search-crew/{mId}', 'Admin\AjaxAdminController@getSearchCrew')->middleware('auth');
    Route::get('/adm/ajax/tree/{mid}', 'Admin\AjaxAdminController@getAdminManagerTree')->middleware('auth');
    Route::get('/adm/search-name-all/{type}/{cabId}', 'Admin\AjaxAdminController@getSearchAllName')->middleware('auth');
    Route::get('/adm/ajax/out/category/{type}/{id}', 'Admin\AjaxAdminController@getOutCategory')->middleware('auth');
    Route::get('/adm/ajax/confirm/transfer/{id}/{type}', 'Admin\AjaxAdminController@getAjaxConfirmTransfer')->middleware('auth');
    Route::get('/adm/ajax/edit/out/{id}/{t_id}', 'Admin\AjaxAdminController@getAjaxEditPengeluaran')->middleware('auth');
    Route::get('/adm/ajax/remove/out/{id}/{t_id}', 'Admin\AjaxAdminController@getAjaxRemovePengeluaran')->middleware('auth');
    Route::get('/adm/pengeluaran/search-manager/{cabId}', 'Admin\AjaxAdminController@getSearchManagerPengeluaran')->middleware('auth');
    Route::get('/adm/search-all-type/{cabId}/{search_type}', 'Admin\AjaxAdminController@getSearchAllType')->middleware('auth');
    Route::get('/adm/search-top-structure/{cabId}', 'Admin\AjaxAdminController@getSearchTopStructure')->middleware('auth');
    Route::get('/adm/search-down-structure/{type}/{cabId}', 'Admin\AjaxAdminController@getSearchDownStructure')->middleware('auth');
    Route::get('/adm/ajax/rm/top-structure/{id}', 'Admin\AjaxAdminController@getAjaxRemoveTopStructure')->middleware('auth');
    Route::get('/adm/ajax/rm/down-structure/{id}', 'Admin\AjaxAdminController@getAjaxRemoveDownStructure')->middleware('auth');
    Route::get('/adm/ajax/org/tree/{mid}', 'Admin\AjaxAdminController@getAdminManagerOrganisasiTree')->middleware('auth');
    Route::get('/adm/ajax/rm/content/{id}', 'Admin\AjaxAdminController@getAdminRemoveContents')->middleware('auth');
    Route::get('/adm/ajax/new/search-manager/{cabId}/{type}', 'Admin\AjaxAdminController@getSearchManagerChangePassword')->middleware('auth');
    Route::get('/adm/ajax/search-data/{id}', 'Admin\AjaxAdminController@getSearchDataUserId')->middleware('auth');
    Route::get('/adm/ajax/safra/{type}/{id}', 'Admin\AjaxAdminController@getDataActionSafra')->middleware('auth');
    Route::get('/adm/ajax/detail-safra/{id}', 'Admin\AjaxAdminController@getDataDetailSafraID')->middleware('auth');
    
    //Ajax Manager
    Route::get('/m/search-sp/{type}/{cabId}', 'Admin\AjaxAdminController@getSearchAsmenTld')->middleware('auth');
    Route::post('/m/cek-sum', 'Admin\AjaxAdminController@postSumPrice')->middleware('auth');
    Route::get('/m/ajax/tree', 'Admin\AjaxAdminController@getTree')->middleware('auth');
    Route::get('/m/search-barang/{date}', 'Admin\AjaxAdminController@getSearchBarang')->middleware('auth');
    Route::get('/m/more-barang/{date}/{counter}', 'Admin\AjaxAdminController@getMoreBarang')->middleware('auth');
    Route::get('/m/total-sales/{date}', 'Admin\AjaxAdminController@getTotalSalesDate')->middleware('auth');
    Route::get('/m/search-category/{date}', 'Admin\AjaxAdminController@getSearchCategory')->middleware('auth');
    Route::get('/m/more-category/{date}', 'Admin\AjaxAdminController@getMoreCategory')->middleware('auth');
    Route::post('/m/cek-transfer', 'Admin\AjaxAdminController@postPriceTransfer')->middleware('auth');
    Route::get('/m/ajax/confirm/transfer/{id}/{type}', 'Admin\AjaxAdminController@getManagerConfirmTransfer')->middleware('auth');
    Route::get('/mqb/total-pengeluaran/{date}', 'Admin\AjaxAdminController@getMQBTotalPengeluaranDate')->middleware('auth');
    Route::post('/mqb/cek-pengeluaran', 'Admin\AjaxAdminController@postMQBPengeluaran')->middleware('auth');
    Route::get('/org/ajax/tree/{id}', 'Admin\AjaxAdminController@getOrganisasiTree')->middleware('auth');
    Route::get('/m/ajax/set/cookies/{idContent}', 'Admin\AjaxAdminController@getManagerSetCookies')->middleware('auth');
    Route::get('/m/search-barang-safra/{date}', 'Admin\AjaxAdminController@getSearchBarangSafra')->middleware('auth');
    Route::post('/m/cek-sum-safra', 'Admin\AjaxAdminController@postSumPriceSafra')->middleware('auth');
    
    
});

