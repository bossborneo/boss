<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(){
        return view('welcome');
    }

    public function getHome(){
        return redirect()->route('adminLogin');
        return view('front_end.home.home');
    }


}
