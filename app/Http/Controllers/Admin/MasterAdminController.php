<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Cabang;
use App\Model\Member;
use App\Model\Purchase;
use App\Model\Itempurchase;
use App\Model\Sale;
use App\Model\Stock;
use App\Model\Order;
use App\Model\Transfer;
use App\Model\Bonus;
use App\Model\Content;

class MasterAdminController extends Controller {
    
    public function __construct(){
        
    }
    
    
    public function getAddManager(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        return view('admin.admin_area.add_manager')
                ->with('headerTitle', 'Tambah Manager')
                ->with('allCabang', $getAllCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function postAddManager(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->password != $request->repassword){
            return redirect()->route('add_manager')
                ->with('message', 'Password tidak sama')
                ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $cekUsername = $modelMember->getCekUsername($request->username);
        if($cekUsername != null){
            return redirect()->route('add_manager')
                ->with('message', 'Username sudah terpakai, gunakan username lain')
                ->with('messageclass', 'danger');
        }
        $modelCabang = New Cabang;
        $fieldName = 'id';
        $cekCabang = $modelCabang->getCheckCabang($fieldName, $request->cabang);
        if($request->parent_id  == null){
            if($request->level_id > 2){
                return redirect()->route('add_manager')
                ->with('message', 'Gagal, tidak bisa memasukan. tidak memiliki Sponsor')
                ->with('messageclass', 'danger');
            }
            $parentId = null;
        } else {
            $getParentId = $modelMember->getParentId($request->parent_id);
            $parentId = $getParentId->sponsor_id.',['.$request->parent_id.']';
            if($getParentId->sponsor_id == null){
                $parentId = '['.$request->parent_id.']';
            }
        }
        $dataInsert = array(
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'is_active' => 1,
            'is_login' => 1,
            'active_at' => date('Y-m-d H:i:s'),
            'user_type' => 5,
            'cabang_id' => $cekCabang->id,
            'cabang_name' => $cekCabang->cabang_name,
            'level_id' => $request->level_id,
            'created_by' => $dataUser->id,
            'sponsor_id' => $parentId,
            'parent_id' => $request->parent_id,
        );
        $insertUsername = $modelMember->getInsertUser($dataInsert);
        $jsonLog = json_encode($dataInsert);
        $dataLog = array(
            'log_data' => 'id: '.$dataUser->id.', name: '.$dataUser->name.', type: '.$dataUser->user_type.'. Insert '.$jsonLog
        );
        $modelMember->getInsertLog($dataLog);
        if($insertUsername->status == false){
            return redirect()->route('add_manager')
                ->with('message', 'terjadi kesalahan pada sistem')
                ->with('messageclass', 'danger');
        }
        return redirect()->route('view_managers')
                ->with('message', 'Tambah manager berhasil')
                ->with('messageclass', 'success');
    }
    
    public function getViewManager(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $modelCabang = New Cabang;
        $cabang = null;
        if($request->cabang != null){
            $cabang = $request->cabang;
        }
        $getAllCabang = $modelCabang->getAllCabang();
        $getAllManager = $modelMember->getAllManager($cabang);
        return view('admin.admin_area.daftar_manager')
                ->with('headerTitle', 'Daftar Manager')
                ->with('allManager', $getAllManager)
                ->with('allCabang', $getAllCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getViewDetailManager($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getManagerId = $modelMember->getUserId($id);
        return view('admin.admin_area.edit_manager')
                ->with('headerTitle', 'Edit Manager')
                ->with('data', $getManagerId)
                ->with('dataUser', $dataUser);
    }
    
    public function postUpdateProfileManager($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cekId != $id){
            return redirect()->route('detail_manager', $request->cekId)
                    ->with('message', 'Dilarang mengutak-atik sistem')
                    ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $data = array(
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address
        );
        $getUpdate = $modelMember->getUpdateMember('id', $id, $data);
        if($getUpdate->status == false){
            return redirect()->route('detail_manager', $request->cekId)
                    ->with('message', 'Terjadi kesalahan pada sistem')
                    ->with('messageclass', 'danger');
        }
        return redirect()->route('detail_manager', $request->cekId)
                ->with('message', 'data manager berhasil diubah')
                ->with('messageclass', 'success');
    }
    
    public function getNonaktifManager($id){
        dd('under construction');
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard')
                ->with('message', 'Area terlarang')
                ->with('messageclass', 'danger');
        }
    }
    
    public function getAddCabang(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
//        $jsonRole = $dataUser->permissions;
//        $role = json_decode($jsonRole);
//        if($dataUser->user_type == 3){
//            $jsonRole = $dataUser->permissions;
//            $role = json_decode($jsonRole);
//            if($role->cabang == false){
//                return redirect()->route('adminDashboard');
//            }
//        }
        return view('admin.admin_area.add_cabang')
                ->with('headerTitle', 'Tambah Cabang')
                ->with('dataUser', $dataUser);
    }
    
    public function postAddCabang(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $extra = $request->extra;
        $extra_strip = $request->extra_strip;
        $extra_alat = $request->extra_alat;
        $extra_wilayah = $request->extra_wilayah;
        if($request->extra == null){
            $extra = 0;
        }
        if($request->extra_strip == null){
            $extra_strip = 0;
        }
        if($request->extra_alat == null){
            $extra_alat = 0;
        }
        if($request->extra_wilayah == null){
            $extra_wilayah = 0;
        }
        $namaCabang = $request->name;
        $modelCabang = New Cabang;
        $fieldName = 'cabang_name';
        $cekCabang = $modelCabang->getCheckCabang($fieldName, $namaCabang);
        if($cekCabang != null){
            return redirect()->route('add_cabang')
                ->with('message', 'Nama cabang '.$namaCabang.' sudah ada')
                ->with('messageclass', 'danger');
        }
        $dataInsert = array(
            'cabang_name' => $namaCabang,
            'extra' => $extra,
            'extra_strip' => $extra_strip,
            'extra_alat' => $extra_alat,
            'wilayah' => $request->wilayah,
            'extra_wilayah' => $extra_wilayah
        );
        $insertCabang = $modelCabang->getInsertCabang($dataInsert);
        if($insertCabang->status == false){
            return redirect()->route('add_cabang')
                ->with('message', $insertCabang->message)
                ->with('messageclass', 'danger');
        }
        return redirect()->route('view_cabangs')
                ->with('message', 'Tambah cabang '.$namaCabang.' berhasil')
                ->with('messageclass', 'success');
    }
    
    public function getViewCabangs(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
//        $jsonRole = $dataUser->permissions;
//        $role = json_decode($jsonRole);
////        if(!isset($role->cabang_index)){
////            dd('kosong');
////        }
////        dd('ada');
//        if($dataUser->user_type == 3){
//            $jsonRole = $dataUser->permissions;
//            $role = json_decode($jsonRole);
//            if($role->cabang == false){
//                return redirect()->route('adminDashboard');
//            }
//        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        return view('admin.admin_area.daftar_cabang')
                ->with('headerTitle', 'Daftar Cabang')
                ->with('allCabang', $getAllCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getViewDetailCabang($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $fieldName = 'id';
        $cekCabang = $modelCabang->getCheckCabang($fieldName, $id);
        return view('admin.admin_area.view_cabang')
                ->with('headerTitle', 'Edit Cabang')
                ->with('cabang', $cekCabang)
                ->with('dataUser', $dataUser);
        
    }
    
    public function postEditCabang($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $fieldName = 'id';
        $cekCabang = $modelCabang->getCheckCabang($fieldName, $id);
        if($cekCabang == null){
            return redirect()->route('view_cabangs')
                ->with('message', 'Cabang tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $extra = $request->extra;
        $extra_strip = $request->extra_strip;
        $extra_alat = $request->extra_alat;
        $extra_wilayah = $request->extra_wilayah;
        if($request->extra == null){
            $extra = 0;
        }
        if($request->extra_strip == null){
            $extra_strip = 0;
        }
        if($request->extra_alat == null){
            $extra_alat = 0;
        }
        if($request->extra_wilayah == null){
            $extra_wilayah = 0;
        }
        $data = array(
            'cabang_name' => $request->name,
            'extra' => $extra,
            'extra_strip' => $extra_strip,
            'extra_alat' => $extra_alat,
            'wilayah' => $request->wilayah,
            'extra_wilayah' => $extra_wilayah
        );
        $updateCabang = $modelCabang->getUpdateCabang($id, $data);
        if($updateCabang->status == false){
            return redirect()->route('view_cabangs')
                ->with('message', $updateCabang->message)
                ->with('messageclass', 'danger');
        }
        //ganti semua cabang di user
        $datauser = array(
            'cabang_name' => $request->name
        );
        $modelMember = New Member;
        $gantiUserCabang = $modelMember->getUpdateMember('cabang_id', $id, $datauser);
        if($gantiUserCabang->status == false){
            return redirect()->route('view_cabangs')
                ->with('message', $gantiUserCabang->message)
                ->with('messageclass', 'danger');
        }
        $jsonLog = json_encode($data);
        $dataLog = array(
            'log_data' => 'id: '.$dataUser->id.', name: '.$dataUser->name.', type: '.$dataUser->user_type.'. Edit '.$jsonLog
        );
        $modelMember->getInsertLog($dataLog);
        return redirect()->route('view_cabangs')
                ->with('message', 'Data Cabang '.$cekCabang->cabang_name.' berhasil di-edit ')
                ->with('messageclass', 'success');
    }
    
    public function getDeleteCabang($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $fieldName = 'id';
        $cekCabang = $modelCabang->getCheckCabang($fieldName, $id);
        if($cekCabang == null){
            return redirect()->route('view_cabangs')
                ->with('message', 'Cabang tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $modelItemPurchase = New Itempurchase;
        $cekUsersCabang = $modelMember->getCekUsersInCabang($id);
        if($cekUsersCabang > 0){
            return redirect()->route('view_cabangs')
                ->with('message', 'Cabang tidak dapat dihapus, ada member yang masih di cabang tersebut')
                ->with('messageclass', 'danger');
        }
        $cekSisa = $modelItemPurchase->getCheckItemStilHas($id);
        if($cekSisa != null){
            if($cekSisa->total_sisa > 0){
                return redirect()->route('view_cabangs')
                    ->with('message', 'Cabang tidak dapat dihapus, masih ada stock')
                    ->with('messageclass', 'danger');
            }
        }
        $deleteCabang = $modelCabang->getDeleteCabangById($id);
        if($deleteCabang->status == false){
            return redirect()->route('view_cabangs')
                ->with('message', $deleteCabang->message)
                ->with('messageclass', 'danger');
        }
//        $jsonLog = json_encode($data);
        $dataLog = array(
            'log_data' => 'id: '.$dataUser->id.', name: '.$dataUser->name.', type: '.$dataUser->user_type.'. Remove Cabang ID '.$id
        );
        $modelMember->getInsertLog($dataLog);
        return redirect()->route('view_cabangs')
                ->with('message', 'Cabang '.$cekCabang->cabang_name.' berhasil dihapus')
                ->with('messageclass', 'success');
    }
    
    public function getAdminReport(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        return view('admin.admin_area.report.report_global')
                ->with('headerTitle', 'Report Global')
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminReportStock(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $cabang = null;
        $modelCabang = New Cabang;
        $modelStock = New Stock;
        $getAllCabang = $modelCabang->getAllCabang();
        $cabangId = null;
        $lastSunday = date('Y-m-d', strtotime( "-2 weeks sunday" ));
        $thisSunday = date('Y-m-d', strtotime( "last sunday" ));
        if(date('w') == 0){
            $lastSunday = date('Y-m-d', strtotime( "last sunday" ));
            $thisSunday = date('Y-m-d');
        } 
        $today = date('Y-m-d');
        if(date('w') == 0){
            $getItemToday = $modelStock->getHistoryAllStockIsSunday($today);
        } else {
            $getItemToday = $modelStock->getHistoryAllStockIsNotSunday($thisSunday, $today);
        }
        return view('admin.admin_area.report.stock_opname')
                ->with('headerTitle', 'Report Stock Global')
                ->with('headerTitle2', null)
                ->with('item', $getItemToday)
                ->with('itemLast', null)
                ->with('allCabang', $getAllCabang)
                ->with('cabang', $cabang)
                ->with('cabangId', $cabangId)
                ->with('dataCabang', null)
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminReportStockCabang(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $cabang = $request->cabang;
        if($request->cabang == null){
            return redirect()->route('admin_report_stock')
                    ->with('message', 'Cabang harus dipilih')
                    ->with('messageclass', 'danger');
        }
        $modelCabang = New Cabang;
        $getDataCabang = $modelCabang->getCheckCabang('id', $cabang);
        $getAllCabang = $modelCabang->getAllCabang();
        $modelStock = New Stock;
        $lastSunday = date('Y-m-d', strtotime( "-2 weeks sunday" ));
        $thisSunday = date('Y-m-d', strtotime( "last sunday" ));
        if(date('w') == 0){
            $lastSunday = date('Y-m-d', strtotime( "last sunday" ));
            $thisSunday = date('Y-m-d');
        } 
        $dataSunday = (object) array(
            'lastSunday' => $lastSunday, 'thisSunday' => $thisSunday
        );
        $today = date('Y-m-d');
        $getItemLast = $modelStock->getSuperAdminHistoryStock($cabang, $lastSunday, $thisSunday);
        if(date('w') == 0){
            $getItemToday = $modelStock->getSuperAdminHistoryStockIsSunday($cabang, $today);
        } else {
            $getItemToday = $modelStock->getSuperAdminHistoryStockIsNotSunday($cabang, $thisSunday, $today);
        }
        $cabangId = true;
        return view('admin.admin_area.report.stock_opname')
                ->with('headerTitle', 'Report Stock Cabang '.$getDataCabang->cabang_name.' '.date('d M Y', strtotime($thisSunday)))
                ->with('headerTitle2', 'Report Stock Cabang '.$getDataCabang->cabang_name.' '.date('d M Y', strtotime($lastSunday)))
                ->with('item', $getItemToday)
                ->with('itemLast', $getItemLast)
                ->with('cabang', $cabang)
                ->with('allCabang', $getAllCabang)
                ->with('cabangId', $cabangId)
                ->with('dataSunday', $dataSunday)
                ->with('dataCabang', $getDataCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminDetailStock($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelItemPurchase = New Itempurchase;
        $getDetailPurchase = $modelItemPurchase->getItemPurchaseCabang($id);
        $getItem = $modelItemPurchase->getHistoryItemPurchase($id);
        $getItemOut = $modelItemPurchase->getHistoryItemPurchaseOut($id);
//        dd($getItemOut);
        return view('admin.admin_area.report.detail_stock')
                ->with('headerTitle', 'Detail Stock ')
                ->with('purchase', $getDetailPurchase)
                ->with('item', $getItem)
                ->with('itemOut', $getItemOut)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminEditStock($id, $date){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelStock = New Stock;
        $getDetailStock = $modelStock->getSuperAdminEdit($id, $date);
        return view('admin.ajax.adm_edit_stock')
                ->with('headerTitle', 'Edit Stock')
                ->with('date', $date)
                ->with('dataUser', $dataUser)
                ->with('dataStock', $getDetailStock);
    }
    
    public function postAdminEditStock($id, $date, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cekId != $id){
            return redirect()->route('admin_report_stock');
        }
        if($request->qty_new <= 0){
            return redirect()->route('admin_report_stock');
        }
        $modelStock = New Stock;
        $getDetailStock = $modelStock->getSuperAdminCekStock($id, $date);
        foreach($getDetailStock as $row){
            $data = array('amount' => 0);
            $modelStock->getUpdateStock($row->id_stock, $data);
        }
        $dataNew = array('amount' => $request->qty_new);
        $modelStock->getUpdateStock($getDetailStock[0]->id_stock, $dataNew);
        //done edit stock admin
        
        $cabang = $getDetailStock[0]->cabang_id;
        $modelCabang = New Cabang;
        $getDataCabang = $modelCabang->getCheckCabang('id', $cabang);
        $getAllCabang = $modelCabang->getAllCabang();
        $lastSunday = date('Y-m-d', strtotime( "-2 weeks sunday" ));
        $thisSunday = date('Y-m-d', strtotime( "last sunday" ));
        if(date('w') == 0){
            $lastSunday = date('Y-m-d', strtotime( "last sunday" ));
            $thisSunday = date('Y-m-d');
        } 
        $dataSunday = (object) array(
            'lastSunday' => $lastSunday, 'thisSunday' => $thisSunday
        );
        $today = date('Y-m-d');
        $getItemLast = $modelStock->getSuperAdminHistoryStock($cabang, $lastSunday, $thisSunday);
        if(date('w') == 0){
            $getItemToday = $modelStock->getSuperAdminHistoryStockIsSunday($cabang, $today);
        } else {
            $getItemToday = $modelStock->getSuperAdminHistoryStockIsNotSunday($cabang, $thisSunday, $today);
        }
        $cabangId = true;
        return view('admin.admin_area.report.stock_opname')
                ->with('headerTitle', 'Report Stock Cabang '.$getDataCabang->cabang_name.' '.date('d M Y', strtotime($thisSunday)))
                ->with('headerTitle2', 'Report Stock Cabang '.$getDataCabang->cabang_name.' '.date('d M Y', strtotime($lastSunday)))
                ->with('item', $getItemToday)
                ->with('itemLast', $getItemLast)
                ->with('cabang', $cabang)
                ->with('allCabang', $getAllCabang)
                ->with('cabangId', $cabangId)
                ->with('dataSunday', $dataSunday)
                ->with('dataCabang', $getDataCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminRemoveStock($id, $date){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelStock = New Stock;
        $getDetailStock = $modelStock->getSuperAdminEdit($id, $date);
        return view('admin.ajax.adm_rm_stock')
                ->with('headerTitle', 'Remove Stock')
                ->with('date', $date)
                ->with('dataUser', $dataUser)
                ->with('dataStock', $getDetailStock);
    }
    
    public function postAdminRemoveStock($id, $date, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cekId != $id){
            return redirect()->route('admin_report_stock');
        }
        $modelStock = New Stock;
        //Mulai dari sini removenya
        $getDetailStock = $modelStock->getSuperAdminCekStock($id, $date);
        foreach($getDetailStock as $row){
            $modelStock->getDeleteStock($row->id_stock);
        }
        $modelCabang = New Cabang;
        $getCabang = $modelCabang->getCheckCabang('id', $getDetailStock[0]->cabang_id);
        return redirect()->route('admin_report_stock')
                ->with('message', 'berhasil hapus stock cabang '.$getCabang->cabang_name.' tanggal '.date('d M Y', strtotime($date)))
                ->with('messageclass', 'success');
    }
    
    public function getAllPurchase(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelPurchase = New Purchase;
        $allPurchase = $modelPurchase->getAllPurchase();
        return view('admin.admin_area.all_purchase')
                ->with('headerTitle', 'Data Barang')
                ->with('purchase', $allPurchase)
                ->with('dataUser', $dataUser);
    }
    
    public function getAddPurchase(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        return view('admin.admin_area.add_purchase')
                ->with('headerTitle', 'Tambah Barang')
                ->with('dataUser', $dataUser);
    } 
    
    public function postAddPurchase(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelPurchase = New Purchase;
        $fieldName = 'purchase_name';
        $cekPurchase = $modelPurchase->getCheckPurchase($fieldName, $request->purchase_name);
        if($cekPurchase != null){
            return redirect()->route('add_purchase')
                ->with('message', 'Nama Barang sudah ada, gunakan yang lain')
                ->with('messageclass', 'danger');
        }
        $dataInsert = array(
            'purchase_name' => $request->purchase_name,
            'type' => $request->type,
            'hpp' => $request->hpp,
            'main_price' => $request->main_price,
            'kondisi' => $request->kondisi,
            'is_poin' => $request->is_poin
        );
        $insertPurchase = $modelPurchase->getInsertPurchase($dataInsert);
        if($insertPurchase->status == false){
            return redirect()->route('add_purchase')
                ->with('message', $insertPurchase->message)
                ->with('messageclass', 'danger');
        }
        return redirect()->route('all_purchase')
                ->with('message', 'Tambah barang '.$request->purchase_name.' berhasil')
                ->with('messageclass', 'success');
    }
    
    public function getEditPurchase($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelPurchase = New Purchase;
        $fieldName = 'id';
        $cekPurchase = $modelPurchase->getCheckPurchase($fieldName, $id);
        if($cekPurchase == null){
            return redirect()->route('all-purchase')
                ->with('message', 'Purchase tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        return view('admin.admin_area.view_purchase')
                ->with('headerTitle', 'Daftar Cabang')
                ->with('barang', $cekPurchase)
                ->with('dataUser', $dataUser);
    }
    
    public function postEditPurchase($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cekId != $id){
            return redirect()->route('all-purchase')
                ->with('message', 'barang tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $modelPurchase = New Purchase;
        $fieldName = 'id';
        $cekPurchase = $modelPurchase->getCheckPurchase($fieldName, $id);
        if($cekPurchase == null){
            return redirect()->route('all-purchase')
                ->with('message', 'barang tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $data = array(
            'purchase_name' => $request->purchase_name,
            'type' => $request->type,
            'hpp' => $request->hpp,
            'main_price' => $request->main_price,
            'kondisi' => $request->kondisi,
            'is_poin' => $request->is_poin
        );
        $updateCabang = $modelPurchase->getUpdatePurchase($id, $data);
        if($updateCabang->status == false){
            return redirect()->route('all-purchase')
                ->with('message', $updateCabang->message)
                ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $jsonLog = json_encode($data);
        $dataLog = array(
            'log_data' => 'id: '.$dataUser->id.', name: '.$dataUser->name.', type: '.$dataUser->user_type.'. Edit '.$jsonLog
        );
        $modelMember->getInsertLog($dataLog);
        return redirect()->route('all_purchase')
                ->with('message', 'Barang '.$cekPurchase->purchase_name.' berhasil diedit')
                ->with('messageclass', 'success');
    }
    
    public function getStatusPurchase($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelPurchase = New Purchase;
        $modelItemPurchase = New Itempurchase;
        $fieldName = 'id';
        $cekPurchase = $modelPurchase->getCheckPurchase($fieldName, $id);
        
        if($cekPurchase == null){
            return redirect()->route('all_purchase')
                ->with('message', 'Purchase tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $cekSisa = $modelItemPurchase->getSisaStockNew($id);
        if(count($cekSisa) > 0){
            return redirect()->route('all_purchase')
                ->with('message', 'Barang '.$cekPurchase->purchase_name.' tidak dapat dihapus, masih ada sisa stock')
                ->with('messageclass', 'danger');
        }
        $data = array('deleted_at' => null);
        $text = 'diaktifkan';
        if($cekPurchase->deleted_at == null){
            $data = array('deleted_at' => date('Y-m-d H:i:s'));
            $text = 'dihapus';
        }
        $rmCabang = $modelPurchase->getUpdatePurchase($id, $data);
        if($rmCabang->status == false){
            return redirect()->route('all-purchase')
                ->with('message', $rmCabang->message)
                ->with('messageclass', 'danger');
        }
        return redirect()->route('all_purchase')
                ->with('message', 'Barang '.$cekPurchase->purchase_name.' telah '.$text)
                ->with('messageclass', 'success');
    }
    
    public function getSearchType($type, $cabId){
        $dataUser = Auth::user();
        $sponsor_id = 0;
        if($type == 3){
            $sponsor_id = 2;
        }
        if($type == 4){
            $sponsor_id = 3;
        }
        if($type == 5){
            $sponsor_id = 4;
        }
        if($type == 6){
            $sponsor_id = 5;
        }
        if($type == 7){
            $sponsor_id = 6;
        }
        $modelMember = New Member;
        $dataSponsor = $modelMember->getAllParent($sponsor_id, $cabId);
        $sponsor = null;
        if(count($dataSponsor) > 0){
            $sponsor = $dataSponsor;
        }
        return view('admin.ajax.add_manager')
                ->with('dataSponsor', $sponsor)
                ->with('type', $type)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminDataSales(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3, 31);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $textDay = 'Data Bulan '.date('F');
//        $getMonth = $modelSale->getThisMonth();
        $getMonth = (object) array(
            'startDay' => null,
            'endDay' => null
        );
        $getSales = null;
        $getCabang = null;
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            if($request->cabang == null){
                return redirect()->route('data_sales_admin')
                        ->with('message', 'anda tidak memilih cabang')
                        ->with('messageclass', 'danger');
            }
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
            $getSales = $modelSale->getDataSalesAdm($getMonth, $request->cabang);
            $getCabang = $request->cabang;
        }
        return view('admin.admin_area.all_penjualan')
                ->with('day', $textDay)
                ->with('headerTitle', 'Data Penjualan')
                ->with('allSales', $getSales)
                ->with('allCabang', $getAllCabang)
                ->with('getCabang', $getCabang)
                ->with('getDate', $getMonth)
                ->with('dataUser', $dataUser);
    }
    
    ///Add Downline Per Manager
    public function getAddDownline($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $arraySpId = array(2, 3, 4, 5, 6, 7, 8);
        $getManagerId = $modelMember->getUserId($id);
        $managerSponsor = $getManagerId->sponsor_id.',['.$getManagerId->id.']';
        if($getManagerId->sponsor_id == null){
            $managerSponsor = '['.$getManagerId->id.']';
        }
        $getDownline = $modelMember->getNewAllDownline($getManagerId->cabang_id, $managerSponsor, $arraySpId);
        return view('admin.admin_area.add_downline')
                ->with('headerTitle', 'Tambah Crew')
                ->with('dataManager', $getManagerId)
                ->with('dataDownline', $getDownline)
                ->with('id', $id)
                ->with('dataUser', $dataUser);
    }
    
    public function postAddDownline($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cekId != $id){
            return redirect()->route('view_managers')
                ->with('message', 'Data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        if($request-> name == null){
            return redirect()->route('add_downline', $id)
                ->with('message', 'Nama tidak boleh kosong')
                ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $getManagerId = $modelMember->getUserId($id);
        $getParentId = $modelMember->getUserId($request->parent_id);
        $getUsername = $modelMember->getUsername();
        $dataInsertUser = array(
            'username' => $getUsername,
            'password' => bcrypt($modelMember->getAutomaticPassword()),
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'is_active' => 1,
            'is_login' => 0,
            'active_at' => date('Y-m-d H:i:s'),
            'user_type' => 10,
            'cabang_id' => $getManagerId->cabang_id,
            'cabang_name' => $getManagerId->cabang_name,
            'level_id' => $request->level_id,
            'created_by' => $dataUser->id,
            'sponsor_id' => $getParentId->sponsor_id.',['.$request->parent_id.']',
            'parent_id' => $request->parent_id,
        );
        $getInsert = $modelMember->getInsertUser($dataInsertUser);
        if($getInsert == false){
            return redirect()->route('add_downline', $id)
                ->with('message', 'Terjadi kesalahan pada sistem')
                ->with('messageclass', 'danger');
        }
        return redirect()->route('add_downline', $id)
                ->with('message', 'Berhasil buat crew baru '.$request->name)
                ->with('messageclass', 'success');
    }
    
    public function getViewDownlineManager($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $arraySpId = array(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
        $getManagerId = $modelMember->getUserId($id);
        $managerSponsor = $getManagerId->sponsor_id.',['.$getManagerId->id.']';
        if($getManagerId->sponsor_id == null){
            $managerSponsor = '['.$getManagerId->id.']';
        }
        $getDownline = $modelMember->getAllDownlineManager($getManagerId->cabang_id, $managerSponsor, $arraySpId);
        return view('admin.admin_area.all_downline')
                ->with('headerTitle', 'Downline')
                ->with('dataManager', $getManagerId)
                ->with('dataDownline', $getDownline)
                ->with('dataUser', $dataUser);
    }
    
    public function getViewStructureManager($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getManagerId = $modelMember->getUserId($id);
        return view('admin.admin_area.all_structure')
                ->with('dataManager', $getManagerId)
                ->with('headerTitle', 'Data Structure '.$getManagerId->name)
                ->with('dataUser', $dataUser);
    }
    
    public function getGlobalManager($type, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $typeText = 'Manager';
        if($type == 'gm'){
            $typeText = 'General Manager';
        }
        if($type == 'sem'){
            $typeText = 'Senior Executive Manager';
        }
        if($type == 'em'){
            $typeText = 'Executive Manager';
        }
        if($type == 'sm'){
            $typeText = 'Senior Manager';
        }
        if($type == 'mqb'){
            $typeText = 'MQB';
        }
        $typeM = $type.'_id';
        $modelSale = New Sale;
        $getMonth = $modelSale->getThisMonth();
        $textDay = 'Data Bulan '.date('F');
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $getData = $modelSale->getGlobalManager($typeM, $getMonth);
//        dd($getData);
        return view('admin.admin_area.global_manager')
                ->with('type', $type)
                ->with('day', $textDay)
                ->with('headerTitle', $typeText)
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getGlobalAsmen(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $textDay = 'Data Bulan '.date('F');
        $modelSale = New Sale;
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = 'asmen_id';
        $getData = $modelSale->getGlobalManager($typeM, $getMonth);
        return view('admin.admin_area.global_asmen')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Assistance Manager')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getGlobalTLD(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = 'tld_id';
        $getData = $modelSale->getGlobalManager($typeM, $getMonth);
        return view('admin.admin_area.global_tld')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Top Leader')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getAddUpgrade($id, $mid){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getUserId = $modelMember->getUserId($id);
        $getMId = $modelMember->getUserId($mid);
        return view('admin.ajax.adm_upgrade')
                ->with('headerTitle', 'Upgrade Crew')
                ->with('dataUser', $dataUser)
                ->with('dataManager', $getMId)
                ->with('dataUserId', $getUserId);
    }
    
    public function postAddUpgrade($id, $mid, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($id != $request->cekId){
            return redirect()->route('view_down_m', $mid)
                ->with('message', 'data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $getUserId = $modelMember->getUserId($id);
        $oldLevel = $getUserId->level_id;
        $username = $getUserId->username;
        $password = $getUserId->password;//bcrypt($modelMember->getAutomaticPassword());
        $is_login = 0;
        $user_type = 10;
        if($oldLevel == 8){
            if($request->password != $request->repassword){
                return redirect()->route('view_down_m', $mid)
                        ->with('message', 'password tidak sama')
                        ->with('messageclass', 'danger');
            }
            if($request->password == null){
                return redirect()->route('view_down_m', $mid)
                        ->with('message', 'password tidak boleh kosong')
                        ->with('messageclass', 'danger');
            }
            if($request->username == null){
                return redirect()->route('view_down_m', $mid)
                        ->with('message', 'username tidak boleh kosong')
                        ->with('messageclass', 'danger');
            }
            $cekUsername = $modelMember->getCekUsername($request->username);
            if($cekUsername != null){
                return redirect()->route('view_down_m', $mid)
                        ->with('message', 'Username sudah terpakai, gunakan username lain')
                        ->with('messageclass', 'danger');
            }
            $password = bcrypt($request->password);
            $username = $request->username;
            $is_login = 1;
            $user_type = 5;
        }
        $newLevel = $oldLevel - 1;
        if($newLevel < 7){
            $is_login = 1;
            $user_type = 5;
        }
        $dataHistory = array(
             'user_id' => $id,
             'old_level_id' => $oldLevel,
             'new_level_id' => $newLevel,
             'created_by' => $dataUser->id,
             'active_at' => date('Y-m-d H:i:s')
        );
        $getInsertHistory = $modelMember->getInsertHistoryLevel($dataHistory);
        if($getInsertHistory->status == false){
            return redirect()->route('view_down_m', $mid)
                ->with('message', 'terjadi kesalahan pada sistem')
                ->with('messageclass', 'danger');
        }
        $data = array(
            'username' => $username,
            'password' => $password,
            'is_login' => $is_login,
            'user_type' => $user_type,
            'level_id' => $newLevel
        );
        $getUpdate = $modelMember->getUpdateMember('id', $id, $data);
        if($getUpdate->status == false){
            return redirect()->route('view_down_m', $mid)
                ->with('message', 'terjadi kesalahan pada sistem')
                ->with('messageclass', 'danger');
        }
        return redirect()->route('view_down_m', $mid)
                ->with('message', 'Upgrade crew '.$getUserId->name.' berhasil')
                ->with('messageclass', 'success');
    }
    
    public function getReportCrew(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $nextSunday = strtotime( "next sunday" );
        $getNextSunday = date('d-M-Y', $nextSunday);
        $getDateSunday = date('Y-m-d', $nextSunday);
        $dataSunday = (object) array(
            'nextSunday' => $getNextSunday,
            'dateSunday' => $getDateSunday
        );
        return view('admin.admin_area.report.report_crew_create')
                ->with('headerTitle', 'Filter Report Crew')
                ->with('allCabang', $getAllCabang)
                ->with('sunday', $dataSunday)
                ->with('dataUser', $dataUser);
    }
    
    public function postReportCrew(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cabang == null){
            return redirect()->route('adm_report_crew')
                ->with('message', 'Cabang harus dipilih')
                ->with('messageclass', 'danger');
        }
        if($request->end_date == null){
            return redirect()->route('adm_report_crew')
                ->with('message', 'Tanggal harus dipilih')
                ->with('messageclass', 'danger');
        }
         if($request->start_date == null){
            return redirect()->route('adm_report_crew')
                ->with('message', 'Tanggal harus dipilih')
                ->with('messageclass', 'danger');
        }
        if($request->manager_id == 0){
            return redirect()->route('adm_report_crew')
                ->with('message', 'Manager harus dipilih')
                ->with('messageclass', 'danger');
        }
//        $cekDate = date('w', strtotime($request->end_date));
//        if($cekDate != 0){
//            return redirect()->route('adm_report_crew')
//                ->with('message', 'Tanggal yang dipilih harus hari minggu')
//                ->with('messageclass', 'danger');
//        }
//        $nextSunday = strtotime( "next sunday" );
//        $getNextSunday = date('d-M-Y', $nextSunday);
//        $getDateSunday = date('Y-m-d', $nextSunday);
        $dataSunday = (object) array(
            'nextSunday' => $request->start_date,
            'dateSunday' => $request->end_date
        );
        $startDay = $request->start_date; //date('Y-m-d', strtotime('-6 day', strtotime($request->end_date))); //date('Y-m-d', strtotime('-6 day', strtotime( $getDateSunday))); //date('Y-m-d', strtotime('-6 day', strtotime( $request->end_date)));
        $modelMember = New Member;
        $modelSale = New Sale;
        $modelCabang = New Cabang;
        $getDataManager = $modelMember->getUserId($request->manager_id);
        if($getDataManager == null){
            return redirect()->route('adm_report_crew')
                    ->with('message', 'Data Manager tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $typeManager = 'gm_id';
        if($getDataManager->level_id == 3){
            $typeManager = 'sem_id';
        }
        if($getDataManager->level_id == 4){
            $typeManager = 'em_id';
        }
        if($getDataManager->level_id == 5){
            $typeManager = 'sm_id';
        }
        if($getDataManager->level_id == 6){
            $typeManager = 'mqb_id';
        }
        if($getDataManager->level_id == 7){
            $typeManager = 'manager_id';
        }
        if($getDataManager->level_id == 8){
            $typeManager = 'asmen_id';
        }
        if($getDataManager->cabang_id != $request->cabang){
            return redirect()->route('adm_report_crew')
                ->with('message', 'Data cabang tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $getParameter = (object) array(
            'startDay' => $startDay,
            'endDay' => $request->end_date, //$getDateSunday, //$request->end_date
            'type' => $typeManager,
            'id' => $getDataManager->id
        );
        $textDay =  'Data '.date('d M Y', strtotime($startDay)).' - '.date('d M Y', strtotime($request->end_date));// 'Data '.date('d M Y', strtotime($startDay)).' - '.date('d M Y', strtotime($getDateSunday));
        $userType = 10;
        $getSales = $modelSale->getSalesReportCrewWithManager($getParameter, $getDataManager->cabang_id, $userType);
        $getAllCabang = $modelCabang->getAllCabang();
        $getAllManagerCabang = $modelMember->getManagerFromCabang($getDataManager->cabang_id);
        return view('admin.admin_area.report.report_crew')
                ->with('allCabang', $getAllCabang)
                ->with('day', $textDay)
                ->with('headerTitle', 'Production Chart ')
                ->with('headerTitle2', $getDataManager->name.' ('.$getDataManager->cabang_name.')')
                ->with('allSales', $getSales)
                ->with('sunday', $dataSunday)
                ->with('dataNama', $getDataManager)
                ->with('dataAllManagerCabang', $getAllManagerCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminViewSalesPerorangan($invo, $uid){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $modelMember = New Member;
        $getData = $modelSale->getDataSalesInvoAndUserId($invo, $uid);
        if(count($getData) <= 0){
            return redirect()->route('data_sales_admin')
                ->with('message', 'Data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $getMember = $modelMember->getUserId($uid);
        return view('admin.admin_area.view_sales')
                ->with('headerTitle', 'Data Sales ')
                ->with('allSales', $getData)
                ->with('member', $getMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminDetailViewSalesPerorangan($invo, $uid){
        $modelSale = New Sale;
        $modelMember = New Member;
        $getData = $modelSale->getDataSalesInvoAndUserId($invo, $uid);
        if(count($getData) <= 0){
            return redirect()->route('data_sales_admin')
                ->with('message', 'Data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $getMember = $modelMember->getUserId($uid);
        return view('admin.admin_area.view_sales_detail')
                ->with('headerTitle', 'Data Sales ')
                ->with('allSales', $getData)
                ->with('member', $getMember);
    }
    
    public function getEditPenjualan($id, $uid){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $getId = $id;
        $modelSale = New Sale;
        $data = $modelSale->getDataSalesActionById($getId, $uid);
        if($data == null){
            return redirect()->route('data_sales_admin')
                ->with('message', 'Data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        return view('admin.ajax.admin_action_sale')
                ->with('headerTitle', 'Edit Penjualan')
                ->with('dataSales', $data)
                ->with('type', 1)
                ->with('dataUser', $dataUser)
                ->with('uid', $uid);
    }
    
    public function getDeletePenjualan($id, $uid){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $getId = $id;
        $modelSale = New Sale;
        $data = $modelSale->getDataSalesActionById($getId, $uid);
        if($data == null){
            return redirect()->route('data_sales_admin')
                ->with('message', 'Data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        return view('admin.ajax.admin_action_sale')
                ->with('headerTitle', 'Hapus Penjualan')
                ->with('dataSales', $data)
                ->with('type', 2)
                ->with('dataUser', $dataUser)
                ->with('uid', $uid);
    }
    
    public function postActionPenjualan($type, $id, $uid, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }

        $modelSale = New Sale;
        $modelStock = New Stock;
        $modelItemPurchase = New Itempurchase;
        $data = $modelSale->getDataSalesActionById($id, $uid);
        $getStock = $modelStock->getCheckStock($data->id);
        if($request->cekId != $id){
            return redirect()->route('data_sales_admin')
                ->with('message', 'Data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
//        return redirect()->route('view_sales_admin', [$data->invoice, $uid])
//                ->with('message', 'WKWKWKWKWKWKWKW')
//                ->with('messageclass', 'danger');
        
        if($type == 'edit'){
            if($request->amount <= 0){
                return redirect()->route('view_sales_admin', [$data->invoice, $uid])
                    ->with('message', 'jumlah stock tidak boleh kurang atau sama dengan 0')
                    ->with('messageclass', 'danger');
            }
            if($request->amount >= $getStock->amount){
                $diffAmount = round(($request->amount - $getStock->amount), 1);
                $sisaItem = round(($data->sisa - $diffAmount), 1);
            }
            if($request->amount < $getStock->amount){
                $diffAmount = round(($getStock->amount - $request->amount), 1);
                $sisaItem = round(($data->sisa + $diffAmount), 1);
            }
            $updateStock = array(
                'amount' => $request->amount
            );
            $updateItem = array(
                'sisa' => $sisaItem
            );
            $updateSales = array(
                'amount' => $request->amount,
                'sale_price' => round($request->amount * $data->price)
            );
            $modelStock->getUpdateStock($getStock->id, $updateStock);
            $modelItemPurchase->getUpdateItem($data->id_item_purchase, $updateItem);
            $modelSale->getUpdateSales($data->id, $updateSales);
            return redirect()->route('view_sales_admin', [$data->invoice, $uid])
                    ->with('message', 'Edit sales berhasil')
                    ->with('messageclass', 'success');
        }
        
        if($type == 'rm'){
            //delete
            $dataInsertStock = array(
                'item_purchase_id' => $data->id_item_purchase,
                'type_stock' => 3,
                'user_id' => $getStock->user_id,
                'amount' => $data->amount,
                'sales_id' => $data->id
            );
            $updateSales = array(
                'deleted_at' => date('Y-m-d H:i:s'),
            );
            $sisaItem1 = round(($data->sisa + $data->amount), 1);
            $updateItem = array(
                'sisa' => $sisaItem1
            );
            $modelStock->getInsertStock($dataInsertStock);
            $modelSale->getUpdateSales($data->id, $updateSales);
            $modelItemPurchase->getUpdateItem($data->id_item_purchase, $updateItem);
            return redirect()->route('view_sales_admin', [$data->invoice, $uid])
                    ->with('message', 'sales telah dihapus')
                    ->with('messageclass', 'success');
        }
    }
    
    public function getTopCabang(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $getTop10 = null;
        return view('admin.admin_area.top_ten')
                ->with('headerTitle', 'Top 10 Sales Cabang')
                ->with('allCabang', $getAllCabang)
                ->with('dataTop', $getTop10)
                ->with('dataUser', $dataUser);
    }
    
    public function postTopCabang(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cabang == null){
            return redirect()->route('top_cabang')
                    ->with('message', 'Cabang tidak dipilih')
                    ->with('messageclass', 'danger');
        }
        $modelCabang = New Cabang;
        $modelSale = New Sale;
        $date = $modelSale->getLastWeek();
        $getAllCabang = $modelCabang->getAllCabang();
        $getTop10 = $modelSale->getTop10SalesForAdminCabang($date, $request->cabang);
        if(count($getTop10) <= 0){
            $fieldName = 'id';
            $cekCabang = $modelCabang->getCheckCabang($fieldName, $request->cabang);
            return redirect()->route('top_cabang')
                    ->with('message', 'Data top 20 Cabang '.$cekCabang->cabang_name.' tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        return view('admin.admin_area.top_ten')
                ->with('headerTitle', 'Top 20 Sales Cabang')
                ->with('allCabang', $getAllCabang)
                ->with('dataTop', $getTop10)
                ->with('dataUser', $dataUser);
    }
    
    public function getTesting(){
        dd(date('Y-m-d H:i:s').' --- '. date('w'));
    }
    
    public function getManagerDetailSales(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3, 31);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $getData = null;
        return view('admin.admin_area.detail_sales_create')
                ->with('headerTitle', 'Detail Penjualan')
                ->with('allCabang', $getAllCabang)
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postManagerDetailSales(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3, 31);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cabang <= 0){
            return redirect()->route('manager_detail_sales')
                    ->with('message', 'Anda Tidak Memilih Cabang')
                    ->with('messageclass', 'danger');
        }
        if($request->start_date == null || $request->end_date == null){
            return redirect()->route('manager_detail_sales')
                    ->with('message', 'Anda Tidak Memilih Tanggal')
                    ->with('messageclass', 'danger');
        }
        if($request->manager_id <= 0){
            return redirect()->route('manager_detail_sales')
                    ->with('message', 'Anda Tidak Memilih Manager')
                    ->with('messageclass', 'danger');
        }
        $modelCabang = New Cabang;
        $modelSale = New Sale;
        $modelMember = New Member;
        $getDate = (object) array(
            'startDay' => $request->start_date,
            'endDay' => $request->end_date
        );
        $getDataManager = $modelMember->getUserId($request->manager_id);
        $text = 'Asisten Manager';
        if($getDataManager->user_type == 5){
            $text = 'Manager';
        }
        $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        $getAllCabang = $modelCabang->getAllCabang();
        $getData = $modelSale->getGlobalMyManagerNewDetailSales($getDate, $getDataManager);
        $getAllManagerCabang = $modelMember->getManagerFromCabang($getDataManager->cabang_id);
        return view('admin.admin_area.detail_sales')
                ->with('day', $textDay)
                ->with('headerTitle', 'Detail Sales '.$text.' '.$getDataManager->name)
                ->with('getData', $getData)
                ->with('allCabang', $getAllCabang)
                ->with('tgl', $getDate)
                ->with('dataNama', $getDataManager)
                ->with('dataAllManagerCabang', $getAllManagerCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerDetailSalesSafra(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 30);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $getData = null;
        return view('admin.admin_area.detail_sales_safra_create')
                ->with('headerTitle', 'Detail Penjualan Safra')
                ->with('allCabang', $getAllCabang)
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postManagerDetailSalesSafra(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 30);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cabang <= 0){
            return redirect()->route('manager_detail_sales_safra')
                    ->with('message', 'Anda Tidak Memilih Cabang')
                    ->with('messageclass', 'danger');
        }
        if($request->start_date == null || $request->end_date == null){
            return redirect()->route('manager_detail_sales_safra')
                    ->with('message', 'Anda Tidak Memilih Tanggal')
                    ->with('messageclass', 'danger');
        }
        if($request->manager_id <= 0){
            return redirect()->route('manager_detail_sales_safra')
                    ->with('message', 'Anda Tidak Memilih Manager')
                    ->with('messageclass', 'danger');
        }
        $modelCabang = New Cabang;
        $modelSale = New Sale;
        $modelMember = New Member;
        $getDate = (object) array(
            'startDay' => $request->start_date,
            'endDay' => $request->end_date
        );
        $getDataManager = $modelMember->getUserId($request->manager_id);
        $text = 'Asisten Manager';
        if($getDataManager->user_type == 5){
            $text = 'Manager';
        }
        $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        $getAllCabang = $modelCabang->getAllCabang();
        $getData = $modelSale->getGlobalMyManagerNewDetailSalesSafra($getDate, $getDataManager);
        $getAllManagerCabang = $modelMember->getManagerFromCabang($getDataManager->cabang_id);
        return view('admin.admin_area.detail_sales_safra_create')
                ->with('day', $textDay)
                ->with('headerTitle', 'Detail Sales Safra '.$text.' '.$getDataManager->name)
                ->with('getData', $getData)
                ->with('allCabang', $getAllCabang)
                ->with('tgl', $getDate)
                ->with('dataNama', $getDataManager)
                ->with('dataAllManagerCabang', $getAllManagerCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getAllSalesSafra(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 30);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $modelMember = New Member;
        $getData = $modelSale->getSafraSalesForAdmin();
        return view('admin.admin_area.all_sales_safra')
                ->with('headerTitle', 'List Safra Sales')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postSalesSafra(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 30);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $modelMember = New Member;
        $id = $request->cekId;
        $getData = $modelSale->getSafraSalesForAdminByID($id);
        $cek = 'Konfirmasi';
        $status = 2;
        $reason = null;
        if($request->type == 2){
            $status = 3;
            $cek = 'Reject';
            $reason = $request->reason;
        }
        $dataUpdate = array(
            'safra_status' => $status,
            'status_at' => date('Y-m-d H:i:s'),
            'reason' => $reason
        );
        $modelSale->getUpdateSales($id, $dataUpdate);
        return redirect()->route('all_sales_safra')
                    ->with('message', $cek.' berhasil')
                    ->with('messageclass', 'success');
    }
    
    public function getAdminEditCrew($id, $mid){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getUserId = $modelMember->getUserId($id);
        return view('admin.ajax.adm_edit_crew')
                ->with('headerTitle', 'Edit Crew')
                ->with('dataUser', $dataUser)
                ->with('manager_id', $mid)
                ->with('dataUserId', $getUserId);
        
    }
    
    public function postAdminEditCrew($id, $mid, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($id != $request->cekId){
            return redirect()->route('view_down_m', $mid)
                ->with('message', 'data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $getUserId = $modelMember->getUserId($id);
        $data = array(
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address
        );
        $getUpdate = $modelMember->getUpdateMember('id', $id, $data);
        if($getUpdate->status == false){
            return redirect()->route('view_down_m', $mid)
                ->with('message', 'terjadi kesalahan pada sistem')
                ->with('messageclass', 'danger');
        }
        return redirect()->route('view_down_m', $mid)
                ->with('message', 'berhasil update')
                ->with('messageclass', 'success');
        
    }
    
    
    ############################
    #Add Admin
    
    public function getAddAdmin(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        return view('admin.admin_area.add_admin')
                ->with('headerTitle', 'Add New Admin')
                ->with('dataUser', $dataUser);
    }
    
    public function postAddAdmin(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->password != $request->repassword){
            return redirect()->route('add_admin')
                ->with('message', 'Password tidak sama')
                ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $cekUsername = $modelMember->getCekUsername($request->username);
        if($cekUsername != null){
            return redirect()->route('add_admin')
                ->with('message', 'Username sudah terpakai, gunakan username lain')
                ->with('messageclass', 'danger');
        }
        $view = true;
//        if($request->view == 1){
//            $view = true;
//        }
        $download = false;
        if($request->download == 1){
            $download = true;
        }
        $role = (object) array(
            'view' => $view,
            'download' => $download
        );
        $jsonRole = json_encode($role);
        $dataInsert = array(
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'name' => 'Admin '.$request->username,
            'is_active' => 1,
            'is_login' => 1,
            'active_at' => date('Y-m-d H:i:s'),
            'user_type' => 3,
            'permissions' => $jsonRole
        );
        $insertUsername = $modelMember->getInsertUser($dataInsert);
        if($insertUsername->status == false){
            return redirect()->route('add_admin')
                ->with('message', 'terjadi kesalahan pada sistem')
                ->with('messageclass', 'danger');
        }
        return redirect()->route('add_admin')
                ->with('message', 'Tambah Admin berhasil')
                ->with('messageclass', 'success');
    }
    
    public function getListAdmin(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $cekAdmin = $modelMember->getAllAdmin();
        return view('admin.admin_area.list_admin')
                ->with('headerTitle', 'Data Admin')
                ->with('admin', $cekAdmin)
                ->with('dataUser', $dataUser);
    }
    
    public function getEditAdmin($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $cekAdmin = $modelMember->getAdminById($id);
        return view('admin.admin_area.edit_admin')
                ->with('headerTitle', 'Edit Admin')
                ->with('getData', $cekAdmin)
                ->with('dataUser', $dataUser);
    }
    
    public function postEditAdmin($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cekId != $id){
            return redirect()->route('add_admin')
                ->with('message', 'Terjadi kesalahan')
                ->with('messageclass', 'danger');
        }
        if($request->password == null){
            return redirect()->route('edit_admin', $id)
                ->with('message', 'Password tidak boleh kosong')
                ->with('messageclass', 'danger');
        }
        if($request->password != $request->repassword){
            return redirect()->route('edit_admin', $id)
                ->with('message', 'Password tidak sama')
                ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $cekUsername = $modelMember->getCekUsernameAdmin($request->username, $id);
        if($cekUsername != null){
            return redirect()->route('edit_admin', $id)
                ->with('message', 'Username sudah terpakai, gunakan username lain')
                ->with('messageclass', 'danger');
        }
        $view = true;
//        if($request->view == 1){
//            $view = true;
//        }
        $download = false;
        if($request->download == 1){
            $download = true;
        }
        $role = (object) array(
            'view' => $view,
            'download' => $download
        );
        $jsonRole = json_encode($role);
        $data = array(
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'name' => 'Admin '.$request->username,
            'permissions' => $jsonRole
        );
        $modelMember->getUpdateMember('id', $id, $data);
        return redirect()->route('list_admin')
                ->with('message', 'Edit Admin berhasil')
                ->with('messageclass', 'success');
    }
    
    ## Batas Add Admin
    ###############
    
    public function getAdmStockBarang($cabid){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelPurchase = New Purchase;
        $modelCabang = New Cabang;
        $getCabang = $modelCabang->getCheckCabang('id', $cabid);
        $newAllPurchase = null;
        $allPurchase = $modelPurchase->getManagerAllInputStock($newAllPurchase);
        return view('admin.admin_area.all_ready_purchase')
                ->with('headerTitle', 'Data Stock')
                ->with('purchseKosong', $allPurchase)
                ->with('getCabang', $getCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdmAddStockBarang($pid, $cabid){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelPurchase = New Purchase;
        $modelCabang = New Cabang;
        $getPurchaseId = $modelPurchase->getManagerPurchaseIdAddStock($pid);
        $getCabang = $modelCabang->getCheckCabang('id', $cabid);
        return view('admin.ajax.adm_add_stock')
                ->with('headerTitle', 'Input Stock Opname')
                ->with('dataUser', $dataUser)
                ->with('dataPurchase', $getPurchaseId)
                ->with('getCabang', $getCabang);
    }
    
    public function postAdmAddStockBarang($pid, $cabid, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cekId != $pid){
            return redirect()->route('adm_add_stock', array($cabid))
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        if($request->qty <= 0){
            return redirect()->route('adm_add_stock', array($cabid))
                    ->with('message', 'Input stock harus diisi')
                    ->with('messageclass', 'danger');
        }
        $modelPurchase = New Purchase;
        $modelItemPurchase = New Itempurchase;
        $modelStock = New Stock;
        $getPurchaseId = $modelPurchase->getManagerPurchaseIdAddStock($pid);
        if($getPurchaseId == null){
            return redirect()->route('adm_add_stock', array($cabid))
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $modelCabang = New Cabang;
        $getCabang = $modelCabang->getCheckCabang('id', $cabid);
        $type = 'Obat';
        $price = round($getPurchaseId->main_price + $getCabang->extra);
        if($getPurchaseId->type == 1){
            $type = 'Alat';
            $price = round($getPurchaseId->main_price + $getCabang->extra_alat);
        }
        if($getPurchaseId->type == 2){
            $type = 'Strip';
            $price = round($getPurchaseId->main_price + $getCabang->extra_strip);
        }
        if($getPurchaseId->kondisi == 1){
            $price = round($getPurchaseId->main_price);
        }
        $cekItem = $modelItemPurchase->getCheckItemAddStockAdmin($cabid, $pid);
        if($cekItem == null){
            $dataInsert = array(
                'purchase_id' => $request->cekId,
                'cabang_id' => $cabid,
                'manager_id' => $dataUser->id,
                'qty' => $request->qty,
                'sisa' => $request->qty,
                'price' => $price
            );
            $getInsertItem = $modelItemPurchase->getInsertItem($dataInsert);
            $getIdItem = $getInsertItem->lastID;
        } else {
            $getIdItem = $cekItem->id;
        }
        $thisSunday = date('Y-m-d 07:i:s', strtotime( "last sunday" ));
        if(date('w') == 0){
            $thisSunday = date('Y-m-d 07:i:s');
        } 
        $dataInsertStock = array(
            'item_purchase_id' => $getIdItem,
            'type_stock' => 1,
            'user_id' => $dataUser->id,
            'amount' => $request->qty,
            'created_at' => $thisSunday
        );
        $modelStock->getInsertStock($dataInsertStock);
        $getSumStock = $modelStock->getGetSumStock($getIdItem);
        $getQty = $getSumStock->amount_tambah;
        $getSisa = $getSumStock->amount_tambah - $getSumStock->amount_kurang;
        $dataUpdateItem = array(
            'qty' => $getQty, 
            'sisa' => $getSisa,
            'price' => $price
        );
        $modelItemPurchase->getUpdateItem($getIdItem, $dataUpdateItem);
        return redirect()->route('adm_add_stock', array($cabid))
                ->with('message', 'stock berhasil ditambah')
                ->with('messageclass', 'success');
    }
    
    public function getPindahStruktur(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $getData = null;
        return view('admin.admin_area.pindah_struktur')
                ->with('headerTitle', 'Pindah Struktur')
                ->with('allCabang', $getAllCabang)
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postPindahStruktur(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $userIdFrom = $request->start_user_id;
        $userIdTo = $request->parent_user_id;
        if($userIdFrom == $userIdTo){
             return redirect()->route('adm_pindah')
                ->with('message', 'tidak bisa dipindahkan, tidak boleh ke diri sendiri')
                ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $getDataFrom = $modelMember->getUserId($userIdFrom);
        $getDataTo = $modelMember->getUserId($userIdTo);
        if($getDataTo == null){
             return redirect()->route('adm_pindah')
                ->with('message', 'data user tujuan tidak ada')
                ->with('messageclass', 'danger');
        }
        $newSponsor = $getDataTo->sponsor_id.',['.$getDataTo->id.']';
        if($getDataTo->sponsor_id == null){
            $newSponsor = '['.$getDataTo->id.']';
        }
        $oldSponsor = $getDataFrom->sponsor_id.',['.$getDataFrom->id.']';
        if($getDataFrom->sponsor_id == null){
            $oldSponsor = '['.$getDataFrom->id.']';
        }
        $arraySpId = array(2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13);
        $cekDownline = $modelMember->getNewCountAllDownline($getDataFrom->cabang_id, $oldSponsor, $arraySpId);
        if($cekDownline > 0){
            return redirect()->route('adm_pindah')
                ->with('message', 'tidak bisa dipindahkan, '.$getDataFrom->name.' masih memiliki bawahan')
                ->with('messageclass', 'danger');
        }
        $data = array(
            'cabang_id' => $getDataTo->cabang_id,
            'cabang_name' => $getDataTo->cabang_name,
            'sponsor_id' => $newSponsor,
            'parent_id' => $getDataTo->id
        );
        $getUpdate = $modelMember->getUpdateMember('id', $getDataFrom->id, $data);
        return redirect()->route('adm_pindah')
                ->with('message', 'pindah struktur berhasil')
                ->with('messageclass', 'success');
    }
    
    public function getAdminRemoveCrew($id, $mid){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getUserId = $modelMember->getUserId($id);
        $getMId = $modelMember->getUserId($mid);
        return view('admin.ajax.adm_remove')
                ->with('headerTitle', 'Hapus Crew')
                ->with('dataUser', $dataUser)
                ->with('dataManager', $getMId)
                ->with('dataUserId', $getUserId);
    }
    
    public function postAdminRemoveCrew($id, $mid, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($id != $request->cekId){
            return redirect()->route('view_down_m', $mid)
                ->with('message', 'data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $getUserId = $modelMember->getUserId($id);
        $oldSponsor = $getUserId->sponsor_id.',['.$getUserId->id.']';
        if($getUserId->sponsor_id == null){
            $oldSponsor = '['.$getUserId->id.']';
        }
        $arraySpId = array(2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12, 13);
        $cekDownline = $modelMember->getNewCountAllDownline($getUserId->cabang_id, $oldSponsor, $arraySpId);
        if($cekDownline > 0){
            return redirect()->route('view_down_m', $mid)
                ->with('message', 'tidak bisa dihapus, '.$getUserId->name.' masih memiliki bawahan')
                ->with('messageclass', 'danger');
        }
        $data = array('is_active' => 0, 'deleted_at' => date('Y-m-d H:i:s'));
        $modelMember->getUpdateMember('id', $id, $data);
        return redirect()->route('view_down_m', $mid)
                ->with('message', 'Hapus crew '.$getUserId->name.' berhasil')
                ->with('messageclass', 'success');
    }
    
    public function getViewCrewCabang(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $modelCabang = New Cabang;
        $cabang = null;
        $getCabang = null;
        $getAllCrewCabang = null;
        if($request->cabang != null){
            $cabang = $request->cabang;
            $getCabang = $modelCabang->getCheckCabang('id', $request->cabang);
            $getAllCrewCabang = $modelMember->getAllMemberCabang($cabang);
        }
        $getAllCabang = $modelCabang->getAllCabang();
        return view('admin.admin_area.daftar_crew_cabang')
                ->with('headerTitle', 'Daftar Manager')
                ->with('allCrew', $getAllCrewCabang)
                ->with('allCabang', $getAllCabang)
                ->with('getCabang', $getCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getEditDateActive($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getUserId = $modelMember->getAllDataMemberById($id);
        return view('admin.ajax.edit_date')
                ->with('headerTitle', 'Daftar Manager')
                ->with('dataUserId', $getUserId)
                ->with('dataUser', $dataUser);
    }
    
    public function postEditDateActive($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getUserId = $modelMember->getAllDataMemberById($id);
        if($request->cekId != $id){
            return redirect('/view-crew-cabang?cabang='.$getUserId->cabang_id)
                    ->with('message', 'data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $dateNew = date('Y-m-d H:i:s', strtotime( $request->date_active ));
        if($getUserId->history_active_at == null){
            $data = array(
                'active_at' => $dateNew
            );
            $getUpdate = $modelMember->getUpdateMember('id', $id, $data);
        } else {
            $getLastHistoryLevel = $modelMember->getHistoryLevelUserId($id);
            $data1 = array(
                'active_at' => $dateNew
            );
            $getUpdate = $modelMember->getUpdateHistoryLevel('id', $getLastHistoryLevel->id, $data1);
        }
        return redirect('/view-crew-cabang?cabang='.$getUserId->cabang_id)
                    ->with('message', 'berhasil update tanggal aktif')
                    ->with('messageclass', 'success');
    }
    
    public function getAdmStockBarangTgl($cabid, $tgl){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelPurchase = New Purchase;
        $modelCabang = New Cabang;
        $getCabang = $modelCabang->getCheckCabang('id', $cabid);
        $newAllPurchase = null;
        $allPurchase = $modelPurchase->getManagerAllInputStock($newAllPurchase);
        return view('admin.admin_area.all_ready_purchase_tgl')
                ->with('headerTitle', 'Data Stock')
                ->with('purchseKosong', $allPurchase)
                ->with('getCabang', $getCabang)
                ->with('tgl', $tgl)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdmAddStockBarangTgl($pid, $cabid, $tgl){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelPurchase = New Purchase;
        $modelCabang = New Cabang;
        $getPurchaseId = $modelPurchase->getManagerPurchaseIdAddStock($pid);
        $getCabang = $modelCabang->getCheckCabang('id', $cabid);
        return view('admin.ajax.adm_add_stock_tgl')
                ->with('headerTitle', 'Input Stock Opname')
                ->with('dataUser', $dataUser)
                ->with('dataPurchase', $getPurchaseId)
                ->with('tgl', $tgl)
                ->with('getCabang', $getCabang);
    }
    
    public function postAdmAddStockBarangTgl($pid, $cabid, $tgl, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cekId != $pid){
            return redirect()->route('adm_add_stock', array($cabid))
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        if($request->qty <= 0){
            return redirect()->route('adm_add_stock', array($cabid))
                    ->with('message', 'Input stock harus diisi')
                    ->with('messageclass', 'danger');
        }
        $modelPurchase = New Purchase;
        $modelItemPurchase = New Itempurchase;
        $modelStock = New Stock;
        $getPurchaseId = $modelPurchase->getManagerPurchaseIdAddStock($pid);
        if($getPurchaseId == null){
            return redirect()->route('adm_add_stock', array($cabid))
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $modelCabang = New Cabang;
        $getCabang = $modelCabang->getCheckCabang('id', $cabid);
        $type = 'Obat';
        $price = round($getPurchaseId->main_price + $getCabang->extra);
        if($getPurchaseId->type == 1){
            $type = 'Alat';
            $price = round($getPurchaseId->main_price + $getCabang->extra_alat);
        }
        if($getPurchaseId->type == 2){
            $type = 'Strip';
            $price = round($getPurchaseId->main_price + $getCabang->extra_strip);
        }
        if($getPurchaseId->kondisi == 1){
            $price = round($getPurchaseId->main_price);
        }
        $cekItem = $modelItemPurchase->getCheckItemAddStockAdmin($cabid, $pid);
        if($cekItem == null){
            $dataInsert = array(
                'purchase_id' => $request->cekId,
                'cabang_id' => $cabid,
                'manager_id' => $dataUser->id,
                'qty' => $request->qty,
                'sisa' => $request->qty,
                'price' => $price
            );
            $getInsertItem = $modelItemPurchase->getInsertItem($dataInsert);
            $getIdItem = $getInsertItem->lastID;
        } else {
            $getIdItem = $cekItem->id;
        }
        $thisSunday = $tgl.' 20:00:00';  //date('Y-m-d 07:i:s', strtotime( $tgl ));
        $dataInsertStock = array(
            'item_purchase_id' => $getIdItem,
            'type_stock' => 1,
            'user_id' => $dataUser->id,
            'amount' => $request->qty,
            'created_at' => $thisSunday
        );
        $modelStock->getInsertStock($dataInsertStock);
        $getSumStock = $modelStock->getGetSumStock($getIdItem);
        $getQty = $getSumStock->amount_tambah;
        $getSisa = $getSumStock->amount_tambah - $getSumStock->amount_kurang;
        $dataUpdateItem = array(
            'qty' => $getQty, 
            'sisa' => $getSisa,
            'price' => $price
        );
        $modelItemPurchase->getUpdateItem($getIdItem, $dataUpdateItem);
        return redirect()->route('adm_add_stock_tgl', array($cabid, $tgl))
                ->with('message', 'stock berhasil ditambah')
                ->with('messageclass', 'success');
    }
    
    public function getDaftarOrderBarang(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelOrder = New Order;
        $getAllOrder = $modelOrder->getAdminListOrder();
        return view('admin.admin_area.list_order_barang')
                ->with('headerTitle', 'List Request Order')
                ->with('getData', $getAllOrder)
                ->with('dataUser', $dataUser);
    }
    
    public function getOrderBarang($order_id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelOrder = New Order;
        $getOrderData = $modelOrder->getAdminOrderId($order_id);
        $getAllOrderItem = $modelOrder->getAdminListOrderItem($order_id);
        return view('admin.admin_area.list_detail_order')
                ->with('headerTitle', 'List Order Barang')
                ->with('getData', $getAllOrderItem)
                ->with('dataOrder', $getOrderData)
                ->with('dataUser', $dataUser);
    }
    
    public function postKirimOrderBarang(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->order_item_id == null){
            return redirect()->route('adm_list_order_barang')
                        ->with('message', 'gagal, tidak ada data order')
                        ->with('messageclass', 'danger');
        }
        $count = count($request->order_item_id);
        $dataAll = array();
        for ($x = 0; $x < $count; $x++) {
            $qty = $request->qty[$x];
            if($request->qty[$x] == null){
                $qty = 0;
            }
            $dataAll[] = (object) array(
                'order_item_id' => $request->order_item_id[$x],
                'order_id' => $request->order_id,
                'qty' => $qty,
                'keterangan' => $request->keterangan[$x],
            );
        }
        $modelOrder = New Order;
        foreach($dataAll as $row){
            $dataUpdateItem = array(
                'qty' => $row->qty,
                'keterangan' => $row->keterangan
            );
            $modelOrder->getUpdateOrderItem($row->order_item_id, $dataUpdateItem);
        }
        $dataStatus = array(
            'status' => 2,
            'kirim_at' => date('Y-m-d H:i:s')
        );
        $modelOrder->getUpdateOrder($request->order_id, $dataStatus);
        return redirect()->route('adm_list_order_barang')
                    ->with('message', 'order berhasil siap dikirim')
                    ->with('messageclass', 'success');
    }
    
    public function getEditOrderItemBarang($order_id, $item_id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelOrder = New Order;
        $getOrderItemById = $modelOrder->getAdminOrderItem($order_id, $item_id);
        return view('admin.ajax.edit_order_stock')
                ->with('headerTitle', 'Edit Order Stock')
                ->with('dataUser', $dataUser)
                ->with('getData', $getOrderItemById);
    }
    
    public function postEditOrderItemBarang($order_id, $item_id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelOrder = New Order;
        if($request->item_id != $item_id){
            return redirect()->route('adm_order_barang', array($order_id))
                    ->with('message', 'terjadi kesalahan')
                    ->with('messageclass', 'danger');
        }
        $newQty = $request->qty;
        $getOrderItemById = $modelOrder->getAdminOrderItem($order_id, $item_id);
        if($request->is_terima == 1){
            $oldQty = $getOrderItemById->qty;
            if($newQty != $oldQty){
                $modelItemPurchase = New Itempurchase;
                $modelStock = New Stock;
                $cekItem = $modelItemPurchase->getCheckItemAddStockAdmin($getOrderItemById->cabang_id, $getOrderItemById->id_purchase);
                $getIdItem = $cekItem->id;
                $dataUpdateStock = array(
                    'amount' => $newQty
                );
                $getIdStock = $modelStock->getCheckStockFromOrder($getIdItem, $getOrderItemById->manager_id);
                $modelStock->getUpdateStock($getIdStock->id, $dataUpdateStock);
                $getSumStock = $modelStock->getGetSumStock($getIdItem);
                $getQty = $getSumStock->amount_tambah;
                $getSisa = $getSumStock->amount_tambah - $getSumStock->amount_kurang;
                $dataUpdateItem = array(
                    'qty' => $getQty, 
                    'sisa' => $getSisa
                );
                $modelItemPurchase->getUpdateItem($getIdItem, $dataUpdateItem);
            }
        }
        $dataUpdateItem = array(
            'qty' => $request->qty,
            'keterangan' => $request->keterangan
        );
        $modelOrder->getUpdateOrderItem($item_id, $dataUpdateItem);
        return redirect()->route('adm_order_barang', array($order_id))
                    ->with('message', 'order barang '.$getOrderItemById->purchase_name.' berhasil di update')
                    ->with('messageclass', 'success');
    }
    
    public function getDeleteOrderItemBarang($order_id, $item_id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelOrder = New Order;
        $getOrderItemById = $modelOrder->getAdminOrderItem($order_id, $item_id);
        return view('admin.ajax.delete_order_stock')
                ->with('headerTitle', 'Hapus Order Stock')
                ->with('dataUser', $dataUser)
                ->with('getData', $getOrderItemById);
    }
    
    public function postDeleteOrderItemBarang($order_id, $item_id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->item_id != $item_id){
            return redirect()->route('adm_order_barang', array($order_id))
                    ->with('message', 'Whoopss.. Something went wrong')
                    ->with('messageclass', 'danger');
        }
        if($request->is_terima == 1){
            return redirect()->route('adm_order_barang', array($order_id))
                    ->with('message', 'gagal hapus, barang '.$request->purchase_name.' sudah diterima')
                    ->with('messageclass', 'danger');
        }
        $modelOrder = New Order;
        $dataUpdateItem = array(
            'deleted_at' => date('Y-m-h H:i:s')
        );
        $remove = $modelOrder->getUpdateOrderItem($item_id, $dataUpdateItem);
        return redirect()->route('adm_order_barang', array($order_id))
                    ->with('message', 'order barang '.$request->purchase_name.' berhasil dihapus')
                    ->with('messageclass', 'success');
    }
    
    //Absen
    
    public function getAdminListAbsen(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        return view('admin.admin_area.list_absen')
                ->with('headerTitle', 'List Absen')
                ->with('allCabang', $getAllCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminListAbsen(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        if($request->start_date == null){
            return redirect()->route('adm_list_absen')
                        ->with('message', 'Bulan harus dipilih')
                        ->with('messageclass', 'danger');
        }
        $month = date('m', strtotime( $request->start_date ));
        $year = date('Y', strtotime( $request->start_date ));
        $date = (object) array(
            'month' => $month,
            'year' => $year
        );
        $getManagerId = $modelMember->getUserId($request->manager_id);
        if($getManagerId == null){
            return redirect()->route('adm_list_absen')
                        ->with('message', 'Manager harus dipilih')
                        ->with('messageclass', 'danger');
        }
        $getData = $modelMember->getAdminAbsen($getManagerId, $date);
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        return view('admin.admin_area.post_list_absen')
                ->with('allCabang', $getAllCabang)
                ->with('headerTitle', 'List Absen '.$getManagerId->name)
                ->with('getData', $getData)
                ->with('dataManager', $getManagerId)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminDetailAbsen($user_id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $cekUsersAbsen = $modelMember->getAbsenDetail($user_id);
        return view('admin.admin_area.detail_absen')
                ->with('headerTitle', 'Detail Absen Bulan '.date('M'))
                ->with('geData', $cekUsersAbsen)
                ->with('dataUser', $dataUser);
    }
    
    ////Kategori Pengeluaran
    public function getAdminListCategoryOut(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $getAllCategory = $modelTransfer->getAllCategoryPengeluaran();
        return view('admin.admin_area.category_out')
                ->with('headerTitle', 'Tambah Kategori Pengeluaran')
                ->with('getData', $getAllCategory)
                ->with('dataUser', $dataUser);
    }
    
    public function postCategoryOut(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $name = $request->name;
        $dataInsert = array(
            'pengeluaran_name' => $name
        );
        $modelTransfer->getInsertCategoryPengeluaran($dataInsert);
        $modelMember = New Member;
        $jsonLog = json_encode($dataInsert);
        $dataLog = array(
            'log_data' => 'id: '.$dataUser->id.', name: '.$dataUser->name.', type: '.$dataUser->user_type.'. Insert '.$jsonLog
        );
        $modelMember->getInsertLog($dataLog);
        return redirect()->route('adm_out_category')
                    ->with('message', 'Tambah Kategori Berhasil')
                    ->with('messageclass', 'success');
    }
    
    public function postCategoryOutByType($type, $id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        if($type == 'edit'){
            $pesan = 'Ganti';
            $dataEdit = array(
                'pengeluaran_name' => $request->new_name
            );
            $modelTransfer->getUpdateCategoryPengeluaran($id, $dataEdit);
        }
        if($type == 'rm'){
            $pesan = 'Hapus';
            $dataDelete = array(
                'deleted_at' => date('Y-m-h H:i:s')
            );
            $modelTransfer->getUpdateCategoryPengeluaran($id, $dataDelete);
        }
        return redirect()->route('adm_out_category')
                    ->with('message', $pesan.' Kategori Berhasil')
                    ->with('messageclass', 'success');
    }
    
    //
    //Admin cek manager yg proses transfer
    //Transfer
    
    public function getAdminListTransfer(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getAllTransferAdmin();
        return view('admin.admin_area.list_transfer')
                ->with('headerTitle', 'Report Transfer')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminTransferById($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getAllTransferAdminId($id);
        $getDetailData = $modelTransfer->getTotalSalesAndOutAdmin($getData->transfer_date, $getData->manager_id);
        return view('admin.admin_area.detail_transfer')
                ->with('headerTitle', 'Detail Transfer')
                ->with('getData', $getData)
                ->with('getDetailData', $getDetailData)
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminConfirmTransfer(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $id = $request->cekId;
        $type = $request->type;
        $keterangan = null;
        $text = 'Approve';
        $updateTransfer = array(
            'is_transfer' => 1,
            'approve_at' => date('Y-m-d H:i:s'),
        );
        if($type == 2){
            $keterangan = $request->keterangan;
            $text = 'Reject';
            $updateTransfer = array(
                'is_transfer' => 2,
                'reject_at' => date('Y-m-d H:i:s'),
                'keterangan' => $request->keterangan
            );
        }
        
        $modelTransfer = New Transfer;
        $modelTransfer->getUpdateTransfer($id, $updateTransfer);
        return redirect()->route('adm_transfer')
                    ->with('message', $text.' Berhasil')
                    ->with('messageclass', 'success');
    }
    
    public function postAdminEditPengeluaran(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $id = $request->cekId;
        $transfer_id = $request->transfer_id;
        $modelTransfer = New Transfer;
        $getDataTransfer = $modelTransfer->getAllTransferAdminId($transfer_id);
        $dataUpdateOut = array(
            'pengeluaran_price' => $request->new_jumlah,
            'keterangan' => $request->keterangan
        );
        $modelTransfer->getUpdatePengeluaran($id, $dataUpdateOut);
        $getTotalOut = $modelTransfer->getTotalPengeluaran($getDataTransfer->transfer_date, $getDataTransfer->manager_id);
        $pengeluaran_price = 0;
        if($getTotalOut->total_out != null){
            $pengeluaran_price = $getTotalOut->total_out;
        }
        $transferPrice = $getDataTransfer->sales_price - $getTotalOut->total_out;
        $dataUpdateTransfer = array(
            'transfer_price' => $transferPrice,
            'pengeluaran_price' => $pengeluaran_price,
        );
        $modelTransfer->getUpdateTransfer($transfer_id, $dataUpdateTransfer);
        $modelMember = New Member;
        $jsonLog = json_encode($dataUpdateTransfer);
        $jsonLog1 = json_encode($dataUpdateOut);
        $dataLog = array(
            'log_data' => 'id: '.$dataUser->id.', name: '.$dataUser->name.', type: '.$dataUser->user_type.'. Edit '.$jsonLog.' '.$jsonLog1
        );
        $modelMember->getInsertLog($dataLog);
        return redirect()->route('adm_transfer_id', $transfer_id)
                    ->with('message', 'Berhasil Update Pengeluaran')
                    ->with('messageclass', 'success');
    }
    
    public function postAdminSearchTransfer(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->start_date == null){
            return redirect()->route('adm_transfer')
                    ->with('message', 'Masukan Tanggal Awal')
                    ->with('messageclass', 'danger');
        }
        if($request->end_date == null){
            return redirect()->route('adm_transfer')
                    ->with('message', 'Masukan Tanggal Akhir')
                    ->with('messageclass', 'danger');
        }
        if($request->type == null){
            return redirect()->route('adm_transfer')
                    ->with('message', 'Pilih Jenis')
                    ->with('messageclass', 'danger');
        }
        $start = $request->start_date;
        $end = $request->end_date;
        $type = $request->type;
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getSearchTransferAdmin($start, $end, $type);
        return view('admin.admin_area.list_transfer')
                ->with('headerTitle', 'Report Transfer')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminRemovePengeluaran(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $id = $request->cekId;
        $transfer_id = $request->transfer_id;
        $modelTransfer = New Transfer;
        $getDataTransfer = $modelTransfer->getAllTransferAdminId($transfer_id);
        $dataUpdateOut = array(
            'deleted_at' => date('Y-m-d H:i:s')
        );
        $modelTransfer->getUpdatePengeluaran($id, $dataUpdateOut);
        $getTotalOut = $modelTransfer->getTotalPengeluaran($getDataTransfer->transfer_date, $getDataTransfer->manager_id);
        $pengeluaran_price = 0;
        if($getTotalOut->total_out != null){
            $pengeluaran_price = $getTotalOut->total_out;
        }
        $transferPrice = $getDataTransfer->sales_price - $getTotalOut->total_out;
        $dataUpdateTransfer = array(
            'transfer_price' => $transferPrice,
            'pengeluaran_price' => $pengeluaran_price,
        );
        $modelTransfer->getUpdateTransfer($transfer_id, $dataUpdateTransfer);
        $modelMember = New Member;
        $jsonLog = json_encode($dataUpdateTransfer);
        $jsonLog1 = json_encode($dataUpdateOut);
        $dataLog = array(
            'log_data' => 'id: '.$dataUser->id.', name: '.$dataUser->name.', type: '.$dataUser->user_type.'. Remove '.$jsonLog.' '.$jsonLog1
        );
        $modelMember->getInsertLog($dataLog);
        return redirect()->route('adm_transfer_id', $transfer_id)
                    ->with('message', 'Berhasil Hapus Pengeluaran')
                    ->with('messageclass', 'success');
    }
    
    public function getAdminListPengeluaran(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $textDay = 'Data Pengeluaran ';
        $getPengeluaran = null;
        $getRequest = (object) array(
                'startDay' => null,
                'endDay' => null,
                'cabang_id' => null,
                'manager_id' => null,
                'getManager' => null
            );
        if($request->start_date != null && $request->end_date != null && $request->cabang != null){
            $modelMember = New Member;
            $manager_id = null;
            $getManager = null;
            if($request->manager_id != null){
                $manager_id = $request->manager_id;
                $getManager = $modelMember->getManagerFromCabang($request->cabang);
            }
            $getRequest = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date,
                'cabang_id' => $request->cabang,
                'manager_id' => $manager_id,
                'getManager' => $getManager
            );
            $textDay =  'Data Pengeluaran '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
            $getPengeluaran = $modelTransfer->getDataPengeluaranbyDateAdmin($getRequest);
        }
        return view('admin.admin_area.all_pengeluaran')
                ->with('day', $textDay)
                ->with('headerTitle', 'Data Pengeluaran')
                ->with('allPengeluaran', $getPengeluaran)
                ->with('allCabang', $getAllCabang)
                ->with('getRequest', $getRequest)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminTransferPerCabang(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $modelTransfer = New Transfer;
        $data = null;
        $getData = $modelTransfer->getAllTransferAdminPerCabang($data);
        return view('admin.admin_area.list_transfer_per_cabang')
                ->with('headerTitle', 'Report Transfer')
                ->with('getData', $getData)
                ->with('allCabang', $getAllCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminTransferPerCabang(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }

        if($request->start_date == null){
            return redirect()->route('adm_transfer_cabang')
                    ->with('message', 'Tanggal start tidak dipilih')
                    ->with('messageclass', 'danger');
        }
        if($request->end_date == null){
            return redirect()->route('adm_transfer_cabang')
                    ->with('message', 'Tanggal end tidak dipilih')
                    ->with('messageclass', 'danger');
        }
        $start = $request->start_date;
        $end = $request->end_date;
        $cabang = $request->cabang;
        $data = (object) array(
            'start' => $start,
            'end' => $end,
            'cabang_id' => $cabang
        );
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getAllTransferAdminPerCabang($data);
        return view('admin.admin_area.list_transfer_per_cabang')
                ->with('headerTitle', 'Report Transfer')
                ->with('getData', $getData)
                ->with('allCabang', $getAllCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminTransferByCabangId($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getAllTransferAdminPerCabang($data);
    }
    
    public function getAdminListMQBPengeluaran(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $textDay = 'Data Pengeluaran ';
        $getPengeluaran = null;
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data Pengeluaran MQB '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
            $getPengeluaran = $modelTransfer->getDataPengeluaranMQBbyDateAdmin($getMonth);
        }
        return view('admin.admin_area.mqb_all_pengeluaran')
                ->with('day', $textDay)
                ->with('headerTitle', 'Data Pengeluaran MQB')
                ->with('allPengeluaran', $getPengeluaran)
                ->with('dataUser', $dataUser);
    }
    
    
    //Setting bonus
    public function getAdminSettingBonus(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelBonus = New Bonus;
        $getSettingBonus = $modelBonus->getSettingBonus();
        if($getSettingBonus == null){
            $getSettingBonus = (object) array(
                'crew_alat' => 0, 'crew_strip' => 0, 'crew_obat' => 0, 'crew_strip_month' => 0, 'crew_obat_month' => 0,
                'tl_strip' => 0, 'tl_obat' => 0, 'tl_strip_month' => 0, 'tl_obat_month' => 0,
                'am_strip' => 0, 'am_obat' => 0, 'am_omset' => 0, 'am_strip_month' => 0, 'am_obat_month' => 0,
                'm_alat' => 0, 'm_strip' => 0, 'm_obat' => 0, 'm_omset' => 0, 'm_strip_month' => 0, 'm_obat_month' => 0,
                'mqb_strip' => 0, 'mqb_obat' => 0, 'mqb_strip_month' => 0, 'mqb_obat_month' => 0, 
                'sm_strip' => 0, 'sm_obat' => 0, 'sm_strip_month' => 0, 'sm_obat_month' => 0, 
                'em_alat' => 0, 'em_strip' => 0, 'em_obat' => 0, 'em_strip_month' => 0, 'em_obat_month' => 0, 
                'sem_alat' => 0, 'sem_strip' => 0, 'sem_obat' => 0, 'sem_strip_month' => 0, 'sem_obat_month' => 0, 
                'gm_alat' => 0, 'gm_strip' => 0, 'gm_obat' => 0, 'gm_strip_month' => 0, 'gm_obat_month' => 0, 
                
                'asmen_omset_obat' => 0, 'manager_omset_obat' => 0, 'em_omset_obat' => 0, 'sem_omset_obat' => 0, 'gm_omset_obat' => 0, 
            );
        }
        return view('admin.admin_area.setting_bonus')
                ->with('headerTitle', 'Data Setting Bonus')
                ->with('getData', $getSettingBonus)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminEditSettingBonus(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelBonus = New Bonus;
        $getSettingBonus = $modelBonus->getSettingBonus();
        if($getSettingBonus == null){
            $getSettingBonus = (object) array(
                'crew_alat' => 0, 'crew_strip' => 0, 'crew_obat' => 0, 'crew_strip_month' => 0, 'crew_obat_month' => 0,
                'tl_strip' => 0, 'tl_obat' => 0, 'tl_strip_month' => 0, 'tl_obat_month' => 0,
                'am_strip' => 0, 'am_obat' => 0, 'am_omset' => 0, 'am_strip_month' => 0, 'am_obat_month' => 0,
                'm_alat' => 0, 'm_strip' => 0, 'm_obat' => 0, 'm_omset' => 0, 'm_strip_month' => 0, 'm_obat_month' => 0,
                'mqb_strip' => 0, 'mqb_obat' => 0, 'mqb_strip_month' => 0, 'mqb_obat_month' => 0, 
                'sm_strip' => 0, 'sm_obat' => 0, 'sm_strip_month' => 0, 'sm_obat_month' => 0, 
                'em_alat' => 0, 'em_strip' => 0, 'em_obat' => 0, 'em_strip_month' => 0, 'em_obat_month' => 0, 
                'sem_alat' => 0, 'sem_strip' => 0, 'sem_obat' => 0, 'sem_strip_month' => 0, 'sem_obat_month' => 0, 
                'gm_alat' => 0, 'gm_strip' => 0, 'gm_obat' => 0, 'gm_strip_month' => 0, 'gm_obat_month' => 0, 
                
                'asmen_omset_obat' => 0, 'manager_omset_obat' => 0, 'em_omset_obat' => 0, 'sem_omset_obat' => 0, 'gm_omset_obat' => 0, 
            );
        }
        return view('admin.admin_area.setting_bonus_edit')
                ->with('headerTitle', 'Ubah Setting Bonus')
                ->with('getData', $getSettingBonus)
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminSettingBonus(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelBonus = New Bonus;
        $getSettingBonus = $modelBonus->getSettingBonus();
        if($getSettingBonus != null){
            $deleteDataSetting = array(
                'is_active' => 0
            );
            $modelBonus->getUpdateSettingBonus($deleteDataSetting);
        }
        $getInsertSetting = array(
            'crew_alat' => $request->crew_alat,
            'crew_strip' => $request->crew_strip,
            'crew_strip_month' => $request->crew_strip_month, 
            'crew_obat' => $request->crew_obat,
            'crew_obat_month' => $request->crew_obat_month,
            'tl_strip' => $request->tl_strip, 
            'tl_strip_month' => $request->tl_strip_month, 
            'tl_obat' => $request->tl_obat,
            'tl_obat_month' => $request->tl_obat_month,
            'am_strip' => $request->am_strip, 
            'am_strip_month' => $request->am_strip_month, 
            'am_obat' => $request->am_obat, 
            'am_obat_month' => $request->am_obat_month, 
            'm_alat' => $request->m_alat, 
            'm_strip' => $request->m_strip, 
            'm_strip_month' => $request->m_strip_month, 
            'm_obat' => $request->m_obat, 
            'm_obat_month' => $request->m_obat_month, 
            'mqb_strip' => $request->mqb_strip, 
            'mqb_strip_month' => $request->mqb_strip_month, 
            'mqb_obat' => $request->mqb_obat, 
            'mqb_obat_month' => $request->mqb_obat_month, 
            'sm_strip' => $request->sm_strip, 
            'sm_strip_month' => $request->sm_strip_month, 
            'sm_obat' => $request->sm_obat, 
            'sm_obat_month' => $request->sm_obat_month, 
            'em_alat' => $request->em_alat,
            'em_strip' => $request->em_strip, 
            'em_strip_month' => $request->em_strip_month, 
            'em_obat' => $request->em_obat, 
            'em_obat_month' => $request->em_obat_month, 
            'sem_alat' => $request->sem_alat,
            'sem_strip' => $request->sem_strip, 
            'sem_strip_month' => $request->sem_strip_month, 
            'sem_obat' => $request->sem_obat, 
            'sem_obat_month' => $request->sem_obat_month, 
            'gm_alat' => $request->gm_alat,
            'gm_strip' => $request->gm_strip, 
            'gm_strip_month' => $request->gm_strip_month, 
            'gm_obat' => $request->gm_obat, 
            'gm_obat_month' => $request->gm_obat_month, 
            'asmen_omset_obat' => $request->asmen_omset_obat, 
            'manager_omset_obat' => $request->manager_omset_obat, 
        );
        $modelBonus->getInsertSettingBonus($getInsertSetting);
        $modelMember = New Member;
        $jsonLog = json_encode($getInsertSetting);
        $dataLog = array(
            'log_data' => 'id: '.$dataUser->id.', name: '.$dataUser->name.', type: '.$dataUser->user_type.'. Edit '.$jsonLog
        );
        $modelMember->getInsertLog($dataLog);
        return redirect()->route('adm_setting_bonus')
                    ->with('message', 'Berhasil Ubah Setting Bonus')
                    ->with('messageclass', 'success');
    }
    
    public function getManagerBonusCrew($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelBonus = New Bonus;
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserId($id);
        $date = null;
        if($request->start_date != null && $request->end_date != null){
            $date = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            ); 
        }
        $getTotalBonusAll = $modelBonus->getTotalAllBonusNew($id, $date);
        $getDetailBonus = $modelBonus->getDetailAllBonusNew($id, $date);
        return view('admin.admin_area.bonus_detail_crew')
                ->with('headerTitle', 'Bonus Detail')
                ->with('getCrew', $getIdMember)
                ->with('getBonusTotal', $getTotalBonusAll)
                ->with('getDetailBonus', $getDetailBonus)
                ->with('id', $id)
                ->with('date', $date)
                ->with('dataUser', $dataUser);
    }
    
    public function getReportCrewBonus(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $nextSunday = strtotime( "next sunday" );
        $getNextSunday = date('d-M-Y', $nextSunday);
        $getDateSunday = date('Y-m-d', $nextSunday);
        $dataSunday = (object) array(
            'nextSunday' => $getNextSunday,
            'dateSunday' => $getDateSunday
        );
        return view('admin.admin_area.report.report_crew_bonus_create')
                ->with('headerTitle', 'Filter Bonus Crew')
                ->with('allCabang', $getAllCabang)
                ->with('sunday', $dataSunday)
                ->with('dataUser', $dataUser);
    }
    
    public function postReportCrewBonus(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cabang == null){
            return redirect()->route('adm_report_bonus_crew')
                ->with('message', 'Cabang harus dipilih')
                ->with('messageclass', 'danger');
        }
        if($request->end_date == null){
            return redirect()->route('adm_report_bonus_crew')
                ->with('message', 'Tanggal harus dipilih')
                ->with('messageclass', 'danger');
        }
         if($request->start_date == null){
            return redirect()->route('adm_report_bonus_crew')
                ->with('message', 'Tanggal harus dipilih')
                ->with('messageclass', 'danger');
        }
        if($request->manager_id == 0){
            return redirect()->route('adm_report_bonus_crew')
                ->with('message', 'Manager harus dipilih')
                ->with('messageclass', 'danger');
        }
        $dataSunday = (object) array(
            'nextSunday' => $request->start_date,
            'dateSunday' => $request->end_date
        );
        $startDay = $request->start_date;
        $modelMember = New Member;
        $modelCabang = New Cabang;
        $modelBonus = New Bonus;
        $getDataManager = $modelMember->getUserId($request->manager_id);
        if($getDataManager == null){
            return redirect()->route('adm_report_bonus_crew')
                    ->with('message', 'Data Manager tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $typeManager = 'gm_id';
        if($getDataManager->level_id == 3){
            $typeManager = 'sem_id';
        }
        if($getDataManager->level_id == 4){
            $typeManager = 'em_id';
        }
        if($getDataManager->level_id == 5){
            $typeManager = 'sm_id';
        }
        if($getDataManager->level_id == 6){
            $typeManager = 'mqb_id';
        }
        if($getDataManager->level_id == 7){
            $typeManager = 'manager_id';
        }
        if($getDataManager->level_id == 8){
            $typeManager = 'asmen_id';
        }
        if($getDataManager->cabang_id != $request->cabang){
            return redirect()->route('adm_report_bonus_crew')
                ->with('message', 'Data cabang tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $getParameter = (object) array(
            'startDay' => $startDay,
            'endDay' => $request->end_date,
            'type' => $typeManager,
            'id' => $getDataManager->id
        );
        $textDay =  'Data Bonus Crew '.date('d M Y', strtotime($startDay)).' - '.date('d M Y', strtotime($request->end_date));
        $userType = 10;
        $getBonus = $modelBonus->getReportBonusCrewWithManager($getParameter, $getDataManager->cabang_id, $userType);
        $getAllCabang = $modelCabang->getAllCabang();
        $getBonusSetting = $modelBonus->getSettingBonus();
       return view('admin.admin_area.report.report_crew_bonus')
                ->with('allCabang', $getAllCabang)
                ->with('day', $textDay)
                ->with('headerTitle', 'Filter ')
                ->with('headerTitle2', $getDataManager->name.' ('.$getDataManager->cabang_name.')')
                ->with('allData', $getBonus)
                ->with('sunday', $dataSunday)
               ->with('bonusSetting', $getBonusSetting)
                ->with('dataUser', $dataUser);
    }
    
    public function getReportOmsetBonus(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $nextSunday = strtotime( "next sunday" );
        $getNextSunday = date('d-M-Y', $nextSunday);
        $getDateSunday = date('Y-m-d', $nextSunday);
        $dataSunday = (object) array(
            'nextSunday' => $getNextSunday,
            'dateSunday' => $getDateSunday
        );
        return view('admin.admin_area.report.report_bonus_omset_create')
                ->with('headerTitle', 'Filter Bonus Omset')
                ->with('allCabang', $getAllCabang)
                ->with('sunday', $dataSunday)
                ->with('dataUser', $dataUser);
    }
    
    public function postReportOmsetBonus(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->cabang == null){
            return redirect()->route('adm_report_bonus_crew')
                ->with('message', 'Cabang harus dipilih')
                ->with('messageclass', 'danger');
        }
        if($request->end_date == null){
            return redirect()->route('adm_report_bonus_crew')
                ->with('message', 'Tanggal harus dipilih')
                ->with('messageclass', 'danger');
        }
         if($request->start_date == null){
            return redirect()->route('adm_report_bonus_crew')
                ->with('message', 'Tanggal harus dipilih')
                ->with('messageclass', 'danger');
        }
        if($request->manager_id == 0){
            return redirect()->route('adm_report_bonus_crew')
                ->with('message', 'Manager harus dipilih')
                ->with('messageclass', 'danger');
        }
        $dataSunday = (object) array(
            'nextSunday' => $request->start_date,
            'dateSunday' => $request->end_date
        );
        $startDay = $request->start_date;
        $modelMember = New Member;
        $modelCabang = New Cabang;
        $modelBonus = New Bonus;
        $getDataManager = $modelMember->getUserId($request->manager_id);
        if($getDataManager == null){
            return redirect()->route('adm_report_bonus_crew')
                    ->with('message', 'Data Manager tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $typeManager = 'GM';
        if($getDataManager->level_id == 3){
            $typeManager = 'SEM';
        }
        if($getDataManager->level_id == 4){
            $typeManager = 'EM';
        }
        if($getDataManager->level_id == 5){
            $typeManager = 'SM';
        }
        if($getDataManager->level_id == 6){
            $typeManager = 'MQB';
        }
        if($getDataManager->level_id == 7){
            $typeManager = 'Manager';
        }
        if($getDataManager->level_id == 8){
            $typeManager = 'Asmen';
        }
        if($getDataManager->cabang_id != $request->cabang){
            return redirect()->route('adm_report_bonus_crew')
                ->with('message', 'Data cabang tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $getParameter = (object) array(
            'startDay' => $startDay,
            'endDay' => $request->end_date,
            'type' => $typeManager,
            'id' => $getDataManager->id
        );
        $textDay =  'Data Bonus Omset '.date('d M Y', strtotime($startDay)).' - '.date('d M Y', strtotime($request->end_date));
        $bonysType = 11;
        $getBonus = $modelBonus->getReportBonusOmset($getParameter, $getDataManager->cabang_id, $bonysType);
        $getAllCabang = $modelCabang->getAllCabang();
       return view('admin.admin_area.report.report_bonus_omset')
                ->with('allCabang', $getAllCabang)
                ->with('day', $textDay)
                ->with('headerTitle', 'Filter ')
                ->with('headerTitle2', $getDataManager->name. ' ('.$typeManager.') Cabang '.$getDataManager->cabang_name)
                ->with('allData', $getBonus)
                ->with('sunday', $dataSunday)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminCSCustomers(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 20);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $cabang = null;
        if($request->cabang != null){
            $cabang = $request->cabang;
        }
        $getAllCabang = $modelCabang->getAllCabang();
        $getData = null;
        $textDay =  'Data Customer';
        return view('admin.admin_area.admin_cs_customers')
                ->with('headerTitle', 'Detail Customers')
                ->with('getData', $getData)
                ->with('allCabang', $getAllCabang)
                ->with('day', $textDay)
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminCSCustomers(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 20);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        if($request->start_date == null || $request->end_date == null){
            return redirect()->route('adm_cs_customers')
                ->with('message', 'tanggal tidak dipilih')
                ->with('messageclass', 'danger');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $getCabang = $modelCabang->getCheckCabang('id', $request->cabang);
        $getParameter = (object) array(
            'startDay' => $request->start_date,
            'endDay' => $request->end_date,
            'cabang_name' => $getCabang->cabang_name
        );
        $textDay =  'Data Customer '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date)).' Cabang '.$getCabang->cabang_name;
        $getData = $modelMember->getListCustomerByDateAdmin($getParameter);
        return view('admin.admin_area.admin_cs_customers')
                ->with('headerTitle', 'Detail Customers')
                ->with('day', $textDay)
                ->with('getData', $getData)
                ->with('allCabang', $getAllCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminTopStructures(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $modelMember = New Member;
        $getTopStructure = $modelMember->getTopStructureList();
        return view('admin.admin_area.top_structure_list')
                ->with('headerTitle', 'Top Structure')
                ->with('allCabang', $getAllCabang)
                ->with('getData', $getTopStructure)
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminCreateTopStructure(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        if($request->manager_id == null){
            return redirect()->route('adm_top_structures')
                ->with('message', 'tidak pilih nama Top Structure')
                ->with('messageclass', 'danger');
        }
        $getCalonTop = $modelMember->getUserId($request->manager_id);
//        $newSponsor = $getDataTo->sponsor_id.',['.$getDataTo->id.']';
//        if($getDataTo->sponsor_id == null){
//            $newSponsor = '['.$getDataTo->id.']';
//        }
        $newStructureId = '['.$getCalonTop->id.']';
        $data = array(
            'structure_type' => 1,
            'top_structure_id' => $getCalonTop->id
        );
        $modelMember->getUpdateMember('id', $getCalonTop->id, $data);
        return redirect()->route('adm_top_structures')
                ->with('message', 'Top Structure berhasil')
                ->with('messageclass', 'success');
    }
    
    public function postAdminRemoveTopStructure(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getCalonTop = $modelMember->getUserId($request->cekId);
        $data = array(
            'structure_id' => null,
            'structure_type' => 0,
            'top_structure_id' => null,
            'sponsor_structure_id' => null
        );
        $modelMember->getUpdateMember('top_structure_id', $getCalonTop->id, $data);
        return redirect()->route('adm_top_structures')
                ->with('message', 'Top Structure berhasil dihapus')
                ->with('messageclass', 'success');
    }
    
    public function getAdminDownStructures($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $modelMember = New Member;
        $getTop = $modelMember->getUserId($id);
        $getDownStructure = $modelMember->getDownlineStructureList($id);
        $getSponsor = $modelMember->getDownlineSponsorFromCabang($id);
        return view('admin.admin_area.down_structure_list')
                ->with('headerTitle', 'Top Structure')
                ->with('allCabang', $getAllCabang)
                ->with('getDataTop', $getTop)
                ->with('getData', $getDownStructure)
                ->with('getSponsor', $getSponsor)
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminCreateDownStructure(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $id = $request->start_user_id;
        $sponsorId = $request->sponsor_structure_id;
        $modelMember = New Member;
        $getSponsor = $modelMember->getUserId($sponsorId);
        if($id == null){
            return redirect()->route('adm_down_structures', $getSponsor->top_structure_id)
                ->with('message', 'Tidak memilih nama')
                ->with('messageclass', 'danger');
        }
        if($id == '0'){
            return redirect()->route('adm_down_structures', $getSponsor->top_structure_id)
                ->with('message', 'Tidak memilih nama')
                ->with('messageclass', 'danger');
        }
        $newStructureId = $getSponsor->structure_id.',['.$getSponsor->id.']';
        if($getSponsor->structure_id == null){
            $newStructureId = '['.$getSponsor->id.']';
        }
        $data = array(
            'structure_id' => $newStructureId,
            'structure_type' => 2,
            'top_structure_id' => $getSponsor->top_structure_id,
            'sponsor_structure_id' => $getSponsor->id
        );
        $modelMember->getUpdateMember('id', $id, $data);
        return redirect()->route('adm_down_structures', $getSponsor->top_structure_id)
                ->with('message', 'Anggota Struktur berhasil dibuat')
                ->with('messageclass', 'success');
    }
    
    public function postAdminRemoveStructure(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getSructureName = $modelMember->getUserId($request->cekId);
        $structureId = $getSructureName->structure_id.',['.$getSructureName->id.']';
        $getDownline = $modelMember->getDownlineSponsorFromSponsorId($structureId);
        foreach($getDownline as $row){
            $dataRow = array(
                'structure_id' => null,
                'structure_type' => 0,
                'top_structure_id' => null,
                'sponsor_structure_id' => null
            );
            $modelMember->getUpdateMember('id', $row->id, $dataRow);
        }
        $data = array(
            'structure_id' => null,
            'structure_type' => 0,
            'top_structure_id' => null,
            'sponsor_structure_id' => null
        );
        $modelMember->getUpdateMember('id', $getSructureName->id, $data);
        return redirect()->route('adm_down_structures', $getSructureName->top_structure_id)
                ->with('message', 'Anggota Struktur '.$getSructureName->name.' berhasil dihapus')
                ->with('messageclass', 'success');
    }
    
    public function getViewStructureOrganisasi($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getTop = $modelMember->getUserId($id);
        return view('admin.admin_area.down_structure_tree')
                ->with('getDataTop', $getTop)
                ->with('headerTitle', 'Data Organisasi '.$getTop->name)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminContents(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelContent = New Content;
        $getData = $modelContent->getAllContent();
        return view('admin.admin_area.news.berita_list')
                ->with('getData', $getData)
                ->with('headerTitle', 'Berita ')
                ->with('dataUser', $dataUser);
        
    }
    
    public function getAdminCreateContents(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        return view('admin.admin_area.news.berita_create')
                ->with('headerTitle', 'New Berita ')
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminCreateContents(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelContent = New Content;
        $dataInsert = array(
            'created_by' => $dataUser->id,
            'title' => $request->title,
            'image_url' => $request->image_url,
            'short_desc' => $request->short_desc,
            'full_desc' => $request->full_desc
        );
        $modelContent->getInsertContent($dataInsert);
        return redirect()->route('adm_contents')
                ->with('message', 'Berita berhasil dibuat')
                ->with('messageclass', 'success');
    }
    
    public function getAdminEditContents($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelContent = New Content;
        $getData = $modelContent->getContentByField('id', $id);
        return view('admin.admin_area.news.berita_edit')
                ->with('headerTitle', 'Edit Berita ')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminEditContents(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $dataUpdate = array(
            'created_by' => $dataUser->id,
            'title' => $request->title,
            'image_url' => $request->image_url,
            'short_desc' => $request->short_desc,
            'full_desc' => $request->full_desc,
            'updated_at' => date('Y-m-d H:i:s'),
        );
        $modelContent = New Content;
        $modelContent->getUpdateContent($request->cekId, $dataUpdate);
        return redirect()->route('adm_contents')
                ->with('message', 'Berita berhasil dihapus')
                ->with('messageclass', 'success');
    }
    
    public function postAdminRemoveContents(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $dataDelete = array(
            'publish' => 0,
            'updated_at' => date('Y-m-d H:i:s'),
        );
        $modelContent = New Content;
        $modelContent->getUpdateContent($request->cekId, $dataDelete);
        return redirect()->route('adm_contents')
                ->with('message', 'Berita berhasil dihapus')
                ->with('messageclass', 'success');
    }
    
    public function getAdminChangePassword(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        return view('admin.admin_area.change_passwd')
                ->with('headerTitle', 'Password ')
                ->with('allCabang', $getAllCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminChangePassword(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->password == null){
            return redirect()->route('adm_password')
                ->with('message', 'Password harus diisi')
                ->with('messageclass', 'danger');
        }
        if($request->password != $request->repassword){
            return redirect()->route('adm_password')
                ->with('message', 'Password tidak sama')
                ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $cekUsername = $modelMember->getCekUsername($request->username);
        if($cekUsername != null){
            return redirect()->route('adm_password')
                ->with('message', 'Username sudah terpakai, gunakan username lain')
                ->with('messageclass', 'danger');
        }
        $pass = bcrypt($request->password);
        $id = $request->manager_id;
        $dataUpdate = array(
            'username' => $request->username,
            'password' => $pass,
            'updated_at' => date('Y-m-d H:i:s')
        );
        $modelMember->getUpdateMember('id', $id, $dataUpdate);
        return redirect()->route('adm_password')
                ->with('message', 'berhasil, Password diubah')
                ->with('messageclass', 'success');
    }
    
    public function getAdminListBanusManager(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $modelBonus = New Bonus;
        $getAllCabang = $modelCabang->getAllCabang();
        $textDay = 'Data Bonus Manager ';
        $getData = null;
        $getRequest = (object) array(
            'startDay' => null,
            'endDay' => null,
            'cabang_id' => null,
        );
        $getBonusSetting = $modelBonus->getSettingBonus();
        if($request->start_date != null && $request->end_date != null){
            $getRequest = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date,
                'cabang_id' => null,
            );
            $textDay =  'Data Bonus Manager '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
            $getData = $modelBonus->getBonusAllMangerCabang('manager_id', $getRequest);
        }
        return view('admin.admin_area.report.all_manager_bonus')
                ->with('headerTitle', $textDay)
                ->with('allData', $getData)
                ->with('allCabang', $getAllCabang)
                ->with('getRequest', $getRequest)
                ->with('bonusSetting', $getBonusSetting)
                ->with('dataUser', $dataUser);
    }
    
    public function getDetailProductSales($type){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelCabang = New Cabang;
        $getAllCabang = $modelCabang->getAllCabang();
        $getData = null;
        $getDataManager = null;
        $getDate = (object) array(
            'startDay' => null,
            'endDay' => null
        );
        return view('admin.admin_area.report.detail_product_sales')
                ->with('headerTitle', 'Detail Product Sales')
                ->with('allCabang', $getAllCabang)
                ->with('getData', $getData)
                ->with('dataNama', $getDataManager)
                ->with('type', $type)
                ->with('tgl', $getDate)
                ->with('dataUser', $dataUser);
    }
    
    public function postDetailProductSales($type, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        
        if($request->cabang <= 0){
            return redirect()->route('manager_detail_sales')
                    ->with('message', 'Anda Tidak Memilih Cabang')
                    ->with('messageclass', 'danger');
        }
        if($request->start_date == null || $request->end_date == null){
            return redirect()->route('manager_detail_sales')
                    ->with('message', 'Anda Tidak Memilih Tanggal')
                    ->with('messageclass', 'danger');
        }
        if($request->manager_id <= 0){
            return redirect()->route('manager_detail_sales')
                    ->with('message', 'Anda Tidak Memilih Manager')
                    ->with('messageclass', 'danger');
        }
        $modelCabang = New Cabang;
        $modelSale = New Sale;
        $modelMember = New Member;
        $getDate = (object) array(
            'startDay' => $request->start_date,
            'endDay' => $request->end_date
        );
        $getDataManager = $modelMember->getUserIdProductDetail($request->manager_id);
        $modelBonus = New Bonus;
        $text = 'Manager';
        if($getDataManager->level_id == 8){
            $text = 'Asmen';
        }
        if($getDataManager->level_id == 9){
            $text = 'TLD';
        }
        $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        $getAllCabang = $modelCabang->getAllCabang();
        $getData = $modelSale->getDetailProductSales($getDate, $getDataManager);
        $getAllManagerCabang = $modelMember->getManagerFromCabang($getDataManager->cabang_id);
        $getBonusSetting = $modelBonus->getSettingBonus();
        return view('admin.admin_area.report.detail_product_sales')
                ->with('day', $textDay)
                ->with('headerTitle', 'Detail Product Sales '.$text.' '.$getDataManager->name)
                ->with('getData', $getData)
                ->with('allCabang', $getAllCabang)
                ->with('tgl', $getDate)
                ->with('dataNama', $getDataManager)
                ->with('manager', $getAllManagerCabang)
                ->with('type', $type)
                ->with('bonusSetting', $getBonusSetting)
                ->with('dataUser', $dataUser);
    }
    
    public function getReportStockSafra(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $cabang = null;
        $modelCabang = New Cabang;
        $modelStock = New Stock;
        $getAllCabang = $modelCabang->getAllCabang();
        $cabangId = null;
        $lastSunday = date('Y-m-d', strtotime( "-2 weeks sunday" ));
        $thisSunday = date('Y-m-d', strtotime( "last sunday" ));
        if(date('w') == 0){
            $lastSunday = date('Y-m-d', strtotime( "last sunday" ));
            $thisSunday = date('Y-m-d');
        } 
        $today = date('Y-m-d');
        if(date('w') == 0){
            $getItemToday = $modelStock->getHistoryAllStockIsSunday($today);
        } else {
            $getItemToday = $modelStock->getHistoryAllStockIsNotSunday($thisSunday, $today);
        }
        return view('admin.admin_area.report.stock_opname_safra')
                ->with('headerTitle', 'Report Stock Safra Global')
                ->with('headerTitle2', null)
                ->with('item', $getItemToday)
                ->with('itemLast', null)
                ->with('allCabang', $getAllCabang)
                ->with('cabang', $cabang)
                ->with('cabangId', $cabangId)
                ->with('dataCabang', null)
                ->with('dataUser', $dataUser);
    }
    
    public function postReportStockSafraCabang(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $cabang = $request->cabang;
        if($request->cabang == null){
            return redirect()->route('admin_report_stock')
                    ->with('message', 'Cabang harus dipilih')
                    ->with('messageclass', 'danger');
        }
        $modelCabang = New Cabang;
        $getDataCabang = $modelCabang->getCheckCabang('id', $cabang);
        $getAllCabang = $modelCabang->getAllCabang();
        $modelStock = New Stock;
        $lastSunday = date('Y-m-d', strtotime( "-2 weeks sunday" ));
        $thisSunday = date('Y-m-d', strtotime( "last sunday" ));
        if(date('w') == 0){
            $lastSunday = date('Y-m-d', strtotime( "last sunday" ));
            $thisSunday = date('Y-m-d');
        } 
        $dataSunday = (object) array(
            'lastSunday' => $lastSunday, 'thisSunday' => $thisSunday
        );
        $today = date('Y-m-d');
        $getItemLast = $modelStock->getSuperAdminHistoryStockSafra($cabang, $lastSunday, $thisSunday);
        if(date('w') == 0){
            $getItemToday = $modelStock->getSuperAdminHistoryStockIsSundaySafra($cabang, $today);
        } else {
            $getItemToday = $modelStock->getSuperAdminHistoryStockIsNotSundaySafra($cabang, $thisSunday, $today);
        }
        $cabangId = true;
        return view('admin.admin_area.report.stock_opname_safra')
                ->with('headerTitle', 'Report Stock Safra Cabang '.$getDataCabang->cabang_name.' '.date('d M Y', strtotime($thisSunday)))
                ->with('headerTitle2', 'Report Stock Safra Cabang '.$getDataCabang->cabang_name.' '.date('d M Y', strtotime($lastSunday)))
                ->with('item', $getItemToday)
                ->with('itemLast', $getItemLast)
                ->with('cabang', $cabang)
                ->with('allCabang', $getAllCabang)
                ->with('cabangId', $cabangId)
                ->with('dataSunday', $dataSunday)
                ->with('dataCabang', $getDataCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminSettingApp(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelBonus = New Bonus;
        $getSettingApp = $modelBonus->getSettingApp();
        if($getSettingApp == null){
            $getSettingApp = (object) array(
                'sale_day' => 3
            );
        }
        return view('admin.admin_area.setting_app')
                ->with('headerTitle', 'Data Setting APp')
                ->with('getData', $getSettingApp)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminEditSettingApp(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelBonus = New Bonus;
        $getSettingApp = $modelBonus->getSettingApp();
        if($getSettingApp == null){
            $getSettingApp = (object) array(
                'sale_day' => 3
            );
        }
        return view('admin.admin_area.setting_app_edit')
                ->with('headerTitle', 'Ubah Setting App')
                ->with('getData', $getSettingApp)
                ->with('dataUser', $dataUser);
    }
    
    public function postAdminSettingApp(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelBonus = New Bonus;
        $getSettingApp = $modelBonus->getSettingApp();
        if($getSettingApp != null){
            $deleteDataApp = array(
                'is_active' => 0
            );
            $modelBonus->getUpdateSettingApp($deleteDataApp);
        }
        $getInsertSettingApp = array(
            'sale_day' => $request->sale_day
        );
        $modelBonus->getInsertSettingApp($getInsertSettingApp);
        $modelMember = New Member;
        $jsonLog = json_encode($getInsertSettingApp);
        $dataLog = array(
            'log_data' => 'id: '.$dataUser->id.', name: '.$dataUser->name.', type: '.$dataUser->user_type.'. Edit '.$jsonLog
        );
        $modelMember->getInsertLog($dataLog);
        return redirect()->route('adm_setting_app')
                    ->with('message', 'Berhasil Ubah Setting App')
                    ->with('messageclass', 'success');
    }
    
    public function getOrderInvoice($order_id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelOrder = New Order;
        $getOrderData = $modelOrder->getAdminOrderId($order_id);
        $getAllOrderItem = $modelOrder->getAdminListOrderItem($order_id);
        return view('admin.admin_area.list_invoice_order')
                ->with('headerTitle', 'List Order Barang')
                ->with('getData', $getAllOrderItem)
                ->with('dataOrder', $getOrderData)
                ->with('dataUser', $dataUser);
    }
    
    
    
}

