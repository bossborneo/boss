<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Cabang;

class AdminController extends Controller {
    use AuthenticatesUsers;
    
    public function __construct(){
        
    }
    
    public function getCredentials(Request $request){
        $credential = $request->only('username', 'password');
        $credential['is_active'] = 1;
        $credential['is_login'] = 1;
        return $credential;
    }
    
    public function getAdminLogin(){
        return view('admin.login');
    }
    
    public function postAdminLogin(Request $request){
//        $username = 'manager1_2018';
//        $password = '123';
//        $username = 'masteradmin@master.com';
//        $password = '123';
        $username = $request->admin_username;
        $password = $request->admin_password;
        $userdata = array( 'username' => $username, 'password'  => $password, 'is_active' => 1, 'is_login' => 1 );
        if($this->guard()->attempt($userdata)){
            $request->session()->regenerate();
            $dataUser = Auth::user();
            if($dataUser->level_id == 7){
                return redirect()->route('input_penjualan');
            }
            return redirect()->route('adminDashboard');
        }
        return redirect()->route('adminLogin')
                ->with('message', 'Tidak dapat login, kesalahan pada username ataupun password')
                ->with('messageclass', 'danger');
    }
    
    public function getAdminLogout(Request $request) {
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect()->route('adminLogin');
    }
    
}

