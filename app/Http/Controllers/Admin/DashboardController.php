<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Member;
use App\Model\Sale;
use App\Model\Cabang;
use App\Model\Transfer;
use App\Model\Content;

class DashboardController extends Controller {
    
    public function __construct(){
        
    }
    
    public function getAdminDashboard(Request $request){
        $dataUser = Auth::user();
        $modelMember = New Member;
        $modelSale = New Sale;
        $modelCabang = New Cabang;
        $modelContent = New Content;
        $onlyManager  = array(5);
        $onlyAdmin  = array(1, 2, 3);
        $date = $modelSale->getThisMonth();
        $mySponsor = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $mySponsor = '['.$dataUser->id.']';
        }
        $transferNotif = null;
        if(in_array($dataUser->user_type, $onlyManager)){
            if(!isset($_COOKIE['_content'])){
                $cekContent = null;
            } else {
                $cekContent = $_COOKIE['_content'];
            }
            $getLastContent = $modelContent->getContentLast($cekContent);
            if($getLastContent != null){
                return redirect()->route('mgr_content', $getLastContent->id);
            }
            $arraySpId = array(8, 9, 10,11, 12, 13);
            $getCountCrew = $modelMember->getNewCountAllDownline($dataUser->cabang_id, $mySponsor, $arraySpId);
            $getCountCrewBar = $modelMember->getNewCountAllDownlineBar($dataUser->cabang_id, $mySponsor, $arraySpId);
            $getTop10 = $modelSale->getTop10SalesManagerDashboard('user_id', $date, $mySponsor, $dataUser);
            $getAllSalesMonth = $modelSale->getSalesAllMonthManagerDashboard('user_id', $date, $mySponsor, $dataUser);
            $getAllSalesMonthBar = $modelSale->getSalesAllMonthManagerDashboardBar('user_id', $date, $mySponsor, $dataUser);
            $getMonth = array();
            $getTotalDownline = array();
            foreach($getCountCrewBar as $row1){
                $getMonth[] = $row1->month_name;
                $getTotalDownline[] = $row1->total;
            }
            $getMonthSale = array();
            $getTotalPrice = array();
            foreach($getAllSalesMonthBar as $row2){
                $getMonthSale[] = $row2->month_name;
                $getTotalPrice[] = (int) $row2->jml_price;
            }
            $resultMonth = "'".implode("','",$getMonth)."'";
            $resultDownline = implode(",",$getTotalDownline);
            $resultMonthSale = "'".implode("','",$getMonthSale)."'";
            $resultPrice = implode(",", $getTotalPrice);
            if($dataUser->level_id == 7){
                $modelTransfer = New Transfer;
                $transferNotif = $modelTransfer->getNotifTransferManager($dataUser);
            }
            $getDataTopBarang = null;
            $textDay =  date('F');
            if($request->start_date != null && $request->end_date != null){
                $date = (object) array(
                    'startDay' => $request->start_date,
                    'endDay' => $request->end_date
                ); 
                $textDay =  date('d M Y', strtotime($date->startDay)).' - '.date('d M Y', strtotime($date->endDay));
            }
            
            if($dataUser->level_id == 6){
                $getDataTopBarang = $modelSale->getGlobalMyManagerNewDetailSalesTop10($date, $dataUser);
            }
            return view('admin.home.dashboard')
                    ->with('dataUser', $dataUser)
                    ->with('getTop10', $getTop10)
                    ->with('getAllSalesMonth', $getAllSalesMonth)
                    ->with('getCountCrew', $getCountCrew)
                    ->with('month', $resultMonth)
                    ->with('down', $resultDownline)
                    ->with('monthSale', $resultMonthSale)
                    ->with('transferNotif', $transferNotif)
                    ->with('getLastContent', $getLastContent)
                    ->with('getDataTopBarang', $getDataTopBarang)
                    ->with('textDay', $textDay)
                    ->with('priceSale', $resultPrice);
        }
        
        if(in_array($dataUser->user_type, $onlyAdmin)){
            $getAllSalesMonth = $modelSale->getSalesMonthAdmin($date);
            $getAllSalesMonthAdminBar = $modelSale->getSalesAllMonthManagerDashboardAdminBar();
            $countLevelAdminBar = $modelMember->getNewCountAllDownlineAdminBar();
            $date = $modelSale->getLastWeek();
            if($request->start_date != null && $request->end_date != null){
                $date = (object) array(
                    'startDay' => $request->start_date,
                    'endDay' => $request->end_date
                ); 
            }
            $getTop10Admin = $modelSale->getTop10SalesForAdmin($date);
            $getMonthAdmin = array();
            $getTotalDownlineAdmin = array();
            foreach($countLevelAdminBar as $row3){
                $getMonthAdmin[] = $row3->month_name;
                $getTotalDownlineAdmin[] = $row3->total;
            }
            $getMonthSaleAdmin = array();
            $getTotalPriceAdmin = array();
            foreach($getAllSalesMonthAdminBar as $row4){
                $getMonthSaleAdmin[] = $row4->month_name;
                $getTotalPriceAdmin[] = (int) $row4->jml_price;
            }
            $resultMonthAdmin = "'".implode("','",$getMonthAdmin)."'";
            $resultDownlineAdmin = implode(",",$getTotalDownlineAdmin);
            $resultMonthSaleAdmin = "'".implode("','",$getMonthSaleAdmin)."'";
            $resultPriceAdmin = implode(",", $getTotalPriceAdmin);
            return view('admin.home.dashboard-admin')
                    ->with('salesAll', $getAllSalesMonth)
                    ->with('getTop10', $getTop10Admin)
                    ->with('date', $date)
                    ->with('dataUser', $dataUser)
                    ->with('month', $resultMonthAdmin)
                    ->with('down', $resultDownlineAdmin)
                    ->with('monthSale', $resultMonthSaleAdmin)
                    ->with('priceSale', $resultPriceAdmin);
        }
        
        if($dataUser->user_type >= 20){
            $getAllSalesMonth = $modelSale->getSalesMonthAdmin($date);
            $countCabang = $modelCabang->getCountCabang();
            return view('admin.home.dashboard-admin-other')
                    ->with('salesAll', $getAllSalesMonth)
                    ->with('totalCabang', $countCabang)
                    ->with('dataUser', $dataUser);
        }
        
        
        
    }
    
    public function getTest(){
        $dataUser = Auth::user();
        return view('admin.home.my-testing')
                ->with('dataUser', $dataUser);
    }
    
    
}

