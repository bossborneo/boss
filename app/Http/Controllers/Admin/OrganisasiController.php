<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Cabang;
use App\Model\Member;
use App\Model\Purchase;
use App\Model\Itempurchase;
use App\Model\Sale;
use App\Model\Stock;
use App\Model\Order;
use App\Model\Transfer;
use App\Model\Bonus;

class OrganisasiController extends Controller {
    
    public function __construct(){
        
    }
    
    public function getDashboard($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $modelSale = New Sale;
        $modelCabang = New Cabang;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('m_down_structures')
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $date = $modelSale->getThisMonth();
        $mySponsor = $getIdMember->sponsor_id.',['.$getIdMember->id.']';
        if($getIdMember->sponsor_id == null){
            $mySponsor = '['.$getIdMember->id.']';
        }
        $transferNotif = null;
        $arraySpId = array(8, 9, 10,11, 12, 13);
            $getCountCrew = $modelMember->getNewCountAllDownline($getIdMember->cabang_id, $mySponsor, $arraySpId);
            $getCountCrewBar = $modelMember->getNewCountAllDownlineBar($getIdMember->cabang_id, $mySponsor, $arraySpId);
            $getTop10 = $modelSale->getTop10SalesManagerDashboard('user_id', $date, $mySponsor, $getIdMember);
            $getAllSalesMonth = $modelSale->getSalesAllMonthManagerDashboard('user_id', $date, $mySponsor, $getIdMember);
            $getAllSalesMonthBar = $modelSale->getSalesAllMonthManagerDashboardBar('user_id', $date, $mySponsor, $getIdMember);
            $getMonth = array();
            $getTotalDownline = array();
            foreach($getCountCrewBar as $row1){
                $getMonth[] = $row1->month_name;
                $getTotalDownline[] = $row1->total;
            }
            $getMonthSale = array();
            $getTotalPrice = array();
            foreach($getAllSalesMonthBar as $row2){
                $getMonthSale[] = $row2->month_name;
                $getTotalPrice[] = (int) $row2->jml_price;
            }
            $resultMonth = "'".implode("','",$getMonth)."'";
            $resultDownline = implode(",",$getTotalDownline);
            $resultMonthSale = "'".implode("','",$getMonthSale)."'";
            $resultPrice = implode(",", $getTotalPrice);
            if($dataUser->level_id == 7){
                $modelTransfer = New Transfer;
                $transferNotif = $modelTransfer->getNotifTransferManager($getIdMember);
            }
            return view('admin.organisasi.dashboard')
                    ->with('dataUser', $dataUser)
                    ->with('getIdOrg', $getIdMember)
                    ->with('getTop10', $getTop10)
                    ->with('getAllSalesMonth', $getAllSalesMonth)
                    ->with('getCountCrew', $getCountCrew)
                    ->with('month', $resultMonth)
                    ->with('down', $resultDownline)
                    ->with('monthSale', $resultMonthSale)
                    ->with('transferNotif', $transferNotif)
                    ->with('priceSale', $resultPrice);
    }
    
    public function getDataSales($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('m_down_structures')
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $modelSale = New Sale;
        $getMonth = $modelSale->getThisMonth();
        $textDay = 'Data Penjualan '. date('d-M-Y',strtotime("-1 days"));
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date,
                'yesterDay' => null
            );
            $textDay =  'Data Penjualan '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $getSales = $modelSale->getDataSales($getIdMember, $getMonth);
        $getSalesZero = $modelSale->getDataSalesZero($getIdMember, $getMonth);
        return view('admin.organisasi.all_penjualan')
                ->with('day', $textDay)
                ->with('getIdOrg', $getIdMember)
                ->with('headerTitle', 'Data Penjualan')
                ->with('allSales', $getSales)
                ->with('allZero', $getSalesZero)
                ->with('dataUser', $dataUser);
    }
    
    public function getDataStock($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('m_down_structures')
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $type_stock = 1; //barang masuk
        $modelStock = New Stock;
        $lastSunday = date('Y-m-d', strtotime( "-2 weeks sunday" ));
        $thisSunday = date('Y-m-d', strtotime( "last sunday" ));
        if(date('w') == 0){
            $lastSunday = date('Y-m-d', strtotime( "last sunday" ));
            $thisSunday = date('Y-m-d');
        } 
        $stockLastSunday = $modelStock->getManagerHistoryStock($getIdMember->cabang_id, $type_stock, $lastSunday);
        $stockThisSunday = $modelStock->getManagerHistoryStock($getIdMember->cabang_id, $type_stock, $thisSunday);
        return view('admin.organisasi.history_stock')
                ->with('headerTitle', 'Data Stock Minggu '.date('d M Y', strtotime($thisSunday)))
                ->with('headerTitle2', 'Data Stock Minggu '.date('d M Y', strtotime($lastSunday)))
                ->with('stockLastSunday', $stockLastSunday)
                ->with('stockThisSunday', $stockThisSunday)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getDataTransfer($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('m_down_structures')
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getAllTransferManager($getIdMember);
        return view('admin.organisasi.list_transfer')
                ->with('headerTitle', 'Data Transfer')
                ->with('getData', $getData)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getGlobalDetailSales($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('m_down_structures')
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $getData = null;
        return view('admin.organisasi.detail_sales')
                ->with('headerTitle', 'Detail Penjualan')
                ->with('getData', $getData)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function postGlobalDetailSales($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('m_down_structures')
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        if($request->start_date == null || $request->end_date == null){
            return redirect()->route('manager_detail_sales')
                    ->with('message', 'Anda Tidak Memilih Tanggal')
                    ->with('messageclass', 'danger');
        }
        $modelSale = New Sale;
        $getDate = (object) array(
            'startDay' => $request->start_date,
            'endDay' => $request->end_date
        );
        $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        $getData = $modelSale->getGlobalMyManagerNewDetailSales($getDate, $getIdMember);
        return view('admin.organisasi.detail_sales')
                ->with('day', $textDay)
                ->with('headerTitle', 'Detail Sales Manager '.$getIdMember->name)
                ->with('getData', $getData)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getDataCrew($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('m_down_structures')
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $type = 1;
        $arraySpId = array(8, 9, 10, 11, 12, 13);
        $mySponsor = $getIdMember->sponsor_id.',['.$getIdMember->id.']';
        if($getIdMember->sponsor_id == null){
            $mySponsor = '['.$getIdMember->id.']';
        }
        $getAllDownline = $modelMember->getNewestAllDownline($getIdMember->cabang_id, $mySponsor, $arraySpId);
        return view('admin.organisasi.list_crew')
                ->with('headerTitle', 'Data Crew')
                ->with('allDownline', $getAllDownline)
                ->with('type', $type)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getTreeStucture($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('m_down_structures')
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        return view('admin.organisasi.tree')
                ->with('headerTitle', 'Data Struktur Tree')
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getDataAbsen($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('m_down_structures')
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $cekDownline = $modelMember->getAbsen($getIdMember);
        return view('admin.organisasi.list_absen')
                ->with('headerTitle', 'List Absen Bulan '.date('M'))
                ->with('allDownline', $cekDownline)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getOrganisasiMyReport($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        $modelSale = New Sale;
        $textDay = date('F');
        $getDay = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getDay = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
        }
        
        $mType = 'manager_id';
        if($getIdMember->level_id == 2){
            $mType = 'gm_id';
        }
        if($getIdMember->level_id == 3){
            $mType = 'sem_id';
        }
        if($getIdMember->level_id == 4){
            $mType = 'em_id';
        }
        if($getIdMember->level_id == 5){
            $mType = 'sm_id';
        }
        if($getIdMember->level_id == 6){
            $mType = 'mqb_id';
        }
        $getJmlPriceM = $modelSale->getManagerReport($mType, $getIdMember->id, $getDay);
        $getDetail = $modelSale->getReportManagerDetail($mType, $getIdMember->id, $getDay);
        return view('admin.organisasi.my_report')
                ->with('headerTitle', 'Detail Penjualan')
                ->with('reportM', $getJmlPriceM)
                ->with('day', $textDay)
                ->with('detail', $getDetail)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getOrganisasiMReportCrew($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        $nextSunday = strtotime( "next sunday" );
        $getNextSunday = date('d-M-Y', $nextSunday);
        $getDateSunday = date('Y-m-d', $nextSunday);
        $dataSunday = (object) array(
            'nextSunday' => $getNextSunday,
            'dateSunday' => $getDateSunday
        );
        return view('admin.organisasi.m_report_crew_create')
                ->with('headerTitle', 'Filter Report Crew')
                ->with('sunday', $dataSunday)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function postOrganisasiMReportCrew($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        $nextSunday = strtotime( "next sunday" );
        $getNextSunday = date('d-M-Y', $nextSunday);
        $getDateSunday = date('Y-m-d', $nextSunday);
        $dataSunday = (object) array(
            'nextSunday' => $getNextSunday,
            'dateSunday' => $getDateSunday
        );
        $startDay = date('Y-m-d', strtotime('-6 day', strtotime( $getDateSunday)));
        $modelSale = New Sale;
        $getWeek = (object) array(
            'startDay' => $startDay,
            'endDay' => $getDateSunday, 
        );
        $textDay =  'Data '.date('d M Y', strtotime($startDay)).' - '.date('d M Y', strtotime($getDateSunday));
        $userType = 10;
        $getSales = $modelSale->getDataSalesReportCrewForManager($getWeek, $getIdMember->cabang_id, $userType, $getIdMember);
        return view('admin.organisasi.m_report_crew')
                ->with('day', $textDay)
                ->with('headerTitle', 'Production Chart ')
                ->with('headerTitle2', $getIdMember->cabang_name)
                ->with('allSales', $getSales)
                ->with('sunday', $dataSunday)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getOrgMyGlobal($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('adm_down_structures');
        }
        $modelSale = New Sale;
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = 'manager_id';
        $type = 'm';
        $typeText = 'Manager';
        if($getIdMember->level_id == 2){
            $typeM = 'gm_id';
            $type = 'gm';
            $typeText = 'General Manager';
        }
        if($getIdMember->level_id == 3){
            $typeM = 'sem_id';
            $type = 'sem';
            $typeText = 'Senior Executive Manager';
        }
        if($getIdMember->level_id == 4){
            $typeM = 'em_id';
            $type = 'em';
            $typeText = 'Executive Manager';
        }
        if($getIdMember->level_id == 5){
            $typeM = 'sm_id';
            $type = 'sm';
            $typeText = 'Senior Manager';
        }
        if($getIdMember->level_id == 6){
            $typeM = 'mqb_id';
            $type = 'mqb';
            $typeText = 'MQB';
        }
        $url = '/org/global/'.$id.'/'.$type;
        $getData = $modelSale->getGlobalMyManager($typeM, $getMonth, $getIdMember);
        return view('admin.organisasi.global_mymanager')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Sales')
                ->with('getData', $getData)
                ->with('url', $url)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getOrgMyGlobalType($id, $type, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('adm_down_structures');
        }
        $modelSale = New Sale;
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = 'manager_id';
        $typeText = 'Manager';
        if($type == 'gm'){
            $typeM = 'gm_id';
            $typeText = 'General Manager';
        }
        if($type == 'sem'){
            $typeM = 'sem_id';
            $typeText = 'Senior Executive Manager';
        }
        if($type == 'em'){
            $typeM = 'em_id';
            $typeText = 'Executive Manager';
        }
        if($type == 'sm'){
            $typeM = 'sm_id';
            $typeText = 'Senior Manager';
        }
        if($type == 'mqb'){
            $typeM = 'mqb_id';
            $typeText = 'MQB';
        }
        if($type == 'asmen'){
            $typeM = 'asmen_id';
            $typeText = 'Assistant Manager';
        }
        if($type == 'tld'){
            $typeM = 'tld_id';
            $typeText = 'Top Leader';
        }
        if($type == 'ld'){
            $typeM = 'ld_id';
            $typeText = 'Leader';
        }
        if($type == 'tr'){
            $typeM = 'tr_id';
            $typeText = 'Trainer';
        }
        if($type == 'md'){
            $typeM = 'md_id';
            $typeText = 'Merchandiser';
        }
        if($type == 'rt'){
            $typeM = 'rt_id';
            $typeText = 'Retrainer';
        }
        $url = '/org/global/'.$id.'/'.$type;
        $getData = $modelSale->getGlobalMyManager($typeM, $getMonth, $getIdMember);
        return view('admin.organisasi.global_mymanager')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Sales '.$typeText)
                ->with('getData', $getData)
                ->with('url', $url)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getOrgMyStock($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('adm_down_structures');
        }
        $modelSale = New Sale;
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = 'manager_id';
        $type = 'm';
        $typeText = 'Manager';
        if($getIdMember->level_id == 2){
            $typeM = 'gm_id';
            $type = 'gm';
            $typeText = 'General Manager';
        }
        if($getIdMember->level_id == 3){
            $typeM = 'sem_id';
            $type = 'sem';
            $typeText = 'Senior Executive Manager';
        }
        if($getIdMember->level_id == 4){
            $typeM = 'em_id';
            $type = 'em';
            $typeText = 'Executive Manager';
        }
        if($getIdMember->level_id == 5){
            $typeM = 'sm_id';
            $type = 'sm';
            $typeText = 'Senior Manager';
        }
        if($getIdMember->level_id == 6){
            $typeM = 'mqb_id';
            $type = 'mqb';
            $typeText = 'MQB';
        }
        $url = '/org/stock/'.$id.'/'.$type;
        $getData = $modelSale->getGlobalStockMyManager($typeM, $getMonth, $getIdMember);
        return view('admin.organisasi.global_stock')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Stock '.$typeText)
                ->with('getData', $getData)
                ->with('url', $url)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getOrgMyStockType($id, $type, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        if($getIdMember == null){
            return redirect()->route('adm_down_structures');
        }
        $modelSale = New Sale;
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = 'manager_id';
        $typeText = 'Manager';
        if($type == 'gm'){
            $typeM = 'gm_id';
            $typeText = 'General Manager';
        }
        if($type == 'sem'){
            $typeM = 'sem_id';
            $typeText = 'Senior Executive Manager';
        }
        if($type == 'em'){
            $typeM = 'em_id';
            $typeText = 'Executive Manager';
        }
        if($type == 'sm'){
            $typeM = 'sm_id';
            $typeText = 'Senior Manager';
        }
        if($type == 'mqb'){
            $typeM = 'mqb_id';
            $typeText = 'MQB';
        }
        if($type == 'asmen'){
            $typeM = 'asmen_id';
            $typeText = 'Assistant Manager';
        }
        if($type == 'tld'){
            $typeM = 'tld_id';
            $typeText = 'Top Leader';
        }
        if($type == 'ld'){
            $typeM = 'ld_id';
            $typeText = 'Leader';
        }
        if($type == 'tr'){
            $typeM = 'tr_id';
            $typeText = 'Trainer';
        }
        if($type == 'md'){
            $typeM = 'md_id';
            $typeText = 'Merchandiser';
        }
        if($type == 'rt'){
            $typeM = 'rt_id';
            $typeText = 'Retrainer';
        }
        $url = '/org/stock/'.$id.'/'.$type;
        $getData = $modelSale->getGlobalStockMyManager($typeM, $getMonth, $getIdMember);
        return view('admin.organisasi.global_stock')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Stock '.$typeText)
                ->with('getData', $getData)
                ->with('url', $url)
                ->with('getIdOrg', $getIdMember)
                ->with('dataUser', $dataUser);
    }
    
    public function getOrgGlobalSalesDownline(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $getData = $modelSale->getGlobalOrgAllManager($dataUser->id, $getMonth);
        return view('admin.organisasi.global_org')
                ->with('day', $textDay)
                ->with('headerTitle', 'Organisasi Global Sales All')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    
    

}