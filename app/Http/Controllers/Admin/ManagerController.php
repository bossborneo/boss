<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Cabang;
use App\Model\Member;
use App\Model\Purchase;
use App\Model\Stock;
use App\Model\Itempurchase;
use App\Model\Sale;
use App\Model\Order;
use App\Model\Transfer;
use App\Model\Bonus;
use Illuminate\Support\Facades\Hash;
use App\Model\Content;

class ManagerController extends Controller {
    
    public function __construct(){
        
    }
    
    private function isSunday () {
        $sunday = 0;
        return $sunday;
    }
    
    public function getAddCrew(){
        return redirect()->route('view_crew');
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $arraySpId = array(8, 9);
        $mySponsor = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $mySponsor = '['.$dataUser->id.']';
        }
        $cekDownline = $modelMember->getNewAllDownline($dataUser->cabang_id, $mySponsor, $arraySpId);
        return view('admin.manager_area.add_crew')
                ->with('headerTitle', 'Tambah Crew')
                ->with('allDownline', $cekDownline)
                ->with('dataUser', $dataUser);
    }
    
    public function postAddCrew(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getParentId = $modelMember->getParentId($request->parent_id);
        if($getParentId == null){
            return redirect()->route('add_crew')
                ->with('message', 'Data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $getUsername = $modelMember->getUsername();
        $dataInsertUser = array(
            'username' => $getUsername,
            'password' => bcrypt($modelMember->getAutomaticPassword()),
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'is_active' => 1,
            'is_login' => 0,
            'active_at' => date('Y-m-d H:i:s'),
            'user_type' => 10,
            'cabang_id' => $dataUser->cabang_id,
            'cabang_name' => $dataUser->cabang_name,
            'level_id' => 13,
            'created_by' => $dataUser->id,
            'sponsor_id' => $getParentId->sponsor_id.',['.$request->parent_id.']',
            'parent_id' => $request->parent_id,
        );
//        dd($dataInsertUser);
        $getInsert = $modelMember->getInsertUser($dataInsertUser);
        if($getInsert == false){
            return redirect()->route('add_crew')
                ->with('message', 'Terjadi kesalahan pada sistem')
                ->with('messageclass', 'danger');
        }
        return redirect()->route('add_crew')
                ->with('message', 'Berhasil buat crew baru '.$request->name)
                ->with('messageclass', 'success');
    }
    
    public function getViewCrew(){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $type = 1;
        $modelMember = New Member;
        $arraySpId = array(8, 9, 10, 11, 12, 13);
        $mySponsor = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $mySponsor = '['.$dataUser->id.']';
        }
        $getAllDownline = $modelMember->getNewestAllDownline($dataUser->cabang_id, $mySponsor, $arraySpId);
        return view('admin.manager_area.list_crew')
                ->with('headerTitle', 'Data Crew')
                ->with('allDownline', $getAllDownline)
                ->with('type', $type)
                ->with('dataUser', $dataUser);
    }
    
    public function getEditCrew($id){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getUserId = $modelMember->getUserId($id);
        return view('admin.ajax.edit_crew')
                ->with('headerTitle', 'Edit Crew')
                ->with('dataUser', $dataUser)
                ->with('dataUserId', $getUserId);
    }
    
    public function postEditCrew($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($id != $request->cekId){
            return redirect()->route('view_crew')
                ->with('message', 'data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $getUserId = $modelMember->getUserId($id);
        if($getUserId->level_id != 13){
            return redirect()->route('view_crew')
                ->with('message', 'Data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $data = array(
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address
        );
        $getUpdate = $modelMember->getUpdateMember('id', $id, $data);
        if($getUpdate->status == false){
            return redirect()->route('view_crew')
                ->with('message', 'terjadi kesalahan pada sistem')
                ->with('messageclass', 'danger');
        }
        return redirect()->route('view_crew')
                ->with('message', 'data crew berhasil di-edit')
                ->with('messageclass', 'success');
    }
    
    public function getMyProfile(){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        dd($dataUser);
    }
    
    public function getDeleteCrew($id){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getUserId = $modelMember->getUserId($id);
//        if($getUserId->level_id == 8 || $getUserId->level_id == 9 || $getUserId->level_id == 10 || $getUserId->level_id == 11){
//            return redirect()->route('adminDashboard');
//        }
        if($getUserId->level_id != 13){
            return redirect()->route('view_crew')
                ->with('message', 'Data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $data = array('is_active' => 0);
        $getUpdate = $modelMember->getUpdateMember('id', $id, $data);
        if($getUpdate->status == false){
            return redirect()->route('view_crew')
                ->with('message', 'terjadi kesalahan pada sistem')
                ->with('messageclass', 'danger');
        }
        return redirect()->route('view_crew')
                ->with('message', 'data crew berhasil di hapus')
                ->with('messageclass', 'success');
    }
    
    public function getInputPenjualan(){
        $dataUser = Auth::user();
        //Hanya Manager sj
        if($dataUser->level_id != 7){
            return redirect()->route('adminDashboard');
        }
//        $onlyUser  = array(5);
//        if(!in_array($dataUser->user_type, $onlyUser)){
//            return redirect()->route('adminDashboard');
//        }
//        if(date('w') == $this->isSunday()){
//            return redirect()->route('data_penjualan')
//                    ->with('message', 'Input Penjualan hanya tidak bisa dilakukan pada hari minggu')
//                    ->with('messageclass', 'danger');
//        }
        $modelContent = New Content;
        if(!isset($_COOKIE['_content'])){
            $cekContent = null;
        } else {
            $cekContent = $_COOKIE['_content'];
        }
        $getLastContent = $modelContent->getContentLast($cekContent);
        if($getLastContent != null){
            return redirect()->route('mgr_content', $getLastContent->id);
        }
        $modelMember = New Member;
        $arraySpId = array(8, 9, 10, 11, 12, 13);
        $mySponsor = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $mySponsor = '['.$dataUser->id.']';
        }
        $thisSunday = date('Y-m-d', strtotime( "last sunday" ));
        $cekDownline = $modelMember->getNewAllDownline($dataUser->cabang_id, $mySponsor, $arraySpId);
        $modelPurchase = New Purchase;
        $allBarang = $modelPurchase->getManagerStockReady($dataUser, $thisSunday);
        $modelBonus = New Bonus;
        $getSettingApp = $modelBonus->getSettingApp();
        if($getSettingApp == null){
            $getSettingApp = (object) array(
                'sale_day' => 3
            );
        }
        return view('admin.manager_area.input_penjualan')
                ->with('headerTitle', 'Input Absensi dan Input Penjualan')
                ->with('cekDownline', $cekDownline)
                ->with('allBarang', $allBarang)
                ->with('getSettingApp', $getSettingApp)
                ->with('dataUser', $dataUser);
    }
    
    public function postInputPenjualan(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->absen_date == null){
            return redirect()->route('input_penjualan')
                    ->with('message', 'Jam tidak dipilih')
                    ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $modelSale = New Sale;
        $modelItemPurchase = New Itempurchase;
        $modelStock = New Stock;
        $modelBonus = New Bonus;
        $getInvo = 'PJ'.date('Ymd').'_'.uniqid();
        $user_id = $dataUser->id;
        $spId = $dataUser->sponsor_id.',['.$user_id.']';
        if($dataUser->sponsor_id == null){
            $spId = '['.$user_id.']';
        }
        $getLevel = $dataUser->level_id;
        if($request->crew != $dataUser->id){
            $user_id = $request->crew;
            $getIdMember = $modelMember->getUserId($user_id);
            $spId = $getIdMember->sponsor_id.',['.$getIdMember->id.']';
            if($getIdMember->sponsor_id == null){
                $spId = '['.$getIdMember->id.']';
            }
            $getLevel = $getIdMember->level_id;
        }
        $getBonusSetting = $modelBonus->getSettingBonus();
        $arraySpId = explode(',', $spId);
        $dataSp = array();
        foreach($arraySpId as $rowSp){
            $rm1 = str_replace('[', '', $rowSp);
            $rm2 = str_replace(']', '', $rm1);
            $int = (int) $rm2;
            $dataSp[] = $int;
        }
//        dd($request);
        $dataAllSp = $modelMember->getUpperUserId($dataSp);
        $get_gm = $modelMember->getLevelLoop($dataAllSp, 2);
        $get_sem = $modelMember->getLevelLoop($dataAllSp, 3);
        $get_em = $modelMember->getLevelLoop($dataAllSp, 4);
        $get_sm = $modelMember->getLevelLoop($dataAllSp, 5);
        $get_mqb = $modelMember->getLevelLoop($dataAllSp, 6);
        $get_m = $modelMember->getLevelLoop($dataAllSp, 7);
        $get_asmen = $modelMember->getLevelLoop($dataAllSp, 8);
        $get_tld = $modelMember->getLevelLoop($dataAllSp, 9);
        $get_ld = $modelMember->getLevelLoop($dataAllSp, 10);
        $get_tr = $modelMember->getLevelLoop($dataAllSp, 11);
        $get_md = $modelMember->getLevelLoop($dataAllSp, 12);
        $get_rt = $modelMember->getLevelLoop($dataAllSp, 13);
        $masuk_at = $request->sale_date.' '.$request->absen_date;
        $is_sales = 0;
        if($request->checkSales == '1'){
            $is_sales = 1;
            $count = 0;
            if($request->item_purchase != null){
                $count = count($request->item_purchase);
            }
            $dataAll = array();
            if($count > 0){
                for ($x = 0; $x < $count; $x++) {
                    $dataAll[] = (object) array(
                        'crew' => $request->crew,
                        'sale_date' => $request->sale_date,
                        'item_purchase' => $request->item_purchase[$x],
                        'amount' => $request->amount[$x],
//                        'is_half' => $request->opsi[$x]
                    );
                }
                foreach ($dataAll as $rowAll){
                    $getItemId = $modelItemPurchase->getPurchaseFromItem($rowAll->item_purchase);
                    if($getItemId != null){
//                        if($getBonusSetting != null){
//                            $modelBonus->getBonusData($getItemId, $getBonusSetting, $user_id, $rowAll, $getInvo, $get_gm, $get_sem, $get_em, $get_sm, $get_mqb, $get_m, $get_asmen, $get_tld, $get_ld, $get_tr, $get_md, $get_rt);
//                        }
                        $pengali = 1;
//                        if($rowAll->is_half == 1){
//                            $pengali = 0.5;
//                        }
                        $dataInsertSale = array(
                            'item_purchase_id' => $rowAll->item_purchase,
                            'user_id' => $user_id,
                            'gm_id' => $get_gm,
                            'sem_id' => $get_sem,
                            'em_id' => $get_em,
                            'sm_id' => $get_sm,
                            'mqb_id' => $get_mqb,
                            'manager_id' => $get_m,
                            'asmen_id' => $get_asmen,
                            'tld_id' => $get_tld,
                            'ld_id' => $get_ld,
                            'tr_id' => $get_tr,
                            'md_id' => $get_md,
                            'rt_id' => $get_rt,
                            'invoice' => $getInvo,
                            'amount' => ($rowAll->amount * $pengali),
                            'sale_price' => round($rowAll->amount * $getItemId->price * $pengali),
                            'sale_date' => $rowAll->sale_date,
                            'is_masuk' => $request->is_masuk,
//                            'is_half' => $rowAll->is_half,
                        );
//                        dd($dataInsertSale);
                        $insertSale = $modelSale->getInsertSale($dataInsertSale);
                        $lastIdSales = $insertSale->lastID;
                        $dataInsertStock = array(
                            'item_purchase_id' => $rowAll->item_purchase,
                            'type_stock' => 2,
                            'user_id' => $user_id,
                            'amount' => $rowAll->amount,
                            'sales_id' => $lastIdSales,
                            'created_at' => $rowAll->sale_date.' 12:00:00'
                        );
                        $modelStock->getInsertStock($dataInsertStock);
                        $getSumStock = $modelStock->getGetSumStock($rowAll->item_purchase);
                        $getQty = $getSumStock->amount_tambah;
                        $getSisa = $getSumStock->amount_tambah - $getSumStock->amount_kurang;
                        $dataUpdateItem = array(
                            'qty' => $getQty, 
                            'sisa' => $getSisa
                        );
                        $modelItemPurchase->getUpdateItem($rowAll->item_purchase, $dataUpdateItem);
                        if($getBonusSetting != null){
                            $modelBonus->getBonusData($getItemId, $getBonusSetting, $user_id, $rowAll, $getInvo, $get_gm, $get_sem, $get_em, $get_sm, $get_mqb, $get_m, $get_asmen, $get_tld, $get_ld, $get_tr, $get_md, $get_rt);
                        }
                    }
                }
            }
            
        }
        if($request->crew != $dataUser->id){ //Klo manager yg input penjualan maka ga perlu masuk absen
            $cekAbsenExist = $modelMember->getAbsenCheck($user_id, $request->sale_date);
            if($cekAbsenExist == null){
                $dataInsertAbsen = array(
                    'user_id' => $user_id,
                    'cabang_id' => $getIdMember->cabang_id,
                    'is_masuk' => $request->is_masuk,
                    'gm_id' => $get_gm,
                    'sem_id' => $get_sem,
                    'em_id' => $get_em,
                    'sm_id' => $get_sm,
                    'mqb_id' => $get_mqb,
                    'manager_id' => $get_m,
                    'asmen_id' => $get_asmen,
                    'tld_id' => $get_tld,
                    'ld_id' => $get_ld,
                    'tr_id' => $get_tr,
                    'md_id' => $get_md,
                    'rt_id' => $get_rt,
                    'masuk_at' => $masuk_at,
                    'keterangan' => null,
                    'is_sales' => $is_sales
                );
                $modelMember->getInsertAbsen($dataInsertAbsen);
            }
        }
        return redirect()->route('input_penjualan')
                ->with('message', 'input penjualan barang berhasil')
                ->with('messageclass', 'success');
    }
    
    public function getDataPenjualan(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $getMonth = $modelSale->getThisMonth();
        $textDay = 'Data Penjualan '. date('d-M-Y',strtotime("-1 days"));
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date,
                'yesterDay' => null
            );
            $textDay =  'Data Penjualan '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $getSales = $modelSale->getDataSales($dataUser, $getMonth);
        $getSalesZero = $modelSale->getDataSalesZero($dataUser, $getMonth);
        return view('admin.manager_area.all_penjualan')
                ->with('day', $textDay)
                ->with('headerTitle', 'Data Penjualan')
                ->with('allSales', $getSales)
                ->with('allZero', $getSalesZero)
                ->with('dataUser', $dataUser);
    }
    
    public function getEditPenjualan($id){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
//        if(date('w') == $this->isSunday()){
//            return redirect()->route('adminDashboard');
//        }
        $getId = substr($id, 16);
        $modelSale = New Sale;
        $data = $modelSale->getDataSalesManagerById($getId, $dataUser->id);
        if($data == null){
            return redirect()->route('data_penjualan')
                ->with('message', 'data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        return view('admin.manager_area.action_sale')
                ->with('headerTitle', 'Edit Penjualan')
                ->with('dataSales', $data)
                ->with('type', 1)
                ->with('dataUser', $dataUser);
    }
    
    public function getDeletePenjualan($id){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
//        if(date('w') == $this->isSunday()){
//            return redirect()->route('adminDashboard');
//        }
        $getId = substr($id, 16);
        $modelSale = New Sale;
        $data = $modelSale->getDataSalesManagerById($getId, $dataUser->id);
        if($data == null){
            return redirect()->route('data_penjualan')
                ->with('message', 'data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        return view('admin.ajax.action_sale')
                ->with('headerTitle', 'Hapus Penjualan')
                ->with('dataSales', $data)
                ->with('type', 2)
                ->with('dataUser', $dataUser);
    }
    
    public function postActionPenjualan($type, $id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
//        if(date('w') == $this->isSunday()){
//            return redirect()->route('adminDashboard');
//        }
        $modelSale = New Sale;
        $modelStock = New Stock;
        $modelItemPurchase = New Itempurchase;
        $data = $modelSale->getDataSalesManagerById($id, $dataUser->id);
        $getStock = $modelStock->getCheckStock($data->id);
        if($request->cekId != $id){
            return redirect()->route('data_penjualan')
                ->with('message', 'data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        
        if($type == 'edit'){
            if($request->amount <= 0){
                return redirect()->route('data_penjualan')
                    ->with('message', 'jumlah stock tidak boleh kurang atau sama dengan 0')
                    ->with('messageclass', 'danger');
            }
            if($request->amount >= $getStock->amount){
                $diffAmount = round(($request->amount - $getStock->amount), 1);
                $sisaItem = round(($data->sisa - $diffAmount), 1);
            }
            if($request->amount < $getStock->amount){
                $diffAmount = round(($getStock->amount - $request->amount), 1);
                $sisaItem = round(($data->sisa + $diffAmount), 1);
            }
            $updateStock = array(
                'amount' => $request->amount
            );
            $updateItem = array(
                'sisa' => $sisaItem
            );
            $updateSales = array(
                'amount' => $request->amount,
                'sale_price' => round($request->amount * $data->price),
                'sale_date' => $request->sale_date,
            );
            $modelStock->getUpdateStock($getStock->id, $updateStock);
            $modelItemPurchase->getUpdateItem($data->id_item_purchase, $updateItem);
            $modelSale->getUpdateSales($data->id, $updateSales);
            return redirect()->route('data_penjualan')
                    ->with('message', 'Edit sales berhasil')
                    ->with('messageclass', 'success');
        }
        
        if($type == 'rm'){
            //delete
            $dataInsertStock = array(
                'item_purchase_id' => $data->id_item_purchase,
                'type_stock' => 3,
                'user_id' => $getStock->user_id,
                'amount' => $data->amount,
                'sales_id' => $data->id
            );
            $updateSales = array(
                'deleted_at' => date('Y-m-d H:i:s'),
            );
            $sisaItem1 = round(($data->sisa + $data->amount), 1);
            $updateItem = array(
                'sisa' => $sisaItem1
            );
            $modelStock->getInsertStock($dataInsertStock);
            $modelSale->getUpdateSales($data->id, $updateSales);
            $modelItemPurchase->getUpdateItem($data->id_item_purchase, $updateItem);
            return redirect()->route('data_penjualan')
                    ->with('message', 'sales telah dihapus')
                    ->with('messageclass', 'success');
        }
    }
    
    
    //Manager Stock barang
    public function getStockBarang(){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if(date('w') != 1 && date('w') != 6){
//        if(date('w') != $this->isSunday()){
            return redirect()->route('history_stock')
                    ->with('message', 'Input Stock hanya bisa dilakukan pada hari Senin dan Sabtu')
                    ->with('messageclass', 'danger');
        }
        $modelPurchase = New Purchase;
        $newAllPurchase = null;
        $allPurchase2 = $modelPurchase->getManagerAllInputStock($newAllPurchase);
        $modelCabang = New Cabang;
        $getCabang = $modelCabang->getCheckCabang('id', $dataUser->cabang_id);
        return view('admin.manager_area.all_purchase')
                ->with('headerTitle', 'Data Stock')
                ->with('purchseKosong', $allPurchase2)
                ->with('getCabang', $getCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getInputStock($purchase_id){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelPurchase = New Purchase;
        $modelCabang = New Cabang;
        $getPurchaseId = $modelPurchase->getManagerPurchaseIdAddStock($purchase_id);
        $getCabang = $modelCabang->getCheckCabang('id', $dataUser->cabang_id);
        return view('admin.ajax.add_stock')
                ->with('headerTitle', 'Input Stock Opname')
                ->with('dataUser', $dataUser)
                ->with('dataPurchase', $getPurchaseId)
                ->with('getCabang', $getCabang);
    }
    
    public function postInputStock($purchase_id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if(date('w') != 1 && date('w') != 6){
//        if(date('w') != $this->isSunday()){
            return redirect()->route('history_stock')
                    ->with('message', 'Input hanya bisa dilakukan pada hari Senin dan Sabtu')
                    ->with('messageclass', 'danger');
        }
        if($request->cekId != $purchase_id){
            return redirect()->route('stock_barang')
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        if($request->qty <= 0){
            return redirect()->route('stock_barang')
                    ->with('message', 'Input stock harus diisi')
                    ->with('messageclass', 'danger');
        }
        $newSunday = date('Y-m-d', strtotime( "last sunday" ));
        if(date('w') == 6){
            $newSunday = date('Y-m-d', strtotime( "next sunday" ));
        } 
        $dateSunday = date('Y-m-d 07:i:s', strtotime( $newSunday ));
        $thisSundaySeninSabtu = date('Y-m-d 07:i:s', strtotime( "last sunday" ));
        $modelPurchase = New Purchase;
        $modelItemPurchase = New Itempurchase;
        $modelStock = New Stock;
        $getPurchaseId = $modelPurchase->getManagerPurchaseIdAddStock($purchase_id);
        if($getPurchaseId == null){
            return redirect()->route('stock_barang')
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $modelCabang = New Cabang;
        $getCabang = $modelCabang->getCheckCabang('id', $dataUser->cabang_id);
        $type = 'Obat';
        $price = round($getPurchaseId->main_price + $getCabang->extra);
        if($getPurchaseId->type == 1){
            $type = 'Alat';
            $price = round($getPurchaseId->main_price + $getCabang->extra_alat);
        }
        if($getPurchaseId->type == 2){
            $type = 'Strip';
            $price = round($getPurchaseId->main_price + $getCabang->extra_strip);
        }
        if($getPurchaseId->kondisi == 1){
            $price = round($getPurchaseId->main_price);
        }
        $cekItem = $modelItemPurchase->getCheckItemAddStock($dataUser, $purchase_id);
        if($cekItem == null){
            $dataInsert = array(
                'purchase_id' => $request->cekId,
                'cabang_id' => $dataUser->cabang_id,
                'manager_id' => $dataUser->id,
                'qty' => $request->qty,
                'sisa' => $request->qty,
                'price' => $price
            );
            $getInsertItem = $modelItemPurchase->getInsertItem($dataInsert);
            if($getInsertItem->status == false){
                return redirect()->route('stock_barang')
                    ->with('message', 'gagal insert barang, terjadi kesalahan sistem')
                    ->with('messageclass', 'danger');
            }
            $getIdItem = $getInsertItem->lastID;
        } else {
            $getIdItem = $cekItem->id;
        }
        $cekStokMingguIni = $modelStock->getCheckStockLastSunday($getIdItem, $newSunday);
        if($cekStokMingguIni != null){
            return redirect()->route('stock_barang')
                        ->with('message', $getPurchaseId->purchase_name.' sudah di input minggu ini')
                        ->with('messageclass', 'danger');
        }
        $dataInsertStock = array(
            'item_purchase_id' => $getIdItem,
            'type_stock' => 1,
            'user_id' => $dataUser->id,
            'amount' => $request->qty,
            'created_at' => $dateSunday
        );
        $modelStock->getInsertStock($dataInsertStock);
//        if($getInsertStock->status == false){
//            return redirect()->route('stock_barang')
//                ->with('message', 'gagal insert barang stock, terjadi kesalahan sistem')
//                ->with('messageclass', 'danger');
//        }
        $getSumStock = $modelStock->getGetSumStock($getIdItem);
        $getQty = $getSumStock->amount_tambah;
        $getSisa = $getSumStock->amount_tambah - $getSumStock->amount_kurang;
        $dataUpdateItem = array(
            'qty' => $getQty, 
            'sisa' => $getSisa,
            'price' => $price
        );
        $modelItemPurchase->getUpdateItem($getIdItem, $dataUpdateItem);
//        if($updateItem->status == false){
//            return redirect()->route('stock_barang')
//                ->with('message', 'gagal update barang stock, terjadi kesalahan sistem')
//                ->with('messageclass', 'danger');
//        }
        return redirect()->route('history_stock')
                ->with('message', 'stock '.$getPurchaseId->purchase_name.' berhasil ditambah')
                ->with('messageclass', 'success');
    }
    
    public function getHistoryStock(){
         $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $type_stock = 1; //barang masuk
        $modelStock = New Stock;
        $lastSunday = date('Y-m-d', strtotime( "-2 weeks sunday" ));
        $thisSunday = date('Y-m-d', strtotime( "last sunday" ));
        if(date('w') == 0){
            $lastSunday = date('Y-m-d', strtotime( "last sunday" ));
            $thisSunday = date('Y-m-d');
        } 
        $stockLastSunday = $modelStock->getManagerHistoryStock($dataUser->cabang_id, $type_stock, $lastSunday);
        $stockThisSunday = $modelStock->getManagerHistoryStock($dataUser->cabang_id, $type_stock, $thisSunday);
        return view('admin.manager_area.history_stock')
                ->with('headerTitle', 'Data Stock Minggu '.date('d M Y', strtotime($thisSunday)))
                ->with('headerTitle2', 'Data Stock Minggu '.date('d M Y', strtotime($lastSunday)))
                ->with('stockLastSunday', $stockLastSunday)
                ->with('stockThisSunday', $stockThisSunday)
                ->with('dataUser', $dataUser);
    }
    
    public function getMyReports(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
//        $modelMember = New Member;
//        $arraySpId = array(8, 9);
//        $getAllDownline = $modelMember->getNewAllDownline($dataUser->cabang_id, $dataUser->sponsor_id, $arraySpId);
        $textDay = date('F');
        $getDay = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getDay = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
        }
        
        $mType = 'manager_id';
        if($dataUser->level_id == 2){
            $mType = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $mType = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $mType = 'em_id';
        }
        if($dataUser->level_id == 5){
            $mType = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $mType = 'mqb_id';
        }
        $getJmlPriceM = $modelSale->getManagerReport($mType, $dataUser->id, $getDay);
//        $getJmlPriceMType = $modelSale->getManagerReportType($mType, $dataUser->id, $getDay);
//        $getJmlAsmen = $modelSale->getManagerReportDownline($mType, $dataUser->id, $getDay, 'asmen_id');
//        $getJmlTld = $modelSale->getManagerReportDownline($mType, $dataUser->id, $getDay, 'tld_id');
        $getDetail = $modelSale->getReportManagerDetail($mType, $dataUser->id, $getDay);
        return view('admin.manager_area.my_report')
                ->with('headerTitle', 'Detail Penjualan')
                ->with('reportM', $getJmlPriceM)
//                ->with('reportMType', $getJmlPriceMType)
//                ->with('reportAsmen', $getJmlAsmen)
//                ->with('reportTld', $getJmlTld)
                ->with('day', $textDay)
                ->with('detail', $getDetail)
                ->with('dataUser', $dataUser);
    }
    
    public function getListUpgradeCrew(){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $type = 2;
        $modelMember = New Member;
        $arraySpId = array(13);
        $mySponsor = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $mySponsor = '['.$dataUser->id.']';
        }
        $getAllDownline = $modelMember->getNewAllDownline($dataUser->cabang_id, $mySponsor, $arraySpId);
        return view('admin.manager_area.list_crew')
                ->with('headerTitle', 'Upgrade Crew')
                ->with('allDownline', $getAllDownline)
                ->with('type', $type)
                ->with('dataUser', $dataUser);
    }
    
    public function getUpgradeCrew($id){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getUserId = $modelMember->getUserId($id);
        return view('admin.ajax.upgrade_crew')
                ->with('headerTitle', 'Upgrade Crew')
                ->with('dataUser', $dataUser)
                ->with('dataUserId', $getUserId);
    }
    
    public function postUpgradeCrew($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        
        if($id != $request->cekId){
            return redirect()->route('view_crew')
                ->with('message', 'data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $retrainer = 13;
        $md = 12;
        $modelMember = New Member;
        $getUserId = $modelMember->getUserId($id);
        $dataHistory = array(
             'user_id' => $id,
             'old_level_id' => $retrainer,
             'new_level_id' => $md,
             'created_by' => $dataUser->id,
             'active_at' => $getUserId->active_at
        );
        $getInsertHistory = $modelMember->getInsertHistoryLevel($dataHistory);
        if($getInsertHistory->status == false){
            return redirect()->route('view_crew')
                ->with('message', 'terjadi kesalahan pada sistem')
                ->with('messageclass', 'danger');
        }
        $data = array('level_id' => $md);
        $getUpdate = $modelMember->getUpdateMember('id', $id, $data);
        if($getUpdate->status == false){
            return redirect()->route('view_crew')
                ->with('message', 'terjadi kesalahan pada sistem')
                ->with('messageclass', 'danger');
        }
        return redirect()->route('view_crew')
                ->with('message', 'Upgrade crew berhasil')
                ->with('messageclass', 'success');
    }
    
    public function getMyGlobal(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = 'manager_id';
        $type = 'manager';
        $typeText = 'Manager';
        if($dataUser->level_id == 2){
            $typeM = 'gm_id';
            $type = 'gm';
            $typeText = 'General Manager';
        }
        if($dataUser->level_id == 3){
            $typeM = 'sem_id';
            $type = 'sem';
            $typeText = 'Senior Executive Manager';
        }
        if($dataUser->level_id == 4){
            $typeM = 'em_id';
            $type = 'em';
            $typeText = 'Executive Manager';
        }
        if($dataUser->level_id == 5){
            $typeM = 'sm_id';
            $type = 'sm';
            $typeText = 'Senior Manager';
        }
        if($dataUser->level_id == 6){
            $typeM = 'mqb_id';
            $type = 'mqb';
            $typeText = 'MQB';
        }
        $url = '/m/global-manager/'.$type;
        $getData = $modelSale->getGlobalMyManager($typeM, $getMonth, $dataUser);
        return view('admin.manager_area.global_mymanager')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Sales '.$typeText)
                ->with('getData', $getData)
                ->with('url', $url)
                ->with('dataUser', $dataUser);
    }
    
    public function getMyGlobalManager($type, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = $type.'_id';
        $typeText = 'Manager';
        if($type == 'gm'){
            $typeText = 'General Manager';
        }
        if($type == 'sem'){
            $typeText = 'Senior Executive Manager';
        }
        if($type == 'em'){
            $typeText = 'Executive Manager';
        }
        if($type == 'sm'){
            $typeText = 'Senior Manager';
        }
        if($type == 'mqb'){
            $typeText = 'MQB';
        }
        $url = '/m/global-manager/'.$type;
        $getData = $modelSale->getGlobalMyManager($typeM, $getMonth, $dataUser);
        return view('admin.manager_area.global_mymanager')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Sales '.$typeText)
                ->with('getData', $getData)
                ->with('url', $url)
                ->with('dataUser', $dataUser);
    }
    
    public function getMyGlobalAsmen(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = 'asmen_id';
        $getData = $modelSale->getGlobalMyManager($typeM, $getMonth, $dataUser);
        return view('admin.manager_area.global_myasmen')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Sales Asmen')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getMyGlobalTld(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = 'tld_id';
        $getData = $modelSale->getGlobalMyManager($typeM, $getMonth, $dataUser);
        return view('admin.manager_area.global_mytld')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Sales Top Leader')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getMyGlobalCrew($type, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = $type.'_id';
        $typeText = 'Retrainer';
        if($type == 'ld'){
            $typeText = 'Leader';
        }
        if($type == 'tr'){
            $typeText = 'Trainer';
        }
        if($type == 'md'){
            $typeText = 'Merchandiser';
        }
        $url = '/m/global-crew/'.$type;
        $getData = $modelSale->getGlobalMyManager($typeM, $getMonth, $dataUser);
        return view('admin.manager_area.global_mycrew')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Sales '.$typeText)
                ->with('getData', $getData)
                ->with('url', $url)
                ->with('dataUser', $dataUser);
    }
    
    public function getMyGlobalStock($type, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = $type.'_id';
        if($type == 'manager'){
            if($dataUser->level_id == 2){
                $typeM = 'gm_id';
                $typeText = 'General Manager';
            }
            if($dataUser->level_id == 3){
                $typeM = 'sem_id';
                $typeText = 'Senior Executive Manager';
            }
            if($dataUser->level_id == 4){
                $typeM = 'em_id';
                $typeText = 'Executive Manager';
            }
            if($dataUser->level_id == 5){
                $typeM = 'sm_id';
                $typeText = 'Senior Manager';
            }
            if($dataUser->level_id == 6){
                $typeM = 'mqb_id';
                $typeText = 'MQB';
            }
            if($dataUser->level_id == 7){
                $typeM = 'manager_id';
                $typeText = 'Manager';
            }
        }
        if($type == 'asmen'){
            $typeText = 'Leader';
        }
        if($type == 'tld'){
            $typeText = 'Top Leader';
        }
        if($type == 'ld'){
            $typeText = 'Leader';
        }
        if($type == 'tr'){
            $typeText = 'Trainer';
        }
        if($type == 'md'){
            $typeText = 'Merchandiser';
        }
        if($type == 'rt'){
            $typeText = 'Retrainer';
        }
        $url = '/m/global-stock/'.$type;
        $getData = $modelSale->getGlobalStockMyManager($typeM, $getMonth, $dataUser);
        return view('admin.manager_area.global_stock')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Stock '.$typeText)
                ->with('getData', $getData)
                ->with('url', $url)
                ->with('dataUser', $dataUser);
    }
    
    public function getMReportCrew(){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $nextSunday = strtotime( "next sunday" );
        $getNextSunday = date('d-M-Y', $nextSunday);
        $getDateSunday = date('Y-m-d', $nextSunday);
        $dataSunday = (object) array(
            'nextSunday' => $getNextSunday,
            'dateSunday' => $getDateSunday
        );
        return view('admin.manager_area.m_report_crew_create')
                ->with('headerTitle', 'Filter Report Crew')
                ->with('sunday', $dataSunday)
                ->with('dataUser', $dataUser);
    }
    
    public function postMReportCrew(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $nextSunday = strtotime( "next sunday" );
        $getNextSunday = date('d-M-Y', $nextSunday);
        $getDateSunday = date('Y-m-d', $nextSunday);
        $dataSunday = (object) array(
            'nextSunday' => $getNextSunday,
            'dateSunday' => $getDateSunday
        );
        $startDay = date('Y-m-d', strtotime('-6 day', strtotime( $getDateSunday))); //date('Y-m-d', strtotime('-6 day', strtotime( $request->end_date)));
        $modelSale = New Sale;
        $getWeek = (object) array(
            'startDay' => $startDay,
            'endDay' => $getDateSunday, 
        );
        $textDay =  'Data '.date('d M Y', strtotime($startDay)).' - '.date('d M Y', strtotime($getDateSunday));
        $userType = 10;
        $getSales = $modelSale->getDataSalesReportCrewForManager($getWeek, $dataUser->cabang_id, $userType, $dataUser);
        return view('admin.manager_area.m_report_crew')
                ->with('day', $textDay)
                ->with('headerTitle', 'Production Chart ')
                ->with('headerTitle2', $dataUser->cabang_name)
                ->with('allSales', $getSales)
                ->with('sunday', $dataSunday)
                ->with('dataUser', $dataUser);
    }
    
    public function getMStructure($id){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $arraySpId = array(8, 9, 10, 11, 12, 13);
        $getUserId = $modelMember->getUserId($id);
        $mySponsor = $getUserId->sponsor_id.',['.$getUserId->id.']';
        if($getUserId->sponsor_id == null){
            $mySponsor = '['.$getUserId->id.']';
        }
        $getAllDownline = $modelMember->getNewestAllDownline($getUserId->cabang_id, $mySponsor, $arraySpId);
        return view('admin.manager_area.struktur_crew')
                ->with('headerTitle', 'Data Crew')
                ->with('allDownline', $getAllDownline)
                ->with('getUserId', $getUserId)
                ->with('dataUser', $dataUser);
    }
    
    public function getTree(Request $request){
        $dataUser = Auth::user();
        $sessionUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $back = false;
        if ($request->get_id != null) {
            if ($request->get_id < $sessionUser->id) {
                return redirect()->route('adminDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
            }
            if ($request->get_id != $dataUser->id) {
                $back = true;
                $dataUser = $modelMember->getUserId($request->get_id);
            }
        }
        if ($dataUser == null) {
            return redirect()->route('m_mySponsorTree');
        }
        $getBinary = $modelMember->getAllMyTree($dataUser->id);
//        dd($getBinary);
        return view('admin.manager_area.tree')
                ->with('headerTitle', 'Data Struktur Tree')
                ->with('getData', $getBinary)
                ->with('back', $back)
                ->with('dataUser', $dataUser)
                ->with('sessionUser', $sessionUser);
        
        
//        $dataUser = Auth::user();
//        $sessionUser = Auth::user();
//        $onlyUser = array(10);
//        if (!in_array($dataUser->user_type, $onlyUser)) {
//            return redirect()->route('mainDashboard')
//                    ->with('message', 'forbiden area')
//                    ->with('messageclass', 'error');
//        }
//        if ($dataUser->is_active == 0) {
//            return redirect()->route('m_newCoin')
//                    ->with('message', 'akun anda belum aktif, silakan beli coin')
//                    ->with('messageclass', 'error');
//        }
//        $modelMember = New Member;
//        $back = false;
//        if ($request->get_id != null) {
//            if ($request->get_id < $sessionUser->id) {
//                return redirect()->route('mainDashboard')
//                    ->with('message', 'forbiden area')
//                    ->with('messageclass', 'error');
//            }
//            if ($request->get_id != $dataUser->id) {
//                $back = true;
//                $dataUser = $modelMember->getUsers('id', $request->get_id);
//            }
//        }
//        if ($dataUser == null) {
//            return redirect()->route('m_mySponsorTree');
//        }
//        $getBinary = $modelMember->getStructureSponsor($dataUser);
//        return view('member.networking.sponsor')
//                        ->with('getData', $getBinary)
//                        ->with('back', $back)
//                        ->with('dataUser', $dataUser)
//                        ->with('sessionUser', $sessionUser);
        
        
    }
    
    public function getManagerDetailSales(){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $getData = null;
        return view('admin.manager_area.detail_sales')
                ->with('headerTitle', 'Detail Penjualan')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postManagerDetailSales(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->start_date == null || $request->end_date == null){
            return redirect()->route('manager_detail_sales')
                    ->with('message', 'Anda Tidak Memilih Tanggal')
                    ->with('messageclass', 'danger');
        }
        $modelSale = New Sale;
        $getDate = (object) array(
            'startDay' => $request->start_date,
            'endDay' => $request->end_date
        );
        $textDay =  'Data '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        $getData = $modelSale->getGlobalMyManagerNewDetailSales($getDate, $dataUser);
        return view('admin.manager_area.detail_sales')
                ->with('day', $textDay)
                ->with('headerTitle', 'Detail Sales Manager '.$dataUser->name)
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getOrderBarang(){
        $dataUser = Auth::user();
        $onlyUser  = array(6);
        if(!in_array($dataUser->level_id, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if(!in_array(date('w'), array(1, 2, 3, 4, 5, 6))){
            return redirect()->route('history_stock')
                    ->with('message', 'Order Stock tidak bisa dilakukan pada hari minggu')
                    ->with('messageclass', 'danger');
        }
        $modelPurchase = New Purchase;
        $newAllPurchase = null;
        $allPurchase2 = $modelPurchase->getManagerAllInputStock($newAllPurchase);
        $modelCabang = New Cabang;
        $getCabang = $modelCabang->getCheckCabang('id', $dataUser->cabang_id);
        return view('admin.manager_area.order_purchase')
                ->with('headerTitle', 'Order Barang')
                ->with('purchseKosong', $allPurchase2)
                ->with('getCabang', $getCabang)
                ->with('dataUser', $dataUser);
    }
    
    public function getListOrderStock(){
        $dataUser = Auth::user();
        $onlyUser  = array(6);
        if(!in_array($dataUser->level_id, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelOrder = New Order;
        $getData = $modelOrder->getListOrder($dataUser->id);
        return view('admin.manager_area.list_order_purchase')
                ->with('headerTitle', 'List Order Barang')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postOrderInputStock(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(6);
        if(!in_array($dataUser->level_id, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelPurchase = New Purchase;
        $modelCabang = New Cabang;
        $modelOrder = New Order;
        $getCabang = $modelCabang->getCheckCabang('id', $dataUser->cabang_id);
        $dataInsertOrder = array(
            'manager_id' => $dataUser->id,
            'status' => 1,
            'is_active' => 1,
            'order_at' => date('Y-m-d H:i:s')
        );
        $getInsertOrder = $modelOrder->getInsertOrder($dataInsertOrder);
        $lastIdOrder = $getInsertOrder->lastID;
        foreach($request->purchase_id as $row){
            $purchase_id = $row;
            $getPurchaseId = $modelPurchase->getManagerPurchaseIdAddStock($purchase_id);
            $type = 'Obat';
            $price = round($getPurchaseId->main_price + $getCabang->extra);
            if($getPurchaseId->type == 1){
                $type = 'Alat';
                $price = round($getPurchaseId->main_price + $getCabang->extra_alat);
            }
            if($getPurchaseId->type == 2){
                $type = 'Strip';
                $price = round($getPurchaseId->main_price + $getCabang->extra_strip);
            }
            $dataInsertOrderItem = array(
                'order_id' => $lastIdOrder,
                'purchase_id' => $purchase_id,
                'qty' => 0,
                'price' => $price
            );
            $getInsertOrder = $modelOrder->getInsertOrderItem($dataInsertOrderItem);
        }
        return redirect()->route('list_order_barang')
                    ->with('message', 'Berhasil order barang')
                    ->with('messageclass', 'success');
    }
    
    public function getMQBOrderBarangId($order_id){
        $dataUser = Auth::user();
        $onlyUser  = array(6);
        if(!in_array($dataUser->level_id, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelOrder = New Order;
        $getData = $modelOrder->getMQBListOrderItemId($dataUser->id, $order_id);
        if(count($getData) == 0){
            return redirect()->route('list_order_barang');
        }
        return view('admin.manager_area.list_detail_order')
                ->with('headerTitle', 'List Detail Order')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postTerimaBarang(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(6);
        if(!in_array($dataUser->level_id, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $order_id = $request->order_id;
        $modelOrder = New Order;
        $getOrderData = $modelOrder->getMQBOrderId($order_id, $dataUser->id);
        if($getOrderData == null){
            return redirect()->route('order_barang')
                    ->with('message', 'data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        if($getOrderData->status == 1){
            return redirect()->route('mqb_order_barang_id', array($order_id))
                    ->with('message', 'masih proses admin')
                    ->with('messageclass', 'danger');
        }
        if($getOrderData->status == 3){
            return redirect()->route('mqb_order_barang_id', array($order_id))
                    ->with('message', 'barang sudah diterima')
                    ->with('messageclass', 'danger');
        }
        $dataStatus = array(
            'status' => 3,
            'terima_at' => date('Y-m-d H:i:s')
        );
        $getOrderItem = $modelOrder->getMQBListOrderItemId($dataUser->id, $order_id);
        $modelOrder->getUpdateOrder($order_id, $dataStatus);
        $modelItemPurchase = New Itempurchase;
        $modelStock = New Stock;
        $thisSunday = date('Y-m-d 12:i:s', strtotime( "last sunday", strtotime($getOrderData->order_at) ));
        foreach($getOrderItem as $row){
            $purchase_id = $row->purchase_id;
            $price = $row->price;
            $cekItem = $modelItemPurchase->getCheckItemAddStock($dataUser, $purchase_id);
            if($cekItem == null){
                $dataInsert = array(
                    'purchase_id' => $purchase_id,
                    'cabang_id' => $dataUser->cabang_id,
                    'manager_id' => $dataUser->id,
                    'qty' => $row->qty,
                    'sisa' => $row->qty,
                    'price' => $price
                );
                $getInsertItem = $modelItemPurchase->getInsertItem($dataInsert);
                $getIdItem = $getInsertItem->lastID;
            } else {
                $getIdItem = $cekItem->id;
            }
            $dataInsertStock = array(
                'item_purchase_id' => $getIdItem,
                'type_stock' => 1,
                'user_id' => $dataUser->id,
                'amount' => $row->qty,
                'created_at' => $thisSunday
            );
            $modelStock->getInsertStock($dataInsertStock);
            $getSumStock = $modelStock->getGetSumStock($getIdItem);
            $getQty = $getSumStock->amount_tambah;
            $getSisa = $getSumStock->amount_tambah - $getSumStock->amount_kurang;
            $dataUpdateItem = array(
                'qty' => $getQty, 
                'sisa' => $getSisa,
                'price' => $price
            );
            $modelItemPurchase->getUpdateItem($getIdItem, $dataUpdateItem);
        }
        return redirect()->route('mqb_order_barang_id', array($order_id))
                    ->with('message', 'order barang diterima, dan masuk ke stock')
                    ->with('messageclass', 'success');
    }
    
    public function getMQBConfirmBarang($order_id, $item_id){
        $dataUser = Auth::user();
        $onlyUser  = array(6);
        if(!in_array($dataUser->level_id, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelOrder = New Order;
        $getOrderItem = $modelOrder->getMQBListOrderItemIdNew($dataUser->id, $order_id, $item_id);
        return view('admin.ajax.confirm_item_barang')
                ->with('headerTitle', 'Confirm Order Barang')
                ->with('getData', $getOrderItem)
                ->with('dataUser', $dataUser);
    }
    
    public function postTerimaBarangPerItem($order_id, $item_id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(6);
        if(!in_array($dataUser->level_id, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelOrder = New Order;
        $getOrderItem = $modelOrder->getMQBListOrderItemIdNew($dataUser->id, $request->cekIdOrder, $request->cekIdOrderItem);
        $modelItemPurchase = New Itempurchase;
        $modelStock = New Stock;
        $thisSunday = date('Y-m-d 12:i:s', strtotime( "last sunday", strtotime(date('Y-m-d H:i:s'))));
        $dataStatus = array(
            'terima_at' => date('Y-m-d H:i:s')
        );
        $modelOrder->getUpdateOrderItem($item_id, $dataStatus);
        $purchase_id = $getOrderItem->purchase_id;
        $price = $getOrderItem->price;
        $cekItem = $modelItemPurchase->getCheckItemAddStock($dataUser, $purchase_id);
        if($cekItem == null){
            $dataInsert = array(
                'purchase_id' => $purchase_id,
                'cabang_id' => $dataUser->cabang_id,
                'manager_id' => $dataUser->id,
                'qty' => $getOrderItem->qty,
                'sisa' => $getOrderItem->qty,
                'price' => $price
            );
            $getInsertItem = $modelItemPurchase->getInsertItem($dataInsert);
            $getIdItem = $getInsertItem->lastID;
        } else {
            $getIdItem = $cekItem->id;
        }
        $dataInsertStock = array(
            'item_purchase_id' => $getIdItem,
            'type_stock' => 1,
            'user_id' => $dataUser->id,
            'amount' => $getOrderItem->qty,
            'created_at' => $thisSunday
        );
        $modelStock->getInsertStock($dataInsertStock);
        $getSumStock = $modelStock->getGetSumStock($getIdItem);
        $getQty = $getSumStock->amount_tambah;
        $getSisa = $getSumStock->amount_tambah - $getSumStock->amount_kurang;
        $dataUpdateItem = array(
            'qty' => $getQty, 
            'sisa' => $getSisa,
            'price' => $price
        );
        $modelItemPurchase->getUpdateItem($getIdItem, $dataUpdateItem);
        return redirect()->route('mqb_order_barang_id', array($order_id))
                    ->with('message', 'order barang diterima, dan sudah masuk ke stock')
                    ->with('messageclass', 'success');
    }
    
    
    //////
    //Absen
    public function getManagerListAbsen(){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $cekDownline = $modelMember->getAbsen($dataUser);
        return view('admin.manager_area.list_absen')
                ->with('headerTitle', 'List Absen Bulan '.date('M'))
                ->with('allDownline', $cekDownline)
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerAbsenDetail($user_id){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $cekUsersAbsen = $modelMember->getAbsenDetail($user_id);
        return view('admin.manager_area.detail_absen')
                ->with('headerTitle', 'Detail Absen Bulan '.date('M'))
                ->with('geData', $cekUsersAbsen)
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerAbsenCreate(){
        return redirect()->route('input_penjualan');
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $arraySpId = array(8, 9, 10, 11, 12);
        $mySponsor = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $mySponsor = '['.$dataUser->id.']';
        }
        $cekDownline = $modelMember->getAllDownlineAbsen($dataUser->cabang_id, $mySponsor, $arraySpId);
        return view('admin.manager_area.absen_create')
                ->with('headerTitle', 'Create Absen')
                ->with('crewDownline', $cekDownline)
                ->with('dataUser', $dataUser);
    }
    
    public function postManagerAbsen(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserId($request->id);
        $spId = $getIdMember->sponsor_id.',['.$getIdMember->id.']';
        if($getIdMember->sponsor_id == null){
            $spId = '['.$getIdMember->id.']';
        }
        $arraySpId = explode(',', $spId);
        $dataSp = array();
        foreach($arraySpId as $rowSp){
            $rm1 = str_replace('[', '', $rowSp);
            $rm2 = str_replace(']', '', $rm1);
            $int = (int) $rm2;
            $dataSp[] = $int;
        }
        $dataAllSp = $modelMember->getUpperUserId($dataSp);
        $get_gm = $modelMember->getLevelLoop($dataAllSp, 2);
        $get_sem = $modelMember->getLevelLoop($dataAllSp, 3);
        $get_em = $modelMember->getLevelLoop($dataAllSp, 4);
        $get_sm = $modelMember->getLevelLoop($dataAllSp, 5);
        $get_mqb = $modelMember->getLevelLoop($dataAllSp, 6);
        $get_m = $modelMember->getLevelLoop($dataAllSp, 7);
        $get_asmen = $modelMember->getLevelLoop($dataAllSp, 8);
        $get_tld = $modelMember->getLevelLoop($dataAllSp, 9);
        $get_ld = $modelMember->getLevelLoop($dataAllSp, 10);
        $get_tr = $modelMember->getLevelLoop($dataAllSp, 11);
        $get_md = $modelMember->getLevelLoop($dataAllSp, 12);
        $get_rt = $modelMember->getLevelLoop($dataAllSp, 13);
        $masuk_at = date('Y-m-d').' '.$request->absen_date;;
        if($request->absen_date == null){
            $masuk_at = date('Y-m-d').' 00:00:00';
        }
        $dataInsertAbsen = array(
            'user_id' => $request->id,
            'cabang_id' => $getIdMember->cabang_id,
            'is_masuk' => $request->is_masuk,
            'gm_id' => $get_gm,
            'sem_id' => $get_sem,
            'em_id' => $get_em,
            'sm_id' => $get_sm,
            'mqb_id' => $get_mqb,
            'manager_id' => $get_m,
            'asmen_id' => $get_asmen,
            'tld_id' => $get_tld,
            'ld_id' => $get_ld,
            'tr_id' => $get_tr,
            'md_id' => $get_md,
            'rt_id' => $get_rt,
            'masuk_at' => $masuk_at,
            'keterangan' => $request->keterangan
        );
        $modelMember->getInsertAbsen($dataInsertAbsen);
        return redirect()->route('create_absen')
                    ->with('message', 'Input absen '.$getIdMember->name.' Berhasil')
                    ->with('messageclass', 'success');
        
    }
    
    //Pengeluaran
    
    public function getManagerCreatePengeluaran(){
       $dataUser = Auth::user();
       //Hanya Manager sj
        if($dataUser->level_id != 7){
            return redirect()->route('adminDashboard');
        }
//        $onlyUser  = array(5);
//        if(!in_array($dataUser->user_type, $onlyUser)){
//            return redirect()->route('adminDashboard');
//        } 
        $modelTransfer = New Transfer;
        $getCategory = $modelTransfer->getAllCategoryPengeluaran();
        return view('admin.manager_area.input_pengeluaran')
                ->with('headerTitle', 'Input Pengeluaran')
                ->with('kategori', $getCategory)
                ->with('dataUser', $dataUser);
    }
    
    public function postManagerPengeluaran(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $modelMember = New Member;
        $spId = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $spId = '['.$dataUser->id.']';
        }
        $arraySpId = explode(',', $spId);
        $dataSp = array();
        foreach($arraySpId as $rowSp){
            $rm1 = str_replace('[', '', $rowSp);
            $rm2 = str_replace(']', '', $rm1);
            $int = (int) $rm2;
            $dataSp[] = $int;
        }
        $dataAllSp = $modelMember->getUpperUserId($dataSp);
        $get_gm = $modelMember->getLevelLoop($dataAllSp, 2);
        $get_sem = $modelMember->getLevelLoop($dataAllSp, 3);
        $get_em = $modelMember->getLevelLoop($dataAllSp, 4);
        $get_sm = $modelMember->getLevelLoop($dataAllSp, 5);
        $get_mqb = $modelMember->getLevelLoop($dataAllSp, 6);
        $get_m = $dataUser->id;
        $count = 0;
        if($request->category_id != null){
            $count = count($request->category_id);
        }
        if($count > 0){
            $getInvo = 'OUT'.date('Ymd').'_'.uniqid();
            for ($x = 0; $x < $count; $x++) {
                $getCat = $modelTransfer->getCategoryPengeluaranId($request->category_id[$x]);
                if($getCat != null){
                    $dataInsertPengeluaran = array(
                        'category_id' => $request->category_id[$x],
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'invoice' => $getInvo,
                        'pengeluaran_price' => $request->amount[$x],
                        'pengeluaran_date' => $request->out_date,
                        'keterangan' => $request->ket[$x]
                    );
                    $modelTransfer->getInsertPengeluaran($dataInsertPengeluaran);
                }
            }
        }
        $getData = $modelTransfer->getTotalSalesAndOut($dataUser, $request->out_date);
        $cekTransfer = $modelTransfer->getCheckTransfer($dataUser, $request->out_date);
        $transferPrice = $getData->sales - $getData->pengeluaran;
        if($cekTransfer == null){
            $getInvo = $modelTransfer->getTransferInvoice($request->out_date);
            $dataInsertTransfer = array(
                'transfer_invoice' => $getInvo,
                'transfer_price' => $transferPrice,
                'sales_price' => $getData->sales,
                'pengeluaran_price' => $getData->pengeluaran,
                'transfer_date' => $request->out_date,
                'gm_id' => $get_gm,
                'sem_id' => $get_sem,
                'em_id' => $get_em,
                'sm_id' => $get_sm,
                'mqb_id' => $get_mqb,
                'manager_id' => $get_m,
            );
            $modelTransfer->getInsertTransfer($dataInsertTransfer);
        } else {
            $dataUpdateTransfer = array(
                'transfer_price' => $transferPrice,
                'sales_price' => $getData->sales,
                'pengeluaran_price' => $getData->pengeluaran,
            );
            $modelTransfer->getUpdateTransfer($cekTransfer->id, $dataUpdateTransfer);
        }
        return redirect()->route('create_pengeluaran')
                    ->with('message', 'Input Pengeluaran  Berhasil')
                    ->with('messageclass', 'success');
    }
    
    //Transfer
    
    public function getManagerListTransfer(){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getAllTransferManager($dataUser);
        return view('admin.manager_area.list_transfer')
                ->with('headerTitle', 'Data Transfer')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerTransferById($id, $type){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getTransferManagerId($id, $dataUser);
        $getDetailData = $modelTransfer->getTotalSalesAndOutAdmin($getData->transfer_date, $dataUser->id);
        $text = 'Detail Transfer';
        $view = false;
        if($type == 5){
            $text = 'Kirim Ulang Transfer';
            $view = true;
        }
        return view('admin.manager_area.detail_transfer')
                ->with('headerTitle', $text)
                ->with('view', $view)
                ->with('getData', $getData)
                ->with('getDetailData', $getDetailData)
                ->with('dataUser', $dataUser);
    }
    
    public function postManagerConfirmTransfer(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $id = $request->cekId;
        $type = $request->type;
        if($type != 5){
            return redirect()->route('m_transfer')
                    ->with('message', 'Whoops, Something went wrong...')
                    ->with('messageclass', 'danger');
        }
        $getData = $modelTransfer->getTransferManagerId($id, $dataUser);
        if($getData == null){
            return redirect()->route('m_transfer')
                    ->with('message', 'data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $updateTransfer = array(
            'is_transfer' => 3,
            'reconfirm_at' => date('Y-m-d H:i:s')
        );
        $modelTransfer->getUpdateTransfer($id, $updateTransfer);
        return redirect()->route('m_transfer')
                    ->with('message', 'berhasil, confirm ulang transfer')
                    ->with('messageclass', 'success');
    }
    
    public function getUpperManagerTransfer(){
        $dataUser = Auth::user();
        if($dataUser->level_id >= 7){
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getAllTransferUpperManager($dataUser);
        return view('admin.manager_area.upper_list_transfer')
                ->with('headerTitle', 'Data Transfer Manager')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    
    //MQB Input pengeluaran
    public function getMQBCreatePengeluaran(){
        $dataUser = Auth::user();
        if($dataUser->level_id != 6){ //Hanya MQB sj
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $getCategory = $modelTransfer->getAllCategoryPengeluaran();
        return view('admin.manager_area.mqb_input_pengeluaran')
                ->with('headerTitle', 'Input Pengeluaran MQB')
                ->with('kategori', $getCategory)
                ->with('dataUser', $dataUser);
    }
    
    public function postMQBPengeluaran(Request $request){
        $dataUser = Auth::user();
        if($dataUser->level_id != 6){ //Hanya MQB sj
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $modelMember = New Member;
        $spId = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $spId = '['.$dataUser->id.']';
        }
        $arraySpId = explode(',', $spId);
        $dataSp = array();
        foreach($arraySpId as $rowSp){
            $rm1 = str_replace('[', '', $rowSp);
            $rm2 = str_replace(']', '', $rm1);
            $int = (int) $rm2;
            $dataSp[] = $int;
        }
        $dataAllSp = $modelMember->getUpperUserId($dataSp);
        $get_gm = $modelMember->getLevelLoop($dataAllSp, 2);
        $get_sem = $modelMember->getLevelLoop($dataAllSp, 3);
        $get_em = $modelMember->getLevelLoop($dataAllSp, 4);
        $get_sm = $modelMember->getLevelLoop($dataAllSp, 5);
        $get_mqb = $dataUser->id;
        $count = 0;
        if($request->category_id != null){
            $count = count($request->category_id);
        }
        if($count > 0){
            $getInvo = 'OUT_MQB_'.uniqid();
            for ($x = 0; $x < $count; $x++) {
                $dataInsertPengeluaran = array(
                    'category_id' => $request->category_id[$x],
                    'gm_id' => $get_gm,
                    'sem_id' => $get_sem,
                    'em_id' => $get_em,
                    'sm_id' => $get_sm,
                    'mqb_id' => $get_mqb,
                    'manager_id' => null,
                    'invoice' => $getInvo,
                    'pengeluaran_price' => $request->amount[$x],
                    'pengeluaran_date' => $request->out_date,
                    'keterangan' => $request->ket[$x],
                    'jenis' => 2
                );
                $modelTransfer->getInsertPengeluaran($dataInsertPengeluaran);
            }
        }
        return redirect()->route('mqb_create_pengeluaran')
                    ->with('message', 'MQB Input Pengeluaran  Berhasil')
                    ->with('messageclass', 'success');
    }
    
    public function getMQBListPengeluaran(){
        $dataUser = Auth::user();
        if($dataUser->level_id != 6){ //Hanya MQB sj
            return redirect()->route('adminDashboard');
        }
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getMQBPengeluaran($dataUser);
        return view('admin.manager_area.mqb_list_pengeluaran')
                ->with('headerTitle', 'Data Pengeluaran MQB')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerBonusCrew($id){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelBonus = New Bonus;
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserId($id);
        $getTotalBonusAll = $modelBonus->getTotalAllBonus($id);
        $getDetailBonus = $modelBonus->getDetailAllBonus($id);
        return view('admin.manager_area.bonus_detail_crew')
                ->with('headerTitle', 'Bonus Detail')
                ->with('getCrew', $getIdMember)
                ->with('getBonusTotal', $getTotalBonusAll)
                ->with('getDetailBonus', $getDetailBonus)
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerMyBonus(){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelBonus = New Bonus;
        $getTotalBonusAll = $modelBonus->getTotalAllBonus($dataUser->id);
        $getDetailBonus = $modelBonus->getDetailAllBonus($dataUser->id);
        return view('admin.manager_area.bonus_detail_my')
                ->with('headerTitle', 'Bonus Detail')
                ->with('getCrew', $dataUser)
                ->with('getBonusTotal', $getTotalBonusAll)
                ->with('getDetailBonus', $getDetailBonus)
                ->with('dataUser', $dataUser);
    }
    
    public function getMQBListCustomer(){
        $dataUser = Auth::user();
        if($dataUser->level_id != 6){ //Hanya MQB sj
            return redirect()->route('adminDashboard');
        }
        $getData = null;
        $textDay =  'Data Customer ';
        return view('admin.manager_area.detail_customer')
                ->with('headerTitle', 'Detail Customers')
                ->with('day', $textDay)
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postMQBListCustomer(Request $request){
        $dataUser = Auth::user();
        if($dataUser->level_id != 6){ //Hanya MQB sj
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        if($request->start_date == null || $request->end_date == null){
            return redirect()->route('mqb_list_customer')
                ->with('message', 'tanggal tidak dipilih')
                ->with('messageclass', 'danger');
        }
        $getParameter = (object) array(
            'startDay' => $request->start_date,
            'endDay' => $request->end_date,
        );
        $textDay =  'Data Customer '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        $getData = $modelMember->getListCustomerByDate($dataUser, $getParameter);
        return view('admin.manager_area.detail_customer')
                ->with('headerTitle', 'Detail Customers')
                ->with('day', $textDay)
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getMQBInputCustomer(){
        $dataUser = Auth::user();
        if($dataUser->level_id != 6){ //Hanya MQB sj
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $modelItemPurchase = New Itempurchase;
        $arraySpId = array(8, 9, 10, 11, 12, 13);
        $mySponsor = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $mySponsor = '['.$dataUser->id.']';
        }
        $cekDownline = $modelMember->getNewAllDownline($dataUser->cabang_id, $mySponsor, $arraySpId);
        $getPurchase = $modelItemPurchase->getReportStockOpnameCabang($dataUser->cabang_id);
        return view('admin.manager_area.mqb_input_customer')
                ->with('headerTitle', 'Input Customer')
                ->with('cekDownline', $cekDownline)
                ->with('getPurchase', $getPurchase)
                ->with('dataUser', $dataUser);
    }
    
    public function postMQBInputCustomer(Request $request){
        $dataUser = Auth::user();
        if($dataUser->level_id != 6){ //Hanya MQB sj
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserId($request->crew);
        $getCustomerCode = $modelMember->getCustomerCode();
        $spId = $getIdMember->sponsor_id.',['.$getIdMember->id.']';
        if($getIdMember->sponsor_id == null){
            $spId = '['.$getIdMember->id.']';
        }
        $arraySpId = explode(',', $spId);
        $dataSp = array();
        foreach($arraySpId as $rowSp){
            $rm1 = str_replace('[', '', $rowSp);
            $rm2 = str_replace(']', '', $rm1);
            $int = (int) $rm2;
            $dataSp[] = $int;
        }
        $dataAllSp = $modelMember->getUpperUserId($dataSp);
        $get_gm = $modelMember->getLevelLoop($dataAllSp, 2);
        $get_sem = $modelMember->getLevelLoop($dataAllSp, 3);
        $get_em = $modelMember->getLevelLoop($dataAllSp, 4);
        $get_sm = $modelMember->getLevelLoop($dataAllSp, 5);
        $get_mqb = $modelMember->getLevelLoop($dataAllSp, 6);
        $get_m = $modelMember->getLevelLoop($dataAllSp, 7);
        $get_asmen = $modelMember->getLevelLoop($dataAllSp, 8);
        $get_tld = $modelMember->getLevelLoop($dataAllSp, 9);
        $get_ld = $modelMember->getLevelLoop($dataAllSp, 10);
        $get_tr = $modelMember->getLevelLoop($dataAllSp, 11);
        $get_md = $modelMember->getLevelLoop($dataAllSp, 12);
        $get_rt = $modelMember->getLevelLoop($dataAllSp, 13);
        $dataInsert = array(
            'code_id' => $getCustomerCode,
            'name' => $request->name,
            'address' => $request->address,
            'hp' => $request->phone,
            'email' => $request->email,
            'keluhan' => $request->keluhan,
            'cabang_id' => $dataUser->cabang_id,
            'cabang_name' => $dataUser->cabang_name,
            'user_id' => $request->crew,
            'gm_id' => $get_gm,
            'sem_id' => $get_sem,
            'em_id' => $get_em,
            'sm_id' => $get_sm,
            'mqb_id' => $get_mqb,
            'manager_id' => $get_m,
            'asmen_id' => $get_asmen,
            'tld_id' => $get_tld,
            'ld_id' => $get_ld,
            'tr_id' => $get_tr,
            'md_id' => $get_md,
            'rt_id' => $get_rt,
            'item_purchase_id' => $request->item_purchase_id,
            'sale_date' => $request->sale_date
        );
        $modelMember->getInsertCustomer($dataInsert);
        return redirect()->route('mqb_input_customer')
                ->with('message', 'input customer berhasil')
                ->with('messageclass', 'success');
    }
    
    public function getManagerDownOrganisasi(){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getDownStructure = $modelMember->getDownlineStructureList($dataUser->id);
        $getSponsor = $modelMember->getDownlineSponsorFromCabang($dataUser->id);
        return view('admin.manager_area.down_structure_list')
                ->with('headerTitle', 'Struktur Organisasi')
                ->with('getData', $getDownStructure)
                ->with('getSponsor', $getSponsor)
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerTreeOrganisasi(){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        return view('admin.manager_area.down_structure_tree')
                ->with('headerTitle', 'Data Organisasi '.$dataUser->name)
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerBonusOrganisasi($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelBonus = New Bonus;
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserId($id);
        $getTotalBonusAll = $modelBonus->getTotalAllBonus($id);
        $getDetailBonus = $modelBonus->getDetailAllBonus($id);
        return view('admin.manager_area.bonus_detail_organisasi')
                ->with('headerTitle', 'Bonus Detail')
                ->with('getCrew', $getIdMember)
                ->with('getBonusTotal', $getTotalBonusAll)
                ->with('getDetailBonus', $getDetailBonus)
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerGlobalSalesOrganisasi($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserId($id);
        $modelSale = New Sale;
        $modelBonus = New Bonus;
        $getTotalBonusAll = $modelBonus->getTotalAllBonus($id);
        $sponsor_dia = $getIdMember->sponsor_id.',['.$getIdMember->id.']';
        if($getIdMember->sponsor_id == null){
            $sponsor_dia = '['.$getIdMember->id.']';
        }
        $arraySpId = array(8, 9, 10,11, 12, 13);
        $getCountCrew = $modelMember->getNewCountAllDownline($getIdMember->cabang_id, $sponsor_dia, $arraySpId);
        $getAllSalesMonth = $modelSale->getSalesAllMonthManagerDashboard('user_id', $modelSale->getThisMonth(), $sponsor_dia, $getIdMember);
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data Global Sales '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $typeM = 'manager_id';
        $type = 'manager';
        $typeText = 'Manager';
        if($getIdMember->level_id == 2){
            $typeM = 'gm_id';
            $type = 'gm';
            $typeText = 'General Manager';
        }
        if($getIdMember->level_id == 3){
            $typeM = 'sem_id';
            $type = 'sem';
            $typeText = 'Senior Executive Manager';
        }
        if($getIdMember->level_id == 4){
            $typeM = 'em_id';
            $type = 'em';
            $typeText = 'Executive Manager';
        }
        if($getIdMember->level_id == 5){
            $typeM = 'sm_id';
            $type = 'sm';
            $typeText = 'Senior Manager';
        }
        if($getIdMember->level_id == 6){
            $typeM = 'mqb_id';
            $type = 'mqb';
            $typeText = 'MQB';
        }
        $url = '/m/org/global-sales/'.$getIdMember->id;
        $getData = $modelSale->getGlobalMyManager($typeM, $getMonth, $getIdMember);
        return view('admin.manager_area.global_org')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Sales ')
                ->with('getData', $getData)
                ->with('url', $url)
                ->with('getCrew', $getIdMember)
                ->with('getBonusTotal', $getTotalBonusAll)
                 ->with('getCountCrew', $getCountCrew)
                ->with('getAllSalesMonth', $getAllSalesMonth)
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerGlobalStockOrganisasi($id, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->structure_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserId($id);
        $sponsor_dia = $getIdMember->sponsor_id.',['.$getIdMember->id.']';
        if($getIdMember->sponsor_id == null){
            $sponsor_dia = '['.$getIdMember->id.']';
        }
        $arraySpId = array(8, 9, 10,11, 12, 13);
        $getCountCrew = $modelMember->getNewCountAllDownline($getIdMember->cabang_id, $sponsor_dia, $arraySpId);
        $modelSale = New Sale;
        $modelBonus = New Bonus;
        $getTotalBonusAll = $modelBonus->getTotalAllBonus($id);
        $textDay = 'Data Bulan '.date('F');
        $getMonth = $modelSale->getThisMonth();
        $getAllSalesMonth = $modelSale->getSalesAllMonthManagerDashboard('user_id', $modelSale->getThisMonth(), $sponsor_dia, $getIdMember);
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
            $textDay =  'Data Global Stock '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        if($getIdMember->level_id == 2){
            $typeM = 'gm_id';
            $typeText = 'General Manager';
        }
        if($getIdMember->level_id == 3){
            $typeM = 'sem_id';
            $typeText = 'Senior Executive Manager';
        }
        if($getIdMember->level_id == 4){
            $typeM = 'em_id';
            $typeText = 'Executive Manager';
        }
        if($getIdMember->level_id == 5){
            $typeM = 'sm_id';
            $typeText = 'Senior Manager';
        }
        if($getIdMember->level_id == 6){
            $typeM = 'mqb_id';
            $typeText = 'MQB';
        }
        if($getIdMember->level_id == 7){
            $typeM = 'manager_id';
            $typeText = 'Manager';
        }
        $url = '/m/org/global-stock/'.$getIdMember->id;
        $getData = $modelSale->getGlobalStockMyManager($typeM, $getMonth, $getIdMember);
        return view('admin.manager_area.global_org_stock')
                ->with('day', $textDay)
                ->with('headerTitle', 'Global Stock ')
                ->with('getData', $getData)
                ->with('url', $url)
                ->with('getCrew', $getIdMember)
                ->with('getBonusTotal', $getTotalBonusAll)
                ->with('getCountCrew', $getCountCrew)
                ->with('getAllSalesMonth', $getAllSalesMonth)
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerContent($id){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelContent = New Content;
        $getData = $modelContent->getContentByField('id', $id);
        if($getData == null){
            return redirect()->route('adminDashboard');
        }
        if(!isset($_COOKIE['_content'])){
            $value = $id;
        } else {
            $value = $_COOKIE['_content'].','.$id;
        }
        setcookie('_content', $value, time() + (86400 * 10), "/"); // 86400 = 1 day
        return view('admin.manager_area.content')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerContents(){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelContent = New Content;
        $getAllContent = $modelContent->getAllContent();
        return view('admin.manager_area.content_all')
                ->with('getData', $getAllContent)
                ->with('headerTitle', 'Berita')
                ->with('dataUser', $dataUser);
    }
    
    public function getManagerListBonus(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelBonus = New Bonus;
        $textDay = 'Data Bonus Manager ';
        $getData = null;
        $getRequest = (object) array(
            'startDay' => null,
            'endDay' => null,
            'id' => null,
        );
        if($request->start_date != null && $request->end_date != null){
            $getRequest = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date,
                'id' => $dataUser->id,
            );
            $textDay =  'Data Bonus '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
            $getData = $modelBonus->getBonusMangerId($getRequest);
        }
        return view('admin.manager_area.manager_bonus')
                ->with('headerTitle', $textDay)
                ->with('allData', $getData)
                ->with('getRequest', $getRequest)
                ->with('dataUser', $dataUser);
    }
    
    public function getInputPenjualanSafra(){
        return redirect()->route('adminDashboard');
        $dataUser = Auth::user();
        if($dataUser->level_id != 7){
            return redirect()->route('adminDashboard');
        }
        $modelMember = New Member;
        $arraySpId = array(8, 9, 10, 11, 12, 13);
        $mySponsor = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $mySponsor = '['.$dataUser->id.']';
        }
        $thisSunday = date('Y-m-d', strtotime( "last sunday" ));
        $cekDownline = $modelMember->getNewAllDownline($dataUser->cabang_id, $mySponsor, $arraySpId);
        $modelPurchase = New Purchase;
        $allBarang = $modelPurchase->getManagerStockReadySafra($dataUser, $thisSunday);
        return view('admin.manager_area.input_penjualan_safra')
                ->with('headerTitle', 'Input Penjualan Safra')
                ->with('cekDownline', $cekDownline)
                ->with('allBarang', $allBarang)
                ->with('dataUser', $dataUser);
    }
    
    public function postInputPenjualanSafra(Request $request){
        return redirect()->route('adminDashboard');
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        if($request->bank_transfer == '0'){
            return redirect()->route('input_penjualan_safra')
                    ->with('message', 'Bank tidak dipilih')
                    ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $modelSale = New Sale;
        $modelItemPurchase = New Itempurchase;
        $modelStock = New Stock;
//        $modelBonus = New Bonus;
        $getInvo = 'SAFRA'.date('Ymd').'_'.uniqid();
        $user_id = $dataUser->id;
        $spId = $dataUser->sponsor_id.',['.$user_id.']';
        if($dataUser->sponsor_id == null){
            $spId = '['.$user_id.']';
        }
        $getLevel = $dataUser->level_id;
        if($request->crew != $dataUser->id){
            $user_id = $request->crew;
            $getIdMember = $modelMember->getUserId($user_id);
            $spId = $getIdMember->sponsor_id.',['.$getIdMember->id.']';
            if($getIdMember->sponsor_id == null){
                $spId = '['.$getIdMember->id.']';
            }
            $getLevel = $getIdMember->level_id;
        }
//        $getBonusSetting = $modelBonus->getSettingBonus();
        $arraySpId = explode(',', $spId);
        $dataSp = array();
        foreach($arraySpId as $rowSp){
            $rm1 = str_replace('[', '', $rowSp);
            $rm2 = str_replace(']', '', $rm1);
            $int = (int) $rm2;
            $dataSp[] = $int;
        }
        
        $dataAllSp = $modelMember->getUpperUserId($dataSp);
        $get_gm = $modelMember->getLevelLoop($dataAllSp, 2);
        $get_sem = $modelMember->getLevelLoop($dataAllSp, 3);
        $get_em = $modelMember->getLevelLoop($dataAllSp, 4);
        $get_sm = $modelMember->getLevelLoop($dataAllSp, 5);
        $get_mqb = $modelMember->getLevelLoop($dataAllSp, 6);
        $get_m = $modelMember->getLevelLoop($dataAllSp, 7);
        $get_asmen = $modelMember->getLevelLoop($dataAllSp, 8);
        $get_tld = $modelMember->getLevelLoop($dataAllSp, 9);
        $get_ld = $modelMember->getLevelLoop($dataAllSp, 10);
        $get_tr = $modelMember->getLevelLoop($dataAllSp, 11);
        $get_md = $modelMember->getLevelLoop($dataAllSp, 12);
        $get_rt = $modelMember->getLevelLoop($dataAllSp, 13);
        $is_sales = 0;
        if($request->checkSales == '1'){
            $is_sales = 1;
            $count = 0;
            if($request->item_purchase != null){
                $count = count($request->item_purchase);
            }
            $bank_transfer = $request->bank_transfer;
            $data_bank = explode('__', $bank_transfer);
            $dataAll = array();
            if($count > 0){
                for ($x = 0; $x < $count; $x++) {
                    $dataAll[] = (object) array(
                        'crew' => $request->crew,
                        'sale_date' => $request->sale_date,
                        'item_purchase' => $request->item_purchase[$x],
                        'amount' => $request->amount[$x],
                        'bank_name' => $data_bank[0],
                        'account_no' => $data_bank[1],
                        'account_name' => $data_bank[2],
                    );
                }
                foreach ($dataAll as $rowAll){
                    $getItemId = $modelItemPurchase->getPurchaseFromItem($rowAll->item_purchase);
                    if($getItemId != null){
                        $dataInsertSale = array(
                            'item_purchase_id' => $rowAll->item_purchase,
                            'user_id' => $user_id,
                            'gm_id' => $get_gm,
                            'sem_id' => $get_sem,
                            'em_id' => $get_em,
                            'sm_id' => $get_sm,
                            'mqb_id' => $get_mqb,
                            'manager_id' => $get_m,
                            'asmen_id' => $get_asmen,
                            'tld_id' => $get_tld,
                            'ld_id' => $get_ld,
                            'tr_id' => $get_tr,
                            'md_id' => $get_md,
                            'rt_id' => $get_rt,
                            'invoice' => $getInvo,
                            'amount' => $rowAll->amount,
                            'sale_price' => round($rowAll->amount * $getItemId->price),
                            'sale_date' => $rowAll->sale_date,
                            'bank_name' => $rowAll->bank_name,
                            'account_no' => $rowAll->account_no,
                            'account_name' => $rowAll->account_name,
                            'is_safra' => 1
                        );
                        $insertSale = $modelSale->getInsertSale($dataInsertSale);
                        $lastIdSales = $insertSale->lastID;
                        $dataInsertStock = array(
                            'item_purchase_id' => $rowAll->item_purchase,
                            'type_stock' => 2,
                            'user_id' => $user_id,
                            'amount' => $rowAll->amount,
                            'sales_id' => $lastIdSales,
                            'created_at' => $rowAll->sale_date.' 12:00:00'
                        );
                        $modelStock->getInsertStock($dataInsertStock);
                        $getSumStock = $modelStock->getGetSumStock($rowAll->item_purchase);
                        $getQty = $getSumStock->amount_tambah;
                        $getSisa = $getSumStock->amount_tambah - $getSumStock->amount_kurang;
                        $dataUpdateItem = array(
                            'qty' => $getQty, 
                            'sisa' => $getSisa
                        );
                        $modelItemPurchase->getUpdateItem($rowAll->item_purchase, $dataUpdateItem);
//                        if($getBonusSetting != null){
//                            $modelBonus->getBonusData($getItemId, $getBonusSetting, $user_id, $rowAll, $getInvo, $get_gm, $get_sem, $get_em, $get_sm, $get_mqb, $get_m, $get_asmen, $get_tld, $get_ld, $get_tr, $get_md, $get_rt);
//                        }
                    }
                }
            }
        }
        
        return redirect()->route('input_penjualan_safra')
                ->with('message', 'input penjualan barang safra berhasil')
                ->with('messageclass', 'success');
    }
    
    public function getDataPenjualanSafra(Request $request){
        return redirect()->route('adminDashboard');
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('adminDashboard');
        }
        $modelSale = New Sale;
        $getMonth = $modelSale->getThisMonth();
        $textDay = 'Data Penjualan Safra '. date('d-M-Y',strtotime("-1 days"));
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date,
                'yesterDay' => null
            );
            $textDay =  'Data Penjualan '.date('d M Y', strtotime($request->start_date)).' - '.date('d M Y', strtotime($request->end_date));
        }
        $getSales = $modelSale->getDataSalesSafra($dataUser, $getMonth);
        return view('admin.manager_area.all_penjualan_safra')
                ->with('day', $textDay)
                ->with('headerTitle', 'Data Penjualan Safra')
                ->with('allSales', $getSales)
                ->with('dataUser', $dataUser);
    }
    
    
    
}

