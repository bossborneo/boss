<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Cabang;
use App\Model\Member;
use App\Model\Purchase;
use App\Model\Itempurchase;
use App\Model\Sale;
use App\Model\Transfer;
use App\Model\Content;

class AjaxAdminController extends Controller {
    
    public function __construct(){
        
    }
    
    public function getSearchSponsor($type, $cabId, $id){
        $dataUser = Auth::user();
        $lvl = 7;
        if($type == 9){
            $lvl = 8;
        }
        if($type == 10){
            $lvl = 9;
        }
        if($type == 11){
            $lvl = 10;
        }
        if($type == 12){
            $lvl = 11;
        }
        if($type == 13){
            $lvl = 12;
        }
        $modelMember = New Member;
        $getManagerId = $modelMember->getUserId($id);
        $dataSponsor = $modelMember->getAllParentManager($lvl, $cabId, $getManagerId);
        $sponsor = null;
        if(count($dataSponsor) > 0){
            $sponsor = $dataSponsor;
        }
        return view('admin.ajax.adm_add_crew')
                ->with('dataSponsor', $sponsor)
                ->with('type', $type)
                ->with('manager', $getManagerId)
                ->with('dataUser', $dataUser);
    }
    
    public function getSearchAsmenTld($id, $cabId){
        $dataUser = Auth::user();
        $modelMember = New Member;
        $getData = $modelMember->getUserId($id);
        $arraySpId = explode(',', $getData->sponsor_id);
        $dataSp = array();
        foreach($arraySpId as $rowSp){
            $rm1 = str_replace('[', '', $rowSp);
            $rm2 = str_replace(']', '', $rm1);
            $int = (int) $rm2;
            $dataSp[] = $int;
        }
        if($getData->level_id <= 8){
            $dataSpAsmen = null;
            $dataSpTld = null;
        } else {
            $dataSpAsmen = $modelMember->getUpperAsmenTld( $cabId, $dataSp, 8);
            $dataSpTld = $modelMember->getUpperAsmenTld( $cabId, $dataSp, 9);
        }
        return view('admin.ajax.view_parent')
                ->with('manager', $getData)
                ->with('spAsmen', $dataSpAsmen)
                ->with('spTld', $dataSpTld)
                ->with('dataUser', $dataUser);
    }
    
    public function postSumPrice(Request $request){
        $modelItem = New Itempurchase;
        $modelMember = New Member;
        $getData = $modelMember->getUserId($request->crew);
        $level = 'Manager';
        if($getData->level_id == 8){
            $level = 'Asisten Manager';
        }
        if($getData->level_id == 9){
            $level = 'Top Leader';
        }
        if($getData->level_id == 10){
            $level = 'Leader';
        }
        if($getData->level_id == 11){
            $level = 'Trainer';
        }
        if($getData->level_id == 12){
            $level = 'Merchandiser';
        }
        if($getData->level_id == 13){
            $level = 'Retrainer';
        }
        $canInsert = (object) array(
            'can' => true,
            'pesan' => ''
        );
        if($request->sale_date == null){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Anda tidak memilih tanggal'
            );
        }
        if(date('w', strtotime($request->sale_date)) == 0){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Input penjualan tidak bisa dilakukan pada hari minggu'
            );
        }
        if(strtotime($request->sale_date) > strtotime(date('Y-m-d')) ){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Input penjualan tidak bisa dilakukan jika input tanggal lebih dari hari ini'
            );
        }
        $count = 0;
        if($request->item_purchase != null){
            $count = count($request->item_purchase);
        }
        $dataAll = array();
        $sum = 0;
        $cekItem = 0;
        $cekJml = 0;
        if($count > 0){
            for ($x = 0; $x < $count; $x++) {
                if($request->amount[$x] == null){
                    $cekJml++;
                }
                $getItem = $modelItem->getPurchaseFromItem($request->item_purchase[$x]);
                if($getItem != null){
//                    if($getItem->is_poin == 0){
//                        if($request->opsi[$x] == 1){
//                            $canInsert = (object) array(
//                                'can' => false,
//                                'pesan' => 'Opsi Setengah hanya obat dengan jumlah 30 saja'
//                            );
//                        }
//                    }
//                    if($getItem->is_poin == 1){
//                        if($request->opsi[$x] == 1){
//                            if(is_float($request->amount) == true){
//                                $canInsert = (object) array(
//                                    'can' => false,
//                                    'pesan' => 'Opsi Setengah hanya obat dengan jumlah 30 saja dan jumlah tidak boleh desimal'
//                                );
//                            }
//                        }
//                    }
                    $type = 'Obat';
                    if($getItem->type == 1){
                        $type = 'Alat';
                    }
                    if($getItem->type == 2){
                        $type = 'Strip';
                    }
                    $dataAll[] = (object) array(
                        'purchase_name' => $getItem->purchase_name,
                        'amount' => $request->amount[$x],
                        'type' => $type,
                        'price' => round(($request->amount[$x] * $getItem->price), 1 ),
//                        'is_half' => $request->opsi[$x],
                    );
                    $sum += $request->amount[$x] * $getItem->price;
                }
                if($getItem == null){
                    $cekItem++;
                }
            }
        }
        if($cekItem > 0){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Anda tidak memilih barang'
            );
        }
        if($cekJml > 0){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Anda tidak memasukan jumlah barang'
            );
        }
        return view('admin.ajax.sales_confirm')
                ->with('dataAll', $dataAll)
                ->with('nama_crew', $getData->name)
                ->with('level', $level)
                ->with('sum', $sum)
                ->with('cek', $canInsert)
                ->with('date', $request->sale_date);
    }
    
    public function postSumPriceSafra(Request $request){
        $modelItem = New Itempurchase;
        $modelMember = New Member;
        $getData = $modelMember->getUserId($request->crew);
        $level = 'Manager';
        if($getData->level_id == 8){
            $level = 'Asisten Manager';
        }
        if($getData->level_id == 9){
            $level = 'Top Leader';
        }
        if($getData->level_id == 10){
            $level = 'Leader';
        }
        if($getData->level_id == 11){
            $level = 'Trainer';
        }
        if($getData->level_id == 12){
            $level = 'Merchandiser';
        }
        if($getData->level_id == 13){
            $level = 'Retrainer';
        }
        $canInsert = (object) array(
            'can' => true,
            'pesan' => ''
        );
        if($request->sale_date == null){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Anda tidak memilih tanggal'
            );
        }
        if(date('w', strtotime($request->sale_date)) == 0){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Input penjualan tidak bisa dilakukan pada hari minggu'
            );
        }
        if(strtotime($request->sale_date) > strtotime(date('Y-m-d')) ){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Input penjualan tidak bisa dilakukan jika input tanggal lebih dari hari ini'
            );
        }
        $count = 0;
        if($request->item_purchase != null){
            $count = count($request->item_purchase);
        }
        $dataAll = array();
        $sum = 0;
        $cekItem = 0;
        $cekJml = 0;
        if($count > 0){
            for ($x = 0; $x < $count; $x++) {
                if($request->amount[$x] == null){
                    $cekJml++;
                }
                $getItem = $modelItem->getPurchaseFromItem($request->item_purchase[$x]);
                if($getItem != null){
                    $type = 'Lainnya';
                    $dataAll[] = (object) array(
                        'purchase_name' => $getItem->purchase_name,
                        'amount' => $request->amount[$x],
                        'type' => $type,
                        'price' => round(($request->amount[$x] * $getItem->price), 1 ),
                    );
                    $sum += $request->amount[$x] * $getItem->price;
                }
                if($getItem == null){
                    $cekItem++;
                }
            }
        }
        if($cekItem > 0){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Anda tidak memilih barang'
            );
        }
        if($cekJml > 0){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Anda tidak memasukan jumlah barang'
            );
        }
        return view('admin.ajax.sales_confirm_safra')
                ->with('dataAll', $dataAll)
                ->with('nama_crew', $getData->name)
                ->with('level', $level)
                ->with('sum', $sum)
                ->with('cek', $canInsert)
                ->with('date', $request->sale_date);
    }
    
    public function getSearchManager($cabId){
        $modelMember = New Member;
        $getManager = $modelMember->getManagerFromCabang($cabId);
        return view('admin.ajax.get_manager')
                ->with('manager', $getManager);
    }
    
    public function getSearchManagerPengeluaran($cabId){
        $modelMember = New Member;
        $getManager = array();
        if($cabId > 0){
            $getManager = $modelMember->getManagerFromCabang($cabId);
        }
        return view('admin.ajax.get_manager_pengeluaran')
                ->with('manager', $getManager);
    }
    
    public function getSearchCrew($mId){
        $modelMember = New Member;
        $getDataManager = $modelMember->getUserId($mId);
        $mySponsor = $getDataManager->sponsor_id.',['.$getDataManager->id.']';
        if($getDataManager->sponsor_id == null){
            $mySponsor = '['.$getDataManager->id.']';
        }
        $arraySpId = array(8, 9, 10,11, 12, 13);
        $getCrew = $modelMember->getNewAllDownlineForAdmin($getDataManager->cabang_id, $mySponsor, $arraySpId);
        return view('admin.ajax.get_crew')
                ->with('crew', $getCrew);
    }
    
    public function getTree(){
        $dataUser = Auth::user();
        $mySponsor = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $mySponsor = '['.$dataUser->id.']';
        }
        $modelMember = New Member;
        $getDown = $modelMember->getAllMyTree($dataUser->id);
        $data = json_encode($getDown);
        echo $data;
    }
    
    public function getAdminManagerTree($mid){
        $dataUser = Auth::user();
        $mySponsor = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $mySponsor = '['.$dataUser->id.']';
        }
        $modelMember = New Member;
        $getDown = $modelMember->getAllMyTree($mid);
        $data = json_encode($getDown);
        echo $data;
    }
    
    public function getSearchBarang($date){
        $dataUser = Auth::user();
        $modelPurchase = New Purchase;
        $allBarang = $modelPurchase->getManagerStockReadyNewest($dataUser, $date);
        return view('admin.ajax.get_barang')
                ->with('allBarang', $allBarang);
    }
    
    public function getSearchBarangSafra($date){
        $dataUser = Auth::user();
        $modelPurchase = New Purchase;
        $allBarang = $modelPurchase->getManagerStockReadyNewestSafra($dataUser, $date);
        return view('admin.ajax.get_barang_safra')
                ->with('allBarang', $allBarang);
    }
    
    public function getMoreBarang($date, $counter){
        $dataUser = Auth::user();
        $modelPurchase = New Purchase;
        $allBarang = $modelPurchase->getManagerStockReadyNewest($dataUser, $date);
        return view('admin.ajax.get_more_barang')
                ->with('allBarang', $allBarang)
                ->with('counter', $counter);
    }
    
    
    public function getSearchAllName($type, $cabId){
        $modelMember = New Member;
        $getManager = $modelMember->getAllDownlineForPindah($cabId);
        return view('admin.ajax.get_all_structur')
                ->with('type', $type)
                ->with('manager', $getManager);
    }
    
    public function getOutCategory($type, $id){
        $modelTransfer = New Transfer;
        $getTransfer = $modelTransfer->getCategoryPengeluaranId($id);
        $header = 'Edit Kategori Pengeluaran';
        if($type == 'rm'){
            $header = 'Hapus Kategori Pengeluaran';
        }
        return view('admin.ajax.adm_out_category')
                    ->with('headerTitle', $header)
                    ->with('type', $type)
                    ->with('getData', $getTransfer);
    }
    
    public function getTotalSalesDate($date){
        $dataUser = Auth::user();
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getTotalSalesAndOut($dataUser, $date);
        return view('admin.ajax.m_total_sales_out')
                ->with('data', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getSearchCategory($date){
        $dataUser = Auth::user();
        $modelTransfer = New Transfer;
        $getCategory = $modelTransfer->getAllCategoryPengeluaran();
        return view('admin.ajax.get_category')
                ->with('allCategory', $getCategory);
    }
    
    public function getMoreCategory($date){
        $dataUser = Auth::user();
        $modelTransfer = New Transfer;
        $getCategory = $modelTransfer->getAllCategoryPengeluaran();
        return view('admin.ajax.get_more_category')
                ->with('allCategory', $getCategory);
    }
    
    public function postPriceTransfer(Request $request){
        $dataUser = Auth::user();
        $modelTransfer = New Transfer;
        $canInsert = (object) array(
            'can' => true,
            'pesan' => ''
        );
        if($request->out_date == null){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Anda tidak memilih tanggal'
            );
        }
        if(strtotime($request->out_date) > strtotime(date('Y-m-d')) ){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Input Pengeluaran tidak bisa dilakukan jika input tanggal lebih dari hari ini'
            );
        }
        $count = 0;
        if($request->category_id != null){
            $count = count($request->category_id);
        }
        $dataAll = array();
        $sum = 0;
        $cekJml = 0;
        if($count > 0){
            for ($x = 0; $x < $count; $x++) {
                if($request->amount[$x] == null){
                    $cekJml++;
                }
                $getCat = $modelTransfer->getCategoryPengeluaranId($request->category_id[$x]);
                if($getCat != null){
                    $dataAll[] = (object) array(
                        'pengeluaran_name' => $getCat->pengeluaran_name,
                        'amount' => $request->amount[$x]
                    );
                    $sum += $request->amount[$x];
                }
            }
        }
        if($cekJml > 0){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Anda tidak memasukan jumlah barang'
            );
        }
        $getData = $modelTransfer->getTotalSalesAndOut($dataUser, $request->out_date);
        return view('admin.ajax.transfer_confirm')
                ->with('dataAll', $dataAll)
                ->with('getData', $getData)
                ->with('sum', $sum)
                ->with('cek', $canInsert)
                ->with('date', $request->out_date)
                ->with('dataUser', $dataUser);
    }
    
    public function getAjaxConfirmTransfer($id, $type){
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getAllTransferAdminId($id);
        $approve = true;
        $text = 'Approve';
        if($type == 2){
            $approve = false;
            $text = 'Reject';
        }
        return view('admin.ajax.adm_confirm_transfer')
                ->with('headerTitle', $text)
                ->with('getData', $getData)
                ->with('type', $type)
                ->with('approve', $approve);
    }
    
    public function getManagerConfirmTransfer($id, $type){
        $dataUser = Auth::user();
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getTransferManagerId($id, $dataUser);
        $text = 'Confirm Ulang';
        return view('admin.ajax.m_confirm_transfer')
                ->with('headerTitle', $text)
                ->with('getData', $getData)
                ->with('type', $type);
    }
    
    public function getAjaxEditPengeluaran($id, $t_id){
        $dataUser = Auth::user();
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getPengeluaranAdminId($id);
        $type = 'edit';
        return view('admin.ajax.adm_edit_pengeluaran')
                    ->with('type', $type)
                    ->with('t_id', $t_id)
                    ->with('getData', $getData);
    }
    
    public function getAjaxRemovePengeluaran($id, $t_id){
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getPengeluaranAdminId($id);
        $type = 'remove';
        return view('admin.ajax.adm_edit_pengeluaran')
                    ->with('type', $type)
                    ->with('t_id', $t_id)
                    ->with('getData', $getData);
    }
    
    public function getMQBTotalPengeluaranDate($date){
        $dataUser = Auth::user();
        $modelTransfer = New Transfer;
        $getData = $modelTransfer->getMQBTotalOut($dataUser, $date);
        return view('admin.ajax.mqb_total_out')
                ->with('data', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postMQBPengeluaran(Request $request){
        $dataUser = Auth::user();
        $modelTransfer = New Transfer;
        $canInsert = (object) array(
            'can' => true,
            'pesan' => ''
        );
        if($request->out_date == null){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Anda tidak memilih tanggal'
            );
        }
        if(strtotime($request->out_date) > strtotime(date('Y-m-d')) ){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Input Pengeluaran tidak bisa dilakukan jika input tanggal lebih dari hari ini'
            );
        }
        $count = 0;
        if($request->category_id != null){
            $count = count($request->category_id);
        }
        $dataAll = array();
        $sum = 0;
        $cekJml = 0;
        if($count > 0){
            for ($x = 0; $x < $count; $x++) {
                if($request->amount[$x] == null){
                    $cekJml++;
                }
                $getCat = $modelTransfer->getCategoryPengeluaranId($request->category_id[$x]);
                if($getCat != null){
                    $dataAll[] = (object) array(
                        'pengeluaran_name' => $getCat->pengeluaran_name,
                        'amount' => $request->amount[$x]
                    );
                    $sum += $request->amount[$x];
                }
            }
        }
        if($cekJml > 0){
            $canInsert = (object) array(
                'can' => false,
                'pesan' => 'Anda tidak memasukan jumlah barang'
            );
        }
        $getData = $modelTransfer->getMQBTotalOut($dataUser, $request->out_date);
        return view('admin.ajax.mqb_pengeluaran_confirm')
                ->with('dataAll', $dataAll)
                ->with('getData', $getData)
                ->with('sum', $sum)
                ->with('cek', $canInsert)
                ->with('date', $request->out_date)
                ->with('dataUser', $dataUser);
    }
    
    public function getSearchAllType($cabId, $search_type){
        $modelMember = New Member;
        $getManager = $modelMember->getManagerFromCabang($cabId);
        return view('admin.ajax.get_search_type')
                ->with('manager', $getManager)
                ->with('search_type', $search_type);
    }
    
    public function getSearchTopStructure($cabId){
        $modelMember = New Member;
        $getManager = $modelMember->getTopStructureFromCabang($cabId);
        return view('admin.ajax.get_manager')
                ->with('manager', $getManager);
    }
    
    public function getSearchDownStructure($type, $cabId){
        $modelMember = New Member;
        $getManager = $modelMember->getDownlineStructureFromCabang($cabId);
        return view('admin.ajax.get_all_structur')
                ->with('type', $type)
                ->with('manager', $getManager);
    }
    
    public function getAjaxRemoveTopStructure($id){
        $modelMember = New Member;
        $getData = $modelMember->getUserId($id);
        return view('admin.ajax.adm_rm_structure')
                    ->with('type', 1)
                    ->with('getData', $getData);
    }
    
    public function getAjaxRemoveDownStructure($id){
        $modelMember = New Member;
        $getData = $modelMember->getUserId($id);
        return view('admin.ajax.adm_rm_structure')
                    ->with('type', 2)
                    ->with('getData', $getData);
    }
    
    public function getAdminManagerOrganisasiTree($mid){
        $modelMember = New Member;
        $getDown = $modelMember->getAllMyTreeOrganisasi($mid);
        $data = json_encode($getDown);
        echo $data;
    }
    
    public function getOrganisasiTree($id){
        $dataUser = Auth::user();
        $modelMember = New Member;
        $getIdMember = $modelMember->getUserIdOrganisasi($id, $dataUser);
        $mySponsor = $getIdMember->sponsor_id.',['.$getIdMember->id.']';
        if($getIdMember->sponsor_id == null){
            $mySponsor = '['.$getIdMember->id.']';
        }
        $getDown = $modelMember->getAllMyTree($getIdMember->id);
        $data = json_encode($getDown);
        echo $data;
    }
    
    public function getAdminRemoveContents($id){
        $modelContent = New Content;
        $getData = $modelContent->getContentByField('id', $id);
        return view('admin.ajax.adm_remove_content')
                    ->with('getData', $getData);
    }
    
    public function getManagerSetCookies($idContent){
        if(!isset($_COOKIE['_content'])){
            $value = $idContent;
        } else {
            $value = $_COOKIE['_content'].','.$idContent;
        }
        setcookie('_content', $value, time() + (86400 * 10), "/"); // 86400 = 1 day
        return redirect()->route('adminDashboard');
    }
    
    public function getSearchManagerChangePassword($cabId, $type){
        $modelMember = New Member;
        $getManager = $modelMember->getManagerFromCabang($cabId);
        return view('admin.ajax.get_manager_passwd')
                ->with('type', $type)
                ->with('manager', $getManager);
    }
    
    public function getSearchDataUserId($id){
        $modelMember = New Member;
        $getManager = $modelMember->getUserId($id);
        return view('admin.ajax.get_data_id')
                ->with('getData', $getManager);
    }
    
    public function getDataActionSafra($type, $id){
        $modelSale = New Sale;
        $getData = $modelSale->getSafraSalesForAdminByID($id);
        return view('admin.ajax.adm_safra_data')
                ->with('type', $type)
                ->with('getData', $getData);
    }
    
    public function getDataDetailSafraID($id){
        $modelSale = New Sale;
        $getData = $modelSale->getSafraSalesForAdminByID($id);
        return view('admin.ajax.adm_safra_detail')
                ->with('getData', $getData);
    }
    
    
}
