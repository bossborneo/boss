<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Model\Member;
use App\Model\Bonus;
use App\Model\Sale;

class BonusOmset extends Command {

    protected $signature = 'bonus_omset';
    protected $description = 'Bonus Omset Mingguan Asmen, Manager';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
//        dd(bcrypt('natasha_em_pon_2019'));
        $modelMember = New Member;
        $modelBonus = New Bonus;
        $modelSales = New Sale;
        $arrayManager = array(7, 8);
        $typeObat = 3;
        $cekCronSetting = $modelBonus->getCekCron();
        $dataCronSetting = $cekCronSetting;
        if($cekCronSetting == null){
            $insertCroSetting = array(
                'start_date' => '2018-10-08'
            );
            $modelBonus->getInsertCron($insertCroSetting);
            $dataCronSetting = (object) array(
                'id' => 1,
                'start_date' =>  '2018-10-08',
                'hari' => 0
            );
        }
        $hari = $dataCronSetting->hari;
        $todayTime = strtotime(date('Y-m-d'));
        $tambahHari = strtotime('+'.$hari.' days', strtotime($dataCronSetting->start_date));
        $date = date('Y-m-d', $tambahHari);
        if($tambahHari < $todayTime){
            $getBonusSetting = $modelBonus->getSettingBonus();
            $getDataManager = $modelMember->getAllDataForOmset($arrayManager);
            $dataBonus_omset = array();
            foreach($getDataManager as $row){
                $spId = $row->sponsor_id.',['.$row->id.']';
                if($row->sponsor_id == null){
                    $spId = '['.$row->id.']';
                }
                $arraySpId = explode(',', $spId);
                $dataSp = array();
                foreach($arraySpId as $rowSp){
                    $rm1 = str_replace('[', '', $rowSp);
                    $rm2 = str_replace(']', '', $rm1);
                    $int = (int) $rm2;
                    $dataSp[] = $int;
                }
                $dataAllSp = $modelMember->getUpperUserId($dataSp);
                $get_gm = $modelMember->getLevelLoop($dataAllSp, 2);
                $get_sem = $modelMember->getLevelLoop($dataAllSp, 3);
                $get_em = $modelMember->getLevelLoop($dataAllSp, 4);
                $get_sm = $modelMember->getLevelLoop($dataAllSp, 5);
                $get_mqb = $modelMember->getLevelLoop($dataAllSp, 6);
                $get_m = $modelMember->getLevelLoop($dataAllSp, 7);
                $get_asmen = $modelMember->getLevelLoop($dataAllSp, 8);
                $get_tld = $modelMember->getLevelLoop($dataAllSp, 9);
                $get_ld = $modelMember->getLevelLoop($dataAllSp, 10);
                $get_tr = $modelMember->getLevelLoop($dataAllSp, 11);
                $get_md = $modelMember->getLevelLoop($dataAllSp, 12);
                $get_rt = $modelMember->getLevelLoop($dataAllSp, 13);
                $getOmsetHarian = $modelSales->getOmsetById($date, $row, $typeObat);
                $omsetHarian = 0;
                if($getOmsetHarian->omset_harian != null){
                    if($row->level_id == 7){
                        $omsetHarian = ($getBonusSetting->manager_omset_obat *  $getOmsetHarian->omset_harian) / 100;
                    }
                    if($row->level_id == 8){
                        $omsetHarian = ($getBonusSetting->asmen_omset_obat *  $getOmsetHarian->omset_harian) / 100;
                    }
                }
                $dataBonus_omset = array(
                    'user_id' => $row->id,
                    'bonus_price' => $omsetHarian,
                    'item_purchase_id' => 0,
                    'bonus_type' => 11,
                    'bonus_date' => $date,
                    'gm_id' => $get_gm,
                    'sem_id' => $get_sem,
                    'em_id' => $get_em,
                    'sm_id' => $get_sm,
                    'mqb_id' => $get_mqb,
                    'manager_id' => $get_m,
                    'asmen_id' => $get_asmen,
                    'tld_id' => $get_tld,
                    'ld_id' => $get_ld,
                    'tr_id' => $get_tr,
                    'md_id' => $get_md,
                    'rt_id' => $get_rt,
                );
                //inset bonus omset here
                DB::table('bonus')->insert($dataBonus_omset);
            }
//            dd($dataBonus_omset);
        }
        $dataUpdateHari = array('hari' => $hari + 1);
        $modelBonus->getUpdateCron($dataUpdateHari);
        dd('done cron omset');
    }
    
    
    
    
}
