<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Purchase extends Model {
    
    public function getAllPurchase(){
        $sql = DB::table('purchase')
                    ->selectRaw('id, purchase_name, type, hpp, main_price, created_at, deleted_at, kondisi, is_poin')
                    ->orderBy('deleted_at', 'ASC')
                    ->orderBy('type', 'ASC')
                    ->orderBy('purchase_name', 'ASC')
                    ->orderBy('created_at', 'DESC')
//                    ->whereNull('deleted_at')
                    ->get();
        return $sql;
    }
    
    public function getManagerAllPurchase($cabangId){
        $sql = DB::table('purchase')
                    ->join('item_purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('purchase.id as id_purchase, purchase.purchase_name, purchase.type, purchase.hpp, '
                            . 'purchase.main_price, item_purchase.qty, item_purchase.sisa, item_purchase.price')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->whereNull('purchase.deleted_at')
                    ->where('item_purchase.cabang_id', '=', $cabangId)
                    ->get();
        return $sql;
    }
    
    public function getManagerAllInputStock($data){
        $whereNot = array(0);
        if($data != null){
            foreach($data as $row){
                $whereNot[] = $row->id_purchase;
            }
        }
        $sql = DB::table('purchase')
                    ->selectRaw('purchase.id as id_purchase, purchase.purchase_name, purchase.type, purchase.hpp, '
                            . 'purchase.main_price, purchase.kondisi, purchase.is_poin')
                    ->whereNull('purchase.deleted_at')
                    ->whereNotIn('purchase.id', $whereNot)
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getManagerAllPurchaseReady($data){
        $sql = DB::table('purchase')
                    ->join('item_purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('purchase.id as id_purchase, purchase.purchase_name, purchase.type, purchase.hpp, '
                            . 'purchase.main_price, item_purchase.qty, item_purchase.sisa, item_purchase.price, item_purchase.id as id_item_purchase')
                    ->whereNull('purchase.deleted_at')
                    ->where('item_purchase.cabang_id', '=', $data->cabang_id)
//                    ->where('item_purchase.manager_id', '=', $data->id) //semua manager di tiap cabang tahu jml stock
                    ->where('item_purchase.sisa', '>', 0)
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getManagerPurchaseId($id){ //salah
        $sql = DB::table('purchase')
                    ->leftJoin('item_purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('purchase.id as id_purchase, purchase.purchase_name, purchase.type, purchase.hpp, '
                            . 'purchase.main_price, item_purchase.qty, item_purchase.sisa, item_purchase.price, item_purchase.id as id_purchase_item')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->whereNull('purchase.deleted_at')
                    ->where('purchase.id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getManagerPurchaseIdAddStock($id){
        $sql = DB::table('purchase')
                    ->selectRaw('purchase.id as id_purchase, purchase.purchase_name, purchase.type, purchase.hpp, purchase.main_price, kondisi, is_poin')
                    ->whereNull('purchase.deleted_at')
                    ->where('purchase.id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getCheckPurchase($fieldName, $name){
        $sql = DB::table('purchase')
                    ->selectRaw('id, purchase_name, type, hpp, main_price, deleted_at, created_at, kondisi, is_poin')
                    ->where($fieldName, '=', $name)
                    ->first();
        return $sql;
    }
    
    public function getInsertPurchase($data){
        try {
            $lastInsertedID = DB::table('purchase')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getDeletePurchaseById($id){
        try {
            DB::table('purchase')->where('id', '=', $id)->delete();
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdatePurchase($id, $data){
        try {
            DB::table('purchase')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getManagerHistoryStock($m_id, $type_stock){
        $sql = DB::table('item_purchase')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('stock', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->selectRaw('purchase.purchase_name, purchase.type,'
                            . 'sum(stock.amount) as amount, DATE(stock.created_at) as date_created')
                    ->whereNull('purchase.deleted_at')
                    ->where('item_purchase.manager_id', '=', $m_id)
                    ->where('stock.type_stock', '=', $type_stock)
                    ->groupBy('date_created')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->orderBy('date_created', 'DESC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getManagerCabangHistoryStock($cbg_id, $type_stock){
        $sql = DB::table('item_purchase')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('stock', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->selectRaw('purchase.purchase_name, purchase.type,'
                            . 'sum(stock.amount) as amount')
                    ->whereNull('purchase.deleted_at')
                    ->where('item_purchase.cabang_id', '=', $cbg_id)
                    ->where('stock.type_stock', '=', $type_stock)
//                    ->groupBy('date_created')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getManagerStockReady($data, $sundayStart){
        //purchase.id as id_purchase, purchase.purchase_name, purchase.type, purchase.hpp, '
//                            . 'purchase.main_price, item_purchase.qty, item_purchase.sisa, item_purchase.price, item_purchase.id as id_item_purchase
        if(date('w') == 0){
            $getNextSunday = date('Y-m-d');
            $sql = DB::table('stock')
                        ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                        ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                        ->selectRaw('purchase.purchase_name, purchase.type, '
                                . 'item_purchase.price, item_purchase.id as id_item_purchase,'
                                . 'sum(case when stock.type_stock = 1 then stock.amount end) as qty_input,'
                                . 'sum(case when stock.type_stock = 2 then stock.amount else 0 end) as qty_output')
                        ->whereNull('purchase.deleted_at')
                        ->where('item_purchase.cabang_id', '=', $data->cabang_id)
                        ->whereDate('stock.created_at', '>=', $sundayStart)
                        ->whereDate('stock.created_at', '<', $getNextSunday)
                        ->groupBy('item_purchase.id')
                        ->groupBy('item_purchase.price')
                        ->groupBy('purchase.purchase_name')
                        ->groupBy('purchase.type')
                        ->orderBy('purchase.type', 'ASC')
                        ->orderBy('purchase.purchase_name', 'ASC')
                        ->get();
        } else {
            $sql = DB::table('stock')
                        ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                        ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                        ->selectRaw('purchase.purchase_name, purchase.type, '
                                . 'item_purchase.price, item_purchase.id as id_item_purchase,'
                                . 'sum(case when stock.type_stock = 1 then stock.amount end) as qty_input,'
                                . 'sum(case when stock.type_stock = 2 then stock.amount else 0 end) as qty_output')
                        ->whereNull('purchase.deleted_at')
                        ->where('item_purchase.cabang_id', '=', $data->cabang_id)
                        ->whereDate('stock.created_at', '>=', $sundayStart)
                        ->whereDate('stock.created_at', '<=', date('Y-m-d'))
                        ->groupBy('item_purchase.id')
                        ->groupBy('item_purchase.price')
                        ->groupBy('purchase.purchase_name')
                        ->groupBy('purchase.type')
                        ->orderBy('purchase.type', 'ASC')
                        ->orderBy('purchase.purchase_name', 'ASC')
                        ->get();
        }
        
        return $sql;
    }
    
    public function getManagerStockReadyNew($data, $date){
        $getLastSunday = date('Y-m-d', strtotime('last Sunday', strtotime($date)));
        if(date('w', strtotime($date)) == 0){
            $getLastSunday = $date;
        } 
        $getNextSunday = date('Y-m-d', strtotime('next Sunday', strtotime($date)));
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('purchase.purchase_name, purchase.type, '
                            . 'item_purchase.price, item_purchase.id as id_item_purchase,'
                            . '(sum(case when stock.type_stock = 1 then stock.amount end) + sum(case when stock.type_stock = 3 then stock.amount else 0 end)) as qty_input,'
                            . 'sum(case when stock.type_stock = 2 then stock.amount else 0 end) as qty_output')
                    ->whereNull('purchase.deleted_at')
                    ->where('item_purchase.cabang_id', '=', $data->cabang_id)
                    ->whereDate('stock.created_at', '>=', $getLastSunday)
                    ->whereDate('stock.created_at', '<', $getNextSunday)
                    ->groupBy('item_purchase.id')
                    ->groupBy('item_purchase.price')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getManagerStockReadyNewest($data, $date){
        $getLastSunday = date('Y-m-d', strtotime('last Sunday', strtotime($date)));
        if(date('w', strtotime($date)) == 0){
            $getLastSunday = $date;
        } 
        $getNextSunday = date('Y-m-d', strtotime('next Sunday', strtotime($date)));
//        dd($getLastSunday.'   -   '.$getNextSunday);
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('item_purchase.id as id_item_purchase, purchase.purchase_name, purchase.type, sum(stock.amount) as qty_input, '
                            . '(
                                        select sum(sales.amount)
                                        from sales 
                                        where sales.item_purchase_id = item_purchase.id 
                                        AND sales.sale_date >= "'.$getLastSunday.'" 
                                        AND sales.sale_date < "'.$getNextSunday.'" 
                                        AND sales.deleted_at is null
                                ) 
                                as qty_output, item_purchase.price')
                    ->where('stock.type_stock', '=', 1)
                    ->whereIn('purchase.type', array(1, 2, 3))
                    ->where('item_purchase.cabang_id', '=', $data->cabang_id)
                    ->whereDate('stock.created_at', '>=', $getLastSunday)
                    ->whereDate('stock.created_at', '<', $getNextSunday)
                    ->groupBy('item_purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->groupBy('item_purchase.price')
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getManagerStockReadySafra($data, $sundayStart){
        //purchase.id as id_purchase, purchase.purchase_name, purchase.type, purchase.hpp, '
//                            . 'purchase.main_price, item_purchase.qty, item_purchase.sisa, item_purchase.price, item_purchase.id as id_item_purchase
        if(date('w') == 0){
            $getNextSunday = date('Y-m-d');
            $sql = DB::table('stock')
                        ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                        ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                        ->selectRaw('purchase.purchase_name, purchase.type, '
                                . 'item_purchase.price, item_purchase.id as id_item_purchase,'
                                . 'sum(case when stock.type_stock = 1 then stock.amount end) as qty_input,'
                                . 'sum(case when stock.type_stock = 2 then stock.amount else 0 end) as qty_output')
                        ->whereNull('purchase.deleted_at')
                        ->where('item_purchase.cabang_id', '=', $data->cabang_id)
                        ->whereDate('stock.created_at', '>=', $sundayStart)
                        ->whereDate('stock.created_at', '<', $getNextSunday)
                        ->groupBy('item_purchase.id')
                        ->groupBy('item_purchase.price')
                        ->groupBy('purchase.purchase_name')
                        ->groupBy('purchase.type')
                        ->orderBy('purchase.type', 'ASC')
                        ->orderBy('purchase.purchase_name', 'ASC')
                        ->get();
        } else {
            $sql = DB::table('stock')
                        ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                        ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                        ->selectRaw('purchase.purchase_name, purchase.type, '
                                . 'item_purchase.price, item_purchase.id as id_item_purchase,'
                                . 'sum(case when stock.type_stock = 1 then stock.amount end) as qty_input,'
                                . 'sum(case when stock.type_stock = 2 then stock.amount else 0 end) as qty_output')
                        ->whereNull('purchase.deleted_at')
                        ->where('item_purchase.cabang_id', '=', $data->cabang_id)
                        ->whereDate('stock.created_at', '>=', $sundayStart)
                        ->whereDate('stock.created_at', '<=', date('Y-m-d'))
                        ->groupBy('item_purchase.id')
                        ->groupBy('item_purchase.price')
                        ->groupBy('purchase.purchase_name')
                        ->groupBy('purchase.type')
                        ->orderBy('purchase.type', 'ASC')
                        ->orderBy('purchase.purchase_name', 'ASC')
                        ->get();
        }
        
        return $sql;
    }
    
    public function getManagerStockReadyNewestSafra($data, $date){
        $getLastSunday = date('Y-m-d', strtotime('last Sunday', strtotime($date)));
        if(date('w', strtotime($date)) == 0){
            $getLastSunday = $date;
        } 
        $getNextSunday = date('Y-m-d', strtotime('next Sunday', strtotime($date)));
//        dd($getLastSunday.'   -   '.$getNextSunday);
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('item_purchase.id as id_item_purchase, purchase.purchase_name, purchase.type, sum(stock.amount) as qty_input, '
                            . '(
                                        select sum(sales.amount)
                                        from sales 
                                        where sales.item_purchase_id = item_purchase.id 
                                        AND sales.sale_date >= "'.$getLastSunday.'" 
                                        AND sales.sale_date < "'.$getNextSunday.'" 
                                        AND sales.deleted_at is null
                                ) 
                                as qty_output, item_purchase.price')
                    ->where('stock.type_stock', '=', 1)
                    ->where('item_purchase.cabang_id', '=', $data->cabang_id)
                    ->where('purchase.type', '=', 10)
                    ->whereDate('stock.created_at', '>=', $getLastSunday)
                    ->whereDate('stock.created_at', '<', $getNextSunday)
                    ->groupBy('item_purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->groupBy('item_purchase.price')
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    
}

