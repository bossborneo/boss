<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Cabang extends Model {
    
    public function getAllCabang(){
        $sql = DB::table('cabang')
                    ->selectRaw('id, cabang_name, extra, extra_strip, extra_alat, extra_wilayah, wilayah')
                    ->whereNull('deleted_at')
                    ->orderBy('cabang_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getCountCabang(){
        $sql = DB::table('cabang')
                    ->selectRaw('id')
                    ->whereNull('deleted_at')
                    ->count();
        return $sql;
    }
    
    public function getCheckCabang($fieldName, $name){
        $sql = DB::table('cabang')
                    ->selectRaw('id, cabang_name, extra, extra_strip, extra_alat, extra_wilayah, wilayah')
                    ->where($fieldName, '=', $name)
                    ->first();
        return $sql;
    }
    
    public function getInsertCabang($data){
        try {
            DB::table('cabang')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getDeleteCabangById($id){
        try {
            DB::table('cabang')->where('id', '=', $id)->delete();
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateCabang($id, $data){
        try {
            DB::table('cabang')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
}
