<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Sale extends Model {
    
    public function getInsertSale($data){
        try {
            $lastInsertedID = DB::table('sales')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateSales($id, $data){
        try {
            DB::table('sales')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getInvoice($date){
        $sql = DB::table('sales')
                    ->selectRaw('id')
                    ->whereDate('sale_date', $date)
                    ->count();
        $tmp = $sql+1;
        $dateCode = date("Ymd", strtotime($date));
        $code = sprintf("%05s", $tmp);
        $newInvoiceNumber =  'PJ'.$dateCode.'-'.$code;
        return $newInvoiceNumber;
    }
    
    public function getDataSales($dataUser, $date){
        $mySponsor = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $mySponsor = '['.$dataUser->id.']';
        }
        $typeText = 'manager_id';
        if($dataUser->level_id == 2){
            $typeText = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeText = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeText = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeText = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeText = 'mqb_id';
        }
//        dd($date);
        if($date->yesterDay == null){
            $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('sales.id, purchase.purchase_name, purchase.type, sales.invoice, sales.amount, '
                            . 'sales.sale_price, sales.sale_date, sales.created_at, users.name, users.level_id, sales.id as id_sales, sales.deleted_at, sales.is_masuk,'
                            . 'sales.user_id as id_user')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->where('sales.'.$typeText, '=', $dataUser->id)
                    ->orderBy('sales.sale_date', 'DESC')
                    ->get();
        } else {
            $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('sales.id, purchase.purchase_name, purchase.type, sales.invoice, sales.amount, '
                            . 'sales.sale_price, sales.sale_date, sales.created_at, users.name, users.level_id, sales.id as id_sales, sales.deleted_at, sales.is_masuk,'
                            . 'sales.user_id as id_user')
                    ->whereDate('sales.sale_date', '=', $date->yesterDay)
                    ->whereNull('sales.deleted_at')
                    ->where('sales.'.$typeText, '=', $dataUser->id)
                    ->orderBy('sales.sale_date', 'DESC')
                    ->get();
        }
        return $sql;
    }
    
    public function getDataSalesAdm($date, $cbg){
        $sql = DB::table('item_purchase')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('sales', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('sum(sales.sale_price) as sale_price, sales.sale_date, users.name, users.level_id, users.cabang_name, sales.invoice, sales.user_id')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->where('sales.is_safra', '=', 0)
                    ->where('users.cabang_id', '=', $cbg)
                    ->groupBy('sales.sale_date')
                    ->groupBy('users.name')
                    ->groupBy('users.level_id')
                    ->groupBy('users.cabang_name')
                    ->groupBy('sales.invoice')
                    ->groupBy('sales.user_id')
                    ->orderBy('sales.sale_date', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getLastWeek(){
        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);
        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d");
        $data = (object) array(
            'startDay' => $start_week,
            'endDay' => $end_week
        );
        return $data;
    }
    
    public function getThisMonth(){
        $start_day = date("Y-m-01");
        $end_day = date("Y-m-t");
        $getYesterday = date('Y-m-d',strtotime("-1 days"));
        $data = (object) array(
            'startDay' => $start_day,
            'endDay' => $end_day,
            'yesterDay' => $getYesterday
        );
        return $data;
    }
    
     public function getDataSalesManagerById($id, $mId){
        $sql = DB::table('item_purchase')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('sales', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('purchase.purchase_name, purchase.type, sales.invoice, sales.amount, '
                            . 'sales.sale_price, sales.sale_date, sales.created_at, users.name, users.level_id, item_purchase.price, sales.id, '
                            . 'item_purchase.id as id_item_purchase, item_purchase.sisa')
                    ->where('sales.id', '=', $id)
//                    ->where('item_purchase.manager_id', '=', $mId)
                    ->whereNull('sales.deleted_at')
                    ->first();
        return $sql;
    }
    
    public function getManagerReport($mType, $id, $date){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('sum(sales.sale_price) as jml_price')
                    ->where('sales.'.$mType, '=', $id)
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->first();
        return $sql;
    }
    
    public function getManagerReportType($mType, $id, $date){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('sum(sales.sale_price) as jml_price, purchase.type as type_b')
                    ->where('sales.'.$mType, '=', $id)
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->groupBy('purchase.type')
                    ->get();
        return $sql;
    }
    
    public function getManagerReportDownline($mType, $id, $date, $downline){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('users', 'users.id', '=', 'sales.'.$downline)
                    ->selectRaw('sum(sales.sale_price) as jml_price, users.name, sales.'.$downline)
                    ->where('sales.'.$mType, '=', $id)
                    ->whereNotNull('sales.'.$downline)
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->groupBy('sales.'.$downline)
                    ->groupBy('users.name')
                    ->get();
        return $sql;
    }
    
    public function getReportManagerDetail($mType, $id, $date){
        $sql = DB::table('sales')
                    ->join('users as u', 'u.id', '=', 'sales.user_id')
                    ->leftJoin('users as ua', 'ua.id', '=', 'sales.asmen_id')
                    ->leftJoin('users as ut', 'ut.id', '=', 'sales.tld_id')
                    ->selectRaw('sales.user_id, sum(sales.sale_price) as tot_price, u.name, u.level_id, sales.sale_date, 
                                            ua.name as asmen_name, ut.name as tld_name, ua.level_id as ua_level_id, ut.level_id as ut_level_id')
                    ->where('sales.'.$mType, '=', $id)
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->groupBy('.'.$mType)
                    ->groupBy('ua.name')
                    ->groupBy('ut.name')
                    ->groupBy('sales.user_id')
                    ->groupBy('u.name')
                    ->groupBy('sales.sale_date')
                    ->groupBy('u.level_id')
                    ->groupBy('ua.level_id')
                    ->groupBy('ut.level_id')
                    ->orderBy('sales.sale_date', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getGlobalManager($mType, $date){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
                    ->join('users as u', 'u.id', '=', 'sales.'.$mType)
                    ->selectRaw('u.name, u.cabang_name, u.phone, 
		sum(case when p.type = 1 then sales.sale_price end) as sum_price_alat,
		sum(case when p.type = 1 then sales.amount end) as sum_alat,
                                    sum(case when p.type = 1 and p.id = 107 then sales.sale_price end) as sum_price_alat_10,
		sum(case when p.type = 1 and p.id = 107 then sales.amount end) as sum_alat_10,
                                    sum(case when p.type = 1 and p.id = 108 then sales.sale_price end) as sum_price_alat_20,
		sum(case when p.type = 1 and p.id = 108 then sales.amount end) as sum_alat_20,
		sum(case when p.type = 2 then sales.sale_price end) as sum_price_strip,
		sum(case when p.type = 2 then sales.amount end) as sum_strip,
		sum(case when p.type = 3 then sales.sale_price end) as sum_price_obat,
		sum(case when p.type = 3 then sales.amount end) as sum_obat,
                                    sum(case when p.type = 3 and p.id IN (92, 93, 94, 95, 96, 97, 99, 100) then sales.sale_price end) as sum_price_obat_30,
		sum(case when p.type = 3 and p.id IN (92, 93, 94, 95, 96, 97, 99, 100) then sales.amount end) as sum_obat_30,
                                    sum(case when p.type = 3 and p.is_poin = 1 then sales.sale_price end) as sum_price_obat_30_new,
		sum(case when p.type = 3 and p.is_poin = 1 then sales.amount end) as sum_obat_30_new,
                                    sum(case when p.type = 3 and p.is_poin = 0 then sales.sale_price end) as sum_price_obat_60_new,
		sum(case when p.type = 3 and p.is_poin = 0 then sales.amount end) as sum_obat_60_new,
		sum(sales.sale_price) as jml_price, 
		sum(sales.amount) as jml')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->whereNotNull('sales.'.$mType)
                    ->groupBy('sales.'.$mType)
                    ->groupBy('u.name')
                    ->groupBy('u.cabang_name')
                    ->groupBy('u.phone')
                    ->groupBy('u.id')
                    ->get();
        return $sql;
    }
    
    public function getAsmen($mType, $date){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
                    ->join('users as u', 'u.id', '=', 'sales.'.$mType)
                    ->selectRaw('u.name, u.cabang_name, 
		sum(case when p.type = 1 then sales.sale_price end) as sum_price_alat,
		sum(case when p.type = 1 then sales.amount end) as sum_alat,
		sum(case when p.type = 2 then sales.sale_price end) as sum_price_strip,
		sum(case when p.type = 2 then sales.amount end) as sum_strip,
		sum(case when p.type = 3 then sales.sale_price end) as sum_price_obat,
		sum(case when p.type = 3 then sales.amount end) as sum_obat,
		sum(sales.sale_price) as jml_price, 
		sum(sales.amount) as jml')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->whereNotNull('sales.'.$mType)
                    ->groupBy('sales.'.$mType)
                    ->groupBy('u.name')
                    ->groupBy('u.cabang_name')
                    ->groupBy('u.id')
                    ->get();
        return $sql;
    }
    
    public function getTop10SalesManager($mType, $date, $spId){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
                    ->join('users as u', 'u.id', '=', 'sales.'.$mType)
                    ->selectRaw('u.name, u.level_id, sum(sales.sale_price) as jml_price')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->where('u.sponsor_id', 'LIKE', $spId.'%')
                    ->groupBy('sales.'.$mType)
                    ->groupBy('u.name')
                    ->groupBy('u.id')
                    ->groupBy('u.level_id')
                    ->orderBy('jml_price', 'DESC')
                    ->take(10)
                    ->get();
        return $sql;
    }
    
    public function getTop10SalesManagerDashboard($mType, $date, $spId, $dataUser){
        $typeText = 'manager_id';
        if($dataUser->level_id == 2){
            $typeText = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeText = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeText = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeText = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeText = 'mqb_id';
        }
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
                    ->join('users as u', 'u.id', '=', 'sales.'.$mType)
                    ->selectRaw('u.name, u.level_id, sum(sales.sale_price) as jml_price')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->where('sales.'.$typeText, '=', $dataUser->id)
                    ->groupBy('sales.'.$mType)
                    ->groupBy('u.name')
                    ->groupBy('u.id')
                    ->groupBy('u.level_id')
                    ->orderBy('jml_price', 'DESC')
                    ->take(10)
                    ->get();
        return $sql;
    }
    
    public function getSalesAllMonthManager($mType, $date, $spId){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
                    ->join('users as u', 'u.id', '=', 'sales.'.$mType)
                    ->selectRaw('sum(sales.sale_price) as jml_price')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->where('u.sponsor_id', 'LIKE', $spId.'%')
                    ->first();
        return $sql;
    }
    
    public function getSalesAllMonthManagerDashboard($mType, $date, $spId, $dataUser){
        $typeText = 'manager_id';
        if($dataUser->level_id == 2){
            $typeText = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeText = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeText = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeText = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeText = 'mqb_id';
        }
        $sql = DB::table('sales')
                    ->selectRaw('sum(sales.sale_price) as jml_price')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->where('sales.'.$typeText, '=', $dataUser->id)
                    ->whereNull('sales.deleted_at')
                    ->first();
        return $sql;
    }
    
    public function getSalesMonthAdmin($date){
        $sql = DB::table('sales')
                    ->selectRaw('sum(sales.sale_price) as jml_price')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->first();
        return $sql;
    }
    
    public function getGlobalMyManager($mType, $date, $m_id){
        $typeM = 'manager_id';
        if($m_id->level_id == 2){
            $typeM = 'gm_id';
        }
        if($m_id->level_id == 3){
            $typeM = 'sem_id';
        }
        if($m_id->level_id == 4){
            $typeM = 'em_id';
        }
        if($m_id->level_id == 5){
            $typeM = 'sm_id';
        }
        if($m_id->level_id == 6){
            $typeM = 'mqb_id';
        }
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
                    ->join('users as u', 'u.id', '=', 'sales.'.$mType)
                    ->selectRaw('u.name, u.cabang_name, u.level_id, u.id, 
		sum(case when p.type = 1 then sales.sale_price end) as sum_price_alat,
		sum(case when p.type = 1 then sales.amount end) as sum_alat,
		sum(case when p.type = 2 then sales.sale_price end) as sum_price_strip,
		sum(case when p.type = 2 then sales.amount end) as sum_strip,
		sum(case when p.type = 3 then sales.sale_price end) as sum_price_obat,
		sum(case when p.type = 3 then sales.amount end) as sum_obat,
                                    sum(case when p.type = 1 and p.id = 107 then sales.sale_price end) as sum_price_alat_10,
		sum(case when p.type = 1 and p.id = 107 then sales.amount end) as sum_alat_10,
                                    sum(case when p.type = 1 and p.id = 108 then sales.sale_price end) as sum_price_alat_20,
		sum(case when p.type = 1 and p.id = 108 then sales.amount end) as sum_alat_20,
		sum(case when p.type = 2 then sales.sale_price end) as sum_price_strip,
		sum(case when p.type = 2 then sales.amount end) as sum_strip,
		sum(case when p.type = 3 then sales.sale_price end) as sum_price_obat,
		sum(case when p.type = 3 then sales.amount end) as sum_obat,
                                    sum(case when p.type = 3 and p.is_poin = 1 then sales.sale_price end) as sum_price_obat_30_new,
		sum(case when p.type = 3 and p.is_poin = 1 then sales.amount end) as sum_obat_30_new,
                                    sum(case when p.type = 3 and p.is_poin = 0 then sales.sale_price end) as sum_price_obat_60_new,
		sum(case when p.type = 3 and p.is_poin = 0 then sales.amount end) as sum_obat_60_new,
		sum(sales.sale_price) as jml_price, 
		sum(sales.amount) as jml')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
//                    ->where('item_purchase.manager_id', '=', $mId) //semua manager di tiap cabang tahu jml stock
//                    ->whereNotNull('sales.'.$mType)
                    ->where('sales.'.$typeM, '=', $m_id->id)
                    ->groupBy('sales.'.$mType)
                    ->groupBy('u.id')
                    ->groupBy('u.name')
                    ->groupBy('u.level_id')
                    ->groupBy('u.cabang_name')
                    ->groupBy('u.id')
                    ->get();
        return $sql;
    }
    
    public function getGlobalMyManagerNewDetailSales($date, $m_id){
        $typeM = 'manager_id';
        if($m_id->level_id == 2){
            $typeM = 'gm_id';
        }
        if($m_id->level_id == 3){
            $typeM = 'sem_id';
        }
        if($m_id->level_id == 4){
            $typeM = 'em_id';
        }
        if($m_id->level_id == 5){
            $typeM = 'sm_id';
        }
        if($m_id->level_id == 6){
            $typeM = 'mqb_id';
        }
        if($m_id->level_id == 8){
            $typeM = 'asmen_id';
        }
        $sql = DB::table('purchase')
                    ->leftJoin(DB::raw('(SELECT i.purchase_id, sum(s.amount) as total, sum(s.sale_price) as jml_price
                                FROM sales as s
                                JOIN item_purchase i ON i.id = s.item_purchase_id
                                WHERE s.'.$typeM.' = '.$m_id->id.'
                                AND s.sale_date >= "'.$date->startDay.'"
                                AND s.sale_date <= "'.$date->endDay.'"
                                AND s.is_safra = 0 
                                AND s.deleted_at IS NULL
                                GROUP BY i.purchase_id) as ip'),function($join){
                          $join->on('ip.purchase_id','=','purchase.id');
                    })
                    ->selectRaw('purchase.id, purchase.purchase_name, purchase.type, ip.total, ip.jml_price')
                    ->orderBy('purchase.type', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getGlobalMyManagerNewDetailSalesTop10($date, $m_id){
        $typeM = 'manager_id';
        if($m_id->level_id == 2){
            $typeM = 'gm_id';
        }
        if($m_id->level_id == 3){
            $typeM = 'sem_id';
        }
        if($m_id->level_id == 4){
            $typeM = 'em_id';
        }
        if($m_id->level_id == 5){
            $typeM = 'sm_id';
        }
        if($m_id->level_id == 6){
            $typeM = 'mqb_id';
        }
        if($m_id->level_id == 8){
            $typeM = 'asmen_id';
        }
        $sql = DB::table('purchase')
                    ->leftJoin(DB::raw('(SELECT i.purchase_id, sum(s.amount) as total, sum(s.sale_price) as jml_price
                                FROM sales as s
                                JOIN item_purchase i ON i.id = s.item_purchase_id
                                WHERE s.'.$typeM.' = '.$m_id->id.'
                                AND s.sale_date >= "'.$date->startDay.'"
                                AND s.sale_date <= "'.$date->endDay.'"
                                AND s.is_safra = 0 
                                AND s.deleted_at IS NULL
                                GROUP BY i.purchase_id) as ip'),function($join){
                          $join->on('ip.purchase_id','=','purchase.id');
                    })
                    ->selectRaw('purchase.id, purchase.purchase_name, purchase.type, ip.total, ip.jml_price')
                    ->orderBy('ip.jml_price', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getGlobalMyManagerNewDetailSalesSafra($date, $m_id){
        $typeM = 'manager_id';
        if($m_id->level_id == 2){
            $typeM = 'gm_id';
        }
        if($m_id->level_id == 3){
            $typeM = 'sem_id';
        }
        if($m_id->level_id == 4){
            $typeM = 'em_id';
        }
        if($m_id->level_id == 5){
            $typeM = 'sm_id';
        }
        if($m_id->level_id == 6){
            $typeM = 'mqb_id';
        }
        if($m_id->level_id == 8){
            $typeM = 'asmen_id';
        }
        $sql = DB::table('purchase')
                    ->leftJoin(DB::raw('(SELECT i.purchase_id, sum(s.amount) as total, sum(s.sale_price) as jml_price
                                FROM sales as s
                                JOIN item_purchase i ON i.id = s.item_purchase_id
                                WHERE s.'.$typeM.' = '.$m_id->id.'
                                AND s.sale_date >= "'.$date->startDay.'"
                                AND s.sale_date <= "'.$date->endDay.'"
                                AND s.is_safra = 1
                                AND s.deleted_at IS NULL
                                GROUP BY i.purchase_id) as ip'),function($join){
                          $join->on('ip.purchase_id','=','purchase.id');
                    })
                    ->selectRaw('purchase.id, purchase.purchase_name, purchase.type, ip.total, ip.jml_price')
                    ->orderBy('purchase.type', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getDataSalesReportCrew($date, $cbgId, $userType){
        $sql = DB::table('item_purchase')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('sales', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('users.name, users.level_id, sum(sales.sale_price) as sale_price, date(sales.sale_date) as date_sale, '
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_senin_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_senin_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 2) then sales.amount else 0 end) as qty_senin_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_senin_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 3) then sales.amount else 0 end) as qty_senin_obat,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_selasa_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_selasa_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 2) then sales.amount else 0 end) as qty_selasa_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_selasa_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 3) then sales.amount else 0 end) as qty_selasa_obat,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_rabu_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_rabu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 2) then sales.amount else 0 end) as qty_rabu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_rabu_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 3) then sales.amount else 0 end) as qty_rabu_obat,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_kamis_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_kamis_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 2) then sales.amount else 0 end) as qty_kamis_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_kamis_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 3) then sales.amount else 0 end) as qty_kamis_obat,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_jumat_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_jumat_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 2) then sales.amount else 0 end) as qty_jumat_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_jumat_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 3) then sales.amount else 0 end) as qty_jumat_obat,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_sabtu_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_sabtu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 2) then sales.amount else 0 end) as qty_sabtu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_sabtu_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 3) then sales.amount else 0 end) as qty_sabtu_obat,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_minggu_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_minggu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 2) then sales.amount else 0 end) as qty_minggu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_minggu_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 3) then sales.amount else 0 end) as qty_minggu_obat')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->where('users.cabang_id', '=', $cbgId)
                    ->where('users.user_type', '=', $userType)
                    ->whereNull('sales.deleted_at')
                    ->groupBy('users.name')
                    ->groupBy('users.level_id')
                    ->groupBy('users.cabang_name')
                    ->groupBy('date_sale')
                    ->orderBy('date_sale', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getSalesReportCrewWithManager($param, $cbgId, $userType){
        $sql = DB::table('item_purchase')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('sales', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('users.name, users.level_id, '
                            . 'sum(case when (purchase.type != 1) then sales.sale_price else 0 end) as sale_price,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_senin_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 2) then sales.amount else 0 end) as qty_senin_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.sale_price else 0 end) as sale_senin_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.amount else 0 end) as qty_senin_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.sale_price else 0 end) as sale_senin_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.amount else 0 end) as qty_senin_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_senin_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 1) then sales.amount else 0 end) as qty_senin_alat,'
                            
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_selasa_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 2) then sales.amount else 0 end) as qty_selasa_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.sale_price else 0 end) as sale_selasa_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.amount else 0 end) as qty_selasa_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.sale_price else 0 end) as sale_selasa_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.amount else 0 end) as qty_selasa_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_selasa_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 1) then sales.amount else 0 end) as qty_selasa_alat,'
                            
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_rabu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 2) then sales.amount else 0 end) as qty_rabu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.sale_price else 0 end) as sale_rabu_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.amount else 0 end) as qty_rabu_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.sale_price else 0 end) as sale_rabu_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.amount else 0 end) as qty_rabu_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_rabu_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 1) then sales.amount else 0 end) as qty_rabu_alat,'
                            
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_kamis_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 2) then sales.amount else 0 end) as qty_kamis_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.sale_price else 0 end) as sale_kamis_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.amount else 0 end) as qty_kamis_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.sale_price else 0 end) as sale_kamis_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.amount else 0 end) as qty_kamis_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_kamis_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 1) then sales.amount else 0 end) as qty_kamis_alat,'
                            
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_jumat_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 2) then sales.amount else 0 end) as qty_jumat_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.sale_price else 0 end) as sale_jumat_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.amount else 0 end) as qty_jumat_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.sale_price else 0 end) as sale_jumat_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.amount else 0 end) as qty_jumat_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_jumat_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 1) then sales.amount else 0 end) as qty_jumat_alat,'
                            
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_sabtu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 2) then sales.amount else 0 end) as qty_sabtu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.sale_price else 0 end) as sale_sabtu_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.amount else 0 end) as qty_sabtu_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.sale_price else 0 end) as sale_sabtu_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.amount else 0 end) as qty_sabtu_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_sabtu_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 1) then sales.amount else 0 end) as qty_sabtu_alat,'
                            
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_minggu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 2) then sales.amount else 0 end) as qty_minggu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.sale_price else 0 end) as sale_minggu_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 3 AND purchase.is_poin = 0) then sales.amount else 0 end) as qty_minggu_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.sale_price else 0 end) as sale_minggu_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 3 AND purchase.is_poin = 1) then sales.amount else 0 end) as qty_minggu_obat_poin_sgth,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_minggu_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 1) then sales.amount else 0 end) as qty_minggu_alat')
                    ->whereDate('sales.sale_date', '>=', $param->startDay)
                    ->whereDate('sales.sale_date', '<=', $param->endDay)
                    ->where('sales.'.$param->type, '=', $param->id)
                    ->where('users.cabang_id', '=', $cbgId)
                    ->where('users.user_type', '=', $userType)
                    ->whereNull('sales.deleted_at')
                    ->groupBy('users.name')
                    ->groupBy('users.id')
                    ->groupBy('users.level_id')
                    ->groupBy('users.cabang_name')
                    ->orderBy('sale_price', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getDataSalesInvoAndUserId($invo, $uid){
        $sql = DB::table('item_purchase')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('sales', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('purchase.purchase_name, purchase.type, sales.invoice, sales.amount, '
                            . 'sales.sale_price, sales.sale_date, sales.created_at, users.name, users.level_id, item_purchase.price, sales.id as id_sales, '
                            . 'item_purchase.id as id_item_purchase, item_purchase.sisa, sales.user_id')
                    ->where('sales.invoice', '=', $invo)
                    ->where('sales.user_id', '=', $uid)
                    ->whereNull('sales.deleted_at')
                    ->get();
        return $sql;
    }
    
    public function getDataSalesActionById($id, $uid){
        $sql = DB::table('item_purchase')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('sales', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('purchase.purchase_name, purchase.type, sales.invoice, sales.amount, '
                            . 'sales.sale_price, sales.sale_date, sales.created_at, users.name, users.level_id, item_purchase.price, sales.id, '
                            . 'item_purchase.id as id_item_purchase, item_purchase.sisa')
                    ->where('sales.id', '=', $id)
                    ->where('sales.user_id', '=', $uid)
                    ->whereNull('sales.deleted_at')
                    ->first();
        return $sql;
    }
    
    public function getTop10SalesForAdmin($date){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
                    ->join('users as u', 'u.id', '=', 'sales.user_id')
                    ->selectRaw('u.name, u.level_id, sum(sales.sale_price) as jml_price, u.cabang_name,  
                            sum(case when p.type != 1 then sales.sale_price end) as sum_price_new,
                            sum(case when p.type = 2 then sales.amount end) as sum_strip,
                            sum(case when p.type = 3 then sales.amount end) as sum_obat,
                            sum(case when p.type = 3 AND p.is_poin = 0 then sales.amount end) as sum_obat_full,
                            sum(case when p.type = 3 AND p.is_poin = 1 then sales.amount end) as sum_obat_sgth')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->groupBy('sales.user_id')
                    ->groupBy('u.name')
                    ->groupBy('u.id')
                    ->groupBy('u.level_id')
                    ->groupBy('u.cabang_name')
                    ->orderBy('jml_price', 'DESC')
                    ->take(40) //10
                    ->get();
        return $sql;
    }
    
    public function getTop10SalesForAdminCabang($date, $cbg){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
                    ->join('users as u', 'u.id', '=', 'sales.user_id')
                    ->selectRaw('u.name, u.level_id, sum(sales.sale_price) as jml_price, u.cabang_name')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->where('u.cabang_id', '=', $cbg)
                    ->whereNull('sales.deleted_at')
                    ->groupBy('sales.user_id')
                    ->groupBy('u.name')
                    ->groupBy('u.id')
                    ->groupBy('u.level_id')
                    ->groupBy('u.cabang_name')
                    ->orderBy('jml_price', 'DESC')
                    ->take(20)
                    ->get();
        return $sql;
    }
    
    public function getGlobalStockMyManager($mType, $date, $dataUser){
        $user_id = $dataUser->id;
        if($dataUser->level_id == 2){
            $typeM = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeM = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeM = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeM = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeM = 'mqb_id';
        }
        if($dataUser->level_id == 7){
            $typeM = 'manager_id';
        }
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
                    ->join('users as u', 'u.id', '=', 'sales.'.$mType)
                    ->selectRaw('u.name, u.cabang_name, 
		sum(case when p.type = 1 then sales.amount end) as sum_alat,
		sum(case when p.type = 2 then sales.amount end) as sum_strip,
		sum(case when p.type = 3 then sales.amount end) as sum_obat,
		sum(sales.amount) as jml')
//                    ->selectRaw('sum(sales.amount) as aaa')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
//                    ->where('item_purchase.manager_id', '=', $mId) //semua manager di tiap cabang tahu jml stock
                    ->where('sales.'.$typeM, '=', $user_id)
//                    ->whereNotNull('sales.'.$mType)
                    ->groupBy('sales.'.$mType)
                    ->groupBy('u.name')
                    ->groupBy('u.cabang_name')
                    ->groupBy('u.id')
                    ->get();
        return $sql;
    }
    
    public function getDataSalesReportCrewForManager($date, $cbgId, $userType, $dataUser){
        $SpId = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $SpId = '['.$dataUser->parent_id.']';
        }
        $sql = DB::table('item_purchase')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('sales', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('users.name, users.level_id, '
                            . 'sum(case when (purchase.type != 1) then sales.sale_price else 0 end) as sale_price,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_senin_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_senin_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 2) then sales.amount else 0 end) as qty_senin_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_senin_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 0 AND purchase.type = 3) then sales.amount else 0 end) as qty_senin_obat,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_selasa_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_selasa_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 2) then sales.amount else 0 end) as qty_selasa_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_selasa_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 1 AND purchase.type = 3) then sales.amount else 0 end) as qty_selasa_obat,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_rabu_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_rabu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 2) then sales.amount else 0 end) as qty_rabu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_rabu_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 2 AND purchase.type = 3) then sales.amount else 0 end) as qty_rabu_obat,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_kamis_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_kamis_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 2) then sales.amount else 0 end) as qty_kamis_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_kamis_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 3 AND purchase.type = 3) then sales.amount else 0 end) as qty_kamis_obat,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_jumat_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_jumat_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 2) then sales.amount else 0 end) as qty_jumat_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_jumat_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 4 AND purchase.type = 3) then sales.amount else 0 end) as qty_jumat_obat,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_sabtu_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_sabtu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 2) then sales.amount else 0 end) as qty_sabtu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_sabtu_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 5 AND purchase.type = 3) then sales.amount else 0 end) as qty_sabtu_obat,'
//                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 1) then sales.sale_price else 0 end) as sale_minggu_alat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 2) then sales.sale_price else 0 end) as sale_minggu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 2) then sales.amount else 0 end) as qty_minggu_strip,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 3) then sales.sale_price else 0 end) as sale_minggu_obat,'
                            . 'sum(case when (WEEKDAY(sales.sale_date) = 6 AND purchase.type = 3) then sales.amount else 0 end) as qty_minggu_obat')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->where('users.cabang_id', '=', $cbgId)
                    ->where('users.user_type', '=', $userType)
                    ->where('users.sponsor_id',  'LIKE', $SpId.'%')
                    ->whereNull('sales.deleted_at')
                    ->groupBy('users.name')
                    ->groupBy('users.level_id')
                    ->groupBy('users.cabang_name')
                    ->orderBy('sale_price', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getSalesAllMonthManagerDashboardBar($mType, $date, $spId, $dataUser){
        $typeText = 'manager_id';
        if($dataUser->level_id == 2){
            $typeText = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeText = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeText = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeText = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeText = 'mqb_id';
        }
        $sql = DB::table('sales')
                    ->selectRaw('CONCAT(DATE_FORMAT(sales.sale_date, "%m"), "-", YEAR(sales.sale_date)) AS month_name, sum(sales.sale_price) as jml_price,'
                            . 'YEAR(sales.sale_date) as tahun, DATE_FORMAT(sales.sale_date, "%m") as bulan')
                    ->whereDate('sales.sale_date', '>=', date('Y-m-01', strtotime("-8 Months")))
                    ->whereDate('sales.sale_date', '<=', date('Y-m-d'))
                    ->where('sales.'.$typeText, '=', $dataUser->id)
                    ->whereNull('sales.deleted_at')
                    ->groupBy('month_name')
                    ->groupBy('tahun')
                    ->groupBy('bulan')
                    ->orderBy('tahun', 'ASC')
                    ->orderBy('bulan', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getSalesAllMonthManagerDashboardAdminBar(){
        $sql = DB::table('sales')
                    ->selectRaw('CONCAT(DATE_FORMAT(sales.sale_date, "%m"), "-", YEAR(sales.sale_date)) AS month_name, sum(sales.sale_price) as jml_price,'
                            . 'YEAR(sales.sale_date) as tahun, DATE_FORMAT(sales.sale_date, "%m") as bulan')
                    ->whereDate('sales.sale_date', '>=', date('Y-m-01', strtotime("-8 Months")))
                    ->whereDate('sales.sale_date', '<=', date('Y-m-d'))
                    ->whereNull('sales.deleted_at')
                    ->groupBy('month_name')
                    ->groupBy('tahun')
                    ->groupBy('bulan')
                    ->orderBy('tahun', 'ASC')
                    ->orderBy('bulan', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getDataSalesZero($dataUser, $date){
        $typeText = 'manager_id';
        if($dataUser->level_id == 2){
            $typeText = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeText = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeText = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeText = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeText = 'mqb_id';
        }
//        dd($date);
        if($date->yesterDay == null){
            $sql = DB::table('absen')
                    ->join('users', 'absen.user_id', '=', 'users.id')
                    ->selectRaw('users.name, users.level_id, absen.masuk_at, absen.is_masuk')
                    ->where('absen.is_sales', '=', 0)
                    ->whereDate('absen.masuk_at', '>=', $date->startDay)
                    ->whereDate('absen.masuk_at', '<=', $date->endDay)
                    ->where('absen.'.$typeText, '=', $dataUser->id)
                    ->orderBy('absen.masuk_at', 'DESC')
                    ->get();
        } else {
            $sql = DB::table('absen')
                    ->join('users', 'absen.user_id', '=', 'users.id')
                    ->selectRaw('users.name, users.level_id, absen.masuk_at, absen.is_masuk')
                    ->where('absen.is_sales', '=', 0)
                    ->whereDate('absen.masuk_at', '=', $date->yesterDay)
                    ->where('absen.'.$typeText, '=', $dataUser->id)
                    ->orderBy('absen.masuk_at', 'DESC')
                    ->get();
        }
        return $sql;
    }
    
    public function getOmsetById($date, $dataUser, $purchase_type){
        $user_id = $dataUser->id;
        $typeM = 'asmen_id';
        if($dataUser->level_id == 2){
            $typeM = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeM = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeM = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeM = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeM = 'mqb_id';
        }
        if($dataUser->level_id == 7){
            $typeM = 'manager_id';
        }
        if($dataUser->level_id == 7){
            $typeM = 'manager_id';
        }
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('sum(sales.amount*purchase.hpp) as omset_harian')
                    ->where('sales.'.$typeM, '=', $user_id)
                    ->whereDate('sales.sale_date', '=', $date)
                    ->where('purchase.type', '=', $purchase_type)
                    ->whereNull('sales.deleted_at')
                    ->first();
        return $sql;
    }
    
    public function getGlobalOrgAllManager($id, $date){
        $sql = DB::table('users')
                    ->selectRaw('id, name, level_id, cabang_name')
                    ->where('is_active', '=', 1)
                    ->where('structure_type', '=', 2)
                    ->where('top_structure_id', '=', $id)
                    ->orderBy('cabang_name', 'ASC')
                    ->get();
        $sql2 = array();
        if($sql != null){
            foreach($sql as $row){
                $typeM = 'manager_id';
                if($row->level_id == 2){
                    $typeM = 'gm_id';
                }
                if($row->level_id == 3){
                    $typeM = 'sem_id';
                }
                if($row->level_id == 4){
                    $typeM = 'em_id';
                }
                if($row->level_id == 5){
                    $typeM = 'sm_id';
                }
                if($row->level_id == 6){
                    $typeM = 'mqb_id';
                }
                $sql2[] = DB::table('sales')
                            ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                            ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
                            ->join('users as u', 'u.id', '=', 'sales.'.$typeM)
                            ->selectRaw('u.name, u.cabang_name, u.level_id, u.id, 
                                                sum(case when p.type = 1 then sales.sale_price end) as sum_price_alat,
                                                sum(case when p.type = 1 then sales.amount end) as sum_alat,
                                                sum(case when p.type = 2 then sales.sale_price end) as sum_price_strip,
                                                sum(case when p.type = 2 then sales.amount end) as sum_strip,
                                                sum(case when p.type = 3 then sales.sale_price end) as sum_price_obat,
                                                sum(case when p.type = 3 then sales.amount end) as sum_obat,
                                                sum(sales.sale_price) as jml_price, 
                                                sum(sales.amount) as jml')
                            ->whereDate('sales.sale_date', '>=', $date->startDay)
                            ->whereDate('sales.sale_date', '<=', $date->endDay)
                            ->whereNull('sales.deleted_at')
                            ->where('sales.'.$typeM, '=', $row->id)
                            ->groupBy('sales.'.$typeM)
                            ->groupBy('u.id')
                            ->groupBy('u.name')
                            ->groupBy('u.level_id')
                            ->groupBy('u.cabang_name')
                            ->groupBy('u.id')
                            ->first();
            }
        }
        return $sql2;
    }
    
    public function getDetailProductSales($date, $m_id){
        $typeM = 'manager_id';
        if($m_id->level_id == 2){
            $typeM = 'gm_id';
        }
        if($m_id->level_id == 3){
            $typeM = 'sem_id';
        }
        if($m_id->level_id == 4){
            $typeM = 'em_id';
        }
        if($m_id->level_id == 5){
            $typeM = 'sm_id';
        }
        if($m_id->level_id == 6){
            $typeM = 'mqb_id';
        }
        if($m_id->level_id == 8){
            $typeM = 'asmen_id';
        }
        if($m_id->level_id == 9){
            $typeM = 'tld_id';
        }
        $sql = DB::table('purchase')
                    ->leftJoin(DB::raw('(SELECT i.purchase_id, sum(s.amount) as total, sum(s.sale_price) as jml_price
                                FROM sales as s
                                JOIN item_purchase i ON i.id = s.item_purchase_id
                                WHERE s.'.$typeM.' = '.$m_id->id.'
                                AND s.sale_date >= "'.$date->startDay.'"
                                AND s.sale_date <= "'.$date->endDay.'"
                                AND s.is_safra = 0
                                AND s.deleted_at IS NULL
                                GROUP BY i.purchase_id) as ip'),function($join){
                          $join->on('ip.purchase_id','=','purchase.id');
                    })
                    ->selectRaw('purchase.id, purchase.purchase_name, purchase.type, ip.total, ip.jml_price, purchase.is_poin')
                    ->where('ip.total', '>', 0)
                    ->orderBy('purchase.type', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getDataSalesSafra($dataUser, $date){
        $mySponsor = $dataUser->sponsor_id.',['.$dataUser->id.']';
        if($dataUser->sponsor_id == null){
            $mySponsor = '['.$dataUser->id.']';
        }
        $typeText = 'manager_id';
        if($dataUser->level_id == 2){
            $typeText = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeText = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeText = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeText = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeText = 'mqb_id';
        }
//        dd($date);
        if($date->yesterDay == null){
            $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('sales.id, purchase.purchase_name, purchase.type, sales.invoice, sales.amount, '
                            . 'sales.sale_price, sales.sale_date, sales.created_at, users.name, users.level_id, sales.id as id_sales, sales.deleted_at, sales.is_masuk,'
                            . 'sales.user_id as id_user, sales.bank_name, sales.account_no, sales.account_name')
                    ->whereDate('sales.sale_date', '>=', $date->startDay)
                    ->whereDate('sales.sale_date', '<=', $date->endDay)
                    ->whereNull('sales.deleted_at')
                    ->where('sales.'.$typeText, '=', $dataUser->id)
                    ->where('sales.is_safra', '=', 1)
                    ->orderBy('sales.sale_date', 'DESC')
                    ->get();
        } else {
            $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('sales.id, purchase.purchase_name, purchase.type, sales.invoice, sales.amount, '
                            . 'sales.sale_price, sales.sale_date, sales.created_at, users.name, users.level_id, sales.id as id_sales, sales.deleted_at, sales.is_masuk,'
                            . 'sales.user_id as id_user, sales.bank_name, sales.account_no, sales.account_name, users.cabang_name')
                    ->whereDate('sales.sale_date', '=', $date->yesterDay)
                    ->whereNull('sales.deleted_at')
                    ->where('sales.is_safra', '=', 1)
                    ->where('sales.'.$typeText, '=', $dataUser->id)
                    ->orderBy('sales.sale_date', 'DESC')
                    ->get();
        }
        return $sql;
    }
    
    public function getSafraSalesForAdmin(){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('sales.id, purchase.purchase_name, purchase.type, sales.invoice, sales.amount, '
                            . 'sales.sale_price, sales.sale_date, sales.created_at, users.name, users.level_id, sales.id as id_sales, sales.deleted_at, sales.is_masuk,'
                            . 'sales.user_id as id_user, sales.bank_name, sales.account_no, sales.account_name, sales.safra_status, sales.status_at, users.cabang_name ')
                    ->whereNull('sales.deleted_at')
                    ->where('sales.is_safra', '=', 1)
                    ->orderBy('sales.sale_date', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getSafraSalesForAdminByID($id){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('users', 'sales.user_id', '=', 'users.id')
                    ->selectRaw('sales.id, purchase.purchase_name, purchase.type, sales.invoice, sales.amount, '
                            . 'sales.sale_price, sales.sale_date, sales.created_at, users.name, users.level_id, sales.id as id_sales, sales.deleted_at, sales.is_masuk,'
                            . 'sales.user_id as id_user, sales.bank_name, sales.account_no, sales.account_name, sales.safra_status, sales.status_at, users.cabang_name, '
                            . 'sales.reason ')
                    ->where('sales.id', '=', $id)
                    ->whereNull('sales.deleted_at')
                    ->where('sales.is_safra', '=', 1)
                    ->orderBy('sales.sale_date', 'DESC')
                    ->first();
        return $sql;
    }
    
    
}
