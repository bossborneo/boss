<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Member extends Model {
    
    public function getAllManager($cbg = null){
        if($cbg == null){
            $sql = DB::table('users')
                    ->selectRaw('id, username, name, level_id, cabang_name')
                    ->where('user_type', '=', 5)
                    ->where('is_active', '=', 1)
                    ->orderBy('cabang_id', 'ASC')
                    ->orderBy('level_id', 'ASC')
                    ->orderBy('name', 'ASC')
                    ->get();
        } else {
            $sql = DB::table('users')
                    ->selectRaw('id, username, name, level_id, cabang_name')
                    ->where('cabang_id', '=', $cbg)
                    ->where('user_type', '=', 5)
                    ->where('is_active', '=', 1)
                    ->orderBy('cabang_id', 'ASC')
                    ->orderBy('level_id', 'ASC')
                    ->orderBy('name', 'ASC')
                    ->get();
        }
        
        return $sql;
    }
    
    public function getCekUsername($username){
        $sql = DB::table('users')
                    ->selectRaw('users.id')
                    ->where('users.username', '=', $username)
                    ->first();
        return $sql;
    }
    
    public function getCekUsersInCabang($cabangId){
        $sql = DB::table('users')
                    ->selectRaw('name, id')
                    ->where('cabang_id', '=', $cabangId)
                    ->where('is_active', '=', 1)
                    ->count();
        return $sql;
    }
    
    public function getInsertUser($data){
        try {
            DB::table('users')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateMember($fieldName, $name, $data){
        try {
            DB::table('users')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAllDownline($cabang_id, $spId){
        $sql = DB::table('users')
                    ->selectRaw('id, name, cabang_name, level_id, phone')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('cabang_id', '=', $cabang_id)
                    ->where('sponsor_id', 'LIKE', '%['.$spId.']%')
                    ->orderBy('level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAllParent($spId, $cabang_id){
        $sql = DB::table('users')
                    ->selectRaw('id, name, cabang_name, level_id, phone')
                    ->where('user_type', '=', 5)
                    ->where('is_active', '=', 1)
                    ->where('cabang_id', '=', $cabang_id)
                    ->where('level_id', '=', $spId)
                    ->orderBy('level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAllParentManager($lvl, $cabang_id, $dataUser){
        $sql = DB::table('users')
                    ->selectRaw('id, name, cabang_name, level_id, phone')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('cabang_id', '=', $cabang_id)
                    ->where('level_id', '<=', $lvl)
                    ->where('sponsor_id', 'LIKE', $dataUser->sponsor_id.'%')
                    ->orderBy('level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getCountAllDownline($cabang_id, $spId){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('cabang_id', '=', $cabang_id)
                    ->where('sponsor_id', 'LIKE', '%['.$spId.']%')
                    ->count();
        return $sql;
    }
    
    public function getParentId($id){
        $sql = DB::table('users')
                    ->selectRaw('id, sponsor_id, parent_id, name, level_id')
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getUsername(){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->count();
        $tmp = $sql+1;
        $code = sprintf("%05s", $tmp);
        $newUsername =  'boss'.date('Y').'_'.$code;
        return $newUsername;
    }
    
    public function getUserId($id){
        $sql = DB::table('users')
                        ->where('id', '=', $id)
                        ->first();
        return $sql;
    }
    
    public function getAllCrew($lvl){
        $sql = DB::table('users')
                    ->selectRaw('id, name,  level_id')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('level_id', '=', $lvl)
                    ->orderBy('name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAutomaticPassword(){
        $pass = 'boss2018';
        return $pass;
    }
    
    public function getUpperUserId($id){
        $sql = DB::table('users')
                        ->selectRaw('id, name,  level_id')
                        ->whereIn('id', $id)
                        ->orderBy('level_id', 'DESC')
                        ->get();
        return $sql;
    }
    
    public  function getLevelLoop($data, $lvl){
        foreach ($data as $key) {
            if ($key->level_id === $lvl) {
                return $key->id;
            }
        }
        return null;
    }
    
    public function getUpperUserIdPart2($data, $lvl){
        //cara pertama di looping order by i desc, cari pakai loop
        //cara kedua query satu2 where level_id order by 
        $sql = DB::table('users')
                        ->selectRaw('id, name,  level_id')
                        ->whereIn('id', $data)
                        ->where('level_id', '=', $lvl)
                        ->orderBy('id', 'DESC')
                        ->first();
        return $sql;
    }
    
    public function getInsertHistoryLevel($data){
        try {
            DB::table('history_level')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getNewestAllDownline($cabang_id, $spId, $arraySpId){
        $sql = DB::table('users')
                    ->leftJoin('history_level', 'history_level.user_id', '=', 'users.id')
                    ->selectRaw('users.id, users.name, users.cabang_name, users.level_id, users.phone, '
                            . '(case when max(history_level.active_at) is null then users.created_at else max(history_level.active_at) end) as active_at')
                    ->where('is_active', '=', 1)
                    ->where('cabang_id', '=', $cabang_id)
                    ->where('sponsor_id', 'LIKE', $spId.'%')
                    ->groupBy('users.id')
                    ->groupBy('users.name')
                    ->groupBy('users.cabang_name')
                    ->groupBy('users.level_id')
                    ->groupBy('users.phone')
                    ->groupBy('users.created_at')
                    ->orderBy('users.level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAllDownlineManager($cabang_id, $spId, $arraySpId){
        $sql = DB::table('users')
                    ->leftJoin('history_level', 'history_level.user_id', '=', 'users.id')
                    ->selectRaw('users.id, users.name, users.cabang_name, users.level_id, users.phone, '
                            . '(case when max(history_level.active_at) is null then users.created_at else max(history_level.active_at) end) as active_at')
                    ->where('users.is_active', '=', 1)
                    ->whereIn('level_id', $arraySpId)
                    ->where('users.cabang_id', '=', $cabang_id)
                    ->where('users.sponsor_id', 'LIKE', $spId.'%')
                    ->groupBy('users.id')
                    ->groupBy('users.name')
                    ->groupBy('users.cabang_name')
                    ->groupBy('users.level_id')
                    ->groupBy('users.phone')
                    ->groupBy('users.created_at')
                    ->orderBy('users.level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getNewAllDownline($cabang_id, $spId, $arraySpId){
        $sql = DB::table('users')
                    ->leftJoin('history_level', 'history_level.user_id', '=', 'users.id')
                    ->selectRaw('users.id, users.name, users.cabang_name, users.level_id, users.phone, '
                            . '(case when max(history_level.active_at) is null then users.created_at else max(history_level.active_at) end) as active_at')
                    ->where('users.user_type', '=', 10)
                    ->where('users.is_active', '=', 1)
                    ->whereIn('level_id', $arraySpId)
                    ->where('users.cabang_id', '=', $cabang_id)
                    ->where('users.sponsor_id', 'LIKE', $spId.'%')
                    ->groupBy('users.id')
                    ->groupBy('users.name')
                    ->groupBy('users.cabang_name')
                    ->groupBy('users.level_id')
                    ->groupBy('users.phone')
                    ->groupBy('users.created_at')
                    ->orderBy('users.level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getNewCountAllDownline($cabang_id, $spId, $arraySpId){
        $sql = DB::table('users')
                    ->selectRaw('id, name, cabang_name, level_id, phone')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->whereIn('level_id', $arraySpId)
                    ->where('cabang_id', '=', $cabang_id)
                    ->where('sponsor_id', 'LIKE', $spId.'%')
                    ->count();
        return $sql;
    }
    
    public function getNewCountAllLevel(){
        $sql = DB::table('users')
                    ->leftJoin('level', 'level.id', '=', 'users.level_id')
                    ->selectRaw('level.level_name, '
                            . 'sum(case when is_active = 1 then 1 else 0 end) as total_level')
                    ->whereNotNull('users.level_id')
                    ->groupBy('users.level_id')
                    ->groupBy('level.level_name')
                    ->get();
        return $sql;
    }
    
    public function getUpperAsmenTld($cabId, $id, $lvl){
        $sql = DB::table('users')
                        ->selectRaw('name,  level_id')
                        ->whereIn('id', $id)
                        ->where('cabang_id', '=', $cabId)
                        ->where('level_id', '=', $lvl)
                        ->where('is_active', '=', 1)
                        ->orderBy('level_id', 'ASC')
                        ->orderBy('id', 'DESC')
                        ->first();
        return $sql;
    }
    
    public function getManagerFromCabang($cbgId){
        $arrayManager = array(2, 3, 4, 5, 6, 7, 8, 9);
        $sql = DB::table('users')
                    ->selectRaw('id, name, level_id, cabang_name')
//                    ->where('user_type', '=', 5)
                    ->whereIn('level_id', $arrayManager)
                    ->where('is_active', '=', 1)
                    ->where('cabang_id', '=', $cbgId)
                    ->orderBy('level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getNewAllDownlineForAdmin($cabang_id, $spId, $arraySpId){
        $sql = DB::table('users')
                    ->selectRaw('level_id')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->whereIn('level_id', $arraySpId)
                    ->where('cabang_id', '=', $cabang_id)
                    ->where('sponsor_id', 'LIKE', $spId.'%')
                    ->groupBy('level_id')
                    ->orderBy('level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAllAdmin(){
        $sql = DB::table('users')
                    ->selectRaw('id, name, username, permissions')
                    ->where('user_type', '=', 3)
                    ->where('is_active', '=', 1)
                    ->get();
        return $sql;
    }
    
    public function getAdminById($id){
        $sql = DB::table('users')
                    ->selectRaw('id, name, username, permissions')
                    ->where('id', '=', $id)
                    ->where('user_type', '=', 3)
                    ->where('is_active', '=', 1)
                    ->first();
        return $sql;
    }
    
    public function getAllDownlineAjax($cabang_id, $spId){
        $sql = DB::table('users')
                    ->join('level', 'level.id', '=', 'users.level_id')
                    ->selectRaw('users.id, users.name, level.level_name, users.parent_id')
                    ->where('users.is_active', '=', 1)
                    ->where('users.cabang_id', '=', $cabang_id)
                    ->where('users.sponsor_id', 'LIKE', $spId.'%')
                    ->orderBy('users.level_id', 'ASC')
                    ->get();
        $dataRow = array();
        foreach($sql as $row){
            $dataRow[$row->id] = array(
                'parent_id' => $row->parent_id, 
                'name' => $row->name
            );
        }
        return $sql;
    }
    
    public function getAllMyTree($parent_id){
        $sql = DB::table('users')
                    ->join('level', 'level.id', '=', 'users.level_id')
                    ->selectRaw('users.id, users.name, level.level_name, users.parent_id')
                    ->where('is_active', '=', 1)
                    ->where('parent_id', '=', $parent_id)
                    ->orderBy('level_id', 'ASC')
                    ->get();
        $dataRow = array();
        foreach($sql as $row){
            $dataRow[] = array(
                'id' => $row->id,
                'name' => $row->name,
                'text' => ' ('.$row->level_name.') '.$row->name,
                'level_name' => $row->level_name,
                'link' => '#',
                'parent_id' => $row->parent_id, 
                'children' => $this->getAllMyTree($row->id)
            );
        }
        return $dataRow;
    }
    
    public function buildTree($array, $currentParent, $currLevel = 0, $prevLevel = -1) {
        foreach ($array as $categoryId => $category) {
            if ($currentParent == $category['parent_id']) {
                if ($currLevel > $prevLevel) echo "<ol id='menutree'>"; 
                if ($currLevel == $prevLevel) echo "</li>";
                echo '<li> <label class="menu_label" for='.$categoryId.'>'.$category['name'].'</label><input type="checkbox" id='.$categoryId.' />';
                if ($currLevel > $prevLevel) { $prevLevel = $currLevel; }
                $currLevel++; 
                buildTree ($array, $categoryId, $currLevel, $prevLevel);
                $currLevel--;   
            }
        }
        if ($currLevel == $prevLevel) echo "</li> </ol>";
    }
    
    public function getAllDownlineForPindah($cabang_id){
        $sql = DB::table('users')
                    ->selectRaw('id, name, level_id, cabang_name')
                    ->where('is_active', '=', 1)
                    ->where('cabang_id', '=', $cabang_id)
                    ->orderBy('level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
//    public function getAllMemberCabang($cabang_id){
//        $sql = DB::table('users')
//                    ->selectRaw('id, name, level_id')
//                    ->where('is_active', '=', 1)
//                    ->where('cabang_id', '=', $cabang_id)
//                    ->orderBy('level_id', 'ASC')
//                    ->get();
//        return $sql;
//    }
    
    public function getStructureSponsor($parent_id){
        $sql = DB::table('users')
                    ->join('level', 'level.id', '=', 'users.level_id')
                    ->selectRaw('users.id, users.name, level.level_name, users.parent_id')
                    ->where('is_active', '=', 1)
                    ->where('parent_id', '=', $parent_id)
                    ->orderBy('level_id', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    
    public function getAllMemberCabang($cabang_id){
        $sql = DB::table('users')
                    ->leftJoin('history_level', 'history_level.user_id', '=', 'users.id')
                    ->selectRaw('users.id, users.name, users.cabang_name, users.level_id, users.phone, '
                            . '(case when max(history_level.active_at) is null then users.active_at else max(history_level.active_at) end) as active_at')
                    ->where('users.is_active', '=', 1)
                    ->where('users.cabang_id', '=', $cabang_id)
                    ->groupBy('users.id')
                    ->groupBy('users.name')
                    ->groupBy('users.cabang_name')
                    ->groupBy('users.level_id')
                    ->groupBy('users.phone')
                    ->groupBy('users.active_at')
                    ->orderBy('users.level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAllDataMemberById($id){
        $sql = DB::table('users')
                    ->leftJoin('history_level', 'history_level.user_id', '=', 'users.id')
                    ->selectRaw('users.id, users.name, users.cabang_name, users.cabang_id, users.level_id, '
                            . '(case when max(history_level.active_at) is null then users.active_at else max(history_level.active_at) end) as active_at, '
                            . 'max(history_level.active_at) history_active_at')
                    ->where('users.is_active', '=', 1)
                    ->where('users.id', '=', $id)
                    ->groupBy('users.id')
                    ->groupBy('users.name')
                    ->groupBy('users.cabang_name')
                    ->groupBy('users.level_id')
                    ->groupBy('users.cabang_id')
                    ->groupBy('users.active_at')
                    ->orderBy('users.level_id', 'ASC')
                    ->first();
        return $sql;
    }
    
    public function getHistoryLevelUserId($id){
        $sql = DB::table('history_level')
                        ->where('user_id', '=', $id)
                        ->orderBy('id', 'DESC')
                        ->first();
        return $sql;
    }
    
    public function getUpdateHistoryLevel($fieldName, $name, $data){
        try {
            DB::table('history_level')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAllDownlineAbsen($cabang_id, $spId, $arraySpId){
        $sql = DB::table('users')
                    ->selectRaw('users.id, users.name, users.cabang_name, users.level_id')
                    ->where('users.user_type', '=', 10)
                    ->where('users.is_active', '=', 1)
                    ->whereIn('level_id', $arraySpId)
                    ->where('users.cabang_id', '=', $cabang_id)
                    ->where('users.sponsor_id', 'LIKE', $spId.'%')
                    ->orderBy('users.level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getInsertAbsen($data){
        try {
            DB::table('absen')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAbsen($dataUser){
        $typeText = 'manager_id';
        if($dataUser->level_id == 2){
            $typeText = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeText = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeText = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeText = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeText = 'mqb_id';
        }
        $sql = DB::table('absen')
                    ->join('users', 'users.id', '=', 'absen.user_id')
                    ->selectRaw('absen.user_id, users.name, users.level_id,'
                            . 'count(case when absen.is_masuk = 1 then absen.user_id end) as jml_masuk,'
                            . 'count(case when absen.is_masuk = 2 then absen.user_id end) as jml_sakit,'
                            . 'count(case when absen.is_masuk = 3 then absen.user_id end) as jml_izin,'
                            . 'count(case when absen.is_masuk = 4 then absen.user_id end) as jml_alpa')
                    ->whereMonth('absen.masuk_at', '=', date('m'))
                    ->where('absen.'.$typeText, '=', $dataUser->id)
                    ->groupBy('absen.user_id')
                    ->groupBy('users.name')
                    ->groupBy('users.level_id')
                    ->orderBy('users.level_id')
                    ->get();
        return $sql;
    }
    
    public function getAbsenDetail($user_id){
        $sql = DB::table('absen')
                    ->join('users', 'users.id', '=', 'absen.user_id')
                    ->selectRaw('absen.user_id, users.name, users.level_id, users.cabang_id, absen.is_masuk, absen.masuk_at, absen.keterangan')
                    ->where('absen.user_id', '=', $user_id)
                    ->whereMonth('absen.masuk_at', '=', date('m'))
                    ->orderBy('users.level_id')
                    ->get();
        return $sql;
    }
    
    public function getAdminAbsen($dataUser, $date){
        $typeText = 'manager_id';
        if($dataUser->level_id == 2){
            $typeText = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeText = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeText = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeText = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeText = 'mqb_id';
        }
        if($dataUser->level_id == 8){
            $typeText = 'asmen_id';
        }
        $sql = DB::table('absen')
                    ->join('users', 'users.id', '=', 'absen.user_id')
                    ->selectRaw('absen.user_id, users.name, users.level_id,'
                            . 'count(case when absen.is_masuk = 1 then absen.user_id end) as jml_masuk,'
                            . 'count(case when absen.is_masuk = 2 then absen.user_id end) as jml_sakit,'
                            . 'count(case when absen.is_masuk = 3 then absen.user_id end) as jml_izin,'
                            . 'count(case when absen.is_masuk = 4 then absen.user_id end) as jml_alpa')
                    ->whereMonth('absen.masuk_at', '=', date('m'))
                    ->whereYear('absen.masuk_at', '=', date('Y'))
                    ->where('absen.'.$typeText, '=', $dataUser->id)
                    ->groupBy('absen.user_id')
                    ->groupBy('users.name')
                    ->groupBy('users.level_id')
                    ->orderBy('users.level_id')
                    ->get();
        return $sql;
    }
    
    public function getNewCountAllDownlineBar($cabang_id, $spId, $arraySpId){
        $sql = DB::table('users')
                    ->selectRaw('CONCAT(DATE_FORMAT(created_at, "%m"), "-", YEAR(created_at)) AS month_name, count(id) as total,'
                            . 'YEAR(created_at) as tahun, DATE_FORMAT(created_at, "%m") as bulan')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->whereDate('created_at', '>=', date('Y-m-01', strtotime("-6 Months")))
                    ->whereDate('created_at', '<=', date('Y-m-d'))
                    ->whereIn('level_id', $arraySpId)
                    ->where('cabang_id', '=', $cabang_id)
                    ->where('sponsor_id', 'LIKE', $spId.'%')
                    ->groupBy('month_name')
                    ->groupBy('tahun')
                    ->groupBy('bulan')
                    ->orderBy('tahun', 'ASC')
                    ->orderBy('bulan', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getNewCountAllDownlineAdminBar(){
        $sql = DB::table('users')
                    ->selectRaw('CONCAT(DATE_FORMAT(created_at, "%m"), "-", YEAR(created_at)) AS month_name, count(id) as total, '
                            . 'YEAR(created_at) as tahun, DATE_FORMAT(created_at, "%m") as bulan')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->whereDate('created_at', '>=', date('Y-m-01', strtotime("-8 Months")))
                    ->whereDate('created_at', '<=', date('Y-m-d'))
                    ->whereIn('level_id', array(8, 9, 10,11, 12, 13))
                    ->groupBy('month_name')
                    ->groupBy('tahun')
                    ->groupBy('bulan')
                    ->orderBy('tahun', 'ASC')
                    ->orderBy('bulan', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAbsenCheck($user_id, $date){
        $sql = DB::table('absen')
                    ->selectRaw('absen.user_id, absen.is_masuk, absen.masuk_at')
                    ->where('absen.user_id', '=', $user_id)
                    ->whereDate('absen.masuk_at', '=', $date)
                    ->first();
        return $sql;
    }
    
    public function getCekUsernameAdmin($username, $id){
        $sql = DB::table('users')
                    ->selectRaw('users.id')
                    ->where('id', '!=', $id)
                    ->where('users.username', '=', $username)
                    ->first();
        return $sql;
    }
    
    public function getAllDataForOmset($arrayManager){
        $sql = DB::table('users')
                    ->selectRaw('id, name, level_id, cabang_name, cabang_id, sponsor_id')
                    ->whereIn('level_id', $arrayManager)
                    ->where('is_active', '=', 1)
                    ->orderBy('cabang_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getInsertLog($data){
        try {
            DB::table('log_edited')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getInsertCustomer($data){
        try {
            DB::table('customer')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getListCustomerByDate($dataMqb, $param){
        $sql = DB::table('customer')
                    ->join('users', 'customer.user_id', '=', 'users.id')
                    ->join('item_purchase', 'item_purchase.id', '=', 'customer.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('customer.code_id, customer.name, customer.address, customer.hp, customer.email, customer.keluhan, '
                            . 'customer.cabang_name, customer.user_id, customer.item_purchase_id, customer.sale_date,'
                            . 'users.name as crew_name, purchase.purchase_name, purchase.type')
                    ->where('mqb_id', '=', $dataMqb->id)
                    ->where('customer.sale_date', '>=', $param->startDay)
                    ->where('customer.sale_date', '<=', $param->endDay)
                    ->orderBy('customer.sale_date', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getListCustomerByDateAdmin($param){
        $sql = DB::table('customer')
                    ->join('users', 'customer.user_id', '=', 'users.id')
                    ->join('item_purchase', 'item_purchase.id', '=', 'customer.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('customer.name, customer.address, customer.hp, customer.email, customer.keluhan, '
                            . 'customer.cabang_name, customer.user_id, customer.item_purchase_id, customer.sale_date,'
                            . 'users.name as crew_name, purchase.purchase_name, purchase.type')
                    ->where('customer.sale_date', '>=', $param->startDay)
                    ->where('customer.sale_date', '<=', $param->endDay)
                    ->where('customer.cabang_name', '=', $param->cabang_name)
                    ->orderBy('customer.sale_date', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getTopStructureFromCabang($cbgId){
        $arrayManager = array(2, 3, 4, 5, 6, 7, 8);
        $sql = DB::table('users')
                    ->selectRaw('id, name, level_id, cabang_name')
                    ->whereIn('level_id', $arrayManager)
                    ->where('is_active', '=', 1)
                    ->where('structure_type', '=', 0)
                    ->where('cabang_id', '=', $cbgId)
                    ->orderBy('level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getTopStructureList(){
        $sql = DB::table('users')
                    ->selectRaw('id, name, level_id, cabang_name')
                    ->where('is_active', '=', 1)
                    ->where('structure_type', '=', 1)
                    ->orderBy('cabang_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getDownlineStructureList($id){
        $sql = DB::table('users')
                    ->selectRaw('id, name, level_id, cabang_name')
                    ->where('is_active', '=', 1)
                    ->where('structure_type', '=', 2)
                    ->where('top_structure_id', '=', $id)
                    ->orderBy('cabang_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getDownlineStructureFromCabang($cbgId){
        $sql = DB::table('users')
                    ->selectRaw('id, name, level_id, cabang_name')
                    ->where('is_active', '=', 1)
                    ->where('structure_type', '=', 0)
                    ->where('cabang_id', '=', $cbgId)
                    ->orderBy('level_id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getDownlineSponsorFromCabang($id){
        $sql = DB::table('users')
                    ->selectRaw('id, name, level_id, cabang_name')
                    ->where('top_structure_id', '=', $id)
                    ->orderBy('structure_type', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getDownlineSponsorFromSponsorId($data){
        $sql = DB::table('users')
                    ->selectRaw('id, name, level_id, cabang_name')
                    ->where('structure_id', 'LIKE', $data.'%')
                    ->get();
        return $sql;
    }
    
    public function getAllMyTreeOrganisasi($id){
//        dd($data);
        $sql = DB::table('users')
                ->selectRaw('users.id, users.name, users.sponsor_structure_id, users.cabang_name')
                ->where('users.sponsor_structure_id', '=', $id)
                ->get();
//        dd($sql);
        $dataRow = array();
        foreach($sql as $row){
            $dataRow[] = array(
                'id' => $row->id,
                'name' => $row->name,
                'text' => ' ('.$row->cabang_name.') '.$row->name,
                'link' => '#',
                'sponsor_structure_id' => $row->sponsor_structure_id, 
                'children' => $this->getAllMyTreeOrganisasi($row->id)
            );
        }
        return $dataRow;
    }
    
    public function getUserIdOrganisasi($id, $dataTopOrg){
        $sql = DB::table('users')
                    ->where('id', '=', $id)
                    ->where('structure_type', '=', 2)
                    ->where('top_structure_id', '=', $dataTopOrg->id)
                    ->first();
        return $sql;
    }
    
    public function getCustomerCode(){
        $sql = DB::table('customer')
                ->selectRaw('id')
                ->whereDate('created_at', '=', date('Y-m-d'))
                ->count();
        $tmp = $sql+1;
        $code = sprintf("%05s", $tmp);
        $newUsername =  'ID'.date('Ymd').$code;
        return $newUsername;
    }
    
    public function getUserIdProductDetail($id){
        $sql = DB::table('users')
                        ->join('cabang', 'cabang.id', '=', 'users.cabang_id')
                        ->selectRaw('users.id, users.name, users.level_id, users.cabang_name, cabang.extra_wilayah, users.cabang_id')
                        ->where('users.id', '=', $id)
                        ->first();
        return $sql;
    }
    
    
    
}
