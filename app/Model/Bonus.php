<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Bonus extends Model {
    
    //Bonus type 1=> bonus for harian all
    //type 2 => bonus for bulanan all
    //type 11 => bonus omset obat for minguan asmen, manager, ets
    public function getSettingBonus(){
        $sql = DB::table('bonus_setting')
                    ->where('is_active', '=', 1)
                    ->orderBy('id', 'DESC')
                    ->first();
        return $sql;
    }
    
    public function getInsertSettingBonus($data){
        try {
            DB::table('bonus_setting')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateSettingBonus($data){
        try {
            DB::table('bonus_setting')->where('is_active', '=', 1)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getInsertBonus($data){
        try {
            DB::table('bonus')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getBonusData($getItemId, $getBonusSetting, $user_id, $rowAll, $getInvo, $get_gm, $get_sem, $get_em, $get_sm, $get_mqb, $get_m, $get_asmen, $get_tld, $get_ld, $get_tr, $get_md, $get_rt){
        //Bonus Si User Id
        $pengali = 1;
        if($user_id != null){
            if($getItemId->type == 1){
                $bonus_price_user_id = 0;
                $bonus_price_user_id_month = 0;
            }
            if($getItemId->type == 2){
                $bonus_price_user_id = $getBonusSetting->crew_strip;
                $bonus_price_user_id_month = $getBonusSetting->crew_strip_month;
            }
            if($getItemId->type == 3){
                $bonus_price_user_id = $pengali * $getBonusSetting->crew_obat;
                $bonus_price_user_id_month = $pengali * $getBonusSetting->crew_obat_month;
            }
            if($bonus_price_user_id > 0){
                $dataBonus_UserId = array(
                    'user_id' => $user_id,
                    'bonus_price' => round($rowAll->amount * $bonus_price_user_id),
                    'item_purchase_id' => $rowAll->item_purchase,
                    'purchase_type' => $getItemId->type,
                    'sales_invoice' => $getInvo,
                    'from_user_id' => $user_id,
                    'bonus_date' => $rowAll->sale_date,
                    'gm_id' => $get_gm,
                    'sem_id' => $get_sem,
                    'em_id' => $get_em,
                    'sm_id' => $get_sm,
                    'mqb_id' => $get_mqb,
                    'manager_id' => $get_m,
                    'asmen_id' => $get_asmen,
                    'tld_id' => $get_tld,
                    'ld_id' => $get_ld,
                    'tr_id' => $get_tr,
                    'md_id' => $get_md,
                    'rt_id' => $get_rt,
                );
                DB::table('bonus')->insert($dataBonus_UserId);
            }
            if($bonus_price_user_id_month > 0){
                $dataBonus_UserId_m = array(
                    'user_id' => $user_id,
                    'bonus_price' => round($rowAll->amount * $bonus_price_user_id_month),
                    'item_purchase_id' => $rowAll->item_purchase,
                    'purchase_type' => $getItemId->type,
                    'sales_invoice' => $getInvo,
                    'from_user_id' => $user_id,
                    'bonus_type' => 2,
                    'bonus_date' => $rowAll->sale_date,
                    'gm_id' => $get_gm,
                    'sem_id' => $get_sem,
                    'em_id' => $get_em,
                    'sm_id' => $get_sm,
                    'mqb_id' => $get_mqb,
                    'manager_id' => $get_m,
                    'asmen_id' => $get_asmen,
                    'tld_id' => $get_tld,
                    'ld_id' => $get_ld,
                    'tr_id' => $get_tr,
                    'md_id' => $get_md,
                    'rt_id' => $get_rt,
                );
                DB::table('bonus')->insert($dataBonus_UserId_m);
            }
            //UserId
        }

        //Bonus Si Top Leader
        if($get_tld != null){
            if($get_tld != $user_id){
                if($getItemId->type == 1){
                    $bonus_price_tl = 0;
                    $bonus_price_tl_m = 0;
                }
                if($getItemId->type == 2){
                    $bonus_price_tl = $getBonusSetting->tl_strip;
                    $bonus_price_tl_m = $getBonusSetting->tl_strip_month;
                }
                if($getItemId->type == 3){
                    $bonus_price_tl = $pengali * $getBonusSetting->tl_obat;
                    $bonus_price_tl_m = $pengali * $getBonusSetting->tl_obat_month;
                }
                if($bonus_price_tl > 0){
                    $dataBonus_TL = array(
                        'user_id' => $get_tld,
                        'bonus_price' => round($rowAll->amount * $bonus_price_tl),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_TL);
                }
                if($bonus_price_tl_m > 0){
                    $dataBonus_TL_m = array(
                        'user_id' => $get_tld,
                        'bonus_price' => round($rowAll->amount * $bonus_price_tl_m),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_type' => 2, 
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_TL_m);
                }
            }
        }

        //Bonus Si Asmen
        if($get_asmen != null){
            if($get_asmen != $user_id){
                if($getItemId->type == 1){
                    $bonus_price_asmen = 0;
                    $bonus_price_asmen_m = 0;
                }
                if($getItemId->type == 2){
                    $bonus_price_asmen = $getBonusSetting->am_strip;
                    $bonus_price_asmen_m = $getBonusSetting->am_strip_month;
                }
                if($getItemId->type == 3){
                    $bonus_price_asmen = $pengali * $getBonusSetting->am_obat;
                    $bonus_price_asmen_m = $pengali * $getBonusSetting->am_obat_month;
                }
                if($bonus_price_asmen > 0){
                    $dataBonus_ASMEN = array(
                        'user_id' => $get_asmen,
                        'bonus_price' => round($rowAll->amount * $bonus_price_asmen),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_ASMEN);
                }
                if($bonus_price_asmen_m > 0){
                    $dataBonus_ASMEN_m = array(
                        'user_id' => $get_asmen,
                        'bonus_price' => round($rowAll->amount * $bonus_price_asmen_m),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_type' => 2,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_ASMEN_m);
                }
            }
        }

        //Bonus Si Manager
        if($get_m != null){
            if($get_m != $user_id){
                if($getItemId->type == 1){
                    $bonus_price_m = $getBonusSetting->m_alat;
                    $bonus_price_m_m = 0;
                }
                if($getItemId->type == 2){
                    $bonus_price_m = $getBonusSetting->m_strip;
                    $bonus_price_m_m = $getBonusSetting->m_strip_month;
                }
                if($getItemId->type == 3){
                    $bonus_price_m = $pengali * $getBonusSetting->m_obat;
                    $bonus_price_m_m = $pengali * $getBonusSetting->m_obat_month;
                }
                if($bonus_price_m > 0){
                    $dataBonus_M = array(
                        'user_id' => $get_m,
                        'bonus_price' => round($rowAll->amount * $bonus_price_m),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_M);
                }
                if($bonus_price_m_m > 0){
                    $dataBonus_M_m = array(
                        'user_id' => $get_m,
                        'bonus_price' => round($rowAll->amount * $bonus_price_m_m),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_type' => 2,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_M_m);
                }
            }
        }

        //Bonus Si MQB
        if($get_mqb != null){
            if($get_mqb != $user_id){
                if($getItemId->type == 1){
                    $bonus_price_mqb = 0;
                    $bonus_price_mqb_m = 0;
                }
                if($getItemId->type == 2){
                    $bonus_price_mqb = $getBonusSetting->mqb_strip;
                    $bonus_price_mqb_m = $getBonusSetting->mqb_strip_month;
                }
                if($getItemId->type == 3){
                    $bonus_price_mqb = $pengali * $getBonusSetting->mqb_obat;
                    $bonus_price_mqb_m = $pengali * $getBonusSetting->mqb_obat_month;
                }
                if($bonus_price_mqb > 0){
                    $dataBonus_MQB = array(
                        'user_id' => $get_mqb,
                        'bonus_price' => round($rowAll->amount * $bonus_price_mqb),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_MQB);
                }

                if($bonus_price_mqb_m > 0){
                    $dataBonus_MQB_m = array(
                        'user_id' => $get_mqb,
                        'bonus_price' => round($rowAll->amount * $bonus_price_mqb_m),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_type' => 2,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_MQB_m);
                }
                //MQB
            }
        }

        //Bonus Si SM
        if($get_sm != null){
            if($get_sm != $user_id){
                if($getItemId->type == 1){
                    $bonus_price_sm = 0;
                    $bonus_price_sm_m = 0;
                }
                if($getItemId->type == 2){
                    $bonus_price_sm = $getBonusSetting->sm_strip;
                    $bonus_price_sm_m = $getBonusSetting->sm_strip_month;
                }
                if($getItemId->type == 3){
                    $bonus_price_sm = $pengali * $getBonusSetting->sm_obat;
                    $bonus_price_sm_m = $pengali * $getBonusSetting->sm_obat_month;
                }
                if($bonus_price_sm > 0){
                    $dataBonus_SM = array(
                        'user_id' => $get_sm,
                        'bonus_price' => round($rowAll->amount * $bonus_price_sm),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_SM);
                }
                if($bonus_price_sm_m > 0){
                    $dataBonus_SM_m = array(
                        'user_id' => $get_sm,
                        'bonus_price' => round($rowAll->amount * $bonus_price_sm_m),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_type' => 2,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_SM_m);
                }
            }
        }

        //Bonus Si EM
        if($get_em != null){
            if($get_em != $user_id){
                if($getItemId->type == 1){
                    $bonus_price_em = $getBonusSetting->em_alat;
                    $bonus_price_em_m = 0;
                }
                if($getItemId->type == 2){
                    $bonus_price_em = $getBonusSetting->em_strip;
                    $bonus_price_em_m = $getBonusSetting->em_strip_month;
                }
                if($getItemId->type == 3){
                    $bonus_price_em = $pengali * $getBonusSetting->em_obat;
                    $bonus_price_em_m = $pengali * $getBonusSetting->em_obat_month;
                }
                if($bonus_price_em > 0){
                    $dataBonus_EM = array(
                        'user_id' => $get_em,
                        'bonus_price' => round($rowAll->amount * $bonus_price_em),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_EM);
                }
                if($bonus_price_em_m > 0){
                    $dataBonus_EM_m = array(
                        'user_id' => $get_em,
                        'bonus_price' => round($rowAll->amount * $bonus_price_em_m),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_type' => 2,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_EM_m);
                }
            }
        }

        //Bonus Si SEM
        if($get_sem != null){
            if($get_sem != $user_id){
                if($getItemId->type == 1){
                    $bonus_price_sem = $getBonusSetting->sem_alat;
                    $bonus_price_sem_m = 0;
                }
                if($getItemId->type == 2){
                    $bonus_price_sem = $getBonusSetting->sem_strip;
                    $bonus_price_sem_m = $getBonusSetting->sem_strip_month;
                }
                if($getItemId->type == 3){
                    $bonus_price_sem = $pengali * $getBonusSetting->sem_obat;
                    $bonus_price_sem_m = $pengali * $getBonusSetting->sem_obat_month;
                }
                if($bonus_price_sem > 0){
                    $dataBonus_SEM = array(
                        'user_id' => $get_sem,
                        'bonus_price' => round($rowAll->amount * $bonus_price_sem),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_SEM);
                }
                if($bonus_price_sem_m > 0){
                    $dataBonus_SEM_m = array(
                        'user_id' => $get_sem,
                        'bonus_price' => round($rowAll->amount * $bonus_price_sem_m),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_type' => 2,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_SEM_m);
                }
            }
        }
        
        //Bonus Si GM
        if($get_gm != null){
            if($get_gm != $user_id){
                if($getItemId->type == 1){
                    $bonus_price_gm = $getBonusSetting->gm_alat;
                    $bonus_price_gm_m = 0;
                }
                if($getItemId->type == 2){
                    $bonus_price_gm = $getBonusSetting->gm_strip;
                    $bonus_price_gm_m = $getBonusSetting->gm_strip_month;
                }
                if($getItemId->type == 3){
                    $bonus_price_gm = $pengali * $getBonusSetting->gm_obat;
                    $bonus_price_gm_m = $pengali * $getBonusSetting->gm_obat_month;
                }
                if($bonus_price_gm > 0){
                    $dataBonus_GM = array(
                        'user_id' => $get_gm,
                        'bonus_price' => round($rowAll->amount * $bonus_price_gm),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_GM);
                }
                if($bonus_price_gm_m > 0){
                    $dataBonus_GM_m = array(
                        'user_id' => $get_gm,
                        'bonus_price' => round($rowAll->amount * $bonus_price_gm_m),
                        'item_purchase_id' => $rowAll->item_purchase,
                        'purchase_type' => $getItemId->type,
                        'sales_invoice' => $getInvo,
                        'from_user_id' => $user_id,
                        'bonus_type' => 2,
                        'bonus_date' => $rowAll->sale_date,
                        'gm_id' => $get_gm,
                        'sem_id' => $get_sem,
                        'em_id' => $get_em,
                        'sm_id' => $get_sm,
                        'mqb_id' => $get_mqb,
                        'manager_id' => $get_m,
                        'asmen_id' => $get_asmen,
                        'tld_id' => $get_tld,
                        'ld_id' => $get_ld,
                        'tr_id' => $get_tr,
                        'md_id' => $get_md,
                        'rt_id' => $get_rt,
                    );
                    DB::table('bonus')->insert($dataBonus_GM_m);
                }
            }
        }
        
    }
    
    public function getTotalAllBonus($id){
        $sql = DB::table('bonus')
                    ->selectRaw('sum(bonus_price) as total_bonus')
                    ->where('user_id', '=', $id)
                    ->where('is_active', '=', 1)
                    ->whereIn('bonus_type', array(1, 2))
                    ->groupBy('user_id')
                    ->first();
        return $sql;
    }
    
    public function getDetailAllBonus($id){
        $sql = DB::table('bonus')
                    ->join('users', 'bonus.from_user_id', '=', 'users.id')
                    ->join('item_purchase', 'item_purchase.id', '=', 'bonus.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('bonus.bonus_price, bonus.from_user_id, bonus.bonus_date, bonus_type, bonus.purchase_type, '
                            . 'purchase.purchase_name, users.name')
                    ->where('bonus.user_id', '=', $id)
                    ->where('bonus.is_active', '=', 1)
                    ->whereIn('bonus_type', array(1, 2))
                    ->orderBy('bonus.id', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getReportBonusCrewWithManager($param, $cbgId, $userType){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
                    ->join('users as u', 'u.id', '=', 'sales.user_id')
                    ->leftJoin('history_level', 'history_level.user_id', '=', 'u.id')
                    ->selectRaw('u.name, u.level_id, 
		sum(case when p.type = 1 then sales.amount end) as sum_alat,
                                    sum(case when p.type = 2 then sales.amount end) as sum_strip,
		sum(case when p.type = 3 and p.is_poin = 1 then sales.amount end) as sum_obat_30_new,
		sum(case when p.type = 3 and p.is_poin = 0 then sales.amount end) as sum_obat_60_new,
                                    (case when max(history_level.active_at) is null then u.created_at else max(history_level.active_at) end) as active_at
                                    ')
                    ->whereDate('sales.sale_date', '>=', $param->startDay)
                    ->whereDate('sales.sale_date', '<=', $param->endDay)
                    ->whereNull('sales.deleted_at')
//                    ->where('u.cabang_id', '=', $cbgId)
                    ->where('u.user_type', '=', $userType)
                    ->where('sales.'.$param->type, '=', $param->id)
                    ->groupBy('sales.user_id')
                    ->groupBy('u.name')
                    ->groupBy('u.level_id')
                    ->orderBy('u.cabang_name')
                    ->groupBy('u.created_at')
                    ->get();
        
//        $sql = DB::table('bonus')
//                    ->join('users', 'bonus.user_id', '=', 'users.id')
//                    ->join('item_purchase', 'item_purchase.id', '=', 'bonus.item_purchase_id')
//                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
//                    ->selectRaw('users.id, users.name, users.level_id, '
//                            . 'sum(case when (bonus.purchase_type = 2 AND bonus.bonus_type = 1) then bonus.bonus_price else 0 end) as bonus_price_strip_harian, '
//                            . 'sum(case when (bonus.purchase_type = 2 AND bonus.bonus_type = 2) then bonus.bonus_price else 0 end) as bonus_price_strip_bulanan, '
//                            . 'sum(case when (bonus.purchase_type = 3 AND bonus.bonus_type = 1) then bonus.bonus_price else 0 end) as bonus_price_obat_harian, '
//                            . 'sum(case when (bonus.purchase_type = 3 AND bonus.bonus_type = 2) then bonus.bonus_price else 0 end) as bonus_price_obat_bulanan,'
//                            . 'sum(case when (bonus.purchase_type = 3 AND bonus.bonus_type = 1 AND purchase.is_poin = 1) then bonus.bonus_price else 0 end) as bonus_price_obat_harian_30,'
//                            . 'sum(case when (bonus.purchase_type = 3 AND bonus.bonus_type = 2 AND purchase.is_poin = 1) then bonus.bonus_price else 0 end) as bonus_price_obat_bulanan_30,'
//                            . 'sum(case when (bonus.purchase_type = 3 AND bonus.bonus_type = 1 AND purchase.is_poin = 0) then bonus.bonus_price else 0 end) as bonus_price_obat_harian_60,'
//                            . 'sum(case when (bonus.purchase_type = 3 AND bonus.bonus_type = 2 AND purchase.is_poin = 0) then bonus.bonus_price else 0 end) as bonus_price_obat_bulanan_60')
//                    ->where('bonus.bonus_date', '>=', $param->startDay)
//                    ->where('bonus.bonus_date', '<=', $param->endDay)
//                    ->where('bonus.'.$param->type, '=', $param->id)
//                    ->where('users.cabang_id', '=', $cbgId)
//                    ->where('users.user_type', '=', $userType)
//                    ->groupBy('users.name')
//                    ->groupBy('users.id')
//                    ->groupBy('users.level_id')
//                    ->groupBy('users.cabang_name')
//                    ->orderBy('users.level_id', 'DESC')
//                    ->get();
        return $sql;
    }
    
    public function getInsertCron($data){
        try {
            DB::table('bonus_cron_setting')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateCron($data){
        try {
            DB::table('bonus_cron_setting')->where('id', '=', 1)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getCekCron(){
        $sql = DB::table('bonus_cron_setting')
                    ->selectRaw('id, start_date, hari')
                    ->where('id', '=', 1)
                    ->first();
        return $sql;
    }
    
    public function getReportBonusOmset($param, $cbgId, $bonus_type){
        $sql = DB::table('bonus')
                    ->join('users', 'bonus.user_id', '=', 'users.id')
                    ->selectRaw('users.id, users.name, users.level_id, bonus.bonus_price, bonus.bonus_date')
                    ->where('bonus.bonus_type', '=', $bonus_type)
                    ->where('bonus.bonus_date', '>=', $param->startDay)
                    ->where('bonus.bonus_date', '<=', $param->endDay)
                    ->where('bonus.user_id', '=', $param->id)
                    ->where('users.cabang_id', '=', $cbgId)
                    ->orderBy('bonus.bonus_date', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getTotalAllBonusNew($id, $date){
        if($date != null){
            $sql = DB::table('bonus')
                    ->selectRaw('sum(bonus_price) as total_bonus')
                    ->where('user_id', '=', $id)
                    ->whereDate('bonus_date', '>=', $date->startDay)
                    ->whereDate('bonus_date', '<=', $date->endDay)
                    ->where('is_active', '=', 1)
                    ->whereIn('bonus_type', array(1, 2))
                    ->groupBy('user_id')
                    ->first();
        } else {
            $sql = DB::table('bonus')
                    ->selectRaw('sum(bonus_price) as total_bonus')
                    ->where('user_id', '=', $id)
                    ->where('is_active', '=', 1)
                    ->whereIn('bonus_type', array(1, 2))
                    ->groupBy('user_id')
                    ->first();
        }
        return $sql;
    }
    
    public function getDetailAllBonusNew($id, $date){
        if($date != null){
            $sql = DB::table('bonus')
                    ->join('users', 'bonus.from_user_id', '=', 'users.id')
                    ->join('item_purchase', 'item_purchase.id', '=', 'bonus.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('bonus.bonus_price, bonus.from_user_id, bonus.bonus_date, bonus_type, bonus.purchase_type, '
                            . 'purchase.purchase_name, users.name')
                    ->where('bonus.user_id', '=', $id)
                    ->whereDate('bonus_date', '>=', $date->startDay)
                    ->whereDate('bonus_date', '<=', $date->endDay)
                    ->where('bonus.is_active', '=', 1)
                    ->whereIn('bonus_type', array(1, 2))
                    ->orderBy('bonus.id', 'DESC')
                    ->get();
        } else {
            $sql = DB::table('bonus')
                    ->join('users', 'bonus.from_user_id', '=', 'users.id')
                    ->join('item_purchase', 'item_purchase.id', '=', 'bonus.item_purchase_id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('bonus.bonus_price, bonus.from_user_id, bonus.bonus_date, bonus_type, bonus.purchase_type, '
                            . 'purchase.purchase_name, users.name')
                    ->where('bonus.user_id', '=', $id)
                    ->where('bonus.is_active', '=', 1)
                    ->whereIn('bonus_type', array(1, 2))
                    ->orderBy('bonus.id', 'DESC')
                    ->get();
        }
        return $sql;
    }
    
    public function getBonusAllMangerCabang($mType, $data){
        $sql = DB::table('sales')
                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
                    ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
                    ->join('users as u', 'u.id', '=', 'sales.'.$mType)
                    ->join('cabang', 'u.cabang_id', '=', 'cabang.id')
                    ->selectRaw('u.name, u.cabang_name, cabang.wilayah, cabang.extra_wilayah,
		sum(case when p.type = 2 then sales.amount end) as sum_strip,
		sum(case when p.type = 3 and p.is_poin = 1 then sales.amount end) as sum_obat_30_new,
		sum(case when p.type = 3 and p.is_poin = 0 then sales.amount end) as sum_obat_60_new
                                    ')
                    ->whereDate('sales.sale_date', '>=', $data->startDay)
                    ->whereDate('sales.sale_date', '<=', $data->endDay)
                    ->whereNull('sales.deleted_at')
                    ->whereNotNull('sales.'.$mType)
//                    ->where('u.id', '=', 2853)
                    ->groupBy('sales.'.$mType)
                    ->groupBy('u.name')
                    ->groupBy('u.cabang_name')
                    ->groupBy('u.id')
                    ->groupBy('cabang.wilayah')
                    ->groupBy('cabang.extra_wilayah')
                    ->orderBy('u.cabang_name')
                    ->get();
//        $sql = DB::table('bonus')
//                    ->join('users', 'bonus.user_id', '=', 'users.id')
//                    ->join('sales', 'bonus.sales_invoice', '=', 'sales.invoice')
//                    ->join('item_purchase', 'item_purchase.id', '=', 'sales.item_purchase_id')
//                    ->join('purchase as p', 'p.id', '=', 'item_purchase.purchase_id')
//                    ->selectRaw('users.name, users.id, users.cabang_name, '
//                            . 'sum(case when (bonus.purchase_type = 1) then bonus.bonus_price else 0 end) as bonus_price_alat, '
//                            . 'sum(case when (bonus.purchase_type = 2) then bonus.bonus_price else 0 end) as bonus_price_strip,'
//                            . 'sum(case when (bonus.purchase_type = 2) then sales.amount end) as sum_strip,'
//                            . 'sum(case when (bonus.purchase_type = 3) then bonus.bonus_price else 0 end) as bonus_price_obat,'
//                            . 'sum(case when bonus.purchase_type = 3 and p.is_poin = 1 then bonus.bonus_price end) as bonus_obat_30,'
//                            . 'sum(case when bonus.purchase_type = 3 and p.is_poin = 1 then sales.amount end) as sum_obat_30,'
//                            . 'sum(case when bonus.purchase_type = 3 and p.is_poin = 0 then bonus.bonus_price end) as bonus_obat_60,'
//                            . 'sum(case when bonus.purchase_type = 3 and p.is_poin = 0 then sales.amount end) as sum_obat_60')
//                    ->where('bonus.is_active', '=', 1)
//                    ->whereDate('bonus_date', '>=', $data->startDay)
//                    ->whereDate('bonus_date', '<=', $data->endDay)
//                    ->whereIn('bonus_type', array(1, 2))
//                    ->where('users.level_id', '=', 7)
//                    ->where('users.id', '=', 2853)
//                    ->groupBy('users.id')
//                    ->groupBy('users.name')
//                    ->groupBy('users.cabang_name')
////                    ->groupBy('bonus.sales_invoice')
//                    ->orderBy('bonus.id', 'DESC')
//                    ->get();
        return $sql;
    }
    
    public function getBonusMangerId($data){
        $sql = DB::table('bonus')
                    ->join('users', 'bonus.user_id', '=', 'users.id')
                    ->selectRaw('users.name, users.id, users.cabang_name, '
                            . 'sum(case when (bonus.purchase_type = 1) then bonus.bonus_price else 0 end) as bonus_price_alat, '
                            . 'sum(case when (bonus.purchase_type = 2) then bonus.bonus_price else 0 end) as bonus_price_strip,'
                            . 'sum(case when (bonus.purchase_type = 3) then bonus.bonus_price else 0 end) as bonus_price_obat')
                    ->where('bonus.is_active', '=', 1)
                    ->where('users.id', '=', $data->id)
                    ->whereDate('bonus_date', '>=', $data->startDay)
                    ->whereDate('bonus_date', '<=', $data->endDay)
                    ->whereIn('bonus_type', array(1, 2))
                    ->groupBy('users.id')
                    ->groupBy('users.name')
                    ->groupBy('users.cabang_name')
//                    ->orderBy('bonus.id', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getSettingApp(){
        $sql = DB::table('setting_app')
                    ->where('is_active', '=', 1)
                    ->orderBy('id', 'DESC')
                    ->first();
        return $sql;
    }
    
    public function getInsertSettingApp($data){
        try {
            DB::table('setting_app')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateSettingApp($data){
        try {
            DB::table('setting_app')->where('is_active', '=', 1)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    
}

