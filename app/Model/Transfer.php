<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Transfer extends Model {
    
    //Kategori pengeluaran
    public function getInsertCategoryPengeluaran($data){
        try {
            DB::table('category_pengeluaran')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateCategoryPengeluaran($id, $data){
        try {
            DB::table('category_pengeluaran')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAllCategoryPengeluaran(){
        $sql = DB::table('category_pengeluaran')
                    ->selectRaw('id, pengeluaran_name, created_at')
                    ->whereNull('deleted_at')
                    ->get();
        return $sql;
    }
    
    public function getCategoryPengeluaranId($id){
        $sql = DB::table('category_pengeluaran')
                    ->selectRaw('id, pengeluaran_name, created_at')
                    ->where('id', '=', $id)
                    ->whereNull('deleted_at')
                    ->first();
        return $sql;
    }
    
    public function getTotalSalesAndOut($dataUser, $date){
        $typeText = 'manager_id';
        if($dataUser->level_id == 2){
            $typeText = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeText = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeText = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeText = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeText = 'mqb_id';
        }
        $sqlSales = DB::table('sales')
                    ->selectRaw('sum(sales.sale_price) as total_sales')
                    ->whereDate('sales.sale_date', '=', $date)
                    ->where('sales.'.$typeText, '=', $dataUser->id)
                    ->where('sales.is_safra', '=', 0)
                    ->whereNull('sales.deleted_at')
                    ->first();
        $sales = 0;
        if($sqlSales->total_sales != null){
            $sales = $sqlSales->total_sales;
        }
        $sqlPengeluaran = DB::table('pengeluaran')
                    ->selectRaw('sum(pengeluaran_price) as total_out')
                    ->whereDate('pengeluaran_date', '=', $date)
                    ->where($typeText, '=', $dataUser->id)
                    ->where('pengeluaran.jenis', '=', 1)
                    ->whereNull('deleted_at')
                    ->first();
        $out = 0;
        if($sqlPengeluaran->total_out != null){
            $out = $sqlPengeluaran->total_out;
        }
        $data = (object) array(
            'sales' => $sales,
            'pengeluaran' => $out
        );
        return $data;
    }
    
    public function getMQBTotalOut($dataUser, $date){
        $typeText = 'manager_id';
        if($dataUser->level_id == 2){
            $typeText = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeText = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeText = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeText = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeText = 'mqb_id';
        }
        $sqlPengeluaran = DB::table('pengeluaran')
                    ->selectRaw('sum(pengeluaran_price) as total_out')
                    ->whereDate('pengeluaran_date', '=', $date)
                    ->where($typeText, '=', $dataUser->id)
                    ->where('pengeluaran.jenis', '=', 2)
                    ->whereNull('deleted_at')
                    ->first();
        $out = 0;
        if($sqlPengeluaran->total_out != null){
            $out = $sqlPengeluaran->total_out;
        }
        $data = (object) array(
            'pengeluaran' => $out
        );
        return $data;
    }
    
    //pengeluaran
    public function getInsertPengeluaran($data){
        try {
            DB::table('pengeluaran')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdatePengeluaran($id, $data){
        try {
            DB::table('pengeluaran')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    
    
    
    //Transfer
    public function getInsertTransfer($data){
        try {
            DB::table('transfer')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateTransfer($id, $data){
        try {
            DB::table('transfer')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getTransferInvoice($date){
        $sql = DB::table('transfer')->selectRaw('id')->whereDate('transfer_date', $date)->count();
        $tmp = $sql+1;
        $dateCode = date("Ymd", strtotime($date));
        $code = sprintf("%05s", $tmp);
        $newInvoice =  'TR'.$dateCode.'-'.$code;
        return $newInvoice;
    }
    
    public function getCheckTransfer($dataUser, $date){
        $typeText = 'manager_id';
        if($dataUser->level_id == 2){
            $typeText = 'gm_id';
        }
        if($dataUser->level_id == 3){
            $typeText = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeText = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeText = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeText = 'mqb_id';
        }
        $sql = DB::table('transfer')
                    ->selectRaw('id, transfer_invoice')
                    ->whereDate('transfer_date', '=', $date)
                    ->where($typeText, '=', $dataUser->id)
                    ->whereNull('deleted_at')
                    ->first();
        return $sql;
    }
    
    public function getAllTransferAdmin(){
        $sql = DB::table('transfer')
                    ->join('users', 'transfer.manager_id', '=', 'users.id')
                    ->selectRaw('transfer.id, transfer.transfer_invoice, transfer.transfer_price, transfer.sales_price, transfer.pengeluaran_price, '
                            . 'transfer.transfer_date, transfer.is_transfer,'
                            . 'transfer.created_at, transfer.approve_at, transfer.reject_at,'
                            . 'users.name, users.cabang_name')
                    ->where('transfer.is_transfer', '!=', 1)
                    ->whereDate('transfer.transfer_date', '>=', date('Y-m-d',strtotime("-30 days")))
                    ->whereDate('transfer.transfer_date', '<=', date('Y-m-d'))
                    ->whereNull('transfer.deleted_at')
                    ->orderBy('transfer.transfer_date', 'DESC')
                    ->orderBy('transfer.transfer_invoice', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAllTransferManager($data){
        $sql = DB::table('transfer')
                    ->selectRaw('transfer.id, transfer.transfer_invoice, transfer.transfer_price, transfer.sales_price, transfer.pengeluaran_price, '
                            . 'transfer.transfer_date, transfer.is_transfer,'
                            . 'transfer.created_at, transfer.approve_at, transfer.reject_at, transfer.keterangan')
                    ->where('transfer.manager_id', '=', $data->id)
                    ->whereDate('transfer.transfer_date', '>=', date('Y-m-d',strtotime("-30 days")))
                    ->whereDate('transfer.transfer_date', '<=', date('Y-m-d'))
                    ->whereNull('transfer.deleted_at')
                    ->orderBy('transfer.transfer_date', 'DESC')
                    ->orderBy('transfer.transfer_invoice', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAllTransferAdminId($id){
        $sql = DB::table('transfer')
                    ->join('users', 'transfer.manager_id', '=', 'users.id')
                    ->selectRaw('transfer.id, transfer.transfer_invoice, transfer.transfer_price, transfer.sales_price, transfer.pengeluaran_price, '
                            . 'transfer.transfer_date, transfer.is_transfer, transfer.keterangan,'
                            . 'transfer.created_at, transfer.approve_at, transfer.reject_at,'
                            . 'users.name, users.cabang_name, transfer.manager_id')
                    ->where('transfer.id', '=', $id)
                    ->whereNull('transfer.deleted_at')
                    ->first();
        return $sql;
    }
    
    public function getTransferManagerId($id, $dataUser){
        $sql = DB::table('transfer')
                    ->selectRaw('transfer.id, transfer.transfer_invoice, transfer.transfer_price, transfer.sales_price, transfer.pengeluaran_price, '
                            . 'transfer.transfer_date, transfer.is_transfer, transfer.keterangan,'
                            . 'transfer.created_at, transfer.approve_at, transfer.reject_at')
                    ->where('transfer.id', '=', $id)
                    ->where('transfer.manager_id', '=', $dataUser->id)
                    ->whereNull('transfer.deleted_at')
                    ->first();
        return $sql;
    }
    
    public function getTotalSalesAndOutAdmin($date, $m_id){
        $sqlSales = DB::table('sales')
                    ->selectRaw('sum(sales.sale_price) as total_sales')
                    ->whereDate('sales.sale_date', '=', $date)
                    ->where('manager_id', '=', $m_id)
                    ->whereNull('sales.deleted_at')
                    ->first();
        $sales = 0;
        if($sqlSales->total_sales != null){
            $sales = $sqlSales->total_sales;
        }
        $sqlPengeluaran = DB::table('pengeluaran')
                    ->join('category_pengeluaran', 'category_pengeluaran.id', '=', 'pengeluaran.category_id')
                    ->selectRaw('pengeluaran.pengeluaran_price, pengeluaran.pengeluaran_date, category_pengeluaran.pengeluaran_name, '
                            . 'pengeluaran.keterangan, pengeluaran.id')
                    ->whereDate('pengeluaran.pengeluaran_date', '=', $date)
                    ->where('pengeluaran.manager_id', '=', $m_id)
                    ->where('pengeluaran.jenis', '=', 1)
                    ->whereNull('pengeluaran.deleted_at')
                    ->get();
        $data = (object) array(
            'sales' => $sales,
            'pengeluaran' => $sqlPengeluaran
        );
        return $data;
    }
    
    public function getNotifTransferManager($dataUser){
        $sql = DB::table('transfer')
                    ->selectRaw('transfer.id, transfer.transfer_invoice, transfer.transfer_price, transfer.sales_price, transfer.pengeluaran_price, '
                            . 'transfer.transfer_date, transfer.is_transfer, transfer.keterangan,'
                            . 'transfer.created_at, transfer.approve_at, transfer.reject_at')
                    ->where('transfer.manager_id', '=', $dataUser->id)
                    ->where('transfer.is_transfer', '=', 2)
                    ->whereNull('transfer.deleted_at')
                    ->orderBy('transfer.transfer_date', 'DESC')
                    ->first();
        return $sql;
    }
    
    public function getAllTransferUpperManager($dataUser){
        $typeText = 'gm_id';
        if($dataUser->level_id == 3){
            $typeText = 'sem_id';
        }
        if($dataUser->level_id == 4){
            $typeText = 'em_id';
        }
        if($dataUser->level_id == 5){
            $typeText = 'sm_id';
        }
        if($dataUser->level_id == 6){
            $typeText = 'mqb_id';
        }
       $sql = DB::table('transfer')
                    ->join('users', 'transfer.manager_id', '=', 'users.id')
                    ->selectRaw('transfer.transfer_price, transfer.sales_price, transfer.pengeluaran_price, transfer.transfer_invoice, '
                            . 'transfer.transfer_date, transfer.is_transfer,'
                            . 'users.name')
                    ->where('transfer.'.$typeText, '=', $dataUser->id)
                    ->whereDate('transfer.transfer_date', '>=', date('Y-m-d',strtotime("-30 days")))
                    ->whereDate('transfer.transfer_date', '<=', date('Y-m-d'))
                    ->whereNull('transfer.deleted_at')
                    ->orderBy('transfer.transfer_date', 'DESC')
                    ->orderBy('transfer.transfer_invoice', 'ASC')
                    ->groupBy('transfer.manager_id')
                    ->groupBy('transfer.transfer_price')
                    ->groupBy('transfer.sales_price')
                    ->groupBy('transfer.pengeluaran_price')
                    ->groupBy('transfer.transfer_date')
                    ->groupBy('transfer.is_transfer')
                    ->groupBy('transfer.transfer_invoice')
                    ->groupBy('users.name')
                    ->get();
        return $sql;
    }
    
    public function getPengeluaranAdminId($id){
        $sql = DB::table('pengeluaran')
                    ->join('category_pengeluaran', 'category_pengeluaran.id', '=', 'pengeluaran.category_id')
                    ->selectRaw('pengeluaran.pengeluaran_price, pengeluaran.pengeluaran_date, category_pengeluaran.pengeluaran_name, '
                            . 'pengeluaran.keterangan, pengeluaran.id')
                    ->where('pengeluaran.id', '=', $id)
                    ->where('pengeluaran.jenis', '=', 1)
                    ->whereNull('pengeluaran.deleted_at')
                    ->first();
        return $sql;
    }
    
    public function getTotalPengeluaran($date, $manager_id){
        $sqlPengeluaran = DB::table('pengeluaran')
                    ->selectRaw('sum(pengeluaran_price) as total_out')
                    ->whereDate('pengeluaran_date', '=', $date)
                    ->where('manager_id', '=', $manager_id)
                    ->where('pengeluaran.jenis', '=', 1)
                    ->whereNull('deleted_at')
                    ->first();
        return $sqlPengeluaran;
    }
    
    public function getDetailPengeluaranManager($date, $m_id){
        $sqlPengeluaran = DB::table('pengeluaran')
                    ->join('category_pengeluaran', 'category_pengeluaran.id', '=', 'pengeluaran.category_id')
                    ->selectRaw('pengeluaran.pengeluaran_price, pengeluaran.pengeluaran_date, category_pengeluaran.pengeluaran_name, '
                            . 'pengeluaran.keterangan, pengeluaran.id')
                    ->whereDate('pengeluaran.pengeluaran_date', '=', $date)
                    ->where('pengeluaran.manager_id', '=', $m_id)
                    ->where('pengeluaran.jenis', '=', 1)
                    ->whereNull('pengeluaran.deleted_at')
                    ->get();
        return $sqlPengeluaran;
    }
    
    public function getSearchTransferAdmin($start, $end, $type){
        if($type == 100){
            $sql = DB::table('transfer')
                    ->join('users', 'transfer.manager_id', '=', 'users.id')
                    ->selectRaw('transfer.id, transfer.transfer_invoice, transfer.transfer_price, transfer.sales_price, transfer.pengeluaran_price, '
                            . 'transfer.transfer_date, transfer.is_transfer,'
                            . 'transfer.created_at, transfer.approve_at, transfer.reject_at,'
                            . 'users.name, users.cabang_name')
                    ->whereDate('transfer.transfer_date', '>=', $start)
                    ->whereDate('transfer.transfer_date', '<=', $end)
                    ->whereNull('transfer.deleted_at')
                    ->orderBy('transfer.transfer_date', 'DESC')
                    ->orderBy('transfer.transfer_invoice', 'ASC')
                    ->get();
        } else {
            $sql = DB::table('transfer')
                    ->join('users', 'transfer.manager_id', '=', 'users.id')
                    ->selectRaw('transfer.id, transfer.transfer_invoice, transfer.transfer_price, transfer.sales_price, transfer.pengeluaran_price, '
                            . 'transfer.transfer_date, transfer.is_transfer,'
                            . 'transfer.created_at, transfer.approve_at, transfer.reject_at,'
                            . 'users.name, users.cabang_name')
                    ->whereDate('transfer.transfer_date', '>=', $start)
                    ->whereDate('transfer.transfer_date', '<=', $end)
                    ->where('transfer.is_transfer', '=', $type)
                    ->whereNull('transfer.deleted_at')
                    ->orderBy('transfer.transfer_date', 'DESC')
                    ->orderBy('transfer.transfer_invoice', 'ASC')
                    ->get();
        }
        
        return $sql;
    }
    
    public function getDataPengeluaranbyDateAdmin($data){
        $sqlPengeluaran = null;
        if($data->cabang_id == 0){
            $sqlPengeluaran = DB::table('pengeluaran')
                    ->join('category_pengeluaran', 'category_pengeluaran.id', '=', 'pengeluaran.category_id')
                    ->join('users', 'pengeluaran.manager_id', '=', 'users.id')
                    ->selectRaw('pengeluaran.pengeluaran_price, pengeluaran.pengeluaran_date, category_pengeluaran.pengeluaran_name, '
                            . 'pengeluaran.keterangan, pengeluaran.id, users.name, users.cabang_name')
                    ->whereDate('pengeluaran.pengeluaran_date', '>=', $data->startDay)
                    ->whereDate('pengeluaran.pengeluaran_date', '<=', $data->endDay)
                    ->where('pengeluaran.jenis', '=', 1)
                    ->where('users.level_id', '=', 7)
//                    ->where('users.is_active', '=', 1)
                    ->whereNull('pengeluaran.deleted_at')
                    ->orderBy('pengeluaran.pengeluaran_date', 'ASC')
                    ->orderBy('users.cabang_name', 'ASC')
                    ->get();
        }
        if($data->cabang_id > 0){
            if($data->manager_id == 0){
                $sqlPengeluaran = DB::table('pengeluaran')
                            ->join('category_pengeluaran', 'category_pengeluaran.id', '=', 'pengeluaran.category_id')
                            ->join('users', 'pengeluaran.manager_id', '=', 'users.id')
                            ->selectRaw('pengeluaran.pengeluaran_price, pengeluaran.pengeluaran_date, category_pengeluaran.pengeluaran_name, '
                                    . 'pengeluaran.keterangan, pengeluaran.id, users.name, users.cabang_name')
                            ->whereDate('pengeluaran.pengeluaran_date', '>=', $data->startDay)
                            ->whereDate('pengeluaran.pengeluaran_date', '<=', $data->endDay)
                            ->where('pengeluaran.jenis', '=', 1)
                            ->where('users.cabang_id', '=', $data->cabang_id)
                            ->where('users.level_id', '=', 7)
                            ->whereNull('pengeluaran.deleted_at')
                            ->orderBy('pengeluaran.pengeluaran_date', 'ASC')
                            ->orderBy('users.cabang_name', 'ASC')
                            ->get();
            }
            if($data->manager_id > 0){
                $sqlPengeluaran = DB::table('pengeluaran')
                            ->join('category_pengeluaran', 'category_pengeluaran.id', '=', 'pengeluaran.category_id')
                            ->join('users', 'pengeluaran.manager_id', '=', 'users.id')
                            ->selectRaw('pengeluaran.pengeluaran_price, pengeluaran.pengeluaran_date, category_pengeluaran.pengeluaran_name, '
                                    . 'pengeluaran.keterangan, pengeluaran.id, users.name, users.cabang_name')
                            ->whereDate('pengeluaran.pengeluaran_date', '>=', $data->startDay)
                            ->whereDate('pengeluaran.pengeluaran_date', '<=', $data->endDay)
                            ->where('pengeluaran.jenis', '=', 1)
                            ->where('users.id', '=', $data->manager_id)
                            ->whereNull('pengeluaran.deleted_at')
                            ->orderBy('pengeluaran.pengeluaran_date', 'ASC')
                            ->orderBy('users.cabang_name', 'ASC')
                            ->get();
            }
        }
        return $sqlPengeluaran;
    }
    
    public function getMQBPengeluaran($data){
        $sqlPengeluaran = DB::table('pengeluaran')
                    ->join('category_pengeluaran', 'category_pengeluaran.id', '=', 'pengeluaran.category_id')
                    ->selectRaw('pengeluaran.pengeluaran_price, pengeluaran.pengeluaran_date, category_pengeluaran.pengeluaran_name, '
                            . 'pengeluaran.keterangan, pengeluaran.id')
                    ->where('pengeluaran.mqb_id', '=', $data->id)
                    ->where('pengeluaran.jenis', '=', 2)
                    ->whereNull('pengeluaran.deleted_at')
                    ->orderBy('pengeluaran.pengeluaran_date', 'ASC')
                    ->get();
        return $sqlPengeluaran;
    }
    
    public function getDataPengeluaranMQBbyDateAdmin($date){
        $sqlPengeluaran = DB::table('pengeluaran')
                    ->join('category_pengeluaran', 'category_pengeluaran.id', '=', 'pengeluaran.category_id')
                    ->join('users', 'pengeluaran.mqb_id', '=', 'users.id')
                    ->selectRaw('pengeluaran.pengeluaran_price, pengeluaran.pengeluaran_date, category_pengeluaran.pengeluaran_name, '
                            . 'pengeluaran.keterangan, pengeluaran.id, users.name, users.cabang_name')
                    ->whereDate('pengeluaran.pengeluaran_date', '>=', $date->startDay)
                    ->whereDate('pengeluaran.pengeluaran_date', '<=', $date->endDay)
                    ->where('pengeluaran.jenis', '=', 2)
                    ->whereNull('pengeluaran.deleted_at')
                    ->orderBy('pengeluaran.pengeluaran_date', 'ASC')
                    ->orderBy('users.cabang_name', 'ASC')
                    ->get();
        return $sqlPengeluaran;
    }
    
    public function getAllTransferAdminPerCabang($data){
        if($data == null){
            $sql = DB::table('transfer')
                    ->join('users', 'transfer.manager_id', '=', 'users.id')
                    ->selectRaw('sum(transfer.transfer_price) as transfer_price, transfer.transfer_date, users.cabang_name, users.cabang_id')
                    ->where('transfer.is_transfer', '!=', 1)
                    ->whereDate('transfer.transfer_date', '>=', date('Y-m-d',strtotime("-30 days")))
                    ->whereDate('transfer.transfer_date', '<=', date('Y-m-d'))
                    ->whereNull('transfer.deleted_at')
                    ->groupBy('transfer.transfer_date')
                    ->groupBy('users.cabang_name')
                    ->groupBy('users.cabang_id')
                    ->orderBy('users.cabang_id', 'ASC')
                    ->orderBy('transfer.transfer_date', 'DESC')
                    ->get();
        } else {
            $sql = DB::table('transfer')
                    ->join('users', 'transfer.manager_id', '=', 'users.id')
                    ->selectRaw('sum(transfer.transfer_price) as transfer_price, transfer.transfer_date, users.cabang_name, users.cabang_id')
                    ->where('transfer.is_transfer', '!=', 1)
                    ->where('users.cabang_id', '=', $data->cabang_id)
                    ->whereDate('transfer.transfer_date', '>=', $data->start)
                    ->whereDate('transfer.transfer_date', '<=', $data->end)
                    ->whereNull('transfer.deleted_at')
                    ->groupBy('transfer.transfer_date')
                    ->groupBy('users.cabang_name')
                    ->groupBy('users.cabang_id')
                    ->orderBy('users.cabang_id', 'ASC')
                    ->orderBy('transfer.transfer_date', 'DESC')
                    ->get();
        }
        return $sql;
    }
    
    public function getAllTransferAdminCabangById($data){
        $sql = DB::table('transfer')
                ->join('users', 'transfer.manager_id', '=', 'users.id')
                ->selectRaw('sum(transfer.transfer_price) as transfer_price, transfer.transfer_date, users.cabang_name, users.cabang_id')
                ->where('transfer.is_transfer', '!=', 1)
                ->where('users.cabang_id', '=', $data->cabang_id)
                ->whereDate('transfer.transfer_date', '>=', $data->start)
                ->whereDate('transfer.transfer_date', '<=', $data->end)
                ->whereNull('transfer.deleted_at')
                ->groupBy('transfer.transfer_date')
                ->groupBy('users.cabang_name')
                ->groupBy('users.cabang_id')
                ->orderBy('users.cabang_id', 'ASC')
                ->orderBy('transfer.transfer_date', 'DESC')
                ->get();
        $sql = DB::table('transfer')
                    ->join('users', 'transfer.manager_id', '=', 'users.id')
                    ->selectRaw('transfer.id, transfer.transfer_invoice, transfer.transfer_price, transfer.sales_price, transfer.pengeluaran_price, '
                            . 'transfer.transfer_date, transfer.is_transfer,'
                            . 'transfer.created_at, transfer.approve_at, transfer.reject_at,'
                            . 'users.name, users.cabang_name')
                    ->where('users.cabang_id', '=', $data->cabang_id)
                    ->whereDate('transfer.transfer_date', '>=', $data->start)
                    ->whereDate('transfer.transfer_date', '<=', $data->end)
                    ->whereNull('transfer.deleted_at')
                    ->orderBy('transfer.transfer_date', 'DESC')
                    ->orderBy('transfer.transfer_invoice', 'ASC')
                    ->get();
        return $sql;
    }
    
}
