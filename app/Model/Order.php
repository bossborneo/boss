<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Order extends Model {
    
    public function getInsertOrder($data){
        try {
            $lastInsertedID = DB::table('order')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateOrder($id, $data){
        try {
            DB::table('order')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getInsertOrderItem($data){
        try {
            $lastInsertedID = DB::table('order_item')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateOrderItem($id, $data){
        try {
            DB::table('order_item')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getListOrder($id){
        $sql = DB::table('order')
                    ->join('order_item', 'order.id', '=', 'order_item.order_id')
                    ->selectRaw('order.id, order.manager_id, order.status, order.is_active, order.created_at, order.order_at, order.confirm_at, order.kirim_at, order.terima_at')
                    ->where('order.manager_id', '=', $id)
                    ->whereNull('order_item.deleted_at')
                    ->groupBy('order.id')
                    ->groupBy('order.manager_id')
                    ->groupBy('order.status')
                    ->groupBy('order.is_active')
                    ->groupBy('order.created_at')
                    ->groupBy('order.order_at')
                    ->groupBy('order.confirm_at')
                    ->groupBy('order.kirim_at')
                    ->groupBy('order.terima_at')
                    ->orderBy('order.status', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAdminListOrder(){
        $sql = DB::table('order')
                    ->join('users', 'users.id', '=', 'order.manager_id')
                    ->join('order_item', 'order.id', '=', 'order_item.order_id')
                    ->selectRaw('order.id, order.manager_id, order.status, order.is_active, order.created_at, order.order_at, '
                            . 'order.confirm_at, order.kirim_at, order.terima_at, users.name, users.cabang_name')
                    ->whereNull('order_item.deleted_at')
                    ->groupBy('order.id')
                    ->groupBy('order.manager_id')
                    ->groupBy('order.status')
                    ->groupBy('order.is_active')
                    ->groupBy('order.created_at')
                    ->groupBy('order.order_at')
                    ->groupBy('order.confirm_at')
                    ->groupBy('order.kirim_at')
                    ->groupBy('order.terima_at')
                    ->groupBy('users.name')
                    ->groupBy('users.cabang_name')
                    ->orderBy('order.status', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAdminListOrderItem($orderId){
        $sql = DB::table('order')
                    ->join('order_item', 'order.id', '=', 'order_item.order_id')
                    ->join('purchase', 'purchase.id', '=', 'order_item.purchase_id')
                    ->selectRaw('order.status, order.is_active, order.created_at, order.order_at, order.confirm_at, order.kirim_at, order.terima_at,'
                            . 'order_item.qty, order_item.price, purchase.purchase_name, purchase.type, order_item.id as order_item_id, order.id as order_id,'
                            . 'order_item.terima_at as item_terima_at')
                    ->where('order_item.order_id', '=', $orderId)
                    ->whereNull('order_item.deleted_at')
                    ->get();
        return $sql;
    }
    
    public function getAdminOrderItem($orderId, $ItemId){
        $sql = DB::table('order_item')
                    ->join('purchase', 'purchase.id', '=', 'order_item.purchase_id')
                    ->join('order', 'order.id', '=', 'order_item.order_id')
                    ->join('users', 'users.id', '=', 'order.manager_id')
                    ->selectRaw('order_item.qty, order_item.price, purchase.purchase_name, purchase.type, order_item.id, order_item.order_id, '
                            . 'order_item.terima_at as item_terima_at, purchase.id as id_purchase, users.cabang_id, order.manager_id, order_item.keterangan')
                    ->where('order_item.id', '=', $ItemId)
                    ->where('order_item.order_id', '=', $orderId)
                    ->whereNull('order_item.deleted_at')
                    ->first();
        return $sql;
    }
    
    public function getAdminOrderId($id){
        $sql = DB::table('order')
                    ->join('users', 'users.id', '=', 'order.manager_id')
                    ->selectRaw('users.name, users.cabang_name, order.status, order.id, order.order_at, order.id, order.kirim_at')
                    ->where('order.id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getMQBOrderId($id, $manager_id){
        $sql = DB::table('order')
                    ->selectRaw('id, manager_id, status, is_active, created_at, order_at, confirm_at, kirim_at, terima_at')
                    ->where('id', '=', $id)
                    ->where('manager_id', '=', $manager_id)
                    ->first();
        return $sql;
    }
    
    public function getMQBListOrderItemId($m_id, $order_id){
        $sql = DB::table('order')
                    ->join('order_item', 'order.id', '=', 'order_item.order_id')
                    ->join('purchase', 'purchase.id', '=', 'order_item.purchase_id')
                    ->selectRaw('order.status, order.is_active, order.created_at, order.order_at, order.confirm_at, order.kirim_at, order.terima_at,'
                            . 'order_item.qty, order_item.price, purchase.purchase_name, purchase.type, order.id as id_order, order_item.id as id_order_item, '
                            . 'order_item.purchase_id, order_item.terima_at as item_terima_at, order_item.keterangan')
                    ->where('order.manager_id', '=', $m_id)
                    ->where('order_item.order_id', '=', $order_id)
                    ->whereNull('order_item.deleted_at')
                    ->get();
        return $sql;
    }
    
    public function getMQBListOrderItem(){
        $sql = DB::table('order')
                    ->selectRaw('id, manager_id, status, is_active, created_at, order_at, confirm_at, kirim_at, terima_at')
                    ->get();
        return $sql;
    }
    
    public function getMQBListOrderItemIdNew($m_id, $order_id, $item_order_id){
        $sql = DB::table('order')
                    ->join('order_item', 'order.id', '=', 'order_item.order_id')
                    ->join('purchase', 'purchase.id', '=', 'order_item.purchase_id')
                    ->selectRaw('order.status, order.is_active, order.created_at, order.order_at, order.confirm_at, order.kirim_at, order.terima_at,'
                            . 'order_item.qty, order_item.price, purchase.purchase_name, purchase.type, order.id as id_order, order_item.id as id_order_item, order_item.purchase_id,'
                            . 'order_item.keterangan')
                    ->where('order.id', '=', $order_id)
                    ->where('order.manager_id', '=', $m_id)
                    ->where('order_item.id', '=', $item_order_id)
                    ->whereNull('order_item.deleted_at')
                    ->first();
        return $sql;
    }
    
    
    
}

