<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Stock extends Model {
    
    public function getInsertStock($data){
        try {
            DB::table('stock')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getGetSumStock($itemId){
        $sql = DB::table('stock')
                    ->selectRaw('sum(case when type_stock = 1 then amount end) as amount_tambah,'
                            . 'sum(case when type_stock = 2 then amount end) as amount_kurang')
                    ->where('item_purchase_id', '=', $itemId)
                    ->first();
        return $sql;
    }
    
    public function getCheckStock($idSales){
        $sql = DB::table('stock')
                    ->selectRaw('id, item_purchase_id, user_id, amount')
                    ->where('sales_id', '=', $idSales)
                    ->where('type_stock', '=', 2)
                    ->first();
        return $sql;
    }
    
    public function getUpdateStock($id, $data){
        try {
            DB::table('stock')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getDeleteStock($id){
        try {
            DB::table('stock')->where('id', '=', $id)->delete();
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getManagerHistoryStock($cbg_id, $type_stock, $date){
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('purchase.purchase_name, purchase.type,'
                            . 'sum(stock.amount) as amount')
                    ->whereNull('purchase.deleted_at')
                    ->where('item_purchase.cabang_id', '=', $cbg_id)
                    ->where('stock.type_stock', '=', $type_stock)
                    ->whereDate('stock.created_at', '=', $date)
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getSuperAdminHistoryStock($cbg_id, $sundayStart, $sundayEnd){
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('item_purchase.id as id_item, purchase.purchase_name, purchase.type, sum(stock.amount) as qty_input, '
                            . '(
                                        select sum(sales.amount)
                                        from sales 
                                        where sales.item_purchase_id = item_purchase.id 
                                        AND sales.sale_date >= "'.$sundayStart.'" 
                                        AND sales.sale_date < "'.$sundayEnd.'" 
                                        AND sales.deleted_at is null
                                ) 
                                as qty_output')
                    ->where('stock.type_stock', '=', 1)
                    ->where('item_purchase.cabang_id', '=', $cbg_id)
                    ->whereDate('stock.created_at', '>=', $sundayStart)
                    ->whereDate('stock.created_at', '<', $sundayEnd)
                    ->groupBy('item_purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getSuperAdminHistoryStockIsSunday($cbg_id, $sundayEnd){
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('item_purchase.id as id_item, purchase.purchase_name, purchase.type, sum(stock.amount) as qty_input, '
                            . '(
                                        select sum(sales.amount)
                                        from sales 
                                        where sales.item_purchase_id = item_purchase.id 
                                        AND sales.sale_date = " '.$sundayEnd.' " 
                                        AND sales.deleted_at is null
                                ) 
                                as qty_output')
                    ->where('stock.type_stock', '=', 1)
                    ->where('item_purchase.cabang_id', '=', $cbg_id)
                    ->whereDate('stock.created_at', '=', $sundayEnd)
                    ->groupBy('item_purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->orderBy('purchase.type', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getSuperAdminHistoryStockIsNotSunday($cbg_id, $sundayStart, $sundayEnd){
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('item_purchase.id as id_item, purchase.purchase_name, purchase.type, sum(stock.amount) as qty_input, '
                            . '(
                                        select sum(sales.amount)
                                        from sales 
                                        where sales.item_purchase_id = item_purchase.id 
                                        AND sales.sale_date >= "'.$sundayStart.'" 
                                        AND sales.sale_date <= "'.$sundayEnd.'" 
                                        AND sales.deleted_at is null
                                ) 
                                as qty_output')
                    ->where('stock.type_stock', '=', 1)
                    ->where('item_purchase.cabang_id', '=', $cbg_id)
                    ->whereDate('stock.created_at', '>=', $sundayStart)
                    ->whereDate('stock.created_at', '<=', $sundayEnd)
                    ->groupBy('item_purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getSuperAdminHistoryStockSafra($cbg_id, $sundayStart, $sundayEnd){
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('item_purchase.id as id_item, purchase.purchase_name, purchase.type, sum(stock.amount) as qty_input, '
                            . '(
                                        select sum(sales.amount)
                                        from sales 
                                        where sales.item_purchase_id = item_purchase.id 
                                        AND sales.sale_date >= "'.$sundayStart.'" 
                                        AND sales.sale_date < "'.$sundayEnd.'" 
                                        AND sales.deleted_at is null
                                ) 
                                as qty_output')
                    ->where('stock.type_stock', '=', 1)
                    ->where('item_purchase.cabang_id', '=', $cbg_id)
                    ->whereDate('stock.created_at', '>=', $sundayStart)
                    ->whereDate('stock.created_at', '<', $sundayEnd)
                    ->where('purchase.type', '=', 10)
                    ->groupBy('item_purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getSuperAdminHistoryStockIsSundaySafra($cbg_id, $sundayEnd){
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('item_purchase.id as id_item, purchase.purchase_name, purchase.type, sum(stock.amount) as qty_input, '
                            . '(
                                        select sum(sales.amount)
                                        from sales 
                                        where sales.item_purchase_id = item_purchase.id 
                                        AND sales.sale_date = " '.$sundayEnd.' " 
                                        AND sales.deleted_at is null
                                ) 
                                as qty_output')
                    ->where('stock.type_stock', '=', 1)
                    ->where('item_purchase.cabang_id', '=', $cbg_id)
                    ->whereDate('stock.created_at', '=', $sundayEnd)
                    ->where('purchase.type', '=', 10)
                    ->groupBy('item_purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->orderBy('purchase.type', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getSuperAdminHistoryStockIsNotSundaySafra($cbg_id, $sundayStart, $sundayEnd){
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('item_purchase.id as id_item, purchase.purchase_name, purchase.type, sum(stock.amount) as qty_input, '
                            . '(
                                        select sum(sales.amount)
                                        from sales 
                                        where sales.item_purchase_id = item_purchase.id 
                                        AND sales.sale_date >= "'.$sundayStart.'" 
                                        AND sales.sale_date <= "'.$sundayEnd.'" 
                                        AND sales.deleted_at is null
                                ) 
                                as qty_output')
                    ->where('stock.type_stock', '=', 1)
                    ->where('item_purchase.cabang_id', '=', $cbg_id)
                    ->whereDate('stock.created_at', '>=', $sundayStart)
                    ->whereDate('stock.created_at', '<=', $sundayEnd)
                    ->where('purchase.type', '=', 10)
                    ->groupBy('item_purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getSuperAdminEdit($item_id, $dateSunday){
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('item_purchase.id as id_item, purchase.purchase_name, purchase.type,'
                            . 'sum(case when stock.type_stock = 1 then stock.amount end) as qty_input')
                    ->whereNull('purchase.deleted_at')
                    ->where('item_purchase.id', '=', $item_id)
                    ->where('stock.type_stock', '=', 1)
                    ->whereDate('stock.created_at', '=', $dateSunday)
                    ->groupBy('item_purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->first();
        return $sql;
    }
    
    public function getSuperAdminCekStock($item_id, $dateSunday){
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->selectRaw('item_purchase.id as id_item, stock.id as id_stock, stock.amount, item_purchase.cabang_id')
                    ->where('item_purchase.id', '=', $item_id)
                    ->where('stock.type_stock', '=', 1)
                    ->whereDate('stock.created_at', '=', $dateSunday)
                    ->orderBy('stock.amount', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getHistoryAllStockIsSunday($sundayEnd){
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('purchase.id, purchase.purchase_name, purchase.type, sum(stock.amount) as qty_input, '
                            . '(
                                        SELECT SUM(s.amount)
                                            FROM stock as ss 
                                            JOIN sales as s ON ss.sales_id = s.id
                                            JOIN item_purchase as i  ON i.id = s.item_purchase_id
                                            JOIN purchase as p ON p.id = i.purchase_id
                                            WHERE ss.type_stock = 2
                                            AND p.id = purchase.id
                                            AND s.sale_date = "'.$sundayEnd.'" 
                                            AND s.deleted_at is null
                                ) 
                                as qty_output')
                    ->where('stock.type_stock', '=', 1)
                    ->whereDate('stock.created_at', '=', $sundayEnd)
                    ->groupBy('purchase.id')
                    ->groupBy('item_purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->orderBy('purchase.type', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getHistoryAllStockIsNotSunday($sundayStart, $sundayEnd){
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('purchase.id, purchase.purchase_name, purchase.type, sum(stock.amount) as qty_input, '
                            . '(
                                        SELECT SUM(s.amount)
                                            FROM stock as ss 
                                            JOIN sales as s ON ss.sales_id = s.id
                                            JOIN item_purchase as i  ON i.id = s.item_purchase_id
                                            JOIN purchase as p ON p.id = i.purchase_id
                                            WHERE ss.type_stock = 2
                                            AND p.id = purchase.id
                                            AND s.sale_date >= "'.$sundayStart.'" 
                                            AND s.sale_date <= "'.$sundayEnd.'" 
                                            AND s.deleted_at is null
                                ) 
                                as qty_output')
                    ->where('stock.type_stock', '=', 1)
                    ->whereDate('stock.created_at', '>=', $sundayStart)
                    ->whereDate('stock.created_at', '<=', $sundayEnd)
                    ->groupBy('purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getCheckStockFromOrder($item_id, $mid){
        $sql = DB::table('stock')
                    ->selectRaw('id')
                    ->where('item_purchase_id', '=', $item_id)
                    ->where('type_stock', '=', 1)
                    ->where('user_id', '=', $mid)
                    ->orderBy('id', 'DESC')
                    ->first();
        return $sql;
    }
    
    public function getCheckStockLastSunday($item_id, $date){
        $sql = DB::table('stock')
                    ->selectRaw('id')
                    ->where('item_purchase_id', '=', $item_id)
                    ->where('type_stock', '=', 1)
                    ->whereDate('created_at', '=', $date)
                    ->first();
        return $sql;
    }
    
    
    
}

