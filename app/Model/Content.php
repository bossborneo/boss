<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Content extends Model {
    
    public function getAllContent(){
        $sql = DB::table('content')
                    ->selectRaw('id, created_by, title, short_desc, full_desc, image_url, publish, created_at')
                    ->where('publish', '=', 1)
                    ->orderBy('id', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getCountContent(){
        $sql = DB::table('content')->selectRaw('id')->where('publish', '=', 1)->count();
        return $sql;
    }
    
    public function getContentByField($fieldName, $name){
        $sql = DB::table('content')
                    ->selectRaw('id, created_by, title, short_desc, full_desc, image_url, publish, created_at')
                    ->where($fieldName, '=', $name)
                    ->where('publish', '=', 1)
                    ->first();
        return $sql;
    }
    
    public function getContentLast($notIn){
        $lastSunday = date('Y-m-d', strtotime( "-1 weeks sunday" ));
        if($notIn == null){
            $sql = DB::table('content')
                    ->selectRaw('id, created_by, title, short_desc, full_desc, image_url, publish, created_at')
                    ->where('publish', '=', 1)
                    ->whereDate('created_at', '>=', $lastSunday)
                    ->orderBy('id', 'DESC')
                    ->first();
        } else {
            $whereNotIn = explode(',', $notIn);
            $sql = DB::table('content')
                    ->selectRaw('id, created_by, title, short_desc, full_desc, image_url, publish, created_at')
                    ->where('publish', '=', 1)
                    ->whereDate('created_at', '>=', $lastSunday)
                    ->whereNotIn('id', $whereNotIn)
                    ->orderBy('id', 'DESC')
                    ->first();
        }
        return $sql;
    }
    
    public function getInsertContent($data){
        try {
            DB::table('content')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getDeleteContentById($id){
        try {
            DB::table('content')->where('id', '=', $id)->delete();
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateContent($id, $data){
        try {
            DB::table('content')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
}
