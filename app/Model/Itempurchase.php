<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Itempurchase extends Model {
    
    public function getInsertItem($data){
        try {
            $lastInsertedID = DB::table('item_purchase')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateItem($id, $data){
        try {
            DB::table('item_purchase')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getCheckItem($fieldName, $name){
        $sql = DB::table('item_purchase')
                    ->where($fieldName, '=', $name)
                    ->first();
        return $sql;
    }
    
    public function getCheckItemAddStock($data, $purchase_id){
        $sql = DB::table('item_purchase')
                    ->selectRaw('id')
//                    ->where('manager_id', '=', $data->id) //biar semua manager di tiap cabang dapat tahu jumlah stock
                    ->where('cabang_id', '=', $data->cabang_id)
                    ->where('purchase_id', '=', $purchase_id)
                    ->first();
        return $sql;
    }
    
    public function getSumSalesByManager($id){
        $sql = DB::table('item_purchase')
                    ->join('sales', 'sales.item_purchase_id', '=', 'item_purchase.id')
                    ->selectRaw('sum(sales.sale_price) as sum_sales ')
                    ->where('item_purchase.manager_id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getReportStockOpname(){
        $sql = DB::table('item_purchase')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('purchase.purchase_name, sum(item_purchase.qty) as sum_qty,
                                            sum(item_purchase.sisa) as sum_sisa, purchase.type')
                    ->groupBy('purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->get();
        return $sql;
    }
    
    public function getReportStockOpnameCabang($cbg){
        $sql = DB::table('item_purchase')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('purchase.purchase_name, sum(item_purchase.qty) as sum_qty,
                                            sum(item_purchase.sisa) as sum_sisa, purchase.type, item_purchase.id as id_item')
                    ->groupBy('purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->groupBy('item_purchase.id')
                    ->where('item_purchase.cabang_id', '=', $cbg)
                    ->get();
        return $sql;
    }
    
    public function getCheckItemStilHas($cbgId){
        $sql = DB::table('item_purchase')
                    ->selectRaw('sum(sisa) as total_sisa')
                    ->where('cabang_id', '=', $cbgId)
                    ->groupBy('cabang_id')
                    ->first();
        return $sql;
    }
    
    public function getCheckItemStilHasBarang($id){
        $sql = DB::table('item_purchase')
                    ->selectRaw('sum(sisa) as total_sisa')
                    ->where('purchase_id', '=', $id)
                    ->groupBy('cabang_id')
                    ->first();
        return $sql;
    }
    
    public function getManagerItem($id){
        $sql = DB::table('item_purchase')
                    ->selectRaw('sum(sisa) as total_sisa')
                    ->where('purchase_id', '=', $id)
                    ->groupBy('cabang_id')
                    ->first();
        return $sql;
    }
    
    public function getPurchaseFromItem($idItem){
        $sql = DB::table('item_purchase')
                    ->selectRaw('purchase.purchase_name, purchase.type, item_purchase.price, purchase.is_poin, purchase.kondisi')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->where('item_purchase.id', '=', $idItem)
                    ->first();
        return $sql;
    }
    
    public function getItemPurchaseCabang($idItem){
        $sql = DB::table('item_purchase')
                    ->selectRaw('purchase.purchase_name, purchase.type, item_purchase.price, cabang.cabang_name,'
                            . 'item_purchase.qty, item_purchase.sisa')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->join('cabang', 'cabang.id', '=', 'item_purchase.cabang_id')
                    ->where('item_purchase.id', '=', $idItem)
                    ->first();
        return $sql;
    }
    
    public function getHistoryItemPurchase($idItem){
        $sql = DB::table('item_purchase')
                    ->selectRaw('stock.type_stock, stock.amount, stock.created_at, users.name, users.level_id')
                    ->join('stock', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('users', 'users.id', '=', 'stock.user_id')
                    ->where('item_purchase.id', '=', $idItem)
                    ->where('stock.type_stock', '=', 1)
                    ->orderBy('stock.created_at', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getHistoryItemPurchaseOut($idItem){
        $sql = DB::table('stock')
                    ->join('sales', 'sales.id', '=', 'stock.sales_id')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('users', 'users.id', '=', 'stock.user_id')
                    ->selectRaw('min(stock.created_at) as created_at, users.name, users.level_id, sales.sale_date,
                        sum(case when stock.type_stock = 2 then stock.amount end) as out_put,
                        sum(case when stock.type_stock = 3 then stock.amount else 0 end) as out_put_delete')
                    ->where('item_purchase.id', '=', $idItem)
                    ->whereIn('stock.type_stock', array(2, 3))
                    ->groupBy('stock.sales_id')
                    ->groupBy('users.name')
                    ->groupBy('users.level_id')
                    ->groupBy('sales.sale_date')
                    ->orderBy('created_at', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getCheckItemAddStockAdmin($cbg_id, $purchase_id){
        $sql = DB::table('item_purchase')
                    ->selectRaw('id')
                    ->where('cabang_id', '=', $cbg_id)
                    ->where('purchase_id', '=', $purchase_id)
                    ->first();
        return $sql;
    }
    
    public function getSisaStockNew($id){
        $sql = DB::table('stock')
                    ->join('item_purchase', 'stock.item_purchase_id', '=', 'item_purchase.id')
                    ->join('purchase', 'purchase.id', '=', 'item_purchase.purchase_id')
                    ->selectRaw('item_purchase.id as id_item_purchase, purchase.purchase_name, purchase.type, sum(stock.amount) as qty_input, '
                            . '(
                                        select sum(sales.amount)
                                        from sales 
                                        where sales.item_purchase_id = item_purchase.id 
                                        AND sales.deleted_at is null
                                ) 
                                as qty_output, item_purchase.price')
                    ->where('stock.type_stock', '=', 1)
                    ->where('item_purchase.purchase_id', '=', $id)
                    ->groupBy('item_purchase.id')
                    ->groupBy('purchase.purchase_name')
                    ->groupBy('purchase.type')
                    ->groupBy('item_purchase.price')
                    ->orderBy('purchase.type', 'ASC')
                    ->orderBy('purchase.purchase_name', 'ASC')
                    ->get();
        return $sql;
    }
    
    
}

