<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder{
    public function run(){
         DB::table('level')->delete();
         $level = array(
            array('kode_name' => 'Adm', 'level_name' => 'Admin'),
            array('kode_name' => 'GM', 'level_name' => 'General Manager'),
            array('kode_name' => 'SEM', 'level_name' => 'Senior Executive Manager'),
            array('kode_name' => 'EM', 'level_name' => 'Executive Manager'),
            array('kode_name' => 'SM', 'level_name' => 'Senior Manager'),
            array('kode_name' => 'MQB', 'level_name' => 'MQB'),
            array('kode_name' => 'M', 'level_name' => 'Manager'),
            array('kode_name' => 'AM', 'level_name' => 'Assistant Manager'),
            array('kode_name' => 'TLD', 'level_name' => 'Top Leader'),
            array('kode_name' => 'LD', 'level_name' => 'Leader'),
            array('kode_name' => 'TR', 'level_name' => 'Trainer'),
            array('kode_name' => 'MD', 'level_name' => 'Merchandiser'),
             array('kode_name' => 'RT', 'level_name' => 'Retrainer'),
        );
        foreach($level as $row){
            DB::table('level')->insert($row);
        }
        
        DB::table('cabang')->delete();
        $cabang = array(
            array('cabang_name' => 'Ambon'),
            array('cabang_name' => 'Bali'),
            array('cabang_name' => 'Balikpapan'),
            array('cabang_name' => 'Banjarmasin'),
            array('cabang_name' => 'Batam'),
            array('cabang_name' => 'Bone'),
            array('cabang_name' => 'Kendari'),
            array('cabang_name' => 'Kupang'),
            array('cabang_name' => 'Lombok Timur'),
            array('cabang_name' => 'Makasar 1'),
            array('cabang_name' => 'Makasar 2'),
            array('cabang_name' => 'Manado'),
            array('cabang_name' => 'Mataram'),
            array('cabang_name' => 'Pontianak'),
            array('cabang_name' => 'Samarinda'),
            array('cabang_name' => 'Singkawang'),
            array('cabang_name' => 'Sintang'),
            array('cabang_name' => 'Sorong', 'extra' => 100000),
            array('cabang_name' => 'Tanjung'),
            array('cabang_name' => 'Ternate'),
            array('cabang_name' => 'Tanjung Pinang'),
            
            
        );
        foreach($cabang as $row){
            DB::table('cabang')->insert($row);
        }
        
       DB::table('users')->delete();
        $users = array(
            array(
                'username' => 'superadmin@master.com',
                'password' => bcrypt('superadmin_boss2018'),
                'name' => 'Super Admin IT',
                'is_active' => 1,
                'is_login' => 1,
                'user_type' => 1
            ),
            array(
                'username' => 'masteradmin@master.com',
                'password' => bcrypt('masteradmin_boss2018'),
                'name' => 'Master Admin',
                'is_active' => 1,
                'is_login' => 1,
                'user_type' => 2
            ),
            array(
                'username' => 'admin1@master.com',
                'password' => bcrypt('admin_boss2018'),
                'name' => 'Admin 1',
                'is_active' => 1,
                'is_login' => 1,
                'user_type' => 3
            ),
            array(
                'username' => 'admin2@master.com',
                'password' => bcrypt('admin_boss2018'),
                'name' => 'Admin 2',
                'is_active' => 1,
                'is_login' => 1,
                'user_type' => 3
            )
        );
        foreach($users as $row){
            DB::table('users')->insert($row);
        }
        
//        $cabang = 'Bali';
//        $sql = DB::table('cabang')
//                    ->selectRaw('id, cabang_name, extra')
//                    ->where('cabang_name', '=', $cabang)
//                    ->first();
//        $users1 = array(
//            array(
//                'username' => 'gmbali2018',
//                'password' => bcrypt('123'),
//                'name' => 'GM Bali',
//                'is_active' => 1,
//                'is_login' => 1,
//                'user_type' => 5,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 2, 
//            ),
//            array(
//                'username' => 'sembali2018',
//                'password' => bcrypt('123'),
//                'name' => 'SEM Bali',
//                'is_active' => 1,
//                'is_login' => 1,
//                'user_type' => 5,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 3, 
//                'sponsor_id' => '[5]',
//                'parent_id' => 5
//            ),
//            array(
//                'username' => 'iskandarbali',
//                'password' => bcrypt('123'),
//                'name' => 'Iskandar',
//                'is_active' => 1,
//                'is_login' => 1,
//                'user_type' => 5,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 4, 
//                'sponsor_id' => '[5],[6]',
//                'parent_id' => 6
//            ),
//            array(
//                'username' => 'smbali2018',
//                'password' => bcrypt('123'),
//                'name' => 'SM Bali',
//                'is_active' => 1,
//                'is_login' => 1,
//                'user_type' => 5,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 5, 
//                'sponsor_id' => '[5],[6],[7]',
//                'parent_id' => 7
//            ),
//            array(
//                'username' => 'mqbbali2018',
//                'password' => bcrypt('123'),
//                'name' => 'MQB Bali',
//                'is_active' => 1,
//                'is_login' => 1,
//                'user_type' => 5,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 6, 
//                'sponsor_id' => '[5],[6],[7],[8]',
//                'parent_id' => 8
//            ),
//            array(
//                'username' => 'denatabali',
//                'password' => bcrypt('123'),
//                'name' => 'Denata',
//                'is_active' => 1,
//                'is_login' => 1,
//                'user_type' => 5,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 7, 
//                'sponsor_id' => '[5],[6],[7],[8],[9]',
//                'parent_id' => 9
//            ),
//            array(
//                'username' => 'boss2018_00011',
//                'password' => bcrypt('123'),
//                'name' => 'Asmen Bali',
//                'is_active' => 1,
//                'is_login' => 0,
//                'user_type' => 10,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 8, 
//                'sponsor_id' => '[5],[6],[7],[8],[9],[10]',
//                'parent_id' => 10
//            ),
//            array(
//                'username' => 'boss2018_00012',
//                'password' => bcrypt('123'),
//                'name' => 'ILHAM NEGARE S.Kep NS',
//                'is_active' => 1,
//                'is_login' => 0,
//                'user_type' => 10,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 9, 
//                'sponsor_id' => '[5],[6],[7],[8],[9],[10],[11]',
//                'parent_id' => 11
//            ),
//            array(
//                'username' => 'boss2018_00013',
//                'password' => bcrypt('123'),
//                'name' => 'SELVIA S.Pd',
//                'is_active' => 1,
//                'is_login' => 0,
//                'user_type' => 10,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 10, 
//                'sponsor_id' => '[5],[6],[7],[8],[9],[10],[11],[12]',
//                'parent_id' => 12
//            ),
//            array(
//                'username' => 'boss2018_00014',
//                'password' => bcrypt('123'),
//                'name' => 'KASIANUS ASAU A.Md',
//                'is_active' => 1,
//                'is_login' => 0,
//                'user_type' => 10,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 10, 
//                'sponsor_id' => '[5],[6],[7],[8],[9],[10],[11],[12]',
//                'parent_id' => 12
//            ),
//            array(
//                'username' => 'boss2018_00015',
//                'password' => bcrypt('123'),
//                'name' => 'SUSTRIANI A.Md. Farm',
//                'is_active' => 1,
//                'is_login' => 0,
//                'user_type' => 10,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 11, 
//                'sponsor_id' => '[5],[6],[7],[8],[9],[10],[11],[12],[13]',
//                'parent_id' => 13
//            ),
//            array(
//                'username' => 'boss2018_00016',
//                'password' => bcrypt('123'),
//                'name' => 'I GEDE S.Kep NS',
//                'is_active' => 1,
//                'is_login' => 0,
//                'user_type' => 10,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 11, 
//                'sponsor_id' => '[5],[6],[7],[8],[9],[10],[11],[12],[14]',
//                'parent_id' => 14
//            ),
//            array(
//                'username' => 'boss2018_00017',
//                'password' => bcrypt('123'),
//                'name' => 'ABI MUNAWAR',
//                'is_active' => 1,
//                'is_login' => 0,
//                'user_type' => 10,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 12, 
//                'sponsor_id' => '[5],[6],[7],[8],[9],[10],[11],[12],[15]',
//                'parent_id' => 15
//            ),
//            array(
//                'username' => 'boss2018_00018',
//                'password' => bcrypt('123'),
//                'name' => 'ICA',
//                'is_active' => 1,
//                'is_login' => 0,
//                'user_type' => 10,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 12, 
//                'sponsor_id' => '[5],[6],[7],[8],[9],[10],[11],[12],[16]',
//                'parent_id' => 16
//            ),
//            array(
//                'username' => 'boss2018_00019',
//                'password' => bcrypt('123'),
//                'name' => 'APRI',
//                'is_active' => 1,
//                'is_login' => 0,
//                'user_type' => 10,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 13, 
//                'sponsor_id' => '[5],[6],[7],[8],[9],[10],[11],[12],[15],[17]',
//                'parent_id' => 17
//            ),
//            array(
//                'username' => 'boss2018_00020',
//                'password' => bcrypt('123'),
//                'name' => 'RENDIK A.Md Kep',
//                'is_active' => 1,
//                'is_login' => 0,
//                'user_type' => 10,
//                'cabang_id' => $sql->id, 
//                'cabang_name' => $sql->cabang_name,
//                'level_id' => 13, 
//                'sponsor_id' => '[5],[6],[7],[8],[9],[10],[11],[12],[16],[18]',
//                'parent_id' => 18
//            ),
//        );
//        foreach($users1 as $row1){
//            DB::table('users')->insert($row1);
//        }
        
        DB::table('purchase')->delete();
        $purchase = array(
            array(
                'purchase_name' => 'Hand Accupoint', 'type' => 1,
                'hpp' => 0, 'main_price' => 40000
            ),
            array(
                'purchase_name' => 'Asam Urat', 'type' => 2,
                'hpp' => 3400,
                'main_price' => 25000
            ),
            array(
                'purchase_name' => 'Gula Darah', 'type' => 2,
                'hpp' => 3400,
                'main_price' => 25000
            ),
            array(
                'purchase_name' => 'Kolesterol', 'type' => 2,
                'hpp' => 13750, 'main_price' => 35000
            ),

            array(
                'purchase_name' => 'Albarokah',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'ALFAPLUS',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'Aloe Vera Softgel',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 412000
            ),
            array(
                'purchase_name' => 'Andrononi',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Antibet',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Asamuric',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'B_Redo',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'B-Coles',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'B-Curcuma',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'B-Curfit',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'Beepropolis',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 500000
            ),
            array(
                'purchase_name' => 'Bio Glukol Capsule N-DS',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Bitter Mellon Softgel',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 456000
            ),
            array(
                'purchase_name' => 'Blueberry Sofgel',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 588000
            ),
            array(
                'purchase_name' => 'B-Refit',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'B-Uric',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'Cabe Jawa',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Canseino',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Capsul Minyak Garlic',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Chitosan Capsule',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Colagen Softgel',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 550000
            ),
            array(
                'purchase_name' => 'Cordyceps Capsule',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'Curmitop',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Daun Kelor',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Diabitas',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Epimedium Capsule',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 402000
            ),
            array(
                'purchase_name' => 'Fish Oil Softgel',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Fitabets',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Gamas Emas',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Ganoderma Lucidum',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 364000
            ),
            array(
                'purchase_name' => 'Ginkgo Biloba Tablet',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 364000
            ),
            array(
                'purchase_name' => 'Golden Chalazz',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'Golden Sea',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'Growth & Development',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 410000
            ),
            array(
                'purchase_name' => 'Habasa Garlic',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Habassyjos',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'Heptofit',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Herbal Bathn',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Hersidar',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Hihg Calcium',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Kapsul VCO',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Lecithin Softgel',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Leka Capsule',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 116000
            ),
            array(
                'purchase_name' => 'M.M.P',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 364000
            ),
            array(
                'purchase_name' => 'Manggistech',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 450000
            ),
            array(
                'purchase_name' => 'Multi-vitamin',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 414000
            ),
            array(
                'purchase_name' => 'Nosterol',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Obamig',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Pare',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Pear Powder Capsul',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 534000
            ),
            array(
                'purchase_name' => 'Pelvi Fit',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Proles',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'RB Propolis',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'Sekar Slim',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'SPIRA',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 400000
            ),
            array(
                'purchase_name' => 'Spirulina Capsule N-DS',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Spirulina Tablets 200',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 364000
            ),
            array(
                'purchase_name' => 'Stabitens',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Total-X',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 350000
            ),
            array(
                'purchase_name' => 'Vitamin C',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 300000
            ),
            array(
                'purchase_name' => 'World Slimming Capsule',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 500000
            ),
            array(
                'purchase_name' => 'Zinc Tablet',
                'type' => 3,
                'hpp' => 100000, 
                'main_price' => 300000
            ),
        );
        foreach($purchase as $rowP){
            DB::table('purchase')->insert($rowP);
        }
        
        
        
    }
}
