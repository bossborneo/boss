<?php

use Illuminate\Database\Seeder;

class manager extends Seeder{

    public function run(){
        $cabang = 'Bali';
        $sql = DB::table('cabang')
                    ->selectRaw('id, cabang_name, extra')
                    ->where('cabang_name', '=', $cabang)
                    ->first();
        $users = array(
            array(
                'username' => 'manager2018',
                'password' => bcrypt('123'),
                'name' => 'Si Manager',
                'is_active' => 1,
                'is_login' => 1,
                'user_type' => 5,
                'cabang_id' => $sql->id, 
                'cabang_name' => $sql->cabang_name,
                'level_id' => 7, 
            ),
            array(
                'username' => 'asmen2018',
                'password' => bcrypt('123'),
                'name' => 'Si Asisten Manager',
                'is_active' => 1,
                'is_login' => 0,
                'user_type' => 10,
                'cabang_id' => $sql->id, 
                'cabang_name' => $sql->cabang_name,
                'level_id' => 8, 
                'sponsor_id' => 5, 
            ),
            array(
                'username' => 'topleader2018',
                'password' => bcrypt('123'),
                'name' => 'Si Top Leader',
                'is_active' => 1,
                'is_login' => 0,
                'user_type' => 10,
                'cabang_id' => $sql->id, 
                'cabang_name' => $sql->cabang_name,
                'level_id' => 9, 
                'sponsor_id' => 5, 
            ),
            array(
                'username' => 'leader2018',
                'password' => bcrypt('123'),
                'name' => 'Si Leader',
                'is_active' => 1,
                'is_login' => 0,
                'user_type' => 10,
                'cabang_id' => $sql->id, 
                'cabang_name' => $sql->cabang_name,
                'level_id' => 10, 
                'sponsor_id' => 5, 
            ),
            array(
                'username' => 'trainer2018',
                'password' => bcrypt('123'),
                'name' => 'Si Trainer',
                'is_active' => 1,
                'is_login' => 0,
                'user_type' => 10,
                'cabang_id' => $sql->id, 
                'cabang_name' => $sql->cabang_name,
                'level_id' => 11, 
                'sponsor_id' => 5, 
            ),
            array(
                'username' => 'md2018',
                'password' => bcrypt('123'),
                'name' => 'Si Merchandiser',
                'is_active' => 1,
                'is_login' => 0,
                'user_type' => 10,
                'cabang_id' => $sql->id, 
                'cabang_name' => $sql->cabang_name,
                'level_id' => 12, 
                'sponsor_id' => 5, 
            ),
        );
        foreach($users as $row){
            DB::table('users')->insert($row);
        }
    }
}
