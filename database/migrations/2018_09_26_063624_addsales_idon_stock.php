<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddsalesIdonStock extends Migration{
    
    public function up(){
        Schema::table('stock', function(Blueprint $table){
            $table->integer('sales_id')->nullable();
            $table->index('sales_id');
        });
    }
	
    public function down(){
        Schema::table('stock', function(Blueprint $table){
            $table->dropColumn('sales_id');
        });
    }
}
