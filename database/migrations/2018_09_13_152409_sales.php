<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sales extends Migration{

    public function up() {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_purchase_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('gm_id')->nullable()->unsigned();
            $table->integer('sem_id')->nullable()->unsigned();
            $table->integer('em_id')->nullable()->unsigned();
            $table->integer('sm_id')->nullable()->unsigned();
            $table->integer('mqb_id')->nullable()->unsigned();
            $table->integer('manager_id')->nullable()->unsigned();
            $table->integer('asmen_id')->nullable()->unsigned();
            $table->integer('tld_id')->nullable()->unsigned();
            $table->integer('ld_id')->nullable()->unsigned();
            $table->integer('tr_id')->nullable()->unsigned();
            $table->integer('md_id')->nullable()->unsigned();
            $table->string('invoice', 40);
            $table->decimal('amount', 8, 2);
            $table->decimal('sale_price', 12, 2);
            $table->date('sale_date');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
            
            $table->index('item_purchase_id');
            $table->index('user_id');
            $table->index('gm_id');
            $table->index('sem_id');
            $table->index('em_id');
            $table->index('sm_id');
            $table->index('mqb_id');
            $table->index('manager_id');
            $table->index('asmen_id');
            $table->index('tld_id');
            $table->index('ld_id');
            $table->index('tr_id');
            $table->index('md_id');
            $table->index('invoice');
            $table->index('amount');
            $table->index('sale_price');
            $table->index('sale_date');
            $table->index('created_at');
            $table->index('deleted_at');
        });
    }

    public function down() {
        Schema::dropIfExists('sales');
    }
}
