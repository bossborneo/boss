<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Purchases extends Migration {

    public function up() {
        Schema::create('purchase', function (Blueprint $table) {
            $table->increments('id');
            $table->string('purchase_name', 150);
            $table->smallInteger('type')->comment('1 = alat, 2 = strip, 3 = obat, 10 = lainnya');
            $table->decimal('hpp', 12, 2);
            $table->decimal('main_price', 12, 2);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
            
            $table->index('purchase_name');
            $table->index('type');
            $table->index('hpp');
            $table->index('main_price');
            $table->index('created_at');
            $table->index('deleted_at');
        });
    }

    public function down() {
        Schema::dropIfExists('purchase');
    }
}
