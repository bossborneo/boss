<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HistoryLevel extends Migration{
    
    public function up(){
        Schema::create('history_level', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->smallInteger('old_level_id')->nullable();
            $table->smallInteger('new_level_id')->nullable();
            $table->integer('created_by')->unsigned();
            $table->timestamp('active_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            
            $table->index('user_id');
            $table->index('old_level_id');
            $table->index('new_level_id');
            $table->index('created_by');
            $table->index('active_at');
            $table->index('created_at');
        });
    }

    public function down(){
        Schema::dropIfExists('history_level');
    }
}
