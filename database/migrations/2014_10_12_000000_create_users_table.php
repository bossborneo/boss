<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
    
    public function up(){
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 50)->unique('users_username_unique');
            $table->string('password', 100);
            $table->string('name', 100);
            $table->string('phone', 25)->nullable();
            $table->text('address')->nullable();
            $table->tinyInteger('is_active')->default(1)->comment('0 = tidak aktif, 1 = aktif');
            $table->tinyInteger('is_login')->default(0)->comment('0 = tidak dapat login, 1 = dapat login');
            $table->timestamp('active_at')->nullable();
            $table->smallInteger('user_type')->default(10)->comment('1 = super admin, 2 = master admin, 3 = admin, 5 = manager, 10 = user');
            $table->text('permissions')->nullable();
            $table->smallInteger('cabang_id')->nullable();
            $table->string('cabang_name', 100)->nullable();
            $table->smallInteger('level_id')->nullable();
            $table->integer('created_by')->default(1)->unsigned();
            $table->text('sponsor_id')->nullable();
            $table->integer('parent_id')->nullable()->unsigned();
            $table->timestamp('last_login')->nullable();
            $table->integer('kanan')->nullable()->unsigned();
            $table->integer('kiri')->nullable()->unsigned();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->rememberToken();
            
            
            $table->index('username');
            $table->index('password');
            $table->index('name');
            $table->index('phone');
            $table->index('is_active');
            $table->index('active_at');
            $table->index('user_type');
            $table->index('cabang_id');
            $table->index('cabang_name');
            $table->index('level_id');
            $table->index('created_by');
            $table->index('parent_id');
            $table->index('last_login');
            $table->index('kanan');
            $table->index('kiri');
            $table->index('created_at');
            $table->index('deleted_at');
        });
    }

    public function down(){
        Schema::dropIfExists('users');
    }
}
