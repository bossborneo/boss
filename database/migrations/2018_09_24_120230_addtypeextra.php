<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addtypeextra extends Migration{
    
    public function up(){
        Schema::table('cabang', function(Blueprint $table){
            $table->integer('extra_strip')->default(0);
            $table->integer('extra_alat')->default(0);
            $table->index('extra_strip');
            $table->index('extra_alat');
        });
    }
	
    public function down(){
        Schema::table('cabang', function(Blueprint $table){
            $table->dropColumn('extra_strip');
            $table->dropColumn('extra_alat');
        });
    }
}
