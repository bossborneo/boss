<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddrtId extends Migration{
    public function up(){
        Schema::table('sales', function(Blueprint $table){
            $table->integer('rt_id')->nullable()->unsigned();
            $table->index('rt_id');
        });
    }
	
    public function down(){
        Schema::table('sales', function(Blueprint $table){
            $table->dropColumn('rt_id');
        });
    }
}
