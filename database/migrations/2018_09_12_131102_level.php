<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Level extends Migration {

    public function up() {
        Schema::create('level', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_name', 30);
            $table->string('level_name', 100);
            $table->timestamp('created_at')->useCurrent();
            
            $table->index('kode_name');
            $table->index('level_name');
            $table->index('created_at');
        });
    }

    public function down() {
        Schema::dropIfExists('level');
    }
}
