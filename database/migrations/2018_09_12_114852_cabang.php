<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cabang extends Migration {

    public function up(){
        Schema::create('cabang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cabang_name', 100);
            $table->integer('extra')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
            
            $table->index('cabang_name');
            $table->index('extra');
            $table->index('created_at');
            $table->index('deleted_at');
        });
    }

    public function down(){
        Schema::dropIfExists('cabang');
    }
}
