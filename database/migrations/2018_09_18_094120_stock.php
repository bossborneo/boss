<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Stock extends Migration{

    public function up() {
        Schema::create('stock', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_purchase_id')->unsigned();
            $table->smallInteger('type_stock')->comment('1 = purchase bertambah, 2 = purchase berkurang');
            $table->integer('user_id')->unsigned();
            $table->decimal('amount', 8, 2);
            $table->timestamp('created_at')->useCurrent();
            
            $table->index('item_purchase_id');
            $table->index('type_stock');
            $table->index('user_id');
            $table->index('amount');
            $table->index('created_at');
        });
    }

    public function down(){
        Schema::dropIfExists('stock');
    }
}
