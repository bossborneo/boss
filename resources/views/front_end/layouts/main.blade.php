<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/png" href="/images/favicon.jpeg"/>
        <title>BOSS - Borneo Organization Support System</title>
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link href="{{ asset('css/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/grayscale.min.css') }}" rel="stylesheet">
        @yield('styles')
    </head>
    <body id="page-top">
        @yield('content')
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/grayscale.min.js') }}"></script>
        @yield('javascripts')
    </body>
</html>
