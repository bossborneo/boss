@extends('front_end.layouts.main')
@section('content')
    
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ URL::to('/boss_login') }}">Sign In</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Header -->
    <header class="masthead">
        <div class="container d-flex h-100 align-items-center">
            <div class="mx-auto text-center">
                <h1 class="mx-auto my-0 text-uppercase"><img src="/images/borneo_logo.png" style='width:85%;height:auto;'></h1>
                <h2 class="text-white-50 mx-auto mt-2 mb-5">
                    We are working day and night to build something great. A really exciting trading experience. 
                </h2>
            </div>
        </div>
    </header>
    <footer class="bg-black small text-center text-white-50">
        <div class="container" style="color:rgba(255,255,255,.5)!important;">
            Copyright &copy; 2018
        </div>
    </footer>

@endsection

