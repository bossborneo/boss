@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar_org')
<?php //MENU UTAMA ?>
<div class="main-panel">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-toggle">
                <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <p class="navbar-brand">{{$getIdOrg->name}}</p>
        </div>
    </div>
</nav>
<div class="content">
    <div class="row">
        @if($transferNotif != null)
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="alert alert-danger alert-with-icon alert-dismissible fade show" data-notify="container">
                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="nc-icon nc-simple-remove"></i>
                    </button>
                    <span data-notify="icon" class="nc-icon nc-alert-circle-i"></span>
                    <span data-notify="message">
                        Transfer Rejected Message ' <b>{{$transferNotif->keterangan}}</b> ' &nbsp;&nbsp;
                        <a class="text-muted" href="{{ URL::to('/') }}/m/transfers">Click Disini</a>
                    </span>
                </div>
            </div>
        @endif
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-3">
                            <div class="icon-big text-center icon-warning">
                                <i class="nc-icon nc-money-coins text-success"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-9">
                            <div class="numbers">
                                <p class="card-category">Total Penjualan ({{date('F')}})</p>
                                <p class="card-title">Rp. {{number_format($getAllSalesMonth->jml_price, 0, ',', '.')}}
                                </p>
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-4">
                            <div class="icon-big text-center icon-warning">
                                <i class="nc-icon nc-single-02 text-primary"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-8">
                            <div class="numbers">
                                <p class="card-category">Total Crew</p>
                                <p class="card-title">{{$getCountCrew}}
                                </p>
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title"> Top 10 Sales Crew ({{date('F')}})</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Level</th>
                                    <th>Penjualan (Rp.)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($getTop10 != null)
                                <?php $no = 0; ?>
                                @foreach($getTop10 as $row)
                                <?php 
                                    $no++; 
                                    $level = 'General Manager';
                                    if($row->level_id == 3){
                                        $level = 'Senior Executive Manager';
                                    }
                                    if($row->level_id == 4){
                                        $level = 'Executive Manager';
                                    }
                                    if($row->level_id == 5){
                                        $level = 'Senior Manager';
                                    }
                                    if($row->level_id == 6){
                                        $level = 'MQB';
                                    }
                                    if($row->level_id == 7){
                                        $level = 'Manager';
                                    }
                                    if($row->level_id == 8){
                                        $level = 'Asmen';
                                    }
                                    if($row->level_id == 9){
                                        $level = 'Top Leader';
                                    }
                                    if($row->level_id == 10){
                                        $level = 'Leader';
                                    }
                                    if($row->level_id == 11){
                                        $level = 'Trainer';
                                    }
                                    if($row->level_id == 12){
                                        $level = 'Merchandiser';
                                    }
                                    if($row->level_id == 13){
                                        $level = 'Retrainer';
                                    }
                                    ?>
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$row->name}}</td>
                                    <td>{{$level}}</td>
                                    <td>{{number_format($row->jml_price, 0, ',', '.')}}</td>
                                </tr>
                                @endforeach    
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="chart-bar"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div id="chart-bar-user"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('admin-javascript')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script type="text/javascript">
    Highcharts.chart('chart-bar', {
        chart: {
            type: 'column',
            zoomType: 'x'
        },
        colors: [
             '#1DC7EA'
        ],
    legend: {
      enabled: false
    },
    title: {
    text:  'Laju Penambahan Crew'
    },
    subtitle: {
      text: ' '
    },
    xAxis: {
    categories: [<?php echo $month ?> ],
      tickmarkPlacement: 'on',
      tickInterval: 1,
      minRange: 1
    },
    yAxis: {
      min: 0,
      title: {
        text: 'total register',
        style: {
          fontSize: '0px'
        }
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' +
    	'<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    plotOptions: {
        series: {
          cursor: 'pointer',
          label: { connectorAllowed: false },
          pointStart: 0,
          pointWidth: 40,
          point: {
              events: {
              }
          }
        }
    },
    series: [{
      name: 'Total',
      data: [<?php echo $down ?>]
    },]
    });
</script>
<script type="text/javascript">
    Highcharts.chart('chart-bar-user', {
    chart: {
      type: 'column',
      zoomType: 'x'
    },
    colors: [
      '#1DC7EA'
    ],
    legend: {
      enabled: false
    },
    title: {
    text:  'Laju Penjualan'
    },
    subtitle: {
      text: ' '
    },
    xAxis: {
    categories: [<?php echo $monthSale ?>],
      tickmarkPlacement: 'on',
      tickInterval: 1,
      minRange: 1
    },
    yAxis: {
      min: 0,
      title: {
        text: 'total register',
        style: {
          fontSize: '0px'
        }
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' +
    	'<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    plotOptions: {
        series: {
          cursor: 'pointer',
          label: { connectorAllowed: false },
          pointStart: 0,
          pointWidth: 40,
          point: {
              events: {
              }
          }
        }
    },
    series: [{
      name: 'Total',
      data: [<?php echo $priceSale ?>]
    },]
    });
</script>
@stop