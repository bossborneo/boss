@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar_org')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$getIdOrg->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-money-coins text-success"></i>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="numbers">
                                    <p class="card-category">Total Penjualan ({{$day}})</p>
                                    <p class="card-title" style="font-size: 25px;">Rp. {{number_format($reportM->jml_price, 0, ',', '.')}}
                                    </p>
                                    <p>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}} ({{$day}})</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Tgl.</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Jumlah</th>
                                        <th>Asmen</th>
                                        <th>Top Leader</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($detail != null)
                                        <?php $no = 0; ?>
                                        @foreach($detail as $row)
                                            <?php $no++; ?>
                                            <?php
                                                $level = 'Manager';
                                                if($row->level_id == 2){
                                                    $level = 'General Manager';
                                                }
                                                if($row->level_id == 3){
                                                    $level = 'Senior Executive Manager';
                                                }
                                                if($row->level_id == 4){
                                                    $level = 'Executive Manager';
                                                }
                                                if($row->level_id == 5){
                                                    $level = 'Senior Manager';
                                                }
                                                if($row->level_id == 6){
                                                    $level = 'MQB';
                                                }
                                                if($row->level_id == 8){
                                                    $level = 'Asmen';
                                                }
                                                if($row->level_id == 9){
                                                    $level = 'Top Leader';
                                                }
                                                if($row->level_id == 10){
                                                    $level = 'Leader';
                                                }
                                                if($row->level_id == 11){
                                                    $level = 'Trainer';
                                                }
                                                if($row->level_id == 12){
                                                    $level = 'Merchandiser';
                                                }
                                                if($row->level_id == 13){
                                                    $level = 'Retrainer';
                                                }
                                            ?>
                                            <tr>
                                            <td>{{$no}}</td>
                                            <td>{{date('d-m-Y', strtotime($row->sale_date))}}</td>
                                            <td>{{$row->name}}</td>
                                            <td>{{$level}}</td>
                                            <td>{{number_format($row->tot_price, 0, ',', ',')}}</td>
                                            <td>
                                                @if($row->level_id != $row->ua_level_id)
                                                    {{$row->asmen_name}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($row->level_id != $row->ut_level_id)
                                                    {{$row->tld_name}}
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export Detail Penjualan {{$day}}'
                    }
                ],
                searching: false
        } );
    } );
</script>
@stop