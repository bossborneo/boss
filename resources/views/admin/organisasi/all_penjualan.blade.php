@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar_org')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$getIdOrg->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">Filter Penjualan</h5>
                    </div>
                    <div class="card-body" style="min-height: auto;">
                        <form class="login100-form validate-form" method="get" action="/org/data-sales/{{$getIdOrg->id}}">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Tanggal Awal</label>
                                            <input type="text" class="form-control datepickerStart" name="start_date">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Tanggal Akhir</label>
                                            <input type="text" class="form-control datepickerEnd" name="end_date">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                             <button type="submit" class="form-control btn btn-sm btn-info " title="Filter" style="margin: 0;">Cari</button>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> 
                            {{$headerTitle}} 
                        </h5>
                    </div>
                    
                    <div class="card-body">
                        <p class="title">{{$day}}</p>
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <!--<th>Invoice</th>-->
                                        <th>Barang</th>
                                        <th>Type</th>
                                        <th>Jumlah</th>
                                        <th>Harga (Rp.)</th>
                                        <th>Tgl. Sale</th>
                                        <th>Nama</th>
                                        <th>Posisi</th>
                                        <th>Absensi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                        ?>
                                    @if($allSales != null)
                                        @foreach($allSales as $row)
                                        <?php 
                                            $no++;
                                            $type = 'Obat';
                                            if($row->type == 1){
                                                $type = 'Alat';
                                            }
                                            if($row->type == 2){
                                                $type = 'Strip';
                                            }
                                            $level = 'General Manager';
                                            if($row->level_id == 3){
                                                $level = 'Senior Executive Manager';
                                            }
                                            if($row->level_id == 4){
                                                $level = 'Executive Manager';
                                            }
                                            if($row->level_id == 5){
                                                $level = 'Senior Manager';
                                            }
                                            if($row->level_id == 6){
                                                $level = 'MQB';
                                            }
                                            if($row->level_id == 7){
                                                $level = 'Manager';
                                            }
                                            if($row->level_id == 8){
                                                $level = 'Assistant Manager';
                                            }
                                            if($row->level_id == 9){
                                                $level = 'Top Leader';
                                            }
                                            if($row->level_id == 10){
                                                $level = 'Leader';
                                            }
                                            if($row->level_id == 11){
                                                $level = 'Trainer';
                                            }
                                            if($row->level_id == 12){
                                                $level = 'Merchandiser';
                                            }
                                            if($row->level_id == 13){
                                                $level = 'Retrainer';
                                            }
                                            $is_masuk = 'Masuk';
                                            if($row->is_masuk == 2){
                                                $is_masuk = 'Sakit';
                                            }
                                            if($row->is_masuk == 2){
                                                $is_masuk = 'Izin';
                                            }
                                            if($row->is_masuk == 2){
                                                $is_masuk = 'Alpha';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <!--<td>{{$row->invoice}}</td>-->
                                                <td>{{$row->purchase_name}}</td>
                                                <td>{{$type}}</td>
                                                <td>{{number_format($row->amount, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_price, 0, ',', ',')}}</td>
                                                <td><?php  echo (date('d-m-Y', strtotime($row->sale_date))) ?></td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$level}}</td>
                                                <td>{{$is_masuk}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    <?php $noz = count($allSales); ?>
                                    @if($allZero != null)
                                        @foreach($allZero as $row)
                                        <?php
                                            $noz++;
                                            $level = 'General Manager';
                                            if($row->level_id == 3){
                                                $level = 'Senior Executive Manager';
                                            }
                                            if($row->level_id == 4){
                                                $level = 'Executive Manager';
                                            }
                                            if($row->level_id == 5){
                                                $level = 'Senior Manager';
                                            }
                                            if($row->level_id == 6){
                                                $level = 'MQB';
                                            }
                                            if($row->level_id == 7){
                                                $level = 'Manager';
                                            }
                                            if($row->level_id == 8){
                                                $level = 'Assistant Manager';
                                            }
                                            if($row->level_id == 9){
                                                $level = 'Top Leader';
                                            }
                                            if($row->level_id == 10){
                                                $level = 'Leader';
                                            }
                                            if($row->level_id == 11){
                                                $level = 'Trainer';
                                            }
                                            if($row->level_id == 12){
                                                $level = 'Merchandiser';
                                            }
                                            if($row->level_id == 13){
                                                $level = 'Retrainer';
                                            }
                                            $is_masuk = 'Masuk';
                                            if($row->is_masuk == 2){
                                                $is_masuk = 'Sakit';
                                            }
                                            if($row->is_masuk == 2){
                                                $is_masuk = 'Izin';
                                            }
                                            if($row->is_masuk == 2){
                                                $is_masuk = 'Alpha';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$noz}}</td>
                                                <td>--</td>
                                                <td>--</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td><?php  echo (date('d-m-Y', strtotime($row->masuk_at))) ?></td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$level}}</td>
                                                <td>{{$is_masuk}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
//    $("#rmSales").on("show.bs.modal", function(e) {
//        var link = $(e.relatedTarget);
//        $(this).find(".modal-content").load(link.attr("href"));
//    });
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export {{$day}}'
                    }
                ],
                searching: false
        } );
    } );
    $('.datepickerStart').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
    $('.datepickerEnd').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
//    $('.datepickerEdit').datetimepicker({
//        format: 'YYYY-MM-DD',
//        icons: {
//          time: "fa fa-clock-o",
//          date: "fa fa-calendar",
//          up: "fa fa-chevron-up",
//          down: "fa fa-chevron-down",
//          previous: 'fa fa-chevron-left',
//          next: 'fa fa-chevron-right',
//          today: 'fa fa-screenshot',
//          clear: 'fa fa-trash',
//          close: 'fa fa-remove'
//        }
//    });
</script>
@stop