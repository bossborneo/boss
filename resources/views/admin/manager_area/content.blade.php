@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="row mb-2">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="news-title">
                                            <h2>{{$getData->title}}</h2>
                                        </div>
                                        <div class="news-cats">
                                        </div>
                                        <hr>
                                        <div class="news-image text-center">
                                            <img src="{{$getData->image_url}}" style="max-width: 640px;">
                                            <p class="text-muted "><?php echo $getData->title; ?></p>
                                        </div>
                                        <div class="news-content">
                                            <?php echo $getData->full_desc; ?>
                                        </div>
                                    </div>
                                </div>
<!--                                <div class="update ml-auto mr-auto">
                                    <a class="btn btn-secondary" href="{{ URL::to('/') }}/m/ajax/set/cookies/{{$getData->id}}">Tutup</a>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@stop