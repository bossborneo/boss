@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> 
                            {{$headerTitle}} 
                        </h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        @if($type == 1)
                            @if($dataSales != null)
                                <form class="login100-form validate-form" method="post" action="/m/action-sale/edit/{{$dataSales->id}}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Barang</label>
                                                <input type="text" disabled="" class="form-control" value="{{$dataSales->purchase_name}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Jumlah</label>
                                                <input type="number" min="0" step="0.5"  name="amount" class="form-control" value="{{number_format($dataSales->amount, 1, '.', '.')}}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tanggal (tahun-bulan-tgl)</label>
                                                <input type="text" class="form-control datepickerEdit" name="sale_date" value="{{$dataSales->sale_date}}">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="cekId" value="{{$dataSales->id}}" >
                                    <div class="modal-footer" style="margin-right: 10px;">
                                        <a class="btn btn-secondary" href="{{ URL::to('/') }}/m/data-sales">Batal</a>
                                        <button type="submit" class="btn btn-primary">Confirm</button>
                                    </div>
                                </form>
                            @endif
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-javascript')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
    
    $('.datepickerEdit').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
</script>
@stop
