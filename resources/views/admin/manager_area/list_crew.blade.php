@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        <!--<a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/add-crew">Tambah Crew</a>-->
                        @if($type == 1)
                            <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/listupgrade-crew">Upgrade Crew</a>
                        @endif
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/m/my-bonus">My Bonus</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Tgl Aktif</th>
                                        <th>Level</th>
                                        <th>Bonus</th>
                                        <th class="td-actions text-left">##</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($type == 1)
                                    <?php $no = 0; ?>
                                    @if($allDownline != null)
                                        @foreach($allDownline as $rows)
                                            <?php
                                                $no++;
                                                $level = '';
                                                if($rows->level_id == 2){
                                                    $level = 'General Manager';
                                                }
                                                if($rows->level_id == 3){
                                                    $level = 'Senior Executive Manager';
                                                }
                                                if($rows->level_id == 4){
                                                    $level = 'Executive Manager';
                                                }
                                                if($rows->level_id == 5){
                                                    $level = 'Senior Manager';
                                                }
                                                if($rows->level_id == 6){
                                                    $level = 'MQB';
                                                }
                                                if($rows->level_id == 7){
                                                    $level = 'Manager';
                                                }
                                                if($rows->level_id == 8){
                                                    $level = 'Asisten Manager';
                                                }
                                                if($rows->level_id == 9){
                                                    $level = 'Top Leader';
                                                }
                                                if($rows->level_id == 10){
                                                    $level = 'Leader';
                                                }
                                                if($rows->level_id == 11){
                                                    $level = 'Trainer';
                                                }
                                                if($rows->level_id == 12){
                                                    $level = 'Merchandiser';
                                                }
                                                if($rows->level_id == 13){
                                                    $level = 'Retrainer';
                                                }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$rows->name}}</td>
                                                <td><?php  echo (date('d M Y', strtotime($rows->active_at))) ?></td>
                                                <td>{{$level}}</td>
                                                <td>
                                                    <a class="text-primary" href="{{ URL::to('/') }}/m/bonus/{{$rows->id}}">detail</a>
                                                </td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        @if($rows->level_id <= 9)
                                                            <a class="text-primary" href="{{ URL::to('/') }}/m/struktur/{{$rows->id}}"><i class="nc-icon nc-vector"></i></a>
                                                        @endif
                                                        @if($rows->level_id == 13)
                                                            <a rel="tooltip"  data-toggle="modal" data-target="#editModal" class="text-primary" href="{{ URL::to('/') }}/edit-crew/{{$rows->id}}"><i class="nc-icon nc-badge"></i></a>
                                                            &nbsp;&nbsp;
                                                            <a rel="tooltip" title="Remove" class="text-danger" href="{{ URL::to('/') }}/rm-crew/{{$rows->id}}" onclick="return confirm('Apakah anda ingin menghapus crew {{$rows->name}}')"><i class="nc-icon nc-simple-remove"></i></a>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    @endif
                                    
                                    @if($type == 2)
                                    @if($allDownline != null)
                                        <?php $noo = 0; ?>
                                        @foreach($allDownline as $rows)
                                            @if($rows->level_id == 13)
                                            <?php
                                                $noo++;
                                            ?>
                                            <tr>
                                                <td>{{$noo}}</td>
                                                 <td>{{$rows->name}}</td>
                                                <td>{{$rows->phone}}</td>
                                                <td>Retrainer</td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                            <a rel="tooltip"  data-toggle="modal" data-target="#upgradeModal" class="text-primary" href="{{ URL::to('/') }}/upgrade-crew/{{$rows->id}}"><i class="nc-icon nc-trophy"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                    @endif
                                </tbody>
                            </table>
                            @if($type == 1)
                            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="crewModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                            @endif
                            @if($type == 2)
                            <div class="modal fade" id="upgradeModal" tabindex="-1" role="dialog" aria-labelledby="crewModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: -1 },
                 ]
        } );
    } );
    $("#editModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $("#upgradeModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop