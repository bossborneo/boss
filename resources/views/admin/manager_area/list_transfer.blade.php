@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Invoice</th>
                                        <th>Transfer (Rp.)</th>
                                        <th>Tgl</th>
                                        <th>###</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                            <?php $no++; ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->transfer_invoice}}</td>
                                                <td>{{number_format($row->transfer_price, 0, ',', ',')}}</td>
                                                <td><?php  echo (date('d M Y', strtotime($row->transfer_date))) ?></td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip" title="Detail Transfer {{$row->transfer_invoice}}" class="btn btn-sm btn-info" href="{{ URL::to('/') }}/m/transfer/{{$row->id}}/1"><i class="fa fa-edit"></i></a>
                                                        @if($row->is_transfer == 0)
                                                            <span class="btn btn-sm btn-warning">Pending</span>
                                                        @endif
                                                        @if($row->is_transfer == 1)
                                                            <span class="btn btn-sm btn-success">Approve</span>
                                                        @endif
                                                        @if($row->is_transfer == 2)
                                                            <a class="btn btn-sm btn-info" href="{{ URL::to('/') }}/m/transfer/{{$row->id}}/5">Kirim Ulang</a>
                                                        @endif
                                                        @if($row->is_transfer == 3)
                                                            <span class="btn btn-sm btn-warning">Re-Pending</span>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
</div>
@stop

    @section('admin-styles')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
    @stop

    @section('admin-javascript')
    <script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    @if($view == true)
    <script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
    @endif
    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'export {{$headerTitle}}'
                        }
                    ],
                    searching: false,
                    columnDefs: [
                        { orderable: false, targets: -1 }
                     ],
                     pageLength: 10
            } );
        } );

        $("#confirmAdmin").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find(".modal-content").load(link.attr("href"));
        });
    </script>
    @stop
