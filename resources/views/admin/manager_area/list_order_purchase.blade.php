@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        @if(date('w') != 0)
                            <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/mqb/order-barang">Order Barang</a>
                        @endif
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Tgl Order</th>
                                        <th>Status</th>
                                        <th class="td-actions text-left">##</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                    @if($getData != null)
                                        @foreach($getData as $rows)
                                            <?php
                                                $no++;
                                                $status = 'Proses Admin';
                                                $btn = 'text-primary';
                                                if($rows->status == 2){
                                                    $status = 'Proses Kirim';
                                                    $btn = 'text-primary';
                                                }
                                                if($rows->status == 3){
                                                    $status = 'Terima barang';
                                                    $btn = 'text-success';
                                                }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td><?php  echo (date('d M Y', strtotime($rows->order_at))) ?></td>
                                                <td><span class="{{$btn}}">{{$status}}</span></td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a class="btn btn-primary btn-sm" href="{{ URL::to('/') }}/mqb/detail/order-barang/{{$rows->id}}">detail</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: -1 },
                 ]
        } );
    } );
</script>
@stop