@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/mqb/list-order-barang">List Order</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <form class="login100-form validate-form" method="post" action="/mqb/order-barang">
                            {{ csrf_field() }}
                            <h4 class="card-title pl-3"><small>Alat</small></h4>
                            <div class="row">
                                @if($purchseKosong != null)
                                    @foreach($purchseKosong as $rowPurchase)
                                        @if($rowPurchase->type == 1)
                                            <div class="col-sm-3">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox"  name="purchase_id[]" value="{{$rowPurchase->id_purchase}}">
                                                        <span class="form-check-sign"></span>
                                                        {{$rowPurchase->purchase_name}}
                                                    </label>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>

                            <h4 class="card-title pl-3"><small>Strip</small></h4>
                            <div class="row">
                                @if($purchseKosong != null)
                                    @foreach($purchseKosong as $rowPurchase)
                                        @if($rowPurchase->type == 2)
                                            <div class="col-sm-3">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name="purchase_id[]" value="{{$rowPurchase->id_purchase}}">
                                                        <span class="form-check-sign"></span>
                                                        {{$rowPurchase->purchase_name}}
                                                    </label>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>

                            <h4 class="card-title pl-3"><small>Obat</small></h4>
                            <div class="row">
                                @if($purchseKosong != null)
                                    @foreach($purchseKosong as $rowPurchase)
                                        @if($rowPurchase->type == 3)
                                            <div class="col-sm-3">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name="purchase_id[]"  value="{{$rowPurchase->id_purchase}}">
                                                        <span class="form-check-sign"></span>
                                                        {{$rowPurchase->purchase_name}}
                                                    </label>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary btn-round">Confirm</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('admin-javascript')
<script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').dataTable({
            "pageLength": 50
        });
    } );
    $("#addStockModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop