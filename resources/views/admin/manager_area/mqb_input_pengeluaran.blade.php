@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">
                    {{$dataUser->name}}
                </p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">{{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <form class="login100-form validate-form"  method="post" action="/mqb/input/pengeluaran" id="frmAll">
                            {{ csrf_field() }}
                            
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Tanggal</label>
                                            <input type="text" class="form-control datepicker" name="out_date" id="out_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="searchTotalSales"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-check">
                                          <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" id="chkPengeluaran" name="checkPengeluaran" value="1"> Input Pengeluaran (unchecked jika tidak ada)
                                            <span class="form-check-sign"></span>
                                          </label>
                                        </div>
                                     </div>   
                                </div>
                                <div class="row" id="searchKategori-list">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="button" class="btn btn-sm btn-block btn-behance " id="addrow" style="display: none" value="Tambah" />
                                </div>
                            </div>    
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <input type="button" name="btn" value="Simpan" id="submitBtn"data-toggle="modal" data-target="#confirmSubmit" class="btn btn-primary btn-round" />
                                </div>
                            </div>
                        </form>
                        <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
                                    </div>
                                    <div class="modal-body" id="confirmSales" style="overflow-y: auto;max-height: 330px;">
                                        
                                    </div>
                                    <div class="modal-footer" style="margin-right: 10px;">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('admin-javascript')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script>
    $('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        },
        maxDate: '<?php echo date('Y-m-d',strtotime("1 days")); ?>',
    });
   
    $(document).ready(function () {
    var counter = 2;

    $("#addrow").on("click", function () {
        var date_value = $('#out_date').val();
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/m/more-category" + "/" + date_value,
            success: function(url){
                    $("#addPenjualan").append(url);
                    counter++;
            }
        });
    });

    $("div#addPenjualan").on("click", "#rmLine", function (event) {
        $(this).closest('div.row').remove();       
        counter -= 1
    });


});
</script>    
<script>
        $('#submitBtn').click(function() {
            var data = $("#frmAll").serializeArray();
            $.ajax({
                type: "POST",
                url: "{{ URL::to('/') }}/mqb/cek-pengeluaran" ,
                data: data,
                success: function(url){
                    $("#confirmSales" ).empty();
                    $("#confirmSales").html(url);
                }
            });
       });

       $('#submit').click(function(){
           $('#frmAll').submit();
       });
</script>
<script type="text/javascript">
    
    $(document).on('dp.change', 'input.datepicker', function() {
        var date_value = $('#out_date').val();
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/mqb/total-pengeluaran/" + date_value,
            success: function(url){
                    $( "#searchTotalSales" ).empty();
                    $("#searchTotalSales").html(url);
            }
        });
    });
    
    function getSum() {
        var oldPrice = $('#_price').val();
        var amount = $('#_amount').val();
        var itemId = $("#_item option:selected").val();
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/m/sum" + "/" + itemId + "/"+ amount +"/ "+oldPrice ,
            success: function(url){
                    $('#_price').val(url);
            }
        });
    }
    
    $(function () {
        $("#chkPengeluaran").click(function () {
            if ($(this).is(":checked")) {
                var date_value = $('#out_date').val();
                if(date_value.length > 0){
                    $.ajax({
                        type: "GET",
                        url: "{{ URL::to('/') }}/m/search-category" + "/" + date_value,
                        success: function(url){
                                $( "#searchKategori-list" ).empty();
                                $("#searchKategori-list").html(url);
                        }
                    });
                    $("#addrow").show();
                }
            } else {
                $("#addrow").hide();
                $('div').closest('#rowKategori').remove();
                $('div').closest('#searchKategori-listRow').remove();
            }
        });
    });
</script>
@stop