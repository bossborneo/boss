@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')

<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">{{$headerTitle}}</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/view-crew">Data Crew</a>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/listupgrade-crew">Upgrade Crew</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <form id="addForm" class="login100-form validate-form" method="post" action="/new-crew">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Cabang (disabled)</label>
                                        <input type="text" class="form-control" disabled="" value="{{$dataUser->cabang_name}}" id="_cabang">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mgr/TL/AsMen</label>
                                        <select name="parent_id" class="form-control" title="Pilih Mgr/TL/AsMen" id="_parent_id">
                                            <option selected="" data-id="belum memilih top leader">- Pilih Mgr/TL/AsMen -</option>
                                            <option value="{{$dataUser->id}}" data-id="{{$dataUser->name}}">{{$dataUser->name}}</option>
                                            @if($allDownline != null)
                                                @foreach($allDownline as $rowDownline)
                                                    <option value="{{$rowDownline->id}}" data-id="{{$rowDownline->name}}">{{$rowDownline->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Lengkap</label>
                                        <input type="text" class="form-control" name="name" id="_name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No Telepon</label>
                                        <input type="text" class="form-control" name="phone" placeholder="optional" id="_phone">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea class="form-control textarea" name="address" id="_address"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <input type="button" name="btn" value="Simpan" id="submitBtn"data-toggle="modal" data-target="#confirmSubmit" class="btn btn-primary btn-round" />
                                </div>
                            </div>
                        </form>
                        <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Nama Lengkap</label>
                                                    <h6 class="form-control-static" id="name"></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Nama Sponsor</label>
                                                    <h6 class="form-control-static" id="parent_id"></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>No. Telepon</label>
                                                    <h6 class="form-control-static" id="phone"></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Alamat</label>
                                                    <h6 class="form-control-static" id="address"></h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer" style="margin-right: 10px;">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-javascript')
    <script>
        $('#submitBtn').click(function() {
            $('#name').text($('#_name').val());
            var parentName = $('select#_parent_id').find(':selected').data('id');
            $('#parent_id').text(parentName);
            $('#phone').text($('#_phone').val());
            $('#address').text($('#_address').val());
       });

       $('#submit').click(function(){
           $('#addForm').submit();
       });
</script>
@stop