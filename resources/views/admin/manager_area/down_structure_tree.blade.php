@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="card card-user">
                    <div class="image" style="background-color: #ccc;">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <img class="avatar border-gray" src="/images/default-avatar.png" alt="...">
                                <h5 class="title">{{$dataUser->name}}</h5>
                            </a>
                        </div>
                        <h5 class="description text-center">{{$dataUser->cabang_name}}</h5>
                    </div>
                    <div class="card-footer">
                        <hr>
                        <div class="button-container">
                            <div class="row">
                                <div class="col-md-12">
                                    <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/m/down-structures">Struktur</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        <div id="treeview_json"></div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/tree.min.css') }}" />
@stop

@section('admin-javascript')
<script src="{{ asset('js/jstree.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){ 
     $('#treeview_json').jstree({
	'plugins': ["wholerow"],
        'core' : {
            'data' : {
                "url" : "{{ URL::to('/') }}/adm/ajax/org/tree/{{$dataUser->id}}",
                "dataType" : "json" 
            }
        }
    }) 
});
</script>
@stop