@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> 
                            {{$headerTitle}} 
                        </h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <div class="row">
                            <?php //<div id="treeview_json"></div> ?>
                            @if($back == true)
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <a href="{{ URL::to('/') }}/m/tree/structure" class="btn btn-dark btn-sm">
                                        Back
                                    </a>
                                    @if($dataUser->id != $sessionUser->id)
                                    <a href="{{ URL::to('/') }}/m/tree/structure?get_id={{$dataUser->parent_id}}" class="btn btn-purple btn-sm">
                                        One Level Up
                                    </a>
                                    @endif
                                </div>
                            @endif
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="region region-content">
                                    <div id="block-system-main" class="block block-system clearfix">
                                        <div class="binary-genealogy-tree binary_tree_extended">
                                            <div class="sponsor-tree-wrapper">
                                                <div class="eps-sponsor-tree eps-tree"> <?php // style="max-width: 270px;" ?>
                                                    <ul>
                                                        <li>
                                                            <div class="eps-nc" nid="12">
                                                                <div class="user-pic">
                                                                    <div class="images_wrapper" style="font-size: 40px;margin: 10px 0;">
                                                                        <a href="#" class="text-success">
                                                                            <i class="text-primary nc-icon nc-single-02"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="user-name">
                                                                   {{$dataUser->name}}
                                                                </div>
                                                                
                                                            </div>
                                                            @if($getData != null)
                                                            <ul>
                                                                @foreach($getData as $row)
                                                                    <li>
                                                                        <div class="eps-nc" nid="13">
                                                                            <div class="user-pic">
                                                                                <div class="images_wrapper text-success" style="font-size: 40px;margin: 10px 0;">
                                                                                    <a href="#">
                                                                                        <i class="text-primary fa fa-user"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="user-name" class="bg-c-blue">
                                                                                {{$row['name']}}
                                                                            </div>
                                                                            <div class="user-name" style="background: #3b5998; color: #fff;">
                                                                                {{$row['level_name']}}
                                                                            </div>
                                                                        </div>
                                                                        @if(!empty($row['children']))
                                                                        <ul>
                                                                            <li>
                                                                                <div class="eps-nc" nid="13" style='padding: 0;'>
                                                                                    <div class="user-pic" style='width: 25px; height: 25px;'>
                                                                                        <div class="images_wrapper text-success" style="font-size: 18px;">
                                                                                            <a href="{{ URL::to('/') }}/m/tree/structure?get_id={{$row['id']}}">
                                                                                                <i class="text-muted nc-icon nc-simple-add"></i>
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        @endif
                                                                    </li>
                                                                    
                                                                @endforeach
                                                            </ul>
                                                            @endif
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
@stop

@section('admin-styles')
<?php //<link rel="stylesheet" href="{{ asset('css/tree.min.css') }}" /> ?>
<link href="{{ asset('css/tree-style.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/developer.css') }}" rel="stylesheet" type="text/css" />
@stop

<?php
/*
@section('admin-javascript')
<script src="{{ asset('js/jstree.min.js') }}"></script> 
<script type="text/javascript">
$(document).ready(function(){ 
     $('#treeview_json').jstree({
	'plugins': ["wholerow"],
        'core' : {
            'data' : {
                "url" : "{{ URL::to('/') }}/m/ajax/tree",
                "dataType" : "json" 
            }
        }
    }) 
});
</script>
@stop
 * 
 */
?>



<?php
/*

@extends('layout.member.master')
@section('title', 'Sponsor Diagram')

@section('content')

<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="region region-content">
                            <div id="block-system-main" class="block block-system clearfix">
                                <div class="binary-genealogy-tree binary_tree_extended">
                                    <div class="sponsor-tree-wrapper">
                                        <div class="eps-sponsor-tree eps-tree"> <?php // style="max-width: 270px;" ?>
                                            <ul>
                                                <li>
                                                    <div class="eps-nc" nid="12">
                                                        <div class="user-pic">
                                                            <div class="images_wrapper" style="font-size: 40px;margin: 10px 0;">
                                                                <a href="{{ URL::to('/') }}/m/my/sponsor-tree?get_id={{$dataUser->id}}" class="text-success">
                                                                    <i class="feather icon-user"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="user-name">
                                                           {{$dataUser->user_code}}
                                                        </div>
                                                        <div class="user-name" style="background: linear-gradient(45deg, #FFB64D, #ffcb80);">
                                                           Total Sp : {{$dataUser->total_sponsor}}
                                                        </div>
                                                    </div>
                                                    @if($getData != null)
                                                    <ul>
                                                        @foreach($getData as $row)
                                                            <?php
                                                                $icon = 'icon-user-unfollow';
                                                                $color = 'text-danger';
                                                                if($row->is_active == 1){
                                                                    $icon = 'icon-user-following';
                                                                    $color = 'text-success';
                                                                }
                                                            ?>
                                                            <li>
                                                                <div class="eps-nc" nid="13">
                                                                    <div class="user-pic">
                                                                        <div class="images_wrapper text-success" style="font-size: 40px;margin: 10px 0;">
                                                                            <a href="{{ URL::to('/') }}/m/my/sponsor-tree?get_id={{$row->id}}">
                                                                                <i class="feather {{$color}} {{$icon}}"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="user-name" class="bg-c-blue">
                                                                        {{$row->user_code}}
                                                                    </div>
                                                                    <div class="user-name" style="background: linear-gradient(45deg, #FFB64D, #ffcb80);">
                                                                        Total Sp : {{$row->total_sponsor}}
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                    @endif
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

@stop


@section('styles')
<link href="{{ asset('asset_member/css/tree-style.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('asset_member/css/developer.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $("#get_id").keyup(function(){
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/m/cek/usercode" + "?name=" + $(this).val() ,
                success: function(data){
                    $("#get_id-box").show();
                    $("#get_id-box").html(data);
                }
            });
        });
    });
    function selectUsername(val) {
        var valNew = val.split("____");
        $("#get_id").val(valNew[1]);
        $("#id_get_id").val(valNew[0]);
        $("#get_id-box").hide();
    }
</script>
@stop
 * 
 */
?>
