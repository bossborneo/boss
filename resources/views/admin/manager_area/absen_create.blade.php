@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <form class="login100-form validate-form" method="post" action="/m/absen">
                            {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <select class="form-control" name="id">
                                                    @foreach($crewDownline as $row)
                                                        <?php
                                                            if($row->level_id == 8){
                                                                $level = 'Assistant Manager';
                                                            }
                                                            if($row->level_id == 9){
                                                                $level = 'Top Leader';
                                                            }
                                                            if($row->level_id == 10){
                                                                $level = 'Leader';
                                                            }
                                                            if($row->level_id == 11){
                                                                $level = 'Trainer';
                                                            }
                                                            if($row->level_id == 12){
                                                                $level = 'Merchandiser';
                                                            }
                                                            if($row->level_id == 13){
                                                                $level = 'Retrainer';
                                                            }
                                                        ?>
                                                        <option value="{{$row->id}}">{{$row->name}} ({{$level}})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Masuk/Absensi</label>
                                                <select class="form-control" name="is_masuk">
                                                    <option value="1">Masuk</option>
                                                    <option value="2">Sakit</option>
                                                    <option value="3">Izin</option>
                                                    <option value="4">Alpha</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Jam</label>
                                                <input type="text" class="form-control datepicker" name="absen_date" id="absen_date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-11">
                                            <div class="form-group">
                                                <label>Keterangan</label>
                                                <textarea rows="1" cols="20" name="keterangan" class="form-control textarea"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary btn-round">Confirm</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('admin-javascript')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script>
    $('.datepicker').datetimepicker({
        format: 'HH:mm',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
    
</script>    
@stop