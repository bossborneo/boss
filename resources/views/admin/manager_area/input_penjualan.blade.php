@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">
                    {{$dataUser->name}}
                </p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">{{$headerTitle}}</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/m/data-sales">Data Penjualan</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <form class="login100-form validate-form"  method="post" action="/m/input-sale" id="frmAll">
                            {{ csrf_field() }}
                            
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Tanggal</label>
                                            <input type="text" class="form-control datepicker" name="sale_date" id="sale_date">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Jam</label>
                                            <input type="text" class="form-control datepickerHour" name="absen_date" id="absen_date">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Masuk/Absensi</label>
                                            <select class="form-control" name="is_masuk">
                                                <option value="1">Masuk</option>
                                                <option value="2">Sakit</option>
                                                <option value="3">Izin</option>
                                                <option value="4">Alpha</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nama Crew</label>
                                            <?php
                                            $level1 = 'Manager';
                                            if($dataUser->level_id == 2){
                                                $level1 = 'GM';
                                            }
                                            if($dataUser->level_id == 3){
                                                $level1 = 'SEM';
                                            }
                                            if($dataUser->level_id == 4){
                                                $level1 = 'EM';
                                            }
                                            if($dataUser->level_id == 5){
                                                $level1 = 'SM';
                                            }
                                            if($dataUser->level_id == 6){
                                                $level1 = 'MQB';
                                            }
                                            ?>
                                            <select class="form-control" name="crew" onChange="getTypeSearch(this.value);">
                                                <option value="{{$dataUser->id}}">{{$level1}} - {{$dataUser->name}}</option>
                                                @if($cekDownline != null)
                                                    @foreach($cekDownline as $rowDownline)
                                                        <?php
                                                            $level = 'Asmen';
                                                            if($rowDownline->level_id == 9){
                                                                $level = 'TLD';
                                                            }
                                                            if($rowDownline->level_id == 10){
                                                                $level = 'LD';
                                                            }
                                                            if($rowDownline->level_id == 11){
                                                                $level = 'TR';
                                                            }
                                                            if($rowDownline->level_id == 12){
                                                                $level = 'MD';
                                                            }
                                                            if($rowDownline->level_id == 13){
                                                                $level = 'RT';
                                                            }
                                                        ?>
                                                        <option value="{{$rowDownline->id}}">{{$level}} - {{$rowDownline->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="typeSearch-list">
                                </div>    
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-check">
                                          <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" id="chkSales" name="checkSales" value="1"> Input Penjualan (unchecked jika tidak ada penjualan)
                                            <span class="form-check-sign"></span>
                                          </label>
                                        </div>
                                     </div>   
                                </div>
                                <div class="row" id="searchBarang-list">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="button" class="btn btn-sm btn-block btn-behance " id="addrow" style="display: none" value="Tambah" />
                                </div>
                            </div>    
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <input type="button" name="btn" value="Simpan" id="submitBtn"data-toggle="modal" data-target="#confirmSubmit" class="btn btn-primary btn-round" />
                                    <!--<button type="submit" class="btn btn-primary btn-round">Confirm</button>-->
                                </div>
                            </div>
                        </form>
                        <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
                                    </div>
                                    <div class="modal-body" id="confirmSales" style="overflow-y: auto;max-height: 330px;">
                                        
                                    </div>
                                    <div class="modal-footer" style="margin-right: 10px;">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary" id="submit">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<style>
  input[type=number] {
    height: 40px;
}

input[type=number]:hover::-webkit-inner-spin-button {  
    width: 25px;
    height: 30px;
}
</style>
@stop
@section('admin-javascript')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script>
    $('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        },
        daysOfWeekDisabled: [0],
        minDate: '<?php echo date('Y-m-d',strtotime("-".$getSettingApp->sale_day." days")); ?>',
        maxDate: '<?php echo date('Y-m-d',strtotime("1 days")); ?>',
    });
   
    $(document).ready(function () {
    var counter = 2;

    $("#addrow").on("click", function () {
        var date_value = $('#sale_date').val();
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/m/more-barang" + "/" + date_value + "/" + counter,
            success: function(url){
                    $("#addPenjualan").append(url);
                    counter++;
            }
        });
    });

    $("div#addPenjualan").on("click", "#rmLine", function (event) {
        $(this).closest('div.row').remove();       
        counter -= 1
    });
    $("div#addPenjualan").on("change", "#_item1", function (event) {
        var option1 = $('option:selected', this).attr('firsttype');
//        alert( option1 );
//        $("_amount12").show();
        if(option1 == 3){
            $(".input2").show().prop( "disabled", false );
            $(".input1").hide().prop( "disabled", true );
        } else{
            $(".input2").hide().prop( "disabled", true );
            $(".input1").show().prop( "disabled", false );
        }
    });
    <?php
        for ($i = 2; $i <= 15; $i++) {
            $ke = ($i * 2) - 1;
            $ke2 =$ke + 1;
        
    ?>
    $("div#addPenjualan").on("change", "#_item{{$i}}", function (event) {
        var option = $('option:selected', this).attr('type{{$i}}');
        if(option == 3){
            $(".input{{$ke2}}").show().prop( "disabled", false );
            $(".input{{$ke}}").hide().prop( "disabled", true );
        } else{
            $(".input{{$ke2}}").hide().prop( "disabled", true );
            $(".input{{$ke}}").show().prop( "disabled", false );
        }
    });
    <?php 
        } 
    ?>
});
    
</script>    
<script>
        $('#submitBtn').click(function() {
            var data = $("#frmAll").serializeArray();
            $.ajax({
                type: "POST",
                url: "{{ URL::to('/') }}/m/cek-sum" ,
                data: data,
                success: function(url){
                    $("#confirmSales" ).empty();
                    $("#confirmSales").html(url);
                }
            });
       });

       $('#submit').click(function(){
           $('#frmAll').submit();
       });
</script>
<script type="text/javascript">
    
    function getTypeSearch(val) {
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/m/search-sp" + "/" + val + "/{{$dataUser->cabang_id}}" ,
            success: function(url){
                    $( "#typeSearch-list" ).empty();
                    $("#typeSearch-list").html(url);
            }
        });
    }
    
    function getSum() {
        var oldPrice = $('#_price').val();
        var amount = $('#_amount').val();
        var itemId = $("#_item option:selected").val();
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/m/sum" + "/" + itemId + "/"+ amount +"/ "+oldPrice ,
            success: function(url){
                    $('#_price').val(url);
            }
        });
    }
    
//    $(document).on('dp.change', 'input.datepicker', function() {
//        var date_value = $('#sale_date').val();
//        $.ajax({
//            type: "GET",
//            url: "{{ URL::to('/') }}/m/search-barang" + "/" + date_value,
//            success: function(url){
//                    $( "#searchBarang-list" ).empty();
//                    $("#searchBarang-list").html(url);
//            }
//        });
//    });
    
    $(function () {
        $("#chkSales").click(function () {
            if ($(this).is(":checked")) {
                var date_value = $('#sale_date').val();
                if(date_value.length > 0){
                    $.ajax({
                        type: "GET",
                        url: "{{ URL::to('/') }}/m/search-barang" + "/" + date_value,
                        success: function(url){
                                $( "#searchBarang-list" ).empty();
                                $("#searchBarang-list").html(url);
                        }
                    });
                    $("#addrow").show();
                }
            } else {
                $("#addrow").hide();
                $('div').closest('#rowBarang').remove();
                $('div').closest('#searchBarang-listRow').remove();
            }
        });
    });
</script>
<script>
    $('.datepickerHour').datetimepicker({
        format: 'HH:mm',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
    
</script>    
@stop