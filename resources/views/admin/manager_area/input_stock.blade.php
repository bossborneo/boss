@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')

<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">
                    {{$dataUser->name}}
                </p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">{{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-danger">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                kalau salah
                            </div>
                        </div>
                        <form class="login100-form validate-form" method="post" action="/new-crew">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal</label>
                                        <input type="text" class="form-control" value="16 September 2018">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Asisten Manager</label>
                                        <select class="form-control" name="type_search">
                                            <option selected value="0">- Pilih Asmen -</option>
                                            <option value="3">Si Asmen 1</option>
                                            <option value="3">Si Asmen 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Top Leader</label>
                                        <select class="form-control" name="type_search">
                                            <option selected value="0">- Pilih Top Leader -</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Crew</label>
                                        <select class="form-control" name="type_search">
                                            <option selected value="0">- Pilih Crew -</option>
                                            <option value="3">Si Crew 1</option>
                                            <option value="3">Si Crew 2</option>
                                            <option value="3">Si Crew 3</option>
                                            <option value="3">Si Crew 4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Lengkap</label>
                                        <input type="text" class="form-control" value="Anthony Ginting">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No Telepon</label>
                                        <input type="text" class="form-control" value="081233445566">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea class="form-control textarea">Jl. Jend. Soedirman No. 22 Larangan Selatan</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" value="ginting2018">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" value="123456">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Ketik Ulang password</label>
                                        <input type="password" class="form-control" value="123456">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary btn-round">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop