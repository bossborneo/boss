@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/m/history/stock">Data Stock</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Barang</th>
                                        <th>Type</th>
                                        <th>Harga (Rp.)</th>
                                        <th class="td-actions text-left">##</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $noKosong = 0;
                                        ?>
                                    <?php 
                                    /*
                                    @if($purchase != null)
                                        @foreach($purchase as $row)
                                        <?php 
                                            $no++;
                                            $type = 'Obat';
                                            if($row->type == 1){
                                                $type = 'Alat';
                                            }
                                            if($row->type == 2){
                                                $type = 'Strip';
                                            }
                                            $qty = 0;
                                            $sisa = 0;
                                            if($row->qty != null){
                                                $qty = $row->qty;
                                                $sisa = $row->sisa;
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->purchase_name}}</td>
                                                <td>{{$type}}</td>
                                                <td>{{number_format($row->main_price + $getCabang->extra, 0, ',', '.')}}</td>
                                                <td>{{number_format($qty, 1, ',', '.')}}</td>
                                                <td>{{number_format($sisa, 1, ',', '.')}}</td>

                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip"  data-toggle="modal" data-target="#addStockModal" class="text-primary" href="{{ URL::to('/') }}/m/add/input-stock/{{$row->id_purchase}}"><i class="nc-icon nc-badge"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    */
                                    ?>
                                    
                                    @if($purchseKosong != null)
                                        @foreach($purchseKosong as $rowKosong)
                                        <?php 
                                            $noKosong++;
                                            $typeKosong = '--';
                                            if($rowKosong->type == 1){
                                                $typeKosong = 'Alat';
                                            }
                                            if($rowKosong->type == 2){
                                                $typeKosong = 'Strip';
                                            }
                                            if($rowKosong->type == 3){
                                                $typeKosong = 'Obat';
                                            }
                                            if($rowKosong->type == 10){
                                                $typeKosong = 'Lainnya';
                                            }
                                            $qtyKosong = 0;
                                            $sisaKosong = 0;
                                        ?>
                                            <tr>
                                                <td>{{$noKosong}}</td>
                                                <td>{{$rowKosong->purchase_name}}</td>
                                                <td>{{$typeKosong}}</td>
                                                @if($rowKosong->type == 1)
                                                    <?php
                                                        $harga = $rowKosong->main_price + $getCabang->extra_alat;
                                                        if($rowKosong->kondisi == 1){
                                                            $harga = $rowKosong->main_price;
                                                        }
                                                    ?>
                                                    <td>{{number_format($harga, 0, ',', '.')}}</td>
                                                @endif
                                                @if($rowKosong->type == 2)
                                                    <?php
                                                        $harga1 = $rowKosong->main_price + $getCabang->extra_strip;
                                                        if($rowKosong->kondisi == 1){
                                                            $harga1 = $rowKosong->main_price;
                                                        }
                                                    ?>
                                                    <td>{{number_format($harga1, 0, ',', '.')}}</td>
                                                @endif
                                                @if($rowKosong->type == 3)
                                                    <?php
                                                        $harga2 = $rowKosong->main_price + $getCabang->extra;
                                                        if($rowKosong->kondisi == 1){
                                                            $harga2 = $rowKosong->main_price;
                                                        }
                                                    ?>
                                                    <td>{{number_format($harga2, 0, ',', '.')}}</td>
                                                @endif
                                                @if($rowKosong->type == 10)
                                                    <?php
                                                        $harga3 = $rowKosong->main_price;
                                                    ?>
                                                    <td>{{number_format($harga3, 0, ',', '.')}}</td>
                                                @endif
                                                <?php
                                                /*
                                                <td>{{number_format($qtyKosong, 0, ',', '.')}}</td>
                                                <td>{{number_format($sisaKosong, 0, ',', '.')}}</td>
                                                 * 
                                                 */
                                                ?>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip"  data-toggle="modal" data-target="#addStockModal" class="text-primary" href="{{ URL::to('/') }}/m/add/input-stock/{{$rowKosong->id_purchase}}"><i class="nc-icon nc-badge"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="addStockModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-javascript')
<script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').dataTable({
            "pageLength": 50
        });
    } );
    $("#addStockModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop