@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-toggle">
                <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <p class="navbar-brand">{{$dataUser->name}}</p>
        </div>
    </div>
</nav>
<div class="content">
    <div class="row">
        <div class="col-lg-5 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-3">
                            <div class="icon-big text-center icon-warning">
                                <i class="nc-icon nc-single-02 text-primary"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-9">
                            <div class="numbers">
                                <p class="card-category">Nama</p>
                                <p class="card-title" style="font-size: 18px;">{{$getCrew->name}}</p>
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-3">
                            <div class="icon-big text-center icon-warning">
                                <i class="nc-icon nc-diamond text-success"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-9">
                            <div class="numbers">
                                <p class="card-category">Total Bonus</p>
                                <?php
                                    $totalBonus = 0;
                                    if($getBonusTotal != null){
                                        $totalBonus = number_format($getBonusTotal->total_bonus, 0, ',', '.');
                                    }
                                ?>
                                <p class="card-title">Rp. {{$totalBonus}}
                                </p>
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($getDetailBonus != null)
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title"> Detail Bonus</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class=" text-primary">
                                <tr>
                                    <th>No</th>
                                    <th>Bonus (Rp)</th>
                                    <th>Jenis</th>
                                    <th>Tgl</th>
                                    <th>Barang</th>
                                    <th>Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0; ?>
                                @foreach($getDetailBonus as $row)
                                    <?php 
                                        $no++; 
                                        $type = 'Obat';
                                        if($row->purchase_type == 1){
                                            $type = 'Alat';
                                        }
                                        if($row->purchase_type == 2){
                                            $type = 'Strip';
                                        }
                                        $jenis = 'Lainnya';
                                        if($row->bonus_type == 1){
                                            $jenis = 'Harian';
                                        }
                                        if($row->bonus_type == 2){
                                            $jenis = 'Bulanan';
                                        }
                                    ?>
                                
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{number_format($row->bonus_price, 0, ',', '.')}}</td>
                                        <td>{{$jenis}}</td>
                                        <td><?php  echo (date('d M Y', strtotime($row->bonus_date))) ?></td>
                                        <td>{{$row->purchase_name}}</td>
                                        <td>{{$type}}</td>
                                    </tr>
                                @endforeach    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: -1 },
                 ]
        } );
    } );
</script>
@stop