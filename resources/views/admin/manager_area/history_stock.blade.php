@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        @if(date('w') == 0)
                            <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/m/stock-barang">Tambah Stock</a>
                        @endif
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Barang</th>
                                        <th>Type</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                    ?>
                                    @if($stockThisSunday != null)
                                        @foreach($stockThisSunday as $row)
                                            <?php 
                                            $no++;
                                            $type = '--';
                                            if($row->type == 1){
                                                $type = 'Alat';
                                            }
                                            if($row->type == 2){
                                                $type = 'Strip';
                                            }
                                            if($row->type == 3){
                                                $type = 'Obat';
                                            }
                                            if($row->type == 10){
                                                $type = 'Lainnya';
                                            }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->purchase_name}}</td>
                                                <td>{{$type}}</td>
                                                <td>{{number_format($row->amount, 1, ',', '.')}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle2}}</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="myTableLastWeek">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Barang</th>
                                        <th>Type</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                    ?>
                                    @if($stockLastSunday != null)
                                        @foreach($stockLastSunday as $row)
                                            <?php 
                                            $no++;
                                            $type = '--';
                                            if($row->type == 1){
                                                $type = 'Alat';
                                            }
                                            if($row->type == 2){
                                                $type = 'Strip';
                                            }
                                            if($row->type == 3){
                                                $type = 'Obat';
                                            }
                                            if($row->type == 10){
                                                $type = 'Lainnya';
                                            }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->purchase_name}}</td>
                                                <td>{{$type}}</td>
                                                <td>{{number_format($row->amount, 1, ',', '.')}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                searching: false,
                pageLength: 5
        });
    });
    $(document).ready(function() {
        $('#myTableLastWeek').DataTable( {
                dom: 'Bfrtip',
                searching: false,
                pageLength: 5
        });
    });
</script>
@stop