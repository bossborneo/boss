@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <h5 class="card-title"> {{$headerTitle}}</h5>
            </div>
            <div class="col-md-5">
                <div class="card card-upgrade">
                    <div class="card-header text-center">
                        <h5 class="card-title">{{$dataUser->name}}</h5>
                        <h5 class="card-category"><?php  echo (date('d F Y', strtotime($getData->transfer_date))) ?></h5>
                    </div>
                    <div class="card-body">
                        <div class="table-upgrade">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-left">Nominal (Rp.)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Penjualan</td>
                                        <td class="text-left">{{number_format($getData->sales_price, 0, ',', '.')}}</td>
                                    </tr>
                                    <tr>
                                        <td>Pengeluaran</td>
                                        <td class="text-left">{{number_format($getData->pengeluaran_price, 0, ',', '.')}}</td>
                                    </tr>
                                    <tr>
                                        <td>Transfer</td>
                                        <td class="text-left">{{number_format($getData->transfer_price, 0, ',', '.')}}</td>
                                    </tr>
                                </tbody>
                            </table>
                            @if($getData->keterangan != null)
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Keterangan Reject</label>
                                    <textarea class="form-control" name="keterangan">{{$getData->keterangan}}</textarea>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            <div class="row">
                                <input type="hidden" name="cekId" value="28">
                                <div class="update ml-auto mr-auto">
                                    <a href="{{ URL::to('/') }}/m/transfers" class="btn btn-dark">Back</a>
                                    @if($view == true)
                                    <a rel="tooltip" title="View" data-toggle="modal" data-target="#reConfirmTransfer" href="{{ URL::to('/') }}/m/ajax/confirm/transfer/{{$getData->id}}/5" class="btn btn-success">Confirm Ulang</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="reConfirmTransfer" tabindex="-1" role="dialog" aria-labelledby="crewModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Detail Pengeluaran</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div>
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>Kategori</th>
                                        <th>Pengeluaran (Rp.)</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($getDetailData->pengeluaran as $row)
                                    <tr>
                                        <td>{{$row->pengeluaran_name}}</td>
                                        <td>{{number_format($row->pengeluaran_price, 0, ',', ',')}}</td>
                                        <td>{{$row->keterangan}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-javascript')
<script type="text/javascript">
    $("#reConfirmTransfer").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop