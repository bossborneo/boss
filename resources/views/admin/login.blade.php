<!DOCTYPE html>
<html>
<head>
    <title>Borneo Group</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    <link rel="stylesheet" href="{{ asset('css/login-util.css') }}">
</head>

<body>
    <div class="limiter">
            <div class="container-login100">
                    <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
                            <span class="login100-form-title p-b-33">
                                    <img src="/images/logo_boss.jpg" style="width: 70%;">
                            </span>
                            @if ( Session::has('message') )
                                <div class="widget-content mt10 mb10 mr15">
                                    <div class="alert alert-{{ Session::get('messageclass') }}">
                                        <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                        {{  Session::get('message')    }} 
                                    </div>
                                </div>
                            @endif
                            <form class="login100-form validate-form" method="post" action="/login_admin">
                                    {{ csrf_field() }}
                                    <div class="wrap-input100 validate-input" data-validate = "Username harus diisi">
                                            <input class="input100" type="text" name="admin_username" placeholder="Username">
                                            <span class="focus-input100-1"></span>
                                            <span class="focus-input100-2"></span>
                                    </div>

                                    <div class="wrap-input100 rs1 validate-input" data-validate="Password harus diisi">
                                            <input class="input100" type="password" name="admin_password" placeholder="Password">
                                            <span class="focus-input100-1"></span>
                                            <span class="focus-input100-2"></span>
                                    </div>

                                    <div class="container-login100-form-btn m-t-20">
                                            <button class="login100-form-btn">
                                                    Sign in
                                            </button>
                                    </div>

                                    <div class="text-center p-t-45 p-b-4">
<!--                                            <span class="txt1">
                                                    Forgot
                                            </span>

                                            <a href="#" class="txt2 hov1">
                                                    Username / Password?
                                            </a>-->
                                    </div>
                            </form>
                    </div>
            </div>
    </div>
    
    
<!--    <div class="container mb20">
            <h3>&nbsp;</h3>
        <div class="row">
            <div class="login-form-wrapper col-md-6 mt10 col-md-offset-3">
                    <div class="well well-sm mr15">
                        <h4>Login</h4>
                    </div>
                    @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                    @endif
                    <form class="login-form" method="post" action="/login_admin">
                        {{ csrf_field() }}
                        <div class="form-group form-group-lg">
                            <label>Email</label>
                            <input name="admin_email" class="form-control" placeholder="my email" type="text" />
                        </div>
                        <div class="form-group form-group-lg">
                            <label>Password</label>
                            <input name="admin_password" class="form-control" type="password" placeholder="my password" />
                        </div>
                        <input class="btn btn-primary" type="submit" value="Sign in" />
                    </form>
                </div>
        </div>    
    </div>-->
    
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>