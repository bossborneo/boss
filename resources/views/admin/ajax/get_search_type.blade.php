@if(count($manager) > 0)
    <div class="form-group">
        <label>Pilih Nama</label>
        <select name="manager_id" class="form-control" title="Pilih Manager" onChange="getTypeSearchCrew(this.value);">
                <option value="0">- Pilih Nama -</option>
                @foreach($manager as $row)
                    @if($search_type == 1)
                        @if($row->level_id == 8 || $row->level_id == 7)
                        <?php
                            $level = 'GM';
                            if($row->level_id == 3){
                                $level = 'SEM';
                            }
                            if($row->level_id == 4){
                                $level = 'EM';
                            }
                            if($row->level_id == 5){
                                $level = 'SM';
                            }
                            if($row->level_id == 6){
                                $level = 'MQB';
                            }
                            if($row->level_id == 7){
                                $level = 'Manager';
                            }
                            if($row->level_id == 8){
                                $level = 'Asisten Manager';
                            }
                        ?>
                        <option value="{{$row->id}}">({{$level}}) {{$row->name}}</option>
                        @endif
                    @endif
                @endforeach
            
        </select>
    </div>
@endif

