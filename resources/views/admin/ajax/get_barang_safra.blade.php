<div class="col-md-6" id="searchBarang-listRow">
    <div class="form-group">
        <label>Barang</label>
        <select class="form-control" name="item_purchase[]" id="_item">
            <option selected value="0">- Pilih Barang -</option>
            @if($allBarang != null)
                @foreach($allBarang as $row)
                    @if($row->qty_input > $row->qty_output)
                        <?php
                            $type = '--';
                            if($row->type == 1){
                                $type = 'Alat';
                            }
                            if($row->type == 2){
                                $type = 'Strip';
                            }
                            if($row->type == 3){
                                $type = 'Obat';
                            }
                            if($row->type == 10){
                                $type = 'Lainnya';
                            }
                        ?>  
                        <option value="{{$row->id_item_purchase}}" data-price="{{$row->price}}">({{$type}}) {{$row->purchase_name}}</option>
                    @endif
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="col-md-4" id="searchBarang-listRow">
    <div class="form-group">
        <label>Jumlah</label>
        <input type="number" min="0" step="0.5"  name="amount[]" class="form-control" id="_amount"/>
    </div>
</div>

