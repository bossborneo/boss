<form class="login100-form validate-form" method="post" action="/upgrade/{{$dataUserId->id}}">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" disabled=""  value="{{$dataUserId->name}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Posisi</label>
                    <input type="text" class="form-control" disabled="" value="Retrainer">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="cekId" value="{{$dataUserId->id}}" >
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Confirm</button>
    </div>
</form>   