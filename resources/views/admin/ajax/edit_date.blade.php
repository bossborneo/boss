<form class="login100-form validate-form" method="post" action="/adm/edit-date/{{$dataUserId->id}}">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="name" value="{{$dataUserId->name}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Tgl Aktif</label>
                    <input type="text" class="form-control" disabled="" value="<?php  echo (date('d M Y', strtotime($dataUserId->active_at))) ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Ubah Tgl Aktif</label>
                    <input type="text" class="form-control datepickerEnd" name="date_active" id="datetimeActive">
                </div>
            </div>
        </div>
       
    </div>
    <input type="hidden" name="cekId" value="{{$dataUserId->id}}" >
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Confirm</button>
    </div>
</form>

<script type="text/javascript">
      
    $('.datepickerEnd').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });

</script>