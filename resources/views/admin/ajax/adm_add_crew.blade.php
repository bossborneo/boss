<?php
$level1 = 'GM';
if($manager->level_id == 3){
    $level1 = 'SEM';
}
if($manager->level_id == 4){
    $level1 = 'EM';
}
if($manager->level_id == 5){
    $level1 = 'SM';
}
if($manager->level_id == 6){
    $level1 = 'MQB';
}
if($manager->level_id == 7){
    $level1 = 'Manager';
}
?>
<div class="form-group">
    <label>Nama Sponsor</label>
    <select name="parent_id" class="form-control" onChange="getTypeSearchParent(this.value);">
        <option value="{{$manager->id}}">({{$level1}}) {{$manager->name}}</option>
        @if($dataSponsor != null)
            @foreach($dataSponsor as $row)
                <?php
                    $level = 'GM';
                    if($row->level_id == 3){
                        $level = 'SEM';
                    }
                    if($row->level_id == 4){
                        $level = 'EM';
                    }
                    if($row->level_id == 5){
                        $level = 'SM';
                    }
                    if($row->level_id == 6){
                        $level = 'MQB';
                    }
                    if($row->level_id == 7){
                        $level = 'Manager';
                    }
                    if($row->level_id == 8){
                        $level = 'Asmen';
                    }
                    if($row->level_id == 9){
                        $level = 'TLD';
                    }
                    if($row->level_id == 10){
                        $level = 'LD';
                    }
                    if($row->level_id == 11){
                        $level = 'TR';
                    }
                    if($row->level_id == 12){
                        $level = 'MD';
                    }
                    if($row->level_id == 13){
                        $level = 'RT';
                    }
                ?>
                <option value="{{$row->id}}">({{$level}}) {{$row->name}}</option>
            @endforeach
        @endif
    </select>
</div>
