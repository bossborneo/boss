<form class="login100-form validate-form" method="post" action="/adm/confirm/transfer">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}} Transfer</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Jumlah</label>
                    <input type="text" disabled="" class="form-control" value="Rp. {{number_format($getData->transfer_price, 0, ',', ',')}}"/>
                </div>
            </div>
        </div>
        @if($approve == false)
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Keterangan Reject</label>
                    <textarea class="form-control" name="keterangan"></textarea>
                </div>
            </div>
        </div>
        @endif
    </div>
    <input type="hidden" name="cekId" value="{{$getData->id}}" >
    <input type="hidden" name="type" value="{{$type}}" >
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">{{$headerTitle}}</button>
    </div>
</form>   