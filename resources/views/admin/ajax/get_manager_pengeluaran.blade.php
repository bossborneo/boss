@if(count($manager) > 0)
    <div class="form-group">
        <label>Pilih Nama</label>
        <select name="manager_id" class="form-control" title="Pilih Manager" onChange="getTypeSearchCrew(this.value);">
                <option value="0">Seluruh Manager</option>
                @foreach($manager as $row)
                    @if($row->level_id == 7)
                    <option value="{{$row->id}}">(Manager) {{$row->name}}</option>
                    @endif
                @endforeach
            
        </select>
    </div>
@endif

