<form class="login100-form validate-form" method="post" action="/adm/upgrade/{{$dataUserId->id}}/{{$dataManager->id}}">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" disabled=""  value="{{$dataUserId->name}}">
                </div>
            </div>
        </div>
        <?php
            $level = 'General Manager';
            if($dataUserId->level_id == 3){
                $level = 'Senior Executive Manager';
            }
            if($dataUserId->level_id == 4){
                $level = 'Executive Manager';
            }
            if($dataUserId->level_id == 5){
                $level = 'Senior Manager';
            }
            if($dataUserId->level_id == 6){
                $level = 'MQB';
            }
            if($dataUserId->level_id == 7){
                $level = 'Manager';
            }
            if($dataUserId->level_id == 8){
                $level = 'Asisten Manager';
            }
            if($dataUserId->level_id == 9){
                $level = 'Top Leader';
            }
            if($dataUserId->level_id == 10){
                $level = 'Leader';
            }
            if($dataUserId->level_id == 11){
                $level = 'Trainer';
            }
            if($dataUserId->level_id == 12){
                $level = 'Merchandiser';
            }
            if($dataUserId->level_id == 13){
                $level = 'Retrainer';
            }
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Posisi</label>
                    <input type="text" class="form-control" disabled="" value="{{$level}}">
                </div>
            </div>
        </div>
        @if($dataUserId->level_id == 8)
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" name="username">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Ketik Ulang Password</label>
                    <input type="password" class="form-control" name="repassword">
                </div>
            </div>
        </div>
        @endif
        
    </div>
    <input type="hidden" name="cekId" value="{{$dataUserId->id}}" >
    <input type="hidden" name="managerId" value="{{$dataUserId->id}}" >
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Confirm</button>
    </div>
</form>   