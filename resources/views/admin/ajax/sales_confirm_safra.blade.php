@if($cek->can == true)
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Nama Crew</label>
            <input type="text" class="form-control" disabled="" value="{{$nama_crew}}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label>Level</label>
            <input type="text" class="form-control" disabled="" value="{{$level}}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Tanggal</label>
            <input type="text" class="form-control" disabled="" value="{{date('d-m-Y', strtotime($date))}}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Total Keseluruhan</label>
            <input type="text" class="form-control" disabled="" value="Rp. {{number_format(round($sum, 1), 0, ',', '.')}}">
        </div>
    </div>
</div>

@if($dataAll != null)
    @foreach($dataAll as $row)
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-7">
                <div class="form-group">
                    <label>Nama Barang</label>
                    <input type="text" class="form-control" disabled="" value="{{$row->purchase_name}}">
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label>Type</label>
                    <input type="text" class="form-control" disabled="" value="{{$row->type}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Jumlah </label>
                    <input type="text" class="form-control" disabled="" value="{{$row->amount}}">
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <label>Harga</label>
                    <input type="text" class="form-control" disabled="" value="Rp. {{number_format($row->price, 0, ',', '.')}}">
                </div>
            </div>
        </div>
    @endforeach
@endif
@endif

@if($cek->can == false)
<div class="row">
        <h5 class="text-danger"> {{$cek->pesan}} </h5>
</div>
@endif
    
