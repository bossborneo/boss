@if(count($manager) > 0)
    @if($type == 1)
    <div class="form-group">
        <label>Pilih Nama</label>
        <select name="start_user_id" class="form-control" title="Pilih Nama" onChange="getTypeSearchCrew(this.value);">
                <option value="0">- Pilih Nama -</option>
                @foreach($manager as $row)
                    <?php
                        $level = 'GM';
                        if($row->level_id == 3){
                            $level = 'SEM';
                        }
                        if($row->level_id == 4){
                            $level = 'EM';
                        }
                        if($row->level_id == 5){
                            $level = 'SM';
                        }
                        if($row->level_id == 6){
                            $level = 'MQB';
                        }
                        if($row->level_id == 7){
                            $level = 'Manager';
                        }
                        if($row->level_id == 8){
                            $level = 'Asisten Manager';
                        }
                        if($row->level_id == 9){
                            $level = 'Top Leader';
                        }
                        if($row->level_id == 10){
                            $level = 'Leader';
                        }
                        if($row->level_id == 11){
                            $level = 'Trainer';
                        }
                        if($row->level_id == 12){
                            $level = 'Merchandiser';
                        }
                        if($row->level_id == 13){
                            $level = 'Retrainer';
                        }
                    ?>
                    <option value="{{$row->id}}">({{$level}}) {{$row->name}}</option>
                @endforeach
            
        </select>
    </div>
    @endif
    
    @if($type == 2)
    <div class="form-group">
        <label>Pilih Nama Sponsor</label>
        <select name="parent_user_id" class="form-control" title="Pilih Nama Sponsor" onChange="getTypeSearchCrew(this.value);">
                <option value="0">- Pilih Nama -</option>
                @foreach($manager as $row)
                    @if($row->level_id != 12 && $row->level_id != 13)
                    <?php
                        $level = 'GM';
                        if($row->level_id == 3){
                            $level = 'SEM';
                        }
                        if($row->level_id == 4){
                            $level = 'EM';
                        }
                        if($row->level_id == 5){
                            $level = 'SM';
                        }
                        if($row->level_id == 6){
                            $level = 'MQB';
                        }
                        if($row->level_id == 7){
                            $level = 'Manager';
                        }
                        if($row->level_id == 8){
                            $level = 'Asisten Manager';
                        }
                        if($row->level_id == 9){
                            $level = 'Top Leader';
                        }
                        if($row->level_id == 10){
                            $level = 'Leader';
                        }
                        if($row->level_id == 11){
                            $level = 'Trainer';
                        }
                        if($row->level_id == 12){
                            $level = 'Merchandiser';
                        }
                        if($row->level_id == 13){
                            $level = 'Retrainer';
                        }
                    ?>
                    <option value="{{$row->id}}">({{$level}}) {{$row->name}}</option>
                    @endif
                @endforeach
            
        </select>
    </div>
    @endif
@endif

