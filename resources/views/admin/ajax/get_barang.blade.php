<div class="col-md-6" id="searchBarang-listRow">
    <div class="form-group">
        <label>Barang</label>
        <select class="form-control" name="item_purchase[]" id="_item1">
            <option selected value="0">- Pilih Barang -</option>
            @if($allBarang != null)
                @foreach($allBarang as $row)
                    @if($row->qty_input > $row->qty_output)
                        <?php
                            $type = '--';
                            if($row->type == 1){
                                $type = 'Alat';
                            }
                            if($row->type == 2){
                                $type = 'Strip';
                            }
                            if($row->type == 3){
                                $type = 'Obat';
                            }
                            if($row->type == 10){
                                $type = 'Lainnya';
                            }
                        ?>  
                        <option value="{{$row->id_item_purchase}}" data-price="{{$row->price}}" firsttype="{{$row->type}}">({{$type}}) {{$row->purchase_name}}</option>
                    @endif
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="col-md-4" id="searchBarang-listRow">
    <div class="form-group">
        <label>Jumlah</label>
        <input type="number" min="0" step="1"  name="amount[]" class="form-control input1" id="_amount"/>
        <input type="number" min="0" step="0.5"  name="amount[]" class="form-control input2" id="_amount" style="display: none;"/>
    </div>
</div>
<?php
/*
<div class="col-md-2">
        <div class="form-group">
            <label>Opsi</label>
            <select class="form-control" name="opsi[]" id="_item">
                <option selected value="0">Full</option>
                <option value="1">Setengah</option>
            </select>
        </div>
    </div>
 * 
 */
?>

