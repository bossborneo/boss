<form class="login100-form validate-form" method="post" action="/adm/rm/order/{{$getData->order_id}}/{{$getData->id}}">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama Barang</label>
                    <input type="text" class="form-control" disabled="" value="{{$getData->purchase_name}}">
                </div>
            </div>
        </div>
        <?php
            $type = 'Obat';
            if($getData->type == 1){
                $type = 'Alat';
            }
            if($getData->type == 2){
                $type = 'Strip';
            }
        ?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Type Barang</label>
                    <input type="text" class="form-control" disabled="" value="{{$type}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jumlah Old</label>
                    <input type="number" class="form-control" disabled="" value="{{number_format($getData->qty, 1, '.', '.')}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Keterangan</label>
                    <input type="text" class="form-control" value="{{$getData->keterangan}}" disabled="">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="item_id" value="{{$getData->id}}">
    <?php
        $isTerima = 0;
        if($getData->item_terima_at != null){
            $isTerima = 1;
        }
    ?>
    <input type="hidden" name="is_terima" value="{{$isTerima}}">
    <input type="hidden" name="purchase_name" value="{{$getData->purchase_name}}">
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Hapus</button>
    </div>
</form>   