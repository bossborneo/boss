<?php
    $cek = 'Konfirmasi';
    if($type == 2){
        $cek = 'Reject';
    }
?>

<form class="login100-form validate-form" method="post" action="/adm/sales-safra">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$cek}}</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        @if($getData != null)
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>UserID</label>
                        <input type="text" class="form-control" readonly="" value="{{$getData->name}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cabang</label>
                        <input type="text" readonly=""  class="form-control" value="{{$getData->cabang_name}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Tgl Transfer</label>
                        <input type="text" class="form-control" readonly="" value="<?php  echo (date('d F Y', strtotime($getData->sale_date))) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Jml. Paket</label>
                        <input type="text" class="form-control" readonly="" value="{{number_format($getData->amount, 0, ',', '.')}}">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Jml. Harga</label>
                        <input type="text" readonly=""  class="form-control" value="{{number_format($getData->sale_price, 0, ',', '.')}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Bank</label>
                        <input type="text" class="form-control" readonly="" value="{{$getData->bank_name}}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nomor Rekening</label>
                        <input type="text" readonly=""  class="form-control" value="{{$getData->account_no}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nama Rekening</label>
                        <input type="text" class="form-control" readonly="" value="{{$getData->account_name}}">
                    </div>
                </div>
            </div>
            @if($type == 2)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Alasan</label>
                        <input type="text" class="form-control" name="reason">
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <input type="hidden" name="cekId" value="{{$getData->id}}" >
                <input type="hidden" name="cekMemberId" value="{{$getData->id_user}}" >
                <input type="hidden" name="type" value="{{$type}}" >
            </div>
        @else 
            Tidak ada data
        @endif
    </div>
    
    <div class="modal-footer">
        <div class="left-side">
            <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Tutup</button>
        </div>
        <div class="divider"></div>
        <div class="right-side">
            <button type="submit" class="btn btn-info btn-link">{{$cek}}</button>
        </div>
    </div>
</form>   