<form class="login100-form validate-form" method="post" action="/m/add/input-stock/{{$dataPurchase->id_purchase}}">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Input Stock</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama Barang</label>
                    <input type="text" class="form-control" disabled="" value="{{$dataPurchase->purchase_name}}">
                </div>
            </div>
        </div>
        <?php
            $type = 'Obat';
            $harga = number_format($dataPurchase->main_price + $getCabang->extra, 0, ',', '.');
            if($dataPurchase->type == 1){
                $type = 'Alat';
                $harga = number_format($dataPurchase->main_price + $getCabang->extra_alat, 0, ',', '.');
            }
            if($dataPurchase->type == 2){
                $type = 'Strip';
                $harga = number_format($dataPurchase->main_price + $getCabang->extra_strip, 0, ',', '.');
            }
            if($dataPurchase->kondisi == 1){
                $harga = number_format($dataPurchase->main_price, 0, ',', '.');
            }
        ?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Type Barang</label>
                    <input type="text" class="form-control" disabled="" value="{{$type}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Harga</label>
                    <input type="text" class="form-control" disabled="" value="Rp. {{$harga}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Jumlah</label>
                    <input type="number" min="0" step="0.5" class="form-control" name="qty">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="cekId" value="{{$dataPurchase->id_purchase}}" >
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Confirm</button>
    </div>
</form>   