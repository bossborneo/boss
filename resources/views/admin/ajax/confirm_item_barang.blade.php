<form class="login100-form validate-form" method="post" action="/mqb/terima-barang/{{$getData->id_order}}/{{$getData->id_order_item}}">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm Terima Barang</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama Barang</label>
                    <input type="text" class="form-control" disabled="" value="{{$getData->purchase_name}}">
                </div>
            </div>
        </div>
        <?php
            $type = 'Obat';
            if($getData->type == 1){
                $type = 'Alat';
            }
            if($getData->type == 2){
                $type = 'Strip';
            }
        ?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Type Barang</label>
                    <input type="text" class="form-control" disabled="" value="{{$type}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jumlah</label>
                    <input type="number" min="0" step="0.5" disabled="" class="form-control" name="qty" value="{{number_format($getData->qty, 1, '.', '.')}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Keterangan</label>
                    <input type="text" class="form-control" disabled="" value="{{$getData->keterangan}}">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="cekIdOrder" value="{{$getData->id_order}}" >
    <input type="hidden" name="cekIdOrderItem" value="{{$getData->id_order_item}}" >
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Confirm</button>
    </div>
</form>   