@if($cek->can == true)
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" disabled="" value="{{$dataUser->name}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Tanggal</label>
                <input type="text" class="form-control" disabled="" value="{{date('d F Y', strtotime($date))}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Total Sales</label>
                <input type="text" class="form-control" disabled="" value="Rp. {{number_format($getData->sales, 0, ',', ',')}}">
            </div>
        </div>
        <?php
            $totalOut = $getData->pengeluaran + $sum;
        ?>
        <div class="col-md-6">
            <div class="form-group">
                <label>Total Pengeluaran</label>
                <input type="text" class="form-control" disabled="" value="Rp. {{number_format(round($totalOut, 1), 0, ',', '.')}}">
            </div>
        </div>
    </div>
    <div class="row">
        <?php
            $selisih = $getData->sales - $totalOut;
        ?>
        <div class="col-md-12">
            <div class="form-group">
                <label>Total Transfer</label>
                <input type="text" class="form-control" disabled="" value="Rp. {{number_format($selisih, 0, ',', ',')}}">
            </div>
        </div>
    </div>
    @if($dataAll != null)
    <hr>
    <span>Detail Pengeluaran</span>
        @foreach($dataAll as $row)
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group">
                        <label>Kategori</label>
                        <input type="text" class="form-control" disabled="" value="{{$row->pengeluaran_name}}">
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label>Pengeluaran</label>
                        <input type="text" class="form-control" disabled="" value="{{number_format($row->amount, 0, ',', '.')}}">
                    </div>
                </div>
            </div>
        @endforeach
    @endif
@endif

@if($cek->can == false)
<div class="row">
        <h5 class="text-danger"> {{$cek->pesan}} </h5>
</div>
@endif
    
