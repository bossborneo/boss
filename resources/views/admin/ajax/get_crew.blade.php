@if(count($crew) > 0)
    <div class="form-group">
        <label>Level</label>
        <select name="level" class="form-control" title="Pilih Level">
                @foreach($crew as $row)
                    <?php
                        $level = 'Retrainer';
                        
                        if($row->level_id == 8){
                            $level = 'Asmen';
                        }
                        if($row->level_id == 9){
                            $level = 'Top Leader';
                        }
                        if($row->level_id == 10){
                            $level = 'Leader';
                        }
                        if($row->level_id == 11){
                            $level = 'Trainer';
                        }
                        if($row->level_id == 12){
                            $level = 'Merchandiser';
                        }
                    ?>
                    <option value="{{$row->level_id}}">{{$level}}</option>
                @endforeach
            
        </select>
    </div>
@endif

