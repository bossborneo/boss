@if($type == 1)
<form class="login100-form validate-form" method="post" action="/adm/rm/top-structure">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Structure</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama Top Structure</label>
                    <input type="text" disabled="" class="form-control" value="{{$getData->name}}"/>
                </div>
            </div>
        </div>
    </div>
    <div>
        <input type="hidden" name="cekId" value="{{$getData->id}}" >
    </div>    
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Hapus</button>
    </div>
</form>
@endif

@if($type == 2)
<form class="login100-form validate-form" method="post" action="/adm/rm/down-structure">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Structure</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama Structure</label>
                    <input type="text" disabled="" class="form-control" value="{{$getData->name}}"/>
                </div>
            </div>
        </div>
    </div>
    <div>
        <input type="hidden" name="cekId" value="{{$getData->id}}" >
    </div>    
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Hapus</button>
    </div>
</form>
@endif