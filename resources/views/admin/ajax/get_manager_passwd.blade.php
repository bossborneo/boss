@if($type == 1)
    @if(count($manager) > 0)
        <div class="form-group">
            <label>Pilih Nama</label>
            <select name="manager_id" class="form-control" title="Pilih Manager" onChange="getTypeSearchDataUser(this.value);">
                    <option value="">- Pilih Manager -</option>
                    @foreach($manager as $row)
                        @if($row->level_id == 6)
                            <option value="{{$row->id}}">(MQB) {{$row->name}}</option>
                        @endif
                        @if($row->level_id == 7)
                            <option value="{{$row->id}}">(Manager) {{$row->name}}</option>
                        @endif
                    @endforeach
            </select>
        </div>
    @endif
@endif

@if($type == 2)
    @if(count($manager) > 0)
        <div class="form-group">
            <label>Pilih Nama</label>
            <select name="manager_id" class="form-control" title="Pilih Manager" onChange="getTypeSearchDataUser(this.value);">
                    <option value="">- Pilih Manager -</option>
                    @foreach($manager as $row)
                        @if($row->level_id == 7)
                            <option value="{{$row->id}}">(Manager) {{$row->name}}</option>
                        @endif
                    @endforeach
            </select>
        </div>
    @endif
@endif

@if($type == 3)
    @if(count($manager) > 0)
        <div class="form-group">
            <label>Pilih Nama</label>
            <select name="manager_id" class="form-control" title="Pilih Asmen" onChange="getTypeSearchDataUser(this.value);">
                    <option value="">- Pilih Asmen -</option>
                    @foreach($manager as $row)
                        @if($row->level_id == 8)
                            <option value="{{$row->id}}">(Asmen) {{$row->name}}</option>
                        @endif
                    @endforeach
            </select>
        </div>
    @endif
@endif

@if($type == 4)
    @if(count($manager) > 0)
        <div class="form-group">
            <label>Pilih Nama</label>
            <select name="manager_id" class="form-control" title="Pilih TLD" onChange="getTypeSearchDataUser(this.value);">
                    <option value="">- Pilih Top Leader -</option>
                    @foreach($manager as $row)
                        @if($row->level_id == 9)
                            <option value="{{$row->id}}">(TLD) {{$row->name}}</option>
                        @endif
                    @endforeach
            </select>
        </div>
    @endif
@endif

