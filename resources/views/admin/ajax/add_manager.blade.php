@if($type > 2)
    @if($dataSponsor != null)
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Sponsor</label>
                <select class="form-control" name="parent_id">
                    <option selected value="0">- Pilih Sponsor -</option>
                    
                        @foreach($dataSponsor as $row1)
                            <option value="{{$row1->id}}">{{$row1->name}}</option>
                        @endforeach
                    
                </select>
            </div>
        </div>
    </div>
    @endif
@endif
