@if($type == 'edit')
<form class="login100-form validate-form" method="post" action="/adm/edit/pengeluaran">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Pengeluaran</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kategori</label>
                    <input type="text" disabled="" class="form-control" value="{{$getData->pengeluaran_name}}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jumlah</label>
                    <input type="text" disabled="" class="form-control" value="Rp. {{number_format($getData->pengeluaran_price, 0, ',', ',')}}"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea class="form-control" disabled="" rows="1">{{$getData->keterangan}}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Jumlah Baru</label>
                    <input type="text" class="form-control" name="new_jumlah"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Keterangan Baru</label>
                    <textarea class="form-control" name="keterangan" rows="1">{{$getData->keterangan}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div>
        <input type="hidden" name="cekId" value="{{$getData->id}}" >
        <input type="hidden" name="transfer_id" value="{{$t_id}}" >
    </div>    
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endif

@if($type == 'remove')
<form class="login100-form validate-form" method="post" action="/adm/remove/pengeluaran">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Pengeluaran</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Kategori</label>
                    <input type="text" disabled="" class="form-control" value="{{$getData->pengeluaran_name}}"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jumlah</label>
                    <input type="text" disabled="" class="form-control" value="Rp. {{number_format($getData->pengeluaran_price, 0, ',', ',')}}"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea class="form-control" disabled="" rows="1">{{$getData->keterangan}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div>
        <input type="hidden" name="cekId" value="{{$getData->id}}" >
        <input type="hidden" name="transfer_id" value="{{$t_id}}" >
    </div>    
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Hapus</button>
    </div>
</form>
@endif