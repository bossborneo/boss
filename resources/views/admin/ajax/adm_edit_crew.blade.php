<form class="login100-form validate-form" method="post" action="/adm/edit-crew/{{$dataUserId->id}}/{{$manager_id}}">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="name" value="{{$dataUserId->name}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>No.Telepon</label>
                    <input type="text" class="form-control" name="phone" value="{{$dataUserId->phone}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea class="form-control textarea" name="address">{{$dataUserId->address}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="cekId" value="{{$dataUserId->id}}" >
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Confirm</button>
    </div>
</form>   