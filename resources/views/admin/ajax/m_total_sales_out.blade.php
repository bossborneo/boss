<div class="col-md-5">
    <div class="form-group">
        <label>Total Sales</label>
        <input type="text" class="form-control" disabled="" value="{{number_format($data->sales, 0, ',', ',')}}">
    </div>
</div>
<div class="col-md-5">
    <div class="form-group">
        <label>Total Pengeluaran</label>
        <input type="text" class="form-control" disabled="" value="{{number_format($data->pengeluaran, 0, ',', ',')}}">
    </div>
</div>
