<form class="login100-form validate-form" method="post" action="/adm/rm-crew/{{$dataUserId->id}}/{{$dataManager->id}}">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" disabled=""  value="{{$dataUserId->name}}">
                </div>
            </div>
        </div>
        <?php
            $level = 'Asistance Manager';
            if($dataUserId->level_id == 9){
                $level = 'Top Leader';
            }
            if($dataUserId->level_id == 10){
                $level = 'Leader';
            }
            if($dataUserId->level_id == 11){
                $level = 'Trainer';
            }
            if($dataUserId->level_id == 12){
                $level = 'Merchandiser';
            }
            if($dataUserId->level_id == 13){
                $level = 'Retrainer';
            }
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Posisi</label>
                    <input type="text" class="form-control" disabled="" value="{{$level}}">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="cekId" value="{{$dataUserId->id}}" >
    <input type="hidden" name="managerId" value="{{$dataUserId->id}}" >
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Confirm</button>
    </div>
</form>   