<form class="login100-form validate-form" method="post" action="/adm/out/category/action/{{$type}}/{{$getData->id}}">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" disabled=""  value="{{$getData->pengeluaran_name}}">
                </div>
            </div>
        </div>
        @if($type == 'edit')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Ubah Nama</label>
                        <input type="text" class="form-control" name="new_name">
                    </div>
                </div>
            </div>
        @endif
    </div>
    <input type="hidden" name="cekId" value="{{$getData->id}}" >
    <?php
        $text = 'Confirm';
        if($type == 'rm'){
            $text = 'Hapus';
        }
    ?>
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">{{$text}}</button>
    </div>
</form>   