@if($type == 1)
    @if($dataSales != null)
        <form class="login100-form validate-form" method="post" action="/adm/action-sale/edit/{{$dataSales->id}}/{{$uid}}">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
            </div>
            <div class="modal-body">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Barang</label>
                            <input type="text" disabled="" class="form-control" value="{{$dataSales->purchase_name}}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Jumlah</label>
                            <input type="number" min="0" step="0.5"  name="amount" class="form-control" value="{{number_format($dataSales->amount, 1, '.', '.')}}"/>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="cekId" value="{{$dataSales->id}}" >
            <div class="modal-footer" style="margin-right: 10px;">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
        </form>
    @endif
@endif

@if($type == 2)
    @if($dataSales != null)
    <form class="login100-form validate-form" method="post" action="/adm/action-sale/rm/{{$dataSales->id}}/{{$uid}}">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
            </div>
            <div class="modal-body">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Barang</label>
                            <input type="text" disabled="" class="form-control" value="{{$dataSales->purchase_name}}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Jumlah</label>
                            <input type="number" disabled="" class="form-control" value="{{number_format($dataSales->amount, 1, '.', '.')}}"/>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="cekId" value="{{$dataSales->id}}" >
            <div class="modal-footer" style="margin-right: 10px;">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Hapus</button>
            </div>
        </form>
    @endif
@endif
