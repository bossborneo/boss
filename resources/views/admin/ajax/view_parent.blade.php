@if($spAsmen != null)
    <div class="col-md-6">
        <div class="form-group">
            <label>Asisten Manager</label>
            <input type="text" class="form-control" disabled="" value="{{$spAsmen->name}}">
        </div>
    </div>
@endif

@if($spTld != null)
    <div class="col-md-6">
        <div class="form-group">
            <label>Top Leader</label>
            <input type="text" class="form-control" disabled="" value="{{$spTld->name}}">
        </div>
    </div>
@endif
