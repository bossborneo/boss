<form class="login100-form validate-form" method="post" action="/adm/rm/stock/{{$dataStock->id_item}}/{{$date}}">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Stock</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <?php
            $type = 'Obat';
            if($dataStock->type == 1){
                $type = 'Alat';
            }
            if($dataStock->type == 2){
                $type = 'Strip';
            }
        ?>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label>Nama Barang</label>
                    <input type="text" class="form-control" disabled="" value="{{$dataStock->purchase_name}}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Type Barang</label>
                    <input type="text" class="form-control" disabled="" value="{{$type}}">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Stock Lama</label>
                    <input type="text" class="form-control" disabled="" value="{{number_format($dataStock->qty_input, 1, '.', '.')}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal</label>
                    <input type="text" class="form-control" disabled="" value="{{date('d M Y', strtotime($date))}}">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="cekId" value="{{$dataStock->id_item}}" >
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Confirm</button>
    </div>
</form>   