<form class="login100-form validate-form" method="post" action="/adm/rm/content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Berita</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <input type="text" class="form-control" value="{{$getData->title}}">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="cekId" value="{{$getData->id}}" >
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Confirm</button>
    </div>
</form>   