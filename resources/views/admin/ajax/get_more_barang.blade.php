<?php
    $ke = ($counter * 2) - 1;
    $ke2 =$ke + 1
?>
<div class="row" id="rowBarang">
    <div class="col-md-6">
        <div class="form-group">
            <label>Barang</label>
            <select class="form-control" name="item_purchase[]" id="_item{{$counter}}">
                <option selected value="0">- Pilih Barang -</option>
                @if($allBarang != null)
                    @foreach($allBarang as $row)
                        @if($row->qty_input > $row->qty_output)
                            <?php
                                $type = '--';
                                if($row->type == 1){
                                    $type = 'Alat';
                                }
                                if($row->type == 2){
                                    $type = 'Strip';
                                }
                                if($row->type == 3){
                                    $type = 'Obat';
                                }
                                if($row->type == 10){
                                    $type = 'Lainnya';
                                }
                            ?>  
                            <option value="{{$row->id_item_purchase}}" data-price="{{$row->price}}" type{{$counter}}="{{$row->type}}">({{$type}}) {{$row->purchase_name}}</option>
                        @endif
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Jumlah</label>
            <input type="number" min="0" step="1"  name="amount[]" class="form-control input{{$ke}}" id="_amount"/>
            <input type="number" min="0" step="0.5"  name="amount[]" class="form-control input{{$ke2}}" id="_amount" style="display: none;"/>
        </div>
    </div>
    <?php
    /*
    <div class="col-md-2">
        <div class="form-group">
            <label>Opsi</label>
            <select class="form-control" name="opsi[]" id="_item">
                <option selected value="0">Full</option>
                <option value="1">Setengah</option>
            </select>
        </div>
    </div>
     * 
     */
    ?>
    <div class="col-md-2">
        <div class="form-group">
            <label>&nbsp;</label>
            <input type="button" class="form-control btn btn-sm btn-danger " value="-" id="rmLine" style="margin:0;">
        </div>
    </div>
</div>    


