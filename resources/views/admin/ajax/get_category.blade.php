<div class="col-md-3"  id="searchKategori-listRow">
    <div class="form-group">
        <label>Kategori Pengeluaran</label>
        <select class="form-control" name="category_id[]" id="_item">
            <option selected value="0">- Pilih -</option>
            @if($allCategory != null)
                @foreach($allCategory as $row)
                        <option value="{{$row->id}}" data-price="{{$row->pengeluaran_name}}">{{$row->pengeluaran_name}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="col-md-2"  id="searchKategori-listRow">
    <div class="form-group">
        <label>Jumlah</label>
        <input type="number" min="0" step="0.5"  name="amount[]" class="form-control" id="_amount"/>
    </div>
</div>
<div class="col-md-5"  id="searchKategori-listRow">
    <div class="form-group">
        <label>Keterangan</label>
        <textarea class="form-control" name="ket[]" rows="1"></textarea>
    </div>
</div>

