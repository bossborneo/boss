    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi Safra</h5>
    </div>
    <div class="modal-body">
        @if($getData != null)
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>UserID</label>
                        <input type="text" class="form-control" readonly="" value="{{$getData->name}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cabang</label>
                        <input type="text" readonly=""  class="form-control" value="{{$getData->cabang_name}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Tgl Transfer</label>
                        <input type="text" class="form-control" readonly="" value="<?php  echo (date('d F Y', strtotime($getData->sale_date))) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Jml. Paket</label>
                        <input type="text" class="form-control" readonly="" value="{{number_format($getData->amount, 0, ',', '.')}}">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Jml. Harga</label>
                        <input type="text" readonly=""  class="form-control" value="{{number_format($getData->sale_price, 0, ',', '.')}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Bank</label>
                        <input type="text" class="form-control" readonly="" value="{{$getData->bank_name}}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nomor Rekening</label>
                        <input type="text" readonly=""  class="form-control" value="{{$getData->account_no}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nama Rekening</label>
                        <input type="text" class="form-control" readonly="" value="{{$getData->account_name}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Status</label>
                        <?php
                            $text = 'Reject';
                            if($getData->safra_status == 2){
                                $text = 'Tuntas';
                            }
                        ?>
                        <input type="text" readonly=""  class="form-control" value="{{$text}}">
                    </div>
                </div>
            </div>
            @if($getData->safra_status == 3)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Status</label>
                        <?php
                            $text = 'Reject';
                            if($getData->safra_status == 2){
                                $text = 'Tuntas';
                            }
                        ?>
                        <input type="text" readonly=""  class="form-control" value="{{$getData->reason}}">
                    </div>
                </div>
            </div>
            @endif
        @else 
            Tidak ada data
        @endif
    </div>
    
    <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Tutup</button>
    </div>
