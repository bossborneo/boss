<?php //SIDEBAR KIRI 5063 erika@boss.com ?>
<?php $idErika = 5063; ?>
<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
        <a href="#" class="simple-text logo-normal">
        &nbsp;&nbsp;
        BOSS
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li> <?php //{{ app('request')}} ?>
            <a href="{{ URL::to('/dashboard') }}">
                <i class="nc-icon nc-bank"></i>
                <p>Dashboard</p>
            </a>
            </li>
            
            @if($dataUser->user_type == 5)
            <li>
                <a data-toggle="collapse" href="#formsPenjualan" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-tag-content"></i>
                    <p>
                        Penjualan
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formsPenjualan" style="">
                    <ul class="nav">
                        @if($dataUser->level_id == 7)
                        <li>
                            <a href="{{ URL::to('/') }}/m/input-sale">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Input Penjualan </span>
                            </a>
                        </li>
                        @endif
                        <li>
                            <a href="{{ URL::to('/') }}/m/data-sales">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Data Penjualan </span>
                            </a>
                        </li>
                        <?php
                        /*
                        @if($dataUser->level_id == 7)
                        <li>
                            <a href="{{ URL::to('/') }}/m/input-sale-safra">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Input Penjualan Safra</span>
                            </a>
                        </li>
                        @endif
                        <li>
                            <a href="{{ URL::to('/') }}/m/data-sales-safra">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Data Penjualan Safra</span>
                            </a>
                        </li>
                         */
                        ?>
                    </ul>
                </div>
            </li>
            
            <li>
                <a data-toggle="collapse" href="#formStock" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-cart-simple"></i>
                    <p>
                        Stock Opname
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formStock" style="">
                    <ul class="nav">
                        @if(date('w') == 1 || date('w') == 6)
                        <li>
                            <a href="{{ URL::to('/') }}/m/stock-barang">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Input Stock </span>
                            </a>
                        </li>
                        @endif
                        <li>
                            <a href="{{ URL::to('/') }}/m/history/stock">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Data Stock </span>
                            </a>
                        </li>
                        @if($dataUser->level_id == 6)
                                <li>
                                    <a href="{{ URL::to('/') }}/mqb/list-order-barang">
                                    <span class="sidebar-mini-icon">&nbsp;</span>
                                    <span class="sidebar-normal" style="font-size: 14px;"> Order Stock </span>
                                    </a>
                                </li>
                        @endif
                    </ul>
                </div>
            </li>
            
            <li>
                <a data-toggle="collapse" href="#formCrew" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-badge"></i>
                    <p>
                        Crew
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formCrew" style="">
                    <ul class="nav">
                        <?php
                        /*
                        <li>
                            <a href="{{ URL::to('/') }}/add-crew">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Input Crew </span>
                            </a>
                        </li>
                         * 
                         */
                        ?>
                        <li>
                            <a href="{{ URL::to('/') }}/view-crew">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Data Crew </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/m/tree/structure">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Data Struktur </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/m/absen">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Data Absen </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#formGlobal" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-tag-content"></i>
                    <p>
                        Global
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formGlobal" style="">
                    <ul class="nav">
                        <li>
                            <a href="{{ URL::to('/') }}/m/myglobal">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Sales </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/m/detail-sales">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Detail Sales </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/m/global-stock/manager">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Stock </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#formLaporan" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-chart-bar-32"></i>
                    <p>
                        Laporan
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formLaporan" style="">
                    <ul class="nav">
                        <li>
                            <a href="{{ URL::to('/') }}/myreport">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Sales </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/m/report-crew') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Crew </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/m/list/bonus/manager') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Bonus </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            
            <li>
                <a data-toggle="collapse" href="#formsTransfer" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-delivery-fast"></i>
                    <p>
                        Transfer
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formsTransfer" style="">

                    <ul class="nav">
                        @if($dataUser->level_id == 7)
                        <li>
                            <a href="{{ URL::to('/') }}/m/input/pengeluaran">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Input Pengeluaran </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/m/transfers">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Data Transfer </span>
                            </a>
                        </li>
                        @endif
                        @if($dataUser->level_id < 7)
                        <li>
                            <a href="{{ URL::to('/') }}/manager/transfers">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Data Transfer </span>
                            </a>
                        </li>
                        @endif
                    </ul>

                </div>
            </li>
                @if($dataUser->level_id == 6)
                    <li>
                        <a data-toggle="collapse" href="#formsPengeluaran" class="collapsed" aria-expanded="false">
                            <i class="nc-icon nc-money-coins"></i>
                            <p>
                                Pengeluaran
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="formsPengeluaran" style="">

                            <ul class="nav">
                                <li>
                                    <a href="{{ URL::to('/') }}/mqb/input/pengeluaran">
                                    <span class="sidebar-mini-icon">&nbsp;</span>
                                    <span class="sidebar-normal" style="font-size: 14px;"> Input Pengeluaran </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ URL::to('/') }}/mqb/list/pengeluaran">
                                    <span class="sidebar-mini-icon">&nbsp;</span>
                                    <span class="sidebar-normal" style="font-size: 14px;"> Data Pengeluaran </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#formsCustomer" class="collapsed" aria-expanded="false">
                            <i class="nc-icon nc-pin-3"></i>
                            <p>
                                Customer
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="formsCustomer" style="">
                            <ul class="nav">
                                <li>
                                    <a href="{{ URL::to('/') }}/mqb/input-customers">
                                    <span class="sidebar-mini-icon">&nbsp;</span>
                                    <span class="sidebar-normal" style="font-size: 14px;"> Input Customer </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ URL::to('/') }}/mqb/customers">
                                    <span class="sidebar-mini-icon">&nbsp;</span>
                                    <span class="sidebar-normal" style="font-size: 14px;"> Data Customer </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endif
                <li>
                    <a href="{{ URL::to('/') }}/m/contents">
                        <i class="nc-icon nc-paper"></i>
                        <p>Berita</p>
                    </a>
                </li>
                @if($dataUser->structure_type == 1)
                    <li>
                        <a href="{{ URL::to('/') }}/m/down-structures">
                            <i class="nc-icon nc-chat-33"></i>
                            <p>Organisasi</p>
                        </a>
                    </li>
                @endif
            @endif
            
            @if($dataUser->user_type == 3 || $dataUser->user_type == 2 || $dataUser->user_type == 1)
            <?php
                $role = null;
                if($dataUser->permissions != null){
                    $jsonRole = $dataUser->permissions;
                    $role = json_decode($jsonRole);
                }
            ?>
            @if($dataUser->id == $idErika)
            <li>
                <a href="{{ URL::to('/view-managers') }}">
                    <i class="nc-icon nc-badge"></i>
                    <p>Manager</p>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('/view-cabangs') }}">
                    <i class="nc-icon nc-pin-3"></i>
                    <p>Cabang</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#formLaporan" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-chart-bar-32"></i>
                    <p>
                        Laporan
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formLaporan" style="">
                    <ul class="nav">
                        <li>
                            <a href="{{ URL::to('/adm/data-sales') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Sales </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/report-crew') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Crew </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#formGlobal" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-globe"></i>
                    <p>
                        Global
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formGlobal" style="">
                    <ul class="nav">
                        <li>
                            <a href="{{ URL::to('/') }}/adm/global-manager/manager">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Manager </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            @endif
            
            @if($dataUser->id != $idErika)
            <li>
                <a href="{{ URL::to('/view-managers') }}">
                    <i class="nc-icon nc-badge"></i>
                    <p>Manager</p>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('/view-cabangs') }}">
                    <i class="nc-icon nc-pin-3"></i>
                    <p>Cabang</p>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('/all-purchase') }}">
                    <i class="nc-icon nc-cart-simple"></i>
                    <p>Barang</p>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('/adm/list-order-barang') }}">
                    <i class="nc-icon nc-box"></i>
                    <p>Order</p>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('/adm/absen') }}">
                    <i class="nc-icon nc-touch-id"></i>
                    <p>Absen</p>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('/adm/out/category') }}">
                    <i class="nc-icon nc-book-bookmark"></i>
                    <p>Kategori Pengeluaran</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#formSetting" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-tag-content"></i>
                    <p>
                        Setting
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formSetting" style="">
                    <ul class="nav">
                        <li>
                            <a href="{{ URL::to('/') }}/adm/setting/bonus">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Bonus Setting </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/adm/setting/app">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> App Setting </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="{{ URL::to('/adm/transfers') }}">
                    <i class="nc-icon nc-delivery-fast"></i>
                    <p>Transfer</p>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('/adm/top-structures') }}">
                    <i class="nc-icon nc-chat-33"></i>
                    <p>Organisasi</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#formLaporan" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-chart-bar-32"></i>
                    <p>
                        Laporan
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formLaporan" style="">
                    <ul class="nav">
                        <li>
                            <a href="{{ URL::to('/adm/report/stock') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Stock </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/data-sales') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Sales </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/manager/detail-sales') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Detail Sales </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/report-crew') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Crew </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/list/pengeluaran') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Pengeluaran </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/list/mqb-pengeluaran') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Pengeluaran MQB </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/report/bonus-crew') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Bonus Crew </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/report/omset') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Bonus Omset </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/list/bonus/manager') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Bonus Manager </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/product-sales/manager') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Product Sales Manager </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/product-sales/asmen') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Product Sales Asmen </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/product-sales/tld') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Product Sales Top Leader </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#formGlobal" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-globe"></i>
                    <p>
                        Global
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formGlobal" style="">
                    <ul class="nav">
                        <li>
                            <a href="{{ URL::to('/') }}/adm/global-manager/manager">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Manager </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/adm/global-asmen">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Asmen </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/') }}/adm/global-tld">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Top Leader </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="{{ URL::to('/adm/contents') }}">
                    <i class="nc-icon nc-tag-content"></i>
                    <p>Berita</p>
                </a>
            </li>
            @endif
            @endif
            
            @if($dataUser->id != $idErika)
            @if($dataUser->user_type == 20)
            <li>
                <a href="{{ URL::to('/adm-cs/customers') }}">
                    <i class="nc-icon nc-pin-3"></i>
                    <p>Data Customer</p>
                </a>
            </li>
            @endif
            @if($dataUser->user_type == 30 || $dataUser->user_type == 2 || $dataUser->user_type == 1)
            <li>
                <a data-toggle="collapse" href="#formLaporanSafra" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-chart-bar-32"></i>
                    <p>
                        Laporan Safra
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formLaporanSafra" style="">
                    <ul class="nav">
                        <li>
                            <a href="{{ URL::to('/adm/manager/detail-sales-safra') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Detail Sales Safra</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/all/sales-safra') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> All Sales Safra</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/report/stock-safra') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Stock Safra</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            @endif
            @if($dataUser->user_type == 2)
            <li>
                <a href="{{ URL::to('/adm/passwd') }}">
                    <i class="nc-icon nc-key-25"></i>
                    <p>Change Passwd</p>
                </a>
            </li>
            @endif
            @endif
            
            @if($dataUser->user_type == 31)
            <li>
                <a data-toggle="collapse" href="#formLaporan" class="collapsed" aria-expanded="false">
                    <i class="nc-icon nc-chart-bar-32"></i>
                    <p>
                        Laporan
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formLaporan" style="">
                    <ul class="nav">
                        <li>
                            <a href="{{ URL::to('/adm/data-sales') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Sales </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/adm/manager/detail-sales') }}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Detail Sales </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            @endif
            <li>
                <a href="{{ URL::to('/logout') }}">
                    <i class="nc-icon nc-button-power text-danger"></i>
                    <p>Keluar</p>
                </a>
            </li>
        </ul>
    </div>
</div>