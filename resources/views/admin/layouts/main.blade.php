<!DOCTYPE html>
<html>
<head>
    <title>Borneo Group</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/png" href="/images/favicon.jpeg"/>
    <link rel="stylesheet" href="{{ asset('css/boss.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/paper-dashboard.css') }}">
    <link rel="stylesheet" href="{{ asset('css/demo.css') }}">
    @yield('admin-styles')
</head>

<body>

    <div class="wrapper ">
        @yield('admin-content')
    </div>

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}"></script>
    <!--<script src="{{ asset('js/chartjs.min.js') }}"></script>-->
    <script src="{{ asset('js/bootstrap-notify.js') }}"></script>
    <script src="{{ asset('js/paper-dashboard.min.js') }}"></script>
    <!--<script src="{{ asset('js/main-script.js') }}"></script>-->
    @yield('admin-javascript')

</body>