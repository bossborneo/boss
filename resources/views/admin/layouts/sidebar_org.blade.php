<?php //SIDEBAR KIRI ?>
<div class="sidebar" data-color="white" data-active-color="danger">
   <div class="logo">
      <a href="#" class="simple-text logo-normal">
      &nbsp;&nbsp;
      BOSS
      </a>
   </div>
   <div class="sidebar-wrapper">
      <ul class="nav">
         <li>
            <a class="text-warning" href="{{ URL::to('/') }}/org/dashboard/{{$getIdOrg->id}}">
               <i class="nc-icon nc-bank"></i>
               <p>Dashboard</p>
            </a>
         </li>
         <li>
            <a class="text-warning" data-toggle="collapse" href="#formsPenjualan" class="collapsed" aria-expanded="false">
               <i class="nc-icon nc-tag-content"></i>
               <p>
                  Penjualan
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse" id="formsPenjualan" style="">
               <ul class="nav">
                  <li>
                     <a class="text-warning" href="{{ URL::to('/') }}/org/data-sales/{{$getIdOrg->id}}">
                     <span class="sidebar-mini-icon">&nbsp;</span>
                     <span class="sidebar-normal" style="font-size: 14px;"> Data Penjualan </span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>
         <li>
            <a class="text-warning" data-toggle="collapse" href="#formStock" class="collapsed" aria-expanded="false">
               <i class="nc-icon nc-cart-simple"></i>
               <p>
                  Stock Opname
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse" id="formStock" style="">
               <ul class="nav">
                  <li>
                     <a href="{{ URL::to('/') }}/org/data-stock/{{$getIdOrg->id}}">
                     <span class="sidebar-mini-icon">&nbsp;</span>
                     <span class="sidebar-normal" style="font-size: 14px;"> Data Stock </span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>
         <li>
            <a class="text-warning" data-toggle="collapse" href="#formCrew" class="collapsed" aria-expanded="false">
               <i class="nc-icon nc-badge"></i>
               <p>
                  Crew
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse" id="formCrew" style="">
               <ul class="nav">
                  <li>
                        <a href="{{ URL::to('/') }}/org/data-crew/{{$getIdOrg->id}}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Data Crew </span>
                        </a>
                  </li>
                  <li>
                        <a href="{{ URL::to('/') }}/org/tree-structure/{{$getIdOrg->id}}">
                           <span class="sidebar-mini-icon">&nbsp;</span>
                           <span class="sidebar-normal" style="font-size: 14px;"> Data Struktur </span>
                        </a>
                  </li>
                  <li>
                        <a href="{{ URL::to('/') }}/org/data-absen/{{$getIdOrg->id}}">
                            <span class="sidebar-mini-icon">&nbsp;</span>
                            <span class="sidebar-normal" style="font-size: 14px;"> Data Absen </span>
                        </a>
                  </li>
               </ul>
            </div>
         </li>
         <li>
            <a class="text-warning" data-toggle="collapse" href="#formGlobal" class="collapsed" aria-expanded="false">
               <i class="nc-icon nc-tag-content"></i>
               <p>
                  Global
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse" id="formGlobal" style="">
               <ul class="nav">
                  <li>
                     <a href="{{ URL::to('/') }}/org/myglobal/{{$getIdOrg->id}}">
                     <span class="sidebar-mini-icon">&nbsp;</span>
                     <span class="sidebar-normal" style="font-size: 14px;"> Sales </span>
                     </a>
                  </li>
                  <li>
                     <a href="{{ URL::to('/') }}/org/global/detail-sales/{{$getIdOrg->id}}">
                     <span class="sidebar-mini-icon">&nbsp;</span>
                     <span class="sidebar-normal" style="font-size: 14px;"> Detail Sales </span>
                     </a>
                  </li>
                  <li>
                     <a href="{{ URL::to('/') }}/org/mystock/{{$getIdOrg->id}}">
                     <span class="sidebar-mini-icon">&nbsp;</span>
                     <span class="sidebar-normal" style="font-size: 14px;"> Stock </span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>
         <li>
            <a class="text-warning" data-toggle="collapse" href="#formLaporan" class="collapsed" aria-expanded="false">
               <i class="nc-icon nc-chart-bar-32"></i>
               <p>
                  Laporan
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse" id="formLaporan" style="">
               <ul class="nav">
                  <li>
                     <a href="{{ URL::to('/') }}/org/myreport/{{$getIdOrg->id}}">
                     <span class="sidebar-mini-icon">&nbsp;</span>
                     <span class="sidebar-normal" style="font-size: 14px;"> Sales </span>
                     </a>
                  </li>
                  <li>
                     <a href="{{ URL::to('/') }}/org/report-crew/{{$getIdOrg->id}}">
                     <span class="sidebar-mini-icon">&nbsp;</span>
                     <span class="sidebar-normal" style="font-size: 14px;"> Crew </span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>
         <li>
            <a class="text-warning" data-toggle="collapse" href="#formsTransfer" class="collapsed" aria-expanded="false">
               <i class="nc-icon nc-delivery-fast"></i>
               <p>
                  Transfer
                  <b class="caret"></b>
               </p>
            </a>
            <div class="collapse" id="formsTransfer" style="">
               <ul class="nav">
                  <li>
                     <a href="{{ URL::to('/') }}/org/data-transfer/{{$getIdOrg->id}}">
                     <span class="sidebar-mini-icon">&nbsp;</span>
                     <span class="sidebar-normal" style="font-size: 14px;"> Data Transfer </span>
                     </a>
                  </li>
               </ul>
            </div>
         </li>
         <li>
            <a href="{{ URL::to('/') }}/m/down-structures">
               <i class="nc-icon nc-chat-33"></i>
               <p>Organisasi</p>
            </a>
         </li>
         <li>
            <a href="{{ URL::to('/logout') }}">
               <i class="nc-icon nc-button-power text-danger"></i>
               <p>Keluar</p>
            </a>
         </li>
      </ul>
   </div>
</div>