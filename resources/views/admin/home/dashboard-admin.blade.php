@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-2">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-money-coins text-success"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-10">
                                <div class="numbers">
                                    <p class="card-category">
                                        Total Penjualan ({{date('F')}})
                                    </p>
                                    <p class="card-title">
                                        Rp. {{number_format($salesAll->jml_price, 0, ',', '.')}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> Top 40 Sales Crew ({{date("d M Y", strtotime($date->startDay))}} - {{date("d M Y", strtotime($date->endDay))}})</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/top-cabang">Top 40 Per Cabang</a>
                    </div>
                    <div class="card-body">
                        <form class="login100-form validate-form" method="get" action="/dashboard" style="margin-bottom: 20px;">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Start Date</label>
                                            <input type="text" class="form-control datepickerStart" name="start_date">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>End Date</label>
                                            <input type="text" class="form-control datepickerEnd" name="end_date">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button class="form-control btn btn-sm btn-primary " style="margin: 0">Cari</button>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </form>
                        <div class="table-responsive">
                            <table class="table" id="myTableData">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Cabang</th>
                                        <th>Penjualan (Rp.)</th>
                                        <th>Jml Strip</th>
                                        <th>Jml Obat 60</th>
                                        <th>Jml Obat 30</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($getTop10 != null)
                                    <?php $no = 0; ?>
                                    @foreach($getTop10 as $row)

                                        <?php 
                                        $no++; 
                                        $level = 'General Manager';
                                        if($row->level_id == 3){
                                            $level = 'Senior Executive Manager';
                                        }
                                        if($row->level_id == 4){
                                            $level = 'Executive Manager';
                                        }
                                        if($row->level_id == 5){
                                            $level = 'Senior Manager';
                                        }
                                        if($row->level_id == 6){
                                            $level = 'MQB';
                                        }
                                        if($row->level_id == 7){
                                            $level = 'Manager';
                                        }
                                        if($row->level_id == 8){
                                            $level = 'Asmen';
                                        }
                                        if($row->level_id == 9){
                                            $level = 'Top Leader';
                                        }
                                        if($row->level_id == 10){
                                            $level = 'Leader';
                                        }
                                        if($row->level_id == 11){
                                            $level = 'Trainer';
                                        }
                                        if($row->level_id == 12){
                                            $level = 'Merchandiser';
                                        }
                                        if($row->level_id == 13){
                                            $level = 'Retrainer';
                                        }
                                        ?>
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{$row->name}}</td>
                                            <td>{{$level}}</td>
                                            <td>{{$row->cabang_name}}</td>
                                            <td>{{number_format($row->jml_price, 0)}}</td>
                                            <td>{{number_format($row->sum_strip, 1)}}</td>
                                            <td>{{number_format($row->sum_obat_full, 1)}}</td>
                                            <td>{{number_format($row->sum_obat_sgth, 1)}}</td>
                                        </tr>
                                    @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="chart-bar"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="chart-bar-user"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@if($download == true)
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTableData').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export Top 20 Sales Crew (last 7 Days)'
                    }
                ],
                searching: false,
                columnDefs: [
                    { orderable: false, targets: -1 }
                 ],
                 pageLength: 10
        } );
    } );
</script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
    $('.datepickerStart').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
    $('.datepickerEnd').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
    </script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script type="text/javascript">
    Highcharts.chart('chart-bar', {
        chart: {
            type: 'column',
            zoomType: 'x'
        },
        colors: [
             '#1DC7EA'
        ],
    legend: {
      enabled: false
    },
    title: {
    text:  'Laju Penambahan Crew'
    },
    subtitle: {
      text: ' '
    },
    xAxis: {
    categories: [<?php echo $month ?> ],
      tickmarkPlacement: 'on',
      tickInterval: 1,
      minRange: 1
    },
    yAxis: {
      min: 0,
      title: {
        text: 'total register',
        style: {
          fontSize: '0px'
        }
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' +
    	'<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    plotOptions: {
        series: {
          cursor: 'pointer',
          label: { connectorAllowed: false },
          pointStart: 0,
          pointWidth: 40,
          point: {
              events: {
              }
          }
        }
    },
    series: [{
      name: 'Total',
      data: [<?php echo $down ?>]
    },]
    });
</script>
<script type="text/javascript">
    Highcharts.chart('chart-bar-user', {
    chart: {
      type: 'column',
      zoomType: 'x'
    },
    colors: [
      '#1DC7EA'
    ],
    legend: {
      enabled: false
    },
    title: {
    text:  'Laju Penjualan'
    },
    subtitle: {
      text: ' '
    },
    xAxis: {
    categories: [<?php echo $monthSale ?>],
      tickmarkPlacement: 'on',
      tickInterval: 1,
      minRange: 1
    },
    yAxis: {
      min: 0,
      title: {
        text: 'total register',
        style: {
          fontSize: '0px'
        }
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' +
    	'<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    plotOptions: {
        series: {
          cursor: 'pointer',
          label: { connectorAllowed: false },
          pointStart: 0,
          pointWidth: 40,
          point: {
              events: {
              }
          }
        }
    },
    series: [{
      name: 'Total',
      data: [<?php echo $priceSale ?>]
    },]
    });
</script>
@stop
