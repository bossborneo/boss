@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php 
        $level1 = 'Manager';
        if($dataManager->level_id == 2){
            $level1 = 'General Manager';
        }
        if($dataManager->level_id == 3){
            $level1 = 'Senior Executive Manager';
        }
        if($dataManager->level_id == 4){
            $level1 = 'Executive Manager';
        }
        if($dataManager->level_id == 5){
            $level1 = 'Senior Manager';
        }
        if($dataManager->level_id == 6){
            $level1 = 'MQB';
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="image">
                        <img src="/images/mlm.jpg" alt="...">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <div>
                                <img class="avatar border-gray" src="/images/default-avatar.png" alt="...">
                                <h5 class="title">{{$dataManager->name}}</h5>
                            </div>
                            <p class="description">
                                {{$level1}}
                            </p>
                        </div>
                        <h5 class="description text-center">{{$dataManager->cabang_name}}</h5>
                    </div>
                    <div class="card-footer">
                        <hr>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/add-down/{{$dataManager->id}}">Tambah</a>
                    </div>
                    <div class="card-body">
                        <div id="treeview_json"></div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/tree.min.css') }}" />
@stop

@section('admin-javascript')
<script src="{{ asset('js/jstree.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){ 
     $('#treeview_json').jstree({
	'plugins': ["wholerow"],
        'core' : {
            'data' : {
                "url" : "{{ URL::to('/') }}/adm/ajax/tree/{{$dataManager->id}}",
                "dataType" : "json" 
            }
        }
    }) 
});
</script>
@stop