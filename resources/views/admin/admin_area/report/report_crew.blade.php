@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">{{$headerTitle}}</h5>
                    </div>
                    <div class="card-body" style="min-height: auto;">
                        <form class="login100-form validate-form" method="post" action="/adm/report-crew">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Start Date</label>
                                            <input type="text" class="form-control datepickerStart" name="start_date" value="{{$sunday->nextSunday}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>End Date</label>
                                            <input type="text" class="form-control datepickerEnd" name="end_date" value="{{$sunday->dateSunday}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Cabang</label>
                                            <select class="form-control" name="cabang" onChange="getTypeSearch(this.value);">
                                                <option value="">- Pilih Cabang -</option>
                                                @if($allCabang != null)
                                                    @foreach($allCabang as $row)
                                                        <option value="{{$row->id}}" @if($dataNama->cabang_id == $row->id) selected="" @endif>{{$row->cabang_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="typeSearch-list">
                                        <div class="form-group">
                                            <label>Pilih Nama</label>
                                            <select name="manager_id" class="form-control" title="Pilih Manager" onChange="getTypeSearchCrew(this.value);">
                                                    <option value="0">- Pilih Nama -</option>
                                                    @foreach($dataAllManagerCabang as $row)
                                                        <?php
                                                            $level = 'GM';
                                                            if($row->level_id == 3){
                                                                $level = 'SEM';
                                                            }
                                                            if($row->level_id == 4){
                                                                $level = 'EM';
                                                            }
                                                            if($row->level_id == 5){
                                                                $level = 'SM';
                                                            }
                                                            if($row->level_id == 6){
                                                                $level = 'MQB';
                                                            }
                                                            if($row->level_id == 7){
                                                                $level = 'Manager';
                                                            }
                                                            if($row->level_id == 8){
                                                                $level = 'Asisten Manager';
                                                            }
                                                        ?>
                                                        <option value="{{$row->id}}" @if($dataNama->id == $row->id) selected="" @endif>({{$level}}) {{$row->name}}</option>
                                                    @endforeach

                                            </select>
                                        </div>
                                    </div>    
                                </div>
                                <div class="row">
                                    <div class="update ml-auto mr-auto">
                                        <button type="submit" class="btn btn-primary">Cari</button>
                                    </div>
                                </div>
                            </div>    
                        </form>
                    </div>
                </div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> 
                            {{$headerTitle}} 
                        </h5>
                        <h5 class="card-title"> 
                            {{$headerTitle2}} 
                        </h5>
                    </div>
                    <div class="card-body">
                        <p class="title">{{$day}}</p>
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Posisi</th>
                                        <th>Senin <br> (Rp Strip)</th>
                                        <th>Senin <br> (Qty Strip)</th>
                                        <th>Senin <br> (Rp Obat)</th>
                                        <th>Senin <br> (Qty Obat)</th>
                                        <th>Senin <br> (Rp Obat 1/2)</th>
                                        <th>Senin <br> (Qty Obat 1/2)</th>
                                        <th>Senin <br> (Rp Alat)</th>
                                        <th>Senin <br> (Qty Alat)</th>
                                        
                                        <th>Selasa <br> (Rp Strip)</th>
                                        <th>Selasa <br> (Qty Strip)</th>
                                        <th>Selasa <br> (Rp Obat)</th>
                                        <th>Selasa <br> (Qty Obat)</th>
                                        <th>Selasa <br> (Rp Obat 1/2)</th>
                                        <th>Selasa <br> (Qty Obat 1/2)</th>
                                        <th>Selasa <br> (Rp Alat)</th>
                                        <th>Selasa <br> (Qty Alat)</th>
                                        
                                        <th>Rabu <br> (Rp Strip)</th>
                                        <th>Rabu <br> (Qty Strip)</th>
                                        <th>Rabu <br> (Rp Obat)</th>
                                        <th>Rabu <br> (Qty Obat)</th>
                                        <th>Rabu <br> (Rp Obat 1/2)</th>
                                        <th>Rabu <br> (Qty Obat 1/2)</th>
                                        <th>Rabu <br> (Rp Alat)</th>
                                        <th>Rabu <br> (Qty Alat)</th>
                                        
                                        <th>Kamis <br> (Rp Strip)</th>
                                        <th>Kamis <br> (Qty Strip)</th>
                                        <th>Kamis <br> (Rp Obat)</th>
                                        <th>Kamis <br> (Qty Obat)</th>
                                        <th>Kamis <br> (Rp Obat 1/2</th>
                                        <th>Kamis <br> (Qty Obat 1/2)</th>
                                        <th>Kamis <br> (Rp Alat)</th>
                                        <th>Kamis <br> (Qty Alat)</th>
                                        
                                        <th>Jumat <br> (Rp Strip)</th>
                                        <th>Jumat <br> (Qty Strip)</th>
                                        <th>Jumat <br> (Rp Obat)</th>
                                        <th>Jumat <br> (Qty Obat)</th>
                                        <th>Jumat <br> (Rp Obat 1/2)</th>
                                        <th>Jumat <br> (Qty Obat 1/2)</th>
                                        <th>Jumat <br> (Rp Alat)</th>
                                        <th>Jumat <br> (Qty Alat)</th>
                                        
                                        <th>Sabtu <br> (Rp Strip)</th>
                                        <th>Sabtu <br> (Qty Strip)</th>
                                        <th>Sabtu <br> (Rp Obat)</th>
                                        <th>Sabtu <br> (Qty Obat)</th>
                                        <th>Sabtu <br> (Rp Obat 1/2)</th>
                                        <th>Sabtu <br> (Qty Obat 1/2)</th>
                                        <th>Sabtu <br> (Rp Alat)</th>
                                        <th>Sabtu <br> (Qty Alat)</th>
                                        
                                        <th>Minggu <br> (Rp Strip)</th>
                                        <th>Minggu <br> (Qty Strip)</th>
                                        <th>Minggu <br> (Rp Obat)</th>
                                        <th>Minggu <br> (Qty Obat)</th>
                                        <th>Minggu <br> (Rp Obat 1/2)</th>
                                        <th>Minggu <br> (Qty Obat 1/2)</th>
                                        <th>Minggu <br> (Rp Alat)</th>
                                        <th>Minggu <br> (Qty Alat)</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                        ?>
                                    @if($allSales != null)
                                        @foreach($allSales as $row)
                                        <?php 
                                            $no++;
                                            if($row->level_id == 8){
                                                $level = 'Assistant Manager';
                                            }
                                            if($row->level_id == 9){
                                                $level = 'Top Leader';
                                            }
                                            if($row->level_id == 10){
                                                $level = 'Leader';
                                            }
                                            if($row->level_id == 11){
                                                $level = 'Trainer';
                                            }
                                            if($row->level_id == 12){
                                                $level = 'Merchandiser';
                                            }
                                            if($row->level_id == 13){
                                                $level = 'Retrainer';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$level}}</td>
                                                <td>{{number_format($row->sale_senin_strip, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_senin_strip, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_senin_obat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_senin_obat, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_senin_obat_poin_sgth, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_senin_obat_poin_sgth/2, 2, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_senin_alat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_senin_alat, 1, '.', '.')}}</td>
                                                
                                                <td>{{number_format($row->sale_selasa_strip, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_selasa_strip, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_selasa_obat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_selasa_obat, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_selasa_obat_poin_sgth, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_selasa_obat_poin_sgth/2, 2, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_selasa_alat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_selasa_alat, 1, '.', '.')}}</td>
                                                
                                                <td>{{number_format($row->sale_rabu_strip, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_rabu_strip, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_rabu_obat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_rabu_obat, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_rabu_obat_poin_sgth, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_rabu_obat_poin_sgth/2, 2, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_rabu_alat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_rabu_alat, 1, '.', '.')}}</td>
                                                
                                                <td>{{number_format($row->sale_kamis_strip, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_kamis_strip, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_kamis_obat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_kamis_obat, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_kamis_obat_poin_sgth, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_kamis_obat_poin_sgth/2, 2, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_kamis_alat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_kamis_alat, 1, '.', '.')}}</td>
                                                
                                                <td>{{number_format($row->sale_jumat_strip, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_jumat_strip, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_jumat_obat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_jumat_obat, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_jumat_obat_poin_sgth, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_jumat_obat_poin_sgth/2, 2, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_jumat_alat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_jumat_alat, 1, '.', '.')}}</td>
                                                
                                                <td>{{number_format($row->sale_sabtu_strip, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_sabtu_strip, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_sabtu_obat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_sabtu_obat, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_sabtu_obat_poin_sgth, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_sabtu_obat_poin_sgth/2, 2, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_sabtu_alat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_sabtu_alat, 1, '.', '.')}}</td>
                                                
                                                <td>{{number_format($row->sale_minggu_strip, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_minggu_strip, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_minggu_obat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_minggu_obat, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_minggu_obat_poin_sgth, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_minggu_obat_poin_sgth/2, 2, '.', '.')}}</td>
                                                <td>{{number_format($row->sale_minggu_alat, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->qty_minggu_alat, 1, '.', '.')}}</td>
                                                
                                                <td>{{number_format($row->sale_price, 0, ',', ',')}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@if($download == true)
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
@endif
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export {{$day}}'
                    }
                ],
                searching: false
        } );
    } );
    
    $('.datepickerStart').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });

    $('.datepickerStart').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
    $('.datepickerEnd').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
</script>
<script type="text/javascript">
    
    function getTypeSearch(val) {
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/adm/search-manager" + "/" + val ,
            success: function(url){
                    $( "#typeSearch-list" ).empty();
                    $("#typeSearch-list").html(url);
            }
        });
    }
</script>
@stop
