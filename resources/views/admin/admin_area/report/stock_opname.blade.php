@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
          <div class="col-md-6">
            <form class="login100-form validate-form" method="post" action="/adm/report/stock-cabang">
                {{ csrf_field() }}
              <div class="card ">
                <div class="card-header ">
                  <h6 class="card-title">Filter Stock per Cabang</h6>
                </div>
                <div class="card-body ">
                  <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" name="cabang">
                            <option value="">- Cabang -</option>
                            @if($allCabang != null)
                                @foreach($allCabang as $row)
                                    <option value="{{$row->id}}" @if($cabang == $row->id) selected @endif>{{$row->cabang_name}}</option>
                                @endforeach
                            @endif
                    </select>
                    </div>
                </div>
                </div>
                <div class="card-footer text-right">
                  <button type="submit" class="btn btn-primary">Cari</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        @if($item != null)
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        @if($add == true)
                            @if($dataCabang != null)
                                <a class="btn btn-info btn-fill btn-sm"  href="{{ URL::to('/') }}/adm/add-stock/{{$dataCabang->id}}">Input Stock ({{$dataCabang->cabang_name}})</a>
                            @endif
                        @endif
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Barang</th>
                                        <th>Type</th>
                                        <th>Input</th>
                                        <th>Output</th>
                                        <th>Balance</th>
                                        @if($add == true)
                                            @if($cabangId != null)
                                                <th>###</th>
                                            @endif
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                    ?>
                                        @foreach($item as $row)
                                            <?php 
                                            $no++;
                                            $type = '--';
                                            if($row->type == 1){
                                                $type = 'Alat';
                                            }
                                            if($row->type == 2){
                                                $type = 'Strip';
                                            }
                                            if($row->type == 3){
                                                $type = 'Obat';
                                            }
                                            if($row->type == 10){
                                                $type = 'Lainnya';
                                            }
                                            $balance = $row->qty_input - $row->qty_output;
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->purchase_name}}</td>
                                                <td>{{$type}}</td>
                                                <td>{{number_format($row->qty_input, 1, '.', '')}}</td>
                                                <td>{{number_format($row->qty_output, 1, '.', '')}}</td>
                                                <td>{{number_format($balance, 1, '.', '')}}</td>
                                                @if($add == true)
                                                    @if($cabangId != null)
                                                        <td class="td-actions text-left" >
                                                            <div class="table-icons">
                                                                <a rel="tooltip" title="History Stock {{$row->purchase_name}}" class="text-primary" target="_blank" href="{{ URL::to('/') }}/adm/detail/stock/{{$row->id_item}}"><i class="nc-icon nc-tile-56"></i></a>
                                                                &nbsp;&nbsp;
                                                                <a rel="tooltip"  title="Edit Stock {{$row->purchase_name}}" data-toggle="modal" data-target="#editStockModal" class="text-primary" href="{{ URL::to('/') }}/adm/edit/stock/{{$row->id_item}}/{{$dataSunday->thisSunday}}"><i class="nc-icon nc-settings-gear-65"></i></a>
                                                                &nbsp;&nbsp;
                                                                <a rel="tooltip"  title="Remove Stock {{$row->purchase_name}}" data-toggle="modal" data-target="#rmStockModal" class="text-danger" href="{{ URL::to('/') }}/adm/rm/stock/{{$row->id_item}}/{{$dataSunday->thisSunday}}"><i class="nc-icon nc-simple-remove"></i></a>
                                                            </div>
                                                        </td>
                                                    @endif
                                                @endif
                                            </tr>
                                        @endforeach
                                    
                                </tbody>
                            </table>
                            <div class="modal fade" id="editStockModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                            <div class="modal fade" id="rmStockModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        
        @if($itemLast != null)
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle2}}</h5>
                        @if($add == true)
                            @if($dataCabang != null)
                                <a class="btn btn-info btn-fill btn-sm"  href="{{ URL::to('/') }}/adm/add-stock/{{$dataCabang->id}}/{{$dataSunday->lastSunday}}">Input Stock ({{$dataCabang->cabang_name}})</a>
                            @endif
                        @endif
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTableLast">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Barang</th>
                                        <th>Type</th>
                                        <th>Input</th>
                                        <th>Output</th>
                                        <th>Balance</th>
                                        @if($add == true)
                                            @if($cabangId != null)
                                                <th>###</th>
                                            @endif
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                    ?>
                                    
                                        @foreach($itemLast as $row)
                                            <?php 
                                            $no++;
                                            $type = '--';
                                            if($row->type == 1){
                                                $type = 'Alat';
                                            }
                                            if($row->type == 2){
                                                $type = 'Strip';
                                            }
                                            if($row->type == 3){
                                                $type = 'Obat';
                                            }
                                            if($row->type == 10){
                                                $type = 'Lainnya';
                                            }
                                            $balance = $row->qty_input  - $row->qty_output;
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->purchase_name}}</td>
                                                <td>{{$type}}</td>
                                                <td>{{number_format($row->qty_input, 1, '.', '')}}</td>
                                                <td>{{number_format($row->qty_output, 1, '.', '')}}</td>
                                                <td>{{number_format($balance, 1, '.', '')}}</td>
                                                @if($add == true)
                                                    @if($cabangId != null)
                                                        <td class="td-actions text-left" >
                                                            <div class="table-icons">
                                                                <a rel="tooltip" title="History Stock {{$row->purchase_name}}" class="text-primary" target="_blank" href="{{ URL::to('/') }}/adm/detail/stock/{{$row->id_item}}"><i class="nc-icon nc-tile-56"></i></a>
                                                                &nbsp;&nbsp;
                                                                <a rel="tooltip"  title="Edit Stock {{$row->purchase_name}}" data-toggle="modal" data-target="#editStockModalOld" class="text-primary" href="{{ URL::to('/') }}/adm/edit/stock/{{$row->id_item}}/{{$dataSunday->lastSunday}}"><i class="nc-icon nc-settings-gear-65"></i></a>
                                                                &nbsp;&nbsp;
                                                                <a rel="tooltip"  title="Remove Stock {{$row->purchase_name}}" data-toggle="modal" data-target="#rmStockModalOld" class="text-danger" href="{{ URL::to('/') }}/adm/rm/stock/{{$row->id_item}}/{{$dataSunday->lastSunday}}"><i class="nc-icon nc-simple-remove"></i></a>
                                                            </div>
                                                        </td>
                                                    @endif
                                                @endif
                                            </tr>
                                        @endforeach
                                    
                                </tbody>
                            </table>
                            <div class="modal fade" id="editStockModalOld" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                            <div class="modal fade" id="rmStockModalOld" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@stop

@if($item != null)
    @section('admin-styles')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
    @stop

    @section('admin-javascript')
    <script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    @if($download == true)
    <script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
    @endif
    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'export {{$headerTitle}}'
                        }
                    ],
                    searching: false,
                    columnDefs: [
                        { orderable: false, targets: -1 }
                     ],
                     pageLength: 10
            } );
        } );
        
        $(document).ready(function() {
            $('#myTableLast').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'export {{$headerTitle2}}'
                        }
                    ],
                    searching: false,
                    columnDefs: [
                        { orderable: false, targets: -1 }
                     ],
                     pageLength: 10
            } );
        } );

        $("#editStockModal").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find(".modal-content").load(link.attr("href"));
        });
        $("#editStockModalOld").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find(".modal-content").load(link.attr("href"));
        });
        $("#rmStockModal").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find(".modal-content").load(link.attr("href"));
        });
        $("#rmStockModalOld").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find(".modal-content").load(link.attr("href"));
        });
    </script>
    @stop
@endif