@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <?php
        $typeSearch = 2;
        if($type == 'asmen'){
            $typeSearch = 3;
        }
        if($type == 'tld'){
            $typeSearch = 4;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">Filter Product Sales {{$type}}</h5>
                    </div>
                    <div class="card-body" style="min-height: auto;">
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <form class="login100-form validate-form" method="post" action="/adm/product-sales/{{$type}}">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Tanggal Awal</label>
                                            <input type="text" class="form-control datepickerStart" name="start_date" @if($tgl->startDay != null ) value="{{$tgl->startDay}}" @endif>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Tanggal Akhir</label>
                                            <input type="text" class="form-control datepickerEnd" name="end_date" @if($tgl->endDay != null ) value="{{$tgl->endDay}}" @endif>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Cabang</label>
                                            <select class="form-control" name="cabang" onChange="getTypeSearch(this.value);">
                                                <option value="0">- Pilih Cabang -</option>
                                                @if($allCabang != null)
                                                    @foreach($allCabang as $row)
                                                        <option value="{{$row->id}}" @if($dataNama !=null) @if($dataNama->cabang_id == $row->id) selected="" @endif @endif>{{$row->cabang_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5" id="typeSearch-list">
                                        @if($dataNama !=null)
                                            @if($typeSearch == 2)
                                                @if(count($manager) > 0)
                                                    <div class="form-group">
                                                        <label>Pilih Nama</label>
                                                        <select name="manager_id" class="form-control" title="Pilih Manager" onChange="getTypeSearchDataUser(this.value);">
                                                                <option value="">- Pilih Manager -</option>
                                                                @foreach($manager as $row)
                                                                    @if($row->level_id == 7)
                                                                        <option value="{{$row->id}}" @if($dataNama->id == $row->id) selected="" @endif>(Manager) {{$row->name}}</option>
                                                                    @endif
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                @endif
                                            @endif

                                            @if($typeSearch == 3)
                                                @if(count($manager) > 0)
                                                    <div class="form-group">
                                                        <label>Pilih Nama</label>
                                                        <select name="manager_id" class="form-control" title="Pilih Asmen" onChange="getTypeSearchDataUser(this.value);">
                                                                <option value="">- Pilih Asmen -</option>
                                                                @foreach($manager as $row)
                                                                    @if($row->level_id == 8)
                                                                        <option value="{{$row->id}}" @if($dataNama->id == $row->id) selected="" @endif>(Asmen) {{$row->name}}</option>
                                                                    @endif
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                @endif
                                            @endif

                                            @if($typeSearch == 4)
                                                @if(count($manager) > 0)
                                                    <div class="form-group">
                                                        <label>Pilih Nama</label>
                                                        <select name="manager_id" class="form-control" title="Pilih TLD" onChange="getTypeSearchDataUser(this.value);">
                                                                <option value="">- Pilih Top Leader -</option>
                                                                @foreach($manager as $row)
                                                                    @if($row->level_id == 9)
                                                                        <option value="{{$row->id}}" @if($dataNama->id == $row->id) selected="" @endif>(TLD) {{$row->name}}</option>
                                                                    @endif
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                @endif
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="update ml-auto mr-auto">
                                        <button type="submit" class="btn btn-primary btn-round">Submit</button>
                                    </div>
                                </div>
                            </div>    
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @if($getData != null)
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        <p class="title">{{$day}}</p>
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Type</th>
                                        <th>Nama Produk</th>
                                        <th>Jml Botol</th>
                                        <th>Jml Poin</th>
                                        <th>Total Bonus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                            @if($row->total > 0)
                                                <?php 
                                                    $no++;
                                                    if($row->type == 1){
                                                        $type = 'Alat';
                                                        if($dataNama->level_id == 7){
                                                            $bonus = $row->total * $bonusSetting->m_alat;
                                                        }
                                                        if($dataNama->level_id == 8){
                                                            $bonus = $row->total * 0;
                                                        }
                                                        if($dataNama->level_id == 9){
                                                            $bonus = $row->total * 0;
                                                        }
                                                        $total = $row->total;
                                                    }
                                                    if($row->type == 2){
                                                        $type = 'Strip';
                                                        if($dataNama->level_id == 7){
                                                            $bonus = $row->total * $bonusSetting->m_strip;
                                                        }
                                                        if($dataNama->level_id == 8){
                                                            $bonus = $row->total * $bonusSetting->am_strip;
                                                        }
                                                        if($dataNama->level_id == 9){
                                                            $bonus = $row->total * $bonusSetting->tl_strip;
                                                        }
                                                        $total = $row->total;
                                                    }
                                                    if($row->type == 3){
                                                        $type = 'Obat';
                                                        $total = $row->total;
                                                        if($row->is_poin == 1){
                                                            $total = $row->total/2;
                                                        }
                                                        if($dataNama->level_id == 7){
                                                            $bonus = $row->total * ($bonusSetting->m_obat + $dataNama->extra_wilayah);
                                                            if($row->is_poin == 1){
                                                                $bonus = $row->total * ($bonusSetting->m_obat + $dataNama->extra_wilayah)/2;
                                                            }
                                                        }
                                                        if($dataNama->level_id == 8){
                                                            $bonus = $row->total * ($bonusSetting->am_obat + $dataNama->extra_wilayah);
                                                            if($row->is_poin == 1){
                                                                $bonus = $row->total  * ($bonusSetting->am_obat + $dataNama->extra_wilayah)/2;
                                                            }
                                                        }
                                                        if($dataNama->level_id == 9){
                                                            $bonus = $row->total * ($bonusSetting->tl_obat + $dataNama->extra_wilayah);
                                                            if($row->is_poin == 1){
                                                                $bonus = $row->total  * ($bonusSetting->tl_obat + $dataNama->extra_wilayah)/2;
                                                            }
                                                        }
                                                    }
                                                ?>
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>{{$type}}</td>
                                                    <td>{{$row->purchase_name}}</td>
                                                    <td>{{number_format($row->total, 1)}}</td>
                                                    <td>{{number_format($total, 1)}}</td>
                                                    <td>{{number_format($bonus, 0)}}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@if($download == true)
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
@endif
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export {{$headerTitle}}'
                    }
                ],
                searching: false,
                "order": [[ 2, "asc" ]],
                 pagingType: "full_numbers"
        } );
    } );
    $('.datepickerStart').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
    $('.datepickerEnd').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
</script>
<script type="text/javascript">
    
    function getTypeSearch(val) {
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/adm/ajax/new/search-manager" + "/" + val + "/{{$typeSearch}}" ,
            success: function(url){
                    $( "#typeSearch-list" ).empty();
                    $("#typeSearch-list").html(url);
            }
        });
    }
</script>
@stop
