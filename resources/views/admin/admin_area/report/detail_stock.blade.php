@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
        ?>
    <div class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="image">
                    </div>
                    <div class="card-body"style="min-height: auto;">
                        <div class="author">
                            <h5 class="title">{{$purchase->purchase_name}}</h5>
                            <?php
                                $type = 'Obat';
                                if($purchase->type == 1){
                                    $type = 'Alat';
                                }
                                if($purchase->type == 2){
                                    $type = 'Strip';
                                }
                                ?>
                            <p class="description">
                                {{$type}}
                            </p>
                        </div>
                        <h5 class="description text-center">
                            Cabang {{$purchase->cabang_name}}
                        </h5>
                    </div>
                    <div class="card-footer">
                        <hr>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">History Stock Input</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Jumlah</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                        ?>
                                    @if($item != null)
                                    @foreach($item as $row)
                                    <?php 
                                        $no++;
                                        $level = 'General Manager';
                                        if($row->level_id == 3){
                                            $level = 'Senior Executive Manager';
                                        }
                                        if($row->level_id == 4){
                                            $level = 'Executive Manager';
                                        }
                                        if($row->level_id == 5){
                                            $level = 'Senior Manager';
                                        }
                                        if($row->level_id == 6){
                                            $level = 'MQB';
                                        }
                                        if($row->level_id == 7){
                                            $level = 'Manager';
                                        }
                                        if($row->level_id == 8){
                                            $level = 'Asmen';
                                        }
                                        if($row->level_id == 9){
                                            $level = 'Top Leader';
                                        }
                                        if($row->level_id == 10){
                                            $level = 'Leader';
                                        }
                                        if($row->level_id == 11){
                                            $level = 'Trainer';
                                        }
                                        if($row->level_id == 12){
                                            $level = 'Merchandiser';
                                        }
                                        ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{$level}}</td>
                                        <td>{{number_format($row->amount, 1, '.', '.')}}</td>
                                        <td><?php  echo (date('d-m-Y', strtotime($row->created_at))) ?></td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">History Stock Output</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="myTable2">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Jumlah</th>
                                        <!--<th>Tanggal Stock</th>-->
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                        ?>
                                    @if($itemOut != null)
                                    @foreach($itemOut as $row)
                                    <?php 
                                        $balance = $row->out_put - $row->out_put_delete;
                                    ?> 
                                    @if($balance > 0)
                                    <?php
                                        $no++;
                                        $level = 'General Manager';
                                        if($row->level_id == 3){
                                            $level = 'Senior Executive Manager';
                                        }
                                        if($row->level_id == 4){
                                            $level = 'Executive Manager';
                                        }
                                        if($row->level_id == 5){
                                            $level = 'Senior Manager';
                                        }
                                        if($row->level_id == 6){
                                            $level = 'MQB';
                                        }
                                        if($row->level_id == 7){
                                            $level = 'Manager';
                                        }
                                        if($row->level_id == 8){
                                            $level = 'Asmen';
                                        }
                                        if($row->level_id == 9){
                                            $level = 'Top Leader';
                                        }
                                        if($row->level_id == 10){
                                            $level = 'Leader';
                                        }
                                        if($row->level_id == 11){
                                            $level = 'Trainer';
                                        }
                                        if($row->level_id == 12){
                                            $level = 'Merchandiser';
                                        }
                                        ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{$level}}</td>
                                        <td>{{number_format($row->out_put, 1, '.', '.')}}</td>
                                        <!--<td><?php  echo (date('d-m-Y', strtotime($row->created_at))) ?></td>-->
                                        <td><?php  echo (date('d-m-Y', strtotime($row->sale_date))) ?></td>
                                    </tr>
                                    @endif
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop
@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                searching: false,
                pageLength: 4,
        } );
    } );
    
    $(document).ready(function() {
        $('#myTable2').DataTable( {
                dom: 'Bfrtip',
                searching: false,
                pageLength: 10,
        } );
    } );
</script>
@stop