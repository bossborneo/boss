@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">List Absen Bulan Dec</h5>
                    </div>
                    <div class="card-body" style="min-height: auto;">
                        <form class="login100-form validate-form" method="post" action="/adm/absen">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Month Date</label>
                                            <input type="text" class="form-control datepickerStart" name="start_date" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Cabang</label>
                                            <select class="form-control" name="cabang" onChange="getTypeSearch(this.value);">
                                                <option value="">- Pilih Cabang -</option>
                                                @if($allCabang != null)
                                                    @foreach($allCabang as $row)
                                                        <option value="{{$row->id}}">{{$row->cabang_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5" id="typeSearch-list"></div>    
                                </div>
                                <div class="row">
                                    <div class="update ml-auto mr-auto">
                                        <button type="submit" class="btn btn-primary">Cari</button>
                                    </div>
                                </div>
                            </div>    
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Jml Masuk</th>
                                        <th>Jml Sakit</th>
                                        <th>Jml Izin</th>
                                        <th>Jml Alpa</th>
                                        <th class="td-actions text-left">##</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                    @if($getData != null)
                                        @foreach($getData as $rows)
                                            <?php
                                                $no++;
                                                $level = '';
                                                if($rows->level_id == 2){
                                                    $level = 'General Manager';
                                                }
                                                if($rows->level_id == 3){
                                                    $level = 'Senior Executive Manager';
                                                }
                                                if($rows->level_id == 4){
                                                    $level = 'Executive Manager';
                                                }
                                                if($rows->level_id == 5){
                                                    $level = 'Senior Manager';
                                                }
                                                if($rows->level_id == 6){
                                                    $level = 'MQB';
                                                }
                                                if($rows->level_id == 7){
                                                    $level = 'Manager';
                                                }
                                                if($rows->level_id == 8){
                                                    $level = 'Asisten Manager';
                                                }
                                                if($rows->level_id == 9){
                                                    $level = 'Top Leader';
                                                }
                                                if($rows->level_id == 10){
                                                    $level = 'Leader';
                                                }
                                                if($rows->level_id == 11){
                                                    $level = 'Trainer';
                                                }
                                                if($rows->level_id == 12){
                                                    $level = 'Merchandiser';
                                                }
                                                if($rows->level_id == 13){
                                                    $level = 'Retrainer';
                                                }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$rows->name}}</td>
                                                <td>{{$level}}</td>
                                                <td>{{$rows->jml_masuk}}</td>
                                                <td>{{$rows->jml_sakit}}</td>
                                                <td>{{$rows->jml_izin}}</td>
                                                <td>{{$rows->jml_alpa}}</td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip"  class="text-primary" href="{{ URL::to('/') }}/adm/absen-detail/{{$rows->user_id}}"><i class="nc-icon nc-badge"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@if($download == true)
    <script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
@endif
<script type="text/javascript">
    $('.datepickerStart').datetimepicker({
        format: 'YYYY-MM',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
</script>
<script type="text/javascript">
    
    function getTypeSearch(val) {
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/adm/search-manager" + "/" + val ,
            success: function(url){
                    $( "#typeSearch-list" ).empty();
                    $("#typeSearch-list").html(url);
            }
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'export Absen List {{$dataManager->name}}'
                        }
                    ],
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: -1 },
                 ]
        } );
    } );
</script>
@stop