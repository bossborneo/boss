@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">{{$headerTitle}}</h5>
                    </div>
                    <div class="card-body" style="min-height: auto;">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <form class="login100-form validate-form" method="post" action="/adm/top-cabang">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Cabang</label>
                                            <select class="form-control" name="cabang">
                                                <option value="">- Pilih Cabang -</option>
                                                @if($allCabang != null)
                                                    @foreach($allCabang as $row)
                                                        <option value="{{$row->id}}">{{$row->cabang_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                             <button type="submit" class="form-control btn btn-sm btn-info " style="margin: 0;">Cari</button>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @if($dataTop != null)
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> Top 10 Sales Cabang {{$dataTop[0]->cabang_name}} (Last 7 Days)</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Penjualan (Rp.)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                    @foreach($dataTop as $row)
                                        <?php 
                                        $no++; 
                                        $level = 'General Manager';
                                        if($row->level_id == 3){
                                            $level = 'Senior Executive Manager';
                                        }
                                        if($row->level_id == 4){
                                            $level = 'Executive Manager';
                                        }
                                        if($row->level_id == 5){
                                            $level = 'Senior Manager';
                                        }
                                        if($row->level_id == 6){
                                            $level = 'MQB';
                                        }
                                        if($row->level_id == 7){
                                            $level = 'Manager';
                                        }
                                        if($row->level_id == 8){
                                            $level = 'Asmen';
                                        }
                                        if($row->level_id == 9){
                                            $level = 'Top Leader';
                                        }
                                        if($row->level_id == 10){
                                            $level = 'Leader';
                                        }
                                        if($row->level_id == 11){
                                            $level = 'Trainer';
                                        }
                                        if($row->level_id == 12){
                                            $level = 'Merchandiser';
                                        }
                                        ?>
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{$row->name}}</td>
                                            <td>{{$level}}</td>
                                            <td>{{number_format($row->jml_price, 0, ',', '.')}}</td>
                                        </tr>
                                    @endforeach    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@stop
