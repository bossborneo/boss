@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">Filter Penjualan</h5>
                    </div>
                    <div class="card-body" style="min-height: auto;">
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <form class="login100-form validate-form" method="get" action="/adm/data-sales">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Tanggal Awal</label>
                                            <input type="text" class="form-control datepickerStart" name="start_date" value="{{$getDate->startDay}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Tanggal Akhir</label>
                                            <input type="text" class="form-control datepickerEnd" name="end_date" value="{{$getDate->endDay}}">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Cabang</label>
                                            <select class="form-control" name="cabang">
                                                <option value="">- Pilih Cabang -</option>
                                                @if($allCabang != null)
                                                    @foreach($allCabang as $row)
                                                        <option value="{{$row->id}}" @if($getCabang != null) @if($getCabang == $row->id) selected="" @endif @endif>{{$row->cabang_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                             <button type="submit" class="form-control btn btn-sm btn-info " title="cari" style="margin: 0;">Cari</button>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @if($allSales != null)
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> 
                            {{$headerTitle}} 
                        </h5>
                    </div>
                    <div class="card-body">
                        <p class="title">{{$day}}</p>
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Cabang</th>
                                        <th>Total Harga (Rp.)</th>
                                        <th>Tgl. Sale</th>
                                        @if($add == true)
                                            <th>###</th>
                                        @endif
                                        @if($dataUser->id == 2220)
                                            <th>###</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                        ?>
                                        @foreach($allSales as $row)
                                        <?php 
                                            $no++;
                                            $level = 'General Manager';
                                            if($row->level_id == 3){
                                                $level = 'Senior Executive Manager';
                                            }
                                            if($row->level_id == 4){
                                                $level = 'Executive Manager';
                                            }
                                            if($row->level_id == 5){
                                                $level = 'Senior Manager';
                                            }
                                            if($row->level_id == 6){
                                                $level = 'MQB';
                                            }
                                            if($row->level_id == 7){
                                                $level = 'Manager';
                                            }
                                            if($row->level_id == 8){
                                                $level = 'Assistant Manager';
                                            }
                                            if($row->level_id == 9){
                                                $level = 'Top Leader';
                                            }
                                            if($row->level_id == 10){
                                                $level = 'Leader';
                                            }
                                            if($row->level_id == 11){
                                                $level = 'Trainer';
                                            }
                                            if($row->level_id == 12){
                                                $level = 'Merchandiser';
                                            }
                                            if($row->level_id == 13){
                                                $level = 'Retrainer';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$level}}</td>
                                                <td>{{$row->cabang_name}}</td>
                                                <td>{{number_format($row->sale_price, 0, ',', ',')}}</td>
                                                <td><?php  echo (date('d-m-Y', strtotime($row->sale_date))) ?></td>
                                                @if($add == true)
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a data-toggle="modal" class="text-primary" data-target="#viewSales" href="{{ URL::to('/') }}/adm/detail-view-sales/{{$row->invoice}}/{{$row->user_id}}"><i class="nc-icon nc-bullet-list-67"></i></a>
                                                        &nbsp;&nbsp;
                                                        <a class="text-primary" href="{{ URL::to('/') }}/adm/view-sales/{{$row->invoice}}/{{$row->user_id}}"><i class="nc-icon nc-single-copy-04"></i></a>
                                                    </div>
                                                </td>
                                                @endif
                                                @if($dataUser->id == 2220)
                                                    <td class="td-actions text-left" >
                                                        <div class="table-icons">
                                                            <a data-toggle="modal" class="text-primary" data-target="#viewSales" href="{{ URL::to('/') }}/adm/detail-view-sales/{{$row->invoice}}/{{$row->user_id}}"><i class="nc-icon nc-bullet-list-67"></i></a>
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                </tbody>
                            </table>
                            <div class="modal fade" id="viewSales" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document" style="max-width:850px;">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@if($download == true)
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
@endif
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
    $("#rmSales").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export {{$headerTitle}}'
                    }
                ],
                searching: false,
                columnDefs: [
                    { orderable: false, targets: -1 }
                 ],
                 pagingType: "full_numbers"
        } );
    } );
    $('.datepickerStart').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
    $('.datepickerEnd').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
</script>
<script type="text/javascript">
    $("#viewSales").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop
