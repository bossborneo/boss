@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">Tambah Anggota</h5>
                    </div>
                    <div class="card-body" style="min-height: auto;">
                        <form class="login100-form validate-form" method="post" action="/adm/create/down-structure">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Sponsor Struktur</label>
                                            <select class="form-control" name="sponsor_structure_id">
                                                @if($getSponsor != null)
                                                    @foreach($getSponsor as $row)
                                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Cabang</label>
                                            <select class="form-control" name="cabang" onChange="getTypeSearch(this.value);">
                                                <option value="">- Pilih Cabang -</option>
                                                @if($allCabang != null)
                                                    @foreach($allCabang as $row)
                                                        <option value="{{$row->id}}">{{$row->cabang_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4" id="typeSearch-list"></div>    
                                </div>
                                <div class="row">
                                    <div class="update ml-auto mr-auto">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </div>    
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card card-user">
                    <div class="image" style="background-color: #ccc;">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <img class="avatar border-gray" src="/images/default-avatar.png" alt="...">
                                <h5 class="title">{{$getDataTop->name}}</h5>
                            </a>
                        </div>
                        <h5 class="description text-center">{{$getDataTop->cabang_name}}</h5>
                    </div>
                    <div class="card-footer">
                        <hr>
                        <div class="button-container">
                            <div class="row">
                                <div class="col-md-12">
                                    <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/tree/top-structure/{{$getDataTop->id}}">Tree Struktur</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> Anggota</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Cabang</th>
                                        <th>###</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                    <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                            <?php $no++; ?>
                                            <?php
                                                $level = 'GM';
                                                if($row->level_id == 3){
                                                    $level = 'SEM';
                                                }
                                                if($row->level_id == 4){
                                                    $level = 'EM';
                                                }
                                                if($row->level_id == 5){
                                                    $level = 'SM';
                                                }
                                                if($row->level_id == 6){
                                                    $level = 'MQB';
                                                }
                                                if($row->level_id == 7){
                                                    $level = 'Manager';
                                                }
                                                if($row->level_id == 8){
                                                    $level = 'Asisten Manager';
                                                }
                                                if($row->level_id == 9){
                                                    $level = 'Top Leader';
                                                }
                                                if($row->level_id == 10){
                                                    $level = 'Leader';
                                                }
                                                if($row->level_id == 11){
                                                    $level = 'Trainer';
                                                }
                                                if($row->level_id == 12){
                                                    $level = 'Merchandiser';
                                                }
                                                if($row->level_id == 13){
                                                    $level = 'Retrainer';
                                                }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$level}}</td>
                                                <td>{{$row->cabang_name}}</td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip" title="Hapus" data-toggle="modal" data-target="#rmThis" class="text-danger" href="{{ URL::to('/') }}/adm/ajax/rm/down-structure/{{$row->id}}"><i class="nc-icon nc-simple-remove"></i></a>
                                                    </div>
                                                </td>
                                            </tr>    
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="rmThis" tabindex="-1" role="dialog" aria-labelledby="crewModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-javascript')
<script type="text/javascript">
    
    function getTypeSearch(val) {
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/adm/search-down-structure" + "/1/" + val ,
            success: function(url){
                    $( "#typeSearch-list" ).empty();
                    $("#typeSearch-list").html(url);
            }
        });
    }
</script>
<script type="text/javascript">
    $("#rmThis").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop