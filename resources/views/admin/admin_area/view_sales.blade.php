@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $level = 'General Manager';
        if($member->level_id == 3){
            $level = 'Senior Executive Manager';
        }
        if($member->level_id == 4){
            $level = 'Executive Manager';
        }
        if($member->level_id == 5){
            $level = 'Senior Manager';
        }
        if($member->level_id == 6){
            $level = 'MQB';
        }
        if($member->level_id == 7){
            $level = 'Manager';
        }
        if($member->level_id == 8){
            $level = 'Assistant Manager';
        }
        if($member->level_id == 9){
            $level = 'Top Leader';
        }
        if($member->level_id == 10){
            $level = 'Leader';
        }
        if($member->level_id == 11){
            $level = 'Trainer';
        }
        if($member->level_id == 12){
            $level = 'Merchandiser';
        }
        if($member->level_id == 13){
            $level = 'Retrainer';
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> 
                            {{$headerTitle}} 
                        </h5>
                    </div>
                    
                    <div class="card-body">
                        <p class="title">{{$member->name}} ({{$level}})</p>
                        <p class="title">{{date('d-M-Y', strtotime($allSales[0]->sale_date))}}</p>
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="mytable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Barang</th>
                                        <th>Type</th>
                                        <th>Jumlah</th>
                                        <th>Harga (Rp.)</th>
                                        <th class="td-actions text-left">##</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                        ?>
                                    @if($allSales != null)
                                        @foreach($allSales as $row)
                                        <?php 
                                            $no++;
                                            $type = '--';
                                            if($row->type == 1){
                                                $type = 'Alat';
                                            }
                                            if($row->type == 2){
                                                $type = 'Strip';
                                            }
                                            if($row->type == 3){
                                                $type = 'Obat';
                                            }
                                            if($row->type == 10){
                                                $type = 'Lainnya';
                                            }
                                            
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->purchase_name}}</td>
                                                <td>{{$type}}</td>
                                                <td>{{number_format($row->amount, 1, ',', '.')}}</td>
                                                <td>{{number_format($row->sale_price, 0, ',', '.')}}</td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a data-toggle="modal" data-target="#editSales"  class="text-primary" href="{{ URL::to('/') }}/adm/edit-sale/{{$row->id_sales}}/{{$row->user_id}}"><i class="nc-icon nc-single-copy-04"></i></a>
                                                        &nbsp;&nbsp;
                                                        <a data-toggle="modal" data-target="#rmSales" class="text-danger" href="{{ URL::to('/') }}/adm/rm-sale/{{$row->id_sales}}/{{$row->user_id}}"><i class="nc-icon nc-simple-remove"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="rmSales" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                            <div class="modal fade" id="editSales" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-javascript')
<script type="text/javascript">
    $("#rmSales").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $("#editSales").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop