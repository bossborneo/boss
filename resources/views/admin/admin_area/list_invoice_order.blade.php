@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $dateCode = date("ymd", strtotime($dataOrder->order_at));
        $code = sprintf("%05s", $dataOrder->id);
        $newInvoiceNumber =  'Order'.$dateCode.$code;
    ?>
    <div class="content">
        
        <div class="row" id="invoice">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body" >
                        <div class="row">
			<div class="col-xs-6">
			</div>
			<div class="col-xs-6 text-right">
			  <h3>Invoice {{$newInvoiceNumber}}</h3>
			</div>
		</div>
                        <div class="row">
		    <div class="col-xs-5">
		      <div class="panel panel-default">
		              <div class="panel-heading">
                                  <h5 style="margin: 5px 0;">{{$dataOrder->name}}</h5>
		              </div>
		              <div class="panel-body">
		                <h6>
		                  {{$dataOrder->cabang_name}}
		                </h6>
		              </div>
		            </div>
		    </div>
		    <div class="col-xs-5 col-xs-offset-2 text-right">
		      <p><strong>Tanggal Order: </strong>{{date('d F Y', strtotime($dataOrder->order_at))}}</p>
		    </div>
		  </div>
                        <table class="table table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Barang</th>
            <th>Type</th>
            <th>Jumlah</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 0; ?>
                                    @if($getData != null)
                                        @foreach($getData as $rows)
                                            <?php
                                                $no++;
                                                $type = 'Obat';
                                                if($rows->type == 1){
                                                    $type = 'Alat';
                                                }
                                                if($rows->type == 2){
                                                    $type = 'Strip';
                                                }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$rows->purchase_name}}</td>
                                                <td>{{$type}}</td>
                                                @if($dataOrder->status == 1)
                                                    <td>--</td>
                                                @else
                                                    <td>{{number_format($rows->qty, 1, '.', '.')}}</td>
                                                @endif
                                                
                                                <td class="td-actions text-left">
                                                    <div class="table-icons">
                                                        @if($dataOrder->status == 1)
                                                            @if($rows->item_terima_at == null)
                                                                <p class="btn btn-warning btn-sm">proses admin</p>
                                                            @endif
                                                        @else
                                                            @if($rows->item_terima_at == null)
                                                                <p class="btn btn-warning btn-sm">proses confirm</p>
                                                            @endif
                                                            @if($rows->item_terima_at != null)
                                                                <p class="btn btn-success btn-sm">terima</p>
                                                            @endif
                                                        @endif
                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
        </tbody>
      </table>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-3">
                <div class="card card-stats">
                    <div class="card-body ">
                        <input id="btn-Preview-Image"  class="btn btn-info btn-sm" type="button"   value="Preview" />  
                        <a id="btn-Convert-Html2Image" class="btn btn-warning btn-sm"  href="#"> Download  </a> 
                        (Jika download preview dulu)
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            
    <div id="previewImage"></div> 
    </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/bootstrap_invoice.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/html2canvas.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                "pageLength": 50
        } );
    } );
    
    $("#editStock").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $("#rmStock").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
<script> 
        $(document).ready(function() { 
            var element = $("#invoice");  
            var getCanvas;  
            $("#btn-Preview-Image").on('click', function() { 
                html2canvas(element, { 
                    onrendered: function(canvas) { 
                        $("#previewImage").append(canvas); 
                        getCanvas = canvas; 
                    } 
                }); 
            }); 
  
            $("#btn-Convert-Html2Image").on('click', function() { 
                var imgageData =  
                    getCanvas.toDataURL("image/png"); 
                var newData = imgageData.replace( 
                /^data:image\/png/, "data:application/octet-stream"); 
              
                $("#btn-Convert-Html2Image").attr( 
                "download", "invoice.png").attr( 
                "href", newData); 
            }); 
        }); 
    </script>
@stop