@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table nowrap" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Cabang</th>
                                        <th>Tgl</th>
                                        <th>Jml. Paket</th>
                                        <th>Jml. Harga</th>
                                        <th>Status</th>
                                        <th class="td-actions text-left">##</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                    ?>
                                    @if($getData != null)
                                        @foreach($getData as $row)
                                            <?php 
                                            $no++;
                                            $status = 'proses transfer';
                                            $btn = 'info';
                                            if($row->safra_status == 1){
                                                $status = 'transfer';
                                                $btn = 'success';
                                            }
                                            if($row->safra_status == 2){
                                                $status = 'tuntas';
                                                $btn = 'primary';
                                            }
                                            if($row->safra_status == 3){
                                                $status = 'reject';
                                                $btn = 'danger';
                                            }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->cabang_name}}</td>
                                                <td><?php  echo (date('d-m-Y', strtotime($row->sale_date))) ?></td>
                                                <td>{{number_format($row->amount, 0, ',', '.')}}</td>
                                                <td>{{number_format($row->sale_price, 0, ',', '.')}}</td>
                                                <td>{{$status}}</td>
                                               
                                                <td class="td-actions text-left" >
                                                     @if($row->safra_status == 0)
                                                    <a rel="tooltip" title="Hapus" data-toggle="modal" data-target="#confirmThis" class="text-success" href="{{ URL::to('/') }}/adm/ajax/safra/1/{{$row->id}}">confirm</a>
                                                    &nbsp;&nbsp;
                                                    <a rel="tooltip" title="Hapus" data-toggle="modal" data-target="#rmThis" class="text-danger" href="{{ URL::to('/') }}/adm/ajax/safra/2/{{$row->id}}">reject</a>
                                                    @endif
                                                    @if($row->safra_status != 0)
                                                    <a rel="tooltip" title="Hapus" data-toggle="modal" data-target="#detailThis" class="text-success" href="{{ URL::to('/') }}/adm/ajax/detail-safra/{{$row->id}}">detail</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="confirmThis" tabindex="-1" role="dialog" aria-labelledby="crewModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                            <div class="modal fade" id="rmThis" tabindex="-1" role="dialog" aria-labelledby="crewModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                            <div class="modal fade" id="detailThis" tabindex="-1" role="dialog" aria-labelledby="crewModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export-sales-safra'
                    }
                ],
                searching: false,
                columnDefs: [
                    { orderable: false, targets: -1 }
                 ],
                 pagingType: "full_numbers"
        } );
    } );
</script>
<script type="text/javascript">
    $("#confirmThis").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });

    $("#rmThis").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    
    $("#detailThis").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop