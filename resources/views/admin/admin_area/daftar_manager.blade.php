@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
          <div class="col-md-6">
            <form class="login100-form validate-form" method="get" action="/view-managers">
                {{ csrf_field() }}
              <div class="card ">
                <div class="card-header ">
                  <h6 class="card-title">Filter per Cabang</h6>
                </div>
                <div class="card-body ">
                  <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" name="cabang">
                            <option value="">- Cabang -</option>
                            @if($allCabang != null)
                                @foreach($allCabang as $row)
                                    <option value="{{$row->id}}">{{$row->cabang_name}}</option>
                                @endforeach
                            @endif
                    </select>
                    </div>
                </div>
                </div>
                <div class="card-footer text-right">
                  <button type="submit" class="btn btn-primary">Cari</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        @if($add == true)
                            <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/add-manager">Tambah Manager</a>
                        @endif
                        @if($dataUser->user_type == 1 || $dataUser->user_type == 2)
                            <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/pindah-crew">Pindah Struktur</a>
                            <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/view-crew-cabang">Data Crew</a>
                        @endif
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No.</th>
                                        <th>Username</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Cabang</th>
                                        @if($add == true)
                                            <th class="td-actions text-left">##</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($allManager != null)
                                    <?php $no = 0; ?>
                                        @foreach($allManager as $rowManager)
                                        <?php
                                            $no++;
                                            $level = 'General Manager';
                                            if($rowManager->level_id == 3){
                                                $level = 'Senior Executive Manager';
                                            }
                                            if($rowManager->level_id == 4){
                                                $level = 'Executive Manager';
                                            }
                                            if($rowManager->level_id == 5){
                                                $level = 'Senior Manager';
                                            }
                                            if($rowManager->level_id == 6){
                                                $level = 'MQB';
                                            }
                                            if($rowManager->level_id == 7){
                                                $level = 'Manager';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$rowManager->username}}</td>
                                                <td>{{$rowManager->name}}</td>
                                                <td>{{$level}}</td>
                                                <td>{{$rowManager->cabang_name}}</td>
                                                @if($add == true)
                                                    <td class="td-actions text-left" >
                                                        <div class="table-icons">
                                                            <a rel="tooltip" title="View" class="text-primary" href="{{ URL::to('/') }}/view-manager/{{$rowManager->id}}"><i class="nc-icon nc-badge"></i></a>
                                                            &nbsp;&nbsp;
                                                            <a rel="tooltip" title="View Crew" class="text-primary" href="{{ URL::to('/') }}/adm/view-down/{{$rowManager->id}}"><i class="nc-icon nc-bullet-list-67"></i></a>
                                                            &nbsp;&nbsp;
                                                            <a rel="tooltip" title="Structure" class="text-primary" href="{{ URL::to('/') }}/adm/view-structure/{{$rowManager->id}}"><i class="nc-icon nc-vector"></i></a>
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@if($download == true)
    <script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export Daftar Manager'
                    }
                ],
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: -1 }
                 ]
        } );
    } );
</script>
@stop