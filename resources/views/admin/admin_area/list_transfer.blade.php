@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">Filter Pengeluaran</h5>
                    </div>
                    <div class="card-body" style="min-height: auto;">
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <form class="login100-form validate-form" method="post" action="/adm/search/data-transfer">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tanggal Awal</label>
                                            <input type="text" class="form-control datepickerStart" name="start_date">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tanggal Akhir</label>
                                            <input type="text" class="form-control datepickerEnd" name="end_date">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select class="form-control" name="type">
                                                <option value="100">All</option>
                                                <option value="0">Confirm</option>
                                                <option value="1">Approve</option>
                                                <option value="2">Reject</option>
                                                <option value="3">Re-Confirm</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                             <button type="submit" class="form-control btn btn-sm btn-info " title="cari" style="margin: 0;">Cari</button>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Cabang</th>
                                        @if($dataUser->id != 2220)
                                            <th>Invoice</th>
                                        @endif
                                        <th>Transfer (Rp.)</th>
                                        <th>Tgl</th>
                                        @if($add == true)
                                            <th class="td-actions text-left">###</th>
                                        @endif
                                        @if($dataUser->id == 2220)
                                            <th>Status</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                            <?php $no++; ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->cabang_name}}</td>
                                                @if($dataUser->id != 2220)
                                                    <td>{{$row->transfer_invoice}}</td>
                                                @endif
                                                <td>{{number_format($row->transfer_price, 0, ',', ',')}}</td>
                                                <td><?php  echo (date('d M Y', strtotime($row->transfer_date))) ?></td>
                                                @if($add == true)
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip" title="View Transfer {{$row->transfer_invoice}}" class="btn btn-sm btn-info" style="padding: 3px 6px;" href="{{ URL::to('/') }}/adm/transfer/{{$row->id}}"><i class="nc-icon nc-badge"></i></a>
                                                        @if($row->is_transfer == 0)
                                                            <a class="btn btn-sm btn-info" style="padding: 3px 6px;" href="{{ URL::to('/') }}/adm/transfer/{{$row->id}}">Confirm</a>
                                                        @endif
                                                        @if($row->is_transfer == 1)
                                                            <span class="btn btn-sm btn-success" style="padding: 3px 6px;">Approve</span>
                                                        @endif
                                                        @if($row->is_transfer == 2)
                                                            <span class="btn btn-sm btn-warning" style="padding: 3px 6px;">Reject</span>
                                                        @endif
                                                        @if($row->is_transfer == 3)
                                                            <a class="btn btn-sm btn-info" style="padding: 3px 6px;" href="{{ URL::to('/') }}/adm/transfer/{{$row->id}}">Re-Confirm</a>
                                                        @endif
                                                    </div>
                                                </td>
                                                @endif
                                                @if($dataUser->id == 2220)
                                                    <td class="td-actions text-left" >
                                                        <div class="table-icons">
                                                            @if($row->is_transfer == 0)
                                                                <span class="btn btn-sm btn-info" style="padding: 3px 6px;">Confirm</span>
                                                            @endif
                                                            @if($row->is_transfer == 1)
                                                                <span class="btn btn-sm btn-success" style="padding: 3px 6px;">Approve</span>
                                                            @endif
                                                            @if($row->is_transfer == 2)
                                                                <span class="btn btn-sm btn-warning" style="padding: 3px 6px;">Reject</span>
                                                            @endif
                                                            @if($row->is_transfer == 3)
                                                                <span class="btn btn-sm btn-info" style="padding: 3px 6px;">Re-Confirm</span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

    @section('admin-styles')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
    @stop

    @section('admin-javascript')
    <script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@if($download == true)
    <script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
@endif
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
    <script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'export {{$headerTitle}}'
                        }
                    ],
                    searching: false,
                    columnDefs: [
                        { orderable: false, targets: -1 }
                     ],
                     pageLength: 10
            } );
        } );

        $("#confirmAdmin").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find(".modal-content").load(link.attr("href"));
        });
        
        $('.datepickerStart').datetimepicker({
            format: 'YYYY-MM-DD',
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fa fa-trash',
              close: 'fa fa-remove'
            },
            maxDate: '<?php echo date('Y-m-d',strtotime("1 days")); ?>',
        });
        $('.datepickerEnd').datetimepicker({
            format: 'YYYY-MM-DD',
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fa fa-trash',
              close: 'fa fa-remove'
            },
            maxDate: '<?php echo date('Y-m-d',strtotime("1 days")); ?>',
        });
    </script>
    @stop
