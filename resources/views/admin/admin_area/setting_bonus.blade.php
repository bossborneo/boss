@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')

<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">{{$headerTitle}}</h5>
                        @if($dataUser->user_type <= 2)
                            <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/edit/setting/bonus">Edit Setting</a>
                        @endif
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Crew Alat Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->crew_alat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Crew Strip Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->crew_strip, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Crew Strip Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->crew_strip_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Crew Obat 60 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->crew_obat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Crew Obat 60 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->crew_obat_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Crew Obat 30 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->crew_obat/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Crew Obat 30 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->crew_obat_month/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>TL Strip Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->tl_strip, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>TL Strip Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->tl_strip_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>TL Obat 60 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->tl_obat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>TL Obat 60 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->tl_obat_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>TL Obat 30 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->tl_obat/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>TL Obat 30 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->tl_obat_month/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Asmen Strip Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->am_strip, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Asmen Strip Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->am_strip_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Asmen Obat 60 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->am_obat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Asmen Obat 60 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->am_obat_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Asmen Obat 30 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->am_obat/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Asmen Obat 30 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->am_obat_month/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
<!--                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Asmen Omset</label>
                                    <input type="text" class="form-control" value="{{$getData->am_omset}}" disabled="">
                                </div>
                            </div>-->
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Mgr Alat</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->m_alat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Mgr Strip Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->m_strip, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Mgr Strip Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->m_strip_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Mgr Obat 60 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->m_obat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Mgr Obat 60 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->m_obat_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Mgr Obat 30 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->m_obat/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Mgr Obat 30 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->m_obat_month/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
<!--                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Manager Omset</label>
                                    <input type="text" class="form-control" value="{{$getData->m_omset}}" disabled="">
                                </div>
                            </div>-->
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>MQB Strip Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->mqb_strip, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>MQB Strip Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->mqb_strip_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>MQB Obat 60 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->mqb_obat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>MQB Obat 60 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->mqb_obat_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>MQB Obat 30 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->mqb_obat/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>MQB Obat 30 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->mqb_obat_month/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SM Strip Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sm_strip, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SM Strip Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sm_strip_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SM Obat 60 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sm_obat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SM Obat 60 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sm_obat_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SM Obat 30 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sm_obat/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SM Obat 30 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sm_obat_month/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>EM Alat</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->em_alat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>EM Strip Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->em_strip, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>EM Strip Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->em_strip_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>EM Obat 60 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->em_obat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>EM Obat 60 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->em_obat_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>EM Obat 30 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->em_obat/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>EM Obat 30 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->em_obat_month/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SEM Alat</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sem_alat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SEM Strip Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sem_strip, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SEM Strip Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sem_strip_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SEM Obat 60 harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sem_obat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SEM Obat 60 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sem_obat_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SEM Obat 30 harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sem_obat/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>SEM Obat 30 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->sem_obat_month/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>GM Alat</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->gm_alat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>GM Strip Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->gm_strip, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>GM Strip Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->gm_strip_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>GM Obat 60 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->gm_obat, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>GM Obat 60 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->gm_obat_month, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>GM Obat 30 Harian</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->gm_obat/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>GM Obat 30 Bulanan</label>
                                    <input type="text" class="form-control" value="Rp. {{number_format($getData->gm_obat_month/2, 0, ',', '.')}}" disabled="">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Asmen Omset Obat</label>
                                    <input type="text" class="form-control" value="{{number_format($getData->asmen_omset_obat, 0, ',', '.')}} %" disabled="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Manager Omset Obat</label>
                                    <input type="text" class="form-control" value="{{number_format($getData->manager_omset_obat, 0, ',', '.')}} %" disabled="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop