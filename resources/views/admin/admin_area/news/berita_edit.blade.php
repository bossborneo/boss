@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')

<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">{{$headerTitle}}</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/contents">List Berita</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <form class="login100-form validate-form" method="post" action="/adm/edit/content">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Judul Berita</label>
                                        <input type="text" class="form-control" name="title" required="" autocomplete="off" value="{{$getData->title}}">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Link Gambar</label>
                                        <input type="text" class="form-control" name="image_url" required="" autocomplete="off" value="{{$getData->image_url}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Deskripsi Singkat</label>
                                        <textarea class="form-control textarea" name="short_desc" id="editor" required="" rows="4">{{$getData->short_desc}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Deskripsi Lengkap</label>
                                        <textarea class="form-control textarea" name="full_desc" id="editor1" required="">{{$getData->full_desc}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="cekId" value="{{$getData->id}}" >
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary btn-round">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop


@section('admin-styles')
<link rel="stylesheet" type="text/css" href="https://simditor.tower.im/assets/styles/simditor.css" />
@stop

@section('admin-javascript')
<script type="text/javascript" src="https://simditor.tower.im/assets/scripts/module.js"></script>
<script type="text/javascript" src="https://simditor.tower.im/assets/scripts/hotkeys.js"></script>
<script type="text/javascript" src="https://simditor.tower.im/assets/scripts/uploader.js"></script>
<script type="text/javascript" src="https://simditor.tower.im/assets/scripts/simditor.js"></script>
<script>
  var editor = new Simditor({
        textarea: $('#editor')
  });
</script>
<script>
   var editor = new Simditor({
        textarea: $('#editor1')
  });
</script>
@stop
