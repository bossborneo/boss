@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-3">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-box text-success"></i>
                                </div>
                            </div>
                            <div class="col-7 col-md-9">
                                <div class="numbers">
                                    <p class="card-category">{{$dataOrder->name}}</p>
                                    <p class="card-title">{{$dataOrder->cabang_name}}
                                    </p>
                                    <p>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/list-order-barang">List Order</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        @if($dataOrder->status == 1)
                        <form class="login100-form validate-form" method="post" action="/adm/order-barang">
                        {{ csrf_field() }}
                        <input type="hidden" name="order_id" value="{{$dataOrder->id}}">
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Barang</th>
                                        <th>Type</th>
                                        <th>Jumlah</th>
                                        <th>Keterangan</th>
                                        <th>##</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                    @if($getData != null)
                                        @foreach($getData as $rows)
                                            <?php
                                                $no++;
                                                $type = 'Obat';
                                                if($rows->type == 1){
                                                    $type = 'Alat';
                                                }
                                                if($rows->type == 2){
                                                    $type = 'Strip';
                                                }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$rows->purchase_name}}</td>
                                                <td>{{$type}}</td>
                                                <td><input type="number" min="0" step="0.5" class="form-control" name="qty[]" style="width: 100px;"></td>
                                                <td><input type="text" class="form-control" name="keterangan[]" style="width: 300px;"></td>
                                                <td><a rel="tooltip"  data-toggle="modal" data-target="#rmStock" class="text-danger" href="{{ URL::to('/') }}/adm/rm/order/{{$dataOrder->id}}/{{$rows->order_item_id}}"><i class="nc-icon nc-simple-remove"></i></a></td>
                                                <input type="hidden" name="order_item_id[]" value="{{$rows->order_item_id}}">
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary btn-round">Confirm</button>
                                </div>
                            </div>
                        </form>
                        @else
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Barang</th>
                                        <th>Type</th>
                                        <th>Jumlah</th>
                                        <th>Status</th>
                                        <th>###</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                    @if($getData != null)
                                        @foreach($getData as $rows)
                                            <?php
                                                $no++;
                                                $type = 'Obat';
                                                if($rows->type == 1){
                                                    $type = 'Alat';
                                                }
                                                if($rows->type == 2){
                                                    $type = 'Strip';
                                                }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$rows->purchase_name}}</td>
                                                <td>{{$type}}</td>
                                                <td>{{number_format($rows->qty, 1, '.', '.')}}</td>
                                                <td class="td-actions text-left">
                                                    <div class="table-icons">
                                                        @if($rows->item_terima_at == null)
                                                            <p class="btn btn-warning btn-sm">proses confirm</p>
                                                        @endif
                                                        @if($rows->item_terima_at != null)
                                                            <p class="btn btn-success btn-sm">terima</p>
                                                        @endif
                                                    </div>
                                                </td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip"  data-toggle="modal" data-target="#editStock" class="text-primary" href="{{ URL::to('/') }}/adm/edit/order-barang/{{$dataOrder->id}}/{{$rows->order_item_id}}"><i class="nc-icon nc-badge"></i></a>
                                                        &nbsp;&nbsp;
                                                        <a rel="tooltip"  data-toggle="modal" data-target="#rmStock" class="text-danger" href="{{ URL::to('/') }}/adm/rm/order/{{$dataOrder->id}}/{{$rows->order_item_id}}"><i class="nc-icon nc-simple-remove"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="editStock" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="modal fade" id="rmStock" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-javascript')
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                "pageLength": 50
        } );
    } );
    
    $("#editStock").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $("#rmStock").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop