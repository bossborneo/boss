@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        @if($add == true)
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/add-purchase">Tambah Barang</a>
                        @endif  
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Type</th>
                                        <th>Promo</th>
                                        <th>Poin</th>
                                        <th>HPP</th>
                                        <th>Harga</th>
                                        <th>Status</th>
                                        @if($add == true)
                                        <th class="td-actions text-left">##</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                    ?>
                                    @if($purchase != null)
                                        @foreach($purchase as $row)
                                            <?php 
                                            $no++;
                                            $type = '--';
                                            if($row->type == 1){
                                                $type = 'Alat';
                                            }
                                            if($row->type == 2){
                                                $type = 'Strip';
                                            }
                                            if($row->type == 3){
                                                $type = 'Obat';
                                            }
                                            if($row->type == 10){
                                                $type = 'Lainnya';
                                            }
                                            $status = 'Aktif';
                                            $button = 'success';
                                            $text = 'Apakah anda ingin menghapus barang '.$row->purchase_name.'?';
                                            $cek = 'nc-simple-remove';
                                            if($row->deleted_at != null){
                                                $status = 'NonAktif';
                                                $button = 'danger';
                                                $text = 'Apakah anda ingin mengaktifkan barang '.$row->purchase_name.'?';
                                                $cek = 'nc-check-2';
                                            }
                                            $kondisi = 'Normal';
                                            if($row->kondisi == 1){
                                                $kondisi = 'Kondisional';
                                            }
                                            $poin = 'Normal';
                                            if($row->is_poin == 1){
                                                $poin = 'Poin 1/2';
                                            }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->purchase_name}}</td>
                                                <td>{{$type}}</td>
                                                <td>{{$kondisi}}</td>
                                                <td>{{$poin}}</td>
                                                <td>{{number_format($row->hpp, 0, ',', '.')}}</td>
                                                <td>{{number_format($row->main_price, 0, ',', '.')}}</td>
                                                <th><span class="btn btn-sm btn-{{$button}}" style="padding: 3px 8px;">{{$status}}</span></th>
                                                @if($add == true)
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip" title="View" class="text-primary" href="{{ URL::to('/') }}/edit-purchase/{{$row->id}}"><i class="nc-icon nc-badge"></i></a>
                                                        &nbsp;&nbsp;
                                                        @if($row->deleted_at == null)
                                                            <a rel="tooltip" title="Remove" onclick="return confirm('{{$text}}')" class="text-danger" href="{{ URL::to('/') }}/status-purchase/{{$row->id}}" onclick="return confirm('Apakah anda ingin menghapus Barang {{$row->purchase_name}}')"><i class="nc-icon {{$cek}}"></i></a>
                                                        @endif
                                                        @if($row->deleted_at != null)
                                                            <a rel="tooltip" title="Active" onclick="return confirm('{{$text}}')" class="text-success" href="{{ URL::to('/') }}/status-purchase/{{$row->id}}" onclick="return confirm('Apakah anda ingin mangaktifkan Barang {{$row->purchase_name}}')"><i class="nc-icon {{$cek}}"></i></a>
                                                        @endif
                                                    </div>
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: -1 },
                    { orderable: false, targets: -2 }
                 ]
        } );
    } );
</script>
@stop