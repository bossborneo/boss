@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')

<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">{{$headerTitle}}</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/all-purchase">Data Barang</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <form class="login100-form validate-form" method="post" action="/edit-purchase/{{$barang->id}}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Nama Barang</label>
                                        <input type="text" class="form-control" name="purchase_name" value="{{$barang->purchase_name}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Jenis Barang</label>
                                        <select class="form-control" name="type">
                                            <option>- Pilih Type -</option>
                                            <option value="1" @if($barang->type == 1) Selected @endif>Alat</option>
                                            <option value="2" @if($barang->type == 2) Selected @endif>Strip</option>
                                            <option value="3" @if($barang->type == 3) Selected @endif>Obat</option>
                                            <option value="10" @if($barang->type == 10) Selected @endif>Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>HPP</label>
                                        <input type="text" class="form-control" name="hpp" value="{{number_format($barang->hpp, 0, ',', '')}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Harga</label>
                                        <input type="text" class="form-control" name="main_price" value="{{number_format($barang->main_price, 0, ',', '')}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Promo</label>
                                        <select class="form-control" name="kondisi">
                                            <option value="0" @if($barang->kondisi == 0) Selected @endif>Normal</option>
                                            <option value="1" @if($barang->kondisi == 1) Selected @endif>Kondisional</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Poin</label>
                                        <select class="form-control" name="is_poin">
                                            <option value="0" @if($barang->is_poin == 0) Selected @endif>Normal</option>
                                            <option value="1" @if($barang->is_poin == 1) Selected @endif>Poin 1/2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <input type="hidden" name='cekId' value="{{$barang->id}}">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary btn-round">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop