@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/absen">List Crew Absen</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Absen</th>
                                        <th>Jam Masuk</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                    @if($geData != null)
                                        @foreach($geData as $rows)
                                            <?php
                                                $no++;
                                                $level = '';
                                                if($rows->level_id == 2){
                                                    $level = 'General Manager';
                                                }
                                                if($rows->level_id == 3){
                                                    $level = 'Senior Executive Manager';
                                                }
                                                if($rows->level_id == 4){
                                                    $level = 'Executive Manager';
                                                }
                                                if($rows->level_id == 5){
                                                    $level = 'Senior Manager';
                                                }
                                                if($rows->level_id == 6){
                                                    $level = 'MQB';
                                                }
                                                if($rows->level_id == 7){
                                                    $level = 'Manager';
                                                }
                                                if($rows->level_id == 8){
                                                    $level = 'Asisten Manager';
                                                }
                                                if($rows->level_id == 9){
                                                    $level = 'Top Leader';
                                                }
                                                if($rows->level_id == 10){
                                                    $level = 'Leader';
                                                }
                                                if($rows->level_id == 11){
                                                    $level = 'Trainer';
                                                }
                                                if($rows->level_id == 12){
                                                    $level = 'Merchandiser';
                                                }
                                                if($rows->level_id == 13){
                                                    $level = 'Retrainer';
                                                }
                                                $is_masuk = 'Masuk';
                                                if($rows->is_masuk == 2){
                                                    $is_masuk = 'Sakit';
                                                }
                                                if($rows->is_masuk == 3){
                                                    $is_masuk = 'Izin';
                                                }
                                                if($rows->is_masuk == 4){
                                                    $is_masuk = 'Alpha';
                                                }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$rows->name}}</td>
                                                <td>{{$level}}</td>
                                                <td>{{$is_masuk}}</td>
                                                <td>
                                                    @if($rows->is_masuk == 1)
                                                        <?php  echo (date('d M Y H:i', strtotime($rows->masuk_at))) ?>
                                                    @else
                                                        <?php  echo (date('d M Y', strtotime($rows->masuk_at))) ?>
                                                    @endif
                                                </td>
                                                <td>{{$rows->keterangan}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@if($download == true)
    <script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'export Absen Detail {{$geData[0]->name}}'
                        }
                    ],
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: -1 },
                 ]
        } );
    } );
    $("#editModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $("#upgradeModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop