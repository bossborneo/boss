@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5>Filter</h5>
                    </div>
                    <div class="card-body" style="min-height: auto;">
                        <form class="login100-form validate-form" method="get" action="/adm/global-manager/{{$type}}">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Tanggal Awal</label>
                                            <input type="text" class="form-control datepickerStart" name="start_date">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Tanggal Akhir</label>
                                            <input type="text" class="form-control datepickerEnd" name="end_date">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                             <button type="submit" class="form-control btn btn-sm btn-info " title="tambah barang" style="margin: 0;">Cari</button>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Global {{$headerTitle}}</h5>
                         <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/global-manager/gm">Global GM</a>
                         <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/global-manager/sem">Global SEM</a>
                         <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/global-manager/em">Global EM</a>
                         <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/global-manager/sm">Global SM</a>
                         <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/global-manager/mqb">Global MQB</a>
                         <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/global-manager/manager">Global Manager</a>
                    </div>
                    <div class="card-body">
                        <p class="title">{{$day}}</p>
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <?php 
                                        /*
                                         * <th>No. Tlp</th> 
                                         */ 
                                        ?>
                                        <th>Cabang</th>
                                        <?php
                                        /*
                                        <th>Alat</th>
                                        <th>Alat (Rp)</th>
                                         */
                                        ?>
                                        <th>Alat 10 (Jml)</th>
                                        <th>Alat 10 (Rp)</th>
                                        <th>Alat 20 (Jml)</th>
                                        <th>Alat 20 (Rp)</th>
                                        <th>Strip (Jml)</th>
                                        <th>Strip (Rp)</th>
                                        <th>Obat (Jml)</th>
                                        <th>Obat (Rp)</th>
                                        <th>Obat 30 (Jml)</th>
                                        <th>Obat 30 (Rp)</th>
                                        <th>Obat 60 (Jml)</th>
                                        <th>Obat 60 (Rp)</th>
                                        <th>Total (Rp.)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                    @if($getData != null)
                                        @foreach($getData as $row)
                                        <?php 
                                            $no++;
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->name}}</td>
                                                <?php 
                                                /*
                                                <td>{{$row->phone}}</td>
                                                 */
                                                ?>
                                                <td>{{$row->cabang_name}}</td>
                                                <?php 
                                                /*
                                                <td>{{number_format($row->sum_alat, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sum_price_alat, 0, ',', ',')}}</td>
                                                 */
                                                ?>
                                                <td>{{number_format($row->sum_alat_10, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sum_price_alat_10, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->sum_alat_20, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sum_price_alat_20, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->sum_strip, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sum_price_strip, 0, ',', ',')}}</td>
                                                <?php
//                                                    $jmlObatNew = ($row->sum_obat_30_new/2) + $row->sum_obat_60_new;
                                                    $jmlObatNew = ($row->sum_obat_30_new/2) + $row->sum_obat_60_new;
                                                ?>
                                                <td>{{number_format($jmlObatNew, 2, '.', '.')}}</td>
                                                <td>{{number_format($row->sum_price_obat, 0, ',', ',')}}</td>
                                                <?php //<td>{{number_format($row->sum_obat_30_new/2, 1, '.', '.')}}</td> ?>
                                                <td>{{number_format($row->sum_obat_30_new/2, 2, '.', '.')}}</td>
                                                <td>{{number_format($row->sum_price_obat_30_new, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->sum_obat_60_new, 1, '.', '.')}}</td>
                                                <td>{{number_format($row->sum_price_obat_60_new, 0, ',', ',')}}</td>
                                                <?php
//                                                    $jmlPrice = $row->sum_price_alat_10 + $row->sum_price_alat_20 + $row->sum_price_strip + $row->sum_price_obat;
                                                    $jmlPrice = $row->sum_price_alat_10 + $row->sum_price_alat_20 + $row->sum_price_strip + $row->sum_price_obat_30_new + $row->sum_price_obat_60_new;
                                                ?>
                                                <?php
                                                /*
                                                <td>{{number_format($row->jml_price, 0, ',', ',')}}</td>
                                                 */
                                                ?>
                                                <td>{{number_format($jmlPrice, 0, ',', ',')}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@if($download == true)
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
@endif
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export {{$headerTitle}}'
                    }
                ],
                searching: false
        } );
    } );
    $('.datepickerStart').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
    $('.datepickerEnd').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
</script>
@stop