@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
          <div class="col-md-6">
            <form class="login100-form validate-form" method="get" action="/view-crew-cabang">
                {{ csrf_field() }}
              <div class="card ">
                <div class="card-header ">
                  <h6 class="card-title">Filter per Cabang</h6>
                </div>
                <div class="card-body ">
                  <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control" name="cabang">
                            <option value="">- Cabang -</option>
                            @if($allCabang != null)
                                @foreach($allCabang as $row)
                                    <option value="{{$row->id}}">{{$row->cabang_name}}</option>
                                @endforeach
                            @endif
                    </select>
                    </div>
                </div>
                </div>
                <div class="card-footer text-right">
                  <button type="submit" class="btn btn-primary">Cari</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        @if($allCrew != null)
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                            <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/view-managers">Daftar Manager</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Tgl Aktif</th>
                                        @if($add == true)
                                            <th class="td-actions text-left">##</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <?php $no = 0; ?>
                                        @foreach($allCrew as $row)
                                        <?php
                                            $no++;
                                            $level = 'General Manager';
                                            if($row->level_id == 3){
                                                $level = 'Senior Executive Manager';
                                            }
                                            if($row->level_id == 4){
                                                $level = 'Executive Manager';
                                            }
                                            if($row->level_id == 5){
                                                $level = 'Senior Manager';
                                            }
                                            if($row->level_id == 6){
                                                $level = 'MQB';
                                            }
                                            if($row->level_id == 7){
                                                $level = 'Manager';
                                            }
                                            if($row->level_id == 8){
                                                $level = 'Assistant Manager';
                                            }
                                            if($row->level_id == 9){
                                                $level = 'Top Leader';
                                            }
                                            if($row->level_id == 10){
                                                $level = 'Leader';
                                            }
                                            if($row->level_id == 11){
                                                $level = 'Trainer';
                                            }
                                            if($row->level_id == 12){
                                                $level = 'Merchandiser';
                                            }
                                            if($row->level_id == 13){
                                                $level = 'Retrainer';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$level}}</td>
                                                <td><?php  echo (date('d M Y', strtotime($row->active_at))) ?></td>
                                                @if($add == true)
                                                    <td class="td-actions text-left" >
                                                        <div class="table-icons">
                                                            <a rel="tooltip"  title="Edit Date" data-toggle="modal" data-target="#editModal" class="text-primary" href="{{ URL::to('/') }}/adm/edit-date/{{$row->id}}"><i class="nc-icon nc-badge"></i></a>
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    
                                </tbody>
                            </table>
                            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="crewModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@stop


@if($allCrew != null)
@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
<style>
    .datetimepicker {
      z-index: 1600 !important; /* has to be larger than 1050 */
    }
</style>
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@if($view == true)
    <script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
@endif
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export Daftar Crew Cabang {{$getCabang->cabang_name}}'
                    }
                ],
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: -1 }
                 ]
        } );
    } );
    
    $("#editModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    
    

</script>
@stop
@endif