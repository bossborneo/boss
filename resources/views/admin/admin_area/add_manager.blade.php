@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')

<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">{{$headerTitle}}</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/view-managers">Data Manager</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <form class="login100-form validate-form" method="post" action="/new-manager">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cabang</label>
                                        <select class="form-control" name="cabang" id="cabang_id">
                                            <option selected>- Cabang -</option>
                                            @foreach($allCabang as $rowCabang)
                                                <option value="{{$rowCabang->id}}">{{$rowCabang->cabang_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Posisi</label>
                                        <select class="form-control" name="level_id" onChange="getTypeSearch(this.value);">
                                            <option selected>- Posisi -</option>
                                                <option value="2">General Manager</option>
                                                <option value="3">Senior Executive Manager</option>
                                                <option value="4">Executive Manager</option>
                                                <option value="5">Senior Manager</option>
                                                <option value="6">MQB</option>
                                                <option value="7">Manager</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="typeSearch-list"></div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" name="username">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Ketik Ulang password</label>
                                        <input type="password" class="form-control" name="repassword">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Lengkap</label>
                                        <input type="text" class="form-control" name="name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No Telepon</label>
                                        <input type="text" class="form-control" name="phone">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea class="form-control textarea" name="address"></textarea>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary btn-round">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-javascript')
<script type="text/javascript">
    
    function getTypeSearch(val) {
        var cabId = $( "#cabang_id" ).val();
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/search-type" + "/" + val + "/" + cabId,
            success: function(url){
                    $( "#typeSearch-list" ).empty();
                    $("#typeSearch-list").html(url);
            }
        });
    }
</script>
@stop
