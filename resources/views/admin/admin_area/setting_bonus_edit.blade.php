@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')

<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">{{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        <form class="login100-form validate-form" method="post" action="/adm/setting/bonus">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Crew Alat Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->crew_alat}}" name="crew_alat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Crew Strip Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->crew_strip}}" name="crew_strip">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Crew Strip Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->crew_strip_month}}" name="crew_strip_month">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Crew Obat Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->crew_obat}}" name="crew_obat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Crew Obat Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->crew_obat_month}}" name="crew_obat_month">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>TLD Strip Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->tl_strip}}" name="tl_strip">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>TLD Strip Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->tl_strip_month}}" name="tl_strip_month">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>TLD Obat harian</label>
                                        <input type="number" class="form-control" value="{{$getData->tl_obat}}" name="tl_obat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>TLD Obat Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->tl_obat_month}}" name="tl_obat_month">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Asmen Strip harian</label>
                                        <input type="number" class="form-control" value="{{$getData->am_strip}}" name="am_strip">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Asmen Strip Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->am_strip_month}}" name="am_strip_month">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Asmen Obat harian</label>
                                        <input type="number" class="form-control" value="{{$getData->am_obat}}" name="am_obat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Asmen Obat Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->am_obat_month}}" name="am_obat_month">
                                    </div>
                                </div>
<!--                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Asmen Omset</label>
                                        <input type="text" class="form-control" value="{{$getData->am_omset}}" name="am_omset">
                                    </div>
                                </div>-->
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Mgr Alat </label>
                                        <input type="number" class="form-control" value="{{$getData->m_alat}}" name="m_alat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Mgr Strip Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->m_strip}}" name="m_strip">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Mgr Strip Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->m_strip_month}}" name="m_strip_month">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Mgr Obat Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->m_obat}}" name="m_obat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Mgr Obat Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->m_obat_month}}" name="m_obat_month">
                                    </div>
                                </div>
<!--                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Manager Omset</label>
                                        <input type="text" class="form-control" value="{{$getData->m_omset}}" name="m_omset">
                                    </div>
                                </div>-->
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>MQB Strip Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->mqb_strip}}" name="mqb_strip">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>MQB Strip Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->mqb_strip_month}}" name="mqb_strip_month">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>MQB Obat Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->mqb_obat}}" name="mqb_obat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>MQB Obat Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->mqb_obat_month}}" name="mqb_obat_month">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>SM Strip Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->sm_strip}}" name="sm_strip">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>SM Strip Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->sm_strip_month}}" name="sm_strip_month">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>SM Obat Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->sm_obat}}" name="sm_obat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>SM Obat Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->sm_obat_month}}" name="sm_obat_month">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>EM Alat </label>
                                        <input type="number" class="form-control" value="{{$getData->em_alat}}" name="em_alat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>EM Strip Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->em_strip}}" name="em_strip">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>EM Strip Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->em_strip_month}}" name="em_strip_month">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>EM Obat Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->em_obat}}" name="em_obat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>EM Obat Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->em_obat_month}}" name="em_obat_month">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>SEM Alat </label>
                                        <input type="number" class="form-control" value="{{$getData->sem_alat}}" name="sem_alat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>SEM Strip Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->sem_strip}}" name="sem_strip">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>SEM Strip Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->sem_strip_month}}" name="sem_strip_month">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>SEM Obat Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->sem_obat}}" name="sem_obat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>SEM Obat Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->sem_obat_month}}" name="sem_obat_month">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>GM Alat </label>
                                        <input type="number" class="form-control" value="{{$getData->gm_alat}}" name="gm_alat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>GM Strip Harian</label>
                                        <input type="number" class="form-control" value="{{$getData->gm_strip}}" name="gm_strip">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>GM Strip Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->gm_strip_month}}" name="gm_strip_month">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>GM Obat harian</label>
                                        <input type="number" class="form-control" value="{{$getData->gm_obat}}" name="gm_obat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>GM Obat Bulanan</label>
                                        <input type="number" class="form-control" value="{{$getData->gm_obat_month}}" name="gm_obat_month">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Asmen Omset Obat (%)</label>
                                        <input type="number" class="form-control" value="{{$getData->asmen_omset_obat}}" name="asmen_omset_obat">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Manager Omset Obat (%)</label>
                                        <input type="number" class="form-control" value="{{$getData->manager_omset_obat}}" name="manager_omset_obat">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <a class="btn btn-dark btn-round" href="{{ URL::to('/') }}/adm/setting/bonus">Cancel</a>
                                    <button type="submit" class="btn btn-primary btn-round">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop