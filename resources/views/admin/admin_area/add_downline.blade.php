@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')

<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">{{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <form id="addForm" class="login100-form validate-form" method="post" action="/adm/add-down/{{$dataManager->id}}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Cabang (disabled)</label>
                                        <input type="text" class="form-control" disabled="" id="cabangId" value="{{$dataManager->cabang_name}}" data-id="{{$dataManager->cabang_id}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Level</label>
                                        <select class="form-control" name="level_id" onChange="getTypeSearch(this.value);">
                                            <option selected>- Pilih Level -</option>
                                            <option value="8">Assistance Manager</option>
                                            <option value="9">Top Leader</option>
                                            <option value="10">Leader</option>
                                            <option value="11">Trainer</option>
                                            <option value="12">Merchandiser</option>
                                            <option value="13">Retrainer</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="typeSearch-list" class="col-md-6">
                                </div>
                            </div>
                            <div class="row" id="typeSearchInfo-list">
                            </div>    
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Lengkap</label>
                                        <input type="text" class="form-control" name="name" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No Telepon</label>
                                        <input type="text" class="form-control" name="phone" placeholder="optional">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea class="form-control textarea" name="address"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <input type="hidden" name="cekId" value="{{$dataManager->id}}">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary btn-round">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-javascript')
<script type="text/javascript">
    
    function getTypeSearch(val) {
        var cabId = $('#cabangId').data('id');
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/adm/search-sp" + "/" + val + "/" + cabId + "/{{$id}}" ,
            success: function(url){
                    $( "#typeSearch-list" ).empty();
                    $("#typeSearch-list").html(url);
            }
        });
    }
    
    function getTypeSearchParent(val) {
        var cabId1 = $('#cabangId').data('id');
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/m/search-sp" + "/" + val + "/" + cabId1,
            success: function(url){
                    $( "#typeSearchInfo-list" ).empty();
                    $("#typeSearchInfo-list").html(url);
            }
        });
    }
</script>
@stop