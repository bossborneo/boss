@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        @if($add == true)
                            <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/add-cabang">Tambah Cabang</a>
                        @endif
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Extra Obat</th>
                                        <th>Extra Strip</th>
                                        <th>Extra Alat</th>
                                        <th>Wilayah</th>
                                        <th>Extra Wilayah</th>
                                        @if($add == true)
                                            <th class="td-actions text-left">##</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0; 
                                    ?>
                                    @if($allCabang != null)
                                        @foreach($allCabang as $rowCabang)
                                            <?php 
                                            $no++;
                                            $wilayah = 'Barat';
                                            if($rowCabang->wilayah == 1){
                                                $wilayah = 'Timur';
                                            }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$rowCabang->cabang_name}}</td>
                                                <td>{{number_format($rowCabang->extra, 0, ',', '.')}}</td>
                                                <td>{{number_format($rowCabang->extra_strip, 0, ',', '.')}}</td>
                                                <td>{{number_format($rowCabang->extra_alat, 0, ',', '.')}}</td>
                                                <td>{{$wilayah}}</td>
                                                <td>{{number_format($rowCabang->extra_wilayah, 0, ',', '.')}}</td>
                                                @if($add == true)
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip" title="View" class="text-primary" href="{{ URL::to('/') }}/view-cabang/{{$rowCabang->id}}"><i class="nc-icon nc-badge"></i></a>
                                                        &nbsp;&nbsp;
                                                        <a rel="tooltip" title="Remove" class="text-danger" href="{{ URL::to('/') }}/rm-cabang/{{$rowCabang->id}}" onclick="return confirm('Apakah anda ingin menghapus cabang {{$rowCabang->cabang_name}}')"><i class="nc-icon nc-simple-remove"></i></a>
                                                    </div>
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: -1 }
                 ]
        } );
    } );
</script>
@stop