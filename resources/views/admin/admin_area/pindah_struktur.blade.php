@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">Pindah Struktur</h5>
                        @if($dataUser->user_type == 1 || $dataUser->user_type == 2)
                            <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/view-managers">Daftar Manager</a>
                        @endif
                    </div>
                    <div class="card-body" style="min-height: auto;">
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <form class="login100-form validate-form" method="post" action="/adm/pindah-crew">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Nama Pindah Struktur</p>
                                    </div>    
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Cabang</label>
                                            <select class="form-control" name="cabang" onChange="getTypeSearch(this.value);">
                                                <option value="0">- Pilih Cabang -</option>
                                                @if($allCabang != null)
                                                    @foreach($allCabang as $row)
                                                        <option value="{{$row->id}}">{{$row->cabang_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8" id="typeSearch-list"></div>    
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Tujuan</p>
                                    </div>    
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Cabang</label>
                                            <select class="form-control" name="cabang" onChange="getTypeSearchTujuan(this.value);">
                                                <option value="0">- Pilih Cabang -</option>
                                                @if($allCabang != null)
                                                    @foreach($allCabang as $row)
                                                        <option value="{{$row->id}}">{{$row->cabang_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8" id="typeSearchTujuan-list"></div>    
                                </div>
                                <div class="row">
                                    <div class="update ml-auto mr-auto">
                                        <button type="submit" class="btn btn-primary btn-round">Submit</button>
                                    </div>
                                </div>
                            </div>    
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export {{$headerTitle}}'
                    }
                ],
                searching: false,
                columnDefs: [
                    { orderable: false, targets: -1 }
                 ],
                 pagingType: "full_numbers"
        } );
    } );
    $('.datepickerStart').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
    $('.datepickerEnd').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
        }
    });
</script>
<script type="text/javascript">
    
    function getTypeSearch(val) {
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/adm/search-name-all" + "/1/" + val ,
            success: function(url){
                    $( "#typeSearch-list" ).empty();
                    $("#typeSearch-list").html(url);
            }
        });
    }
    
    function getTypeSearchTujuan(val) {
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/adm/search-name-all" + "/2/" + val ,
            success: function(url){
                    $( "#typeSearchTujuan-list" ).empty();
                    $("#typeSearchTujuan-list").html(url);
            }
        });
    }
    
    function getTypeSearchCrew(val) {
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/adm/search-crewname" + "/" + val ,
            success: function(url){
                    $( "#typeSearchCrew-list" ).empty();
                    $("#typeSearchCrew-list").html(url);
            }
        });
    }
</script>
@stop
