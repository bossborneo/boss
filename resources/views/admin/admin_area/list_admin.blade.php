@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php
        $add = true;
        $view = true;
        $download = true;
        if($dataUser->user_type == 3){
            $add = false;
            $jsonRole = $dataUser->permissions;
            $role = json_decode($jsonRole);
            $view = $role->view;
            $download = $role->download;
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        @if($add == true)
                            <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/add-admin">Tambah Admin</a>
                        @endif
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }} 
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No.</th>
                                        <th>Username</th>
                                        <th>Name</th>
                                        <th>Role</th>
                                        <th>###</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($admin != null)
                                    <?php $no = 0; ?>
                                        @foreach($admin as $row)
                                        <?php
                                            $no++;
                                            $jsonRole = json_decode($row->permissions);
                                            $download = '';
                                            if($jsonRole->download == true){
                                                $download = 'btn-success';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->username}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>
                                                    <span class="btn btn-sm btn-success" style="padding: 3px 8px;">View</span>
                                                    <span class="btn btn-sm {{$download}}" style="padding: 3px 8px;">Download</span>
                                                </td>
                                                <td><a class="btn btn-sm btn-primary" style="padding: 3px 8px;" href="{{ URL::to('/') }}/adm/edit-admin/{{$row->id}}">Edit</a></td>
                                             </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop