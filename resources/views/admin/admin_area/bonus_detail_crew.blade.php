@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-toggle">
                <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <p class="navbar-brand">{{$dataUser->name}}</p>
        </div>
    </div>
</nav>
<?php
    $add = true;
    $view = true;
    $download = true;
    if($dataUser->user_type == 3){
        $add = false;
        $jsonRole = $dataUser->permissions;
        $role = json_decode($jsonRole);
        $view = $role->view;
        $download = $role->download;
    }
?>
<?php 
    $level = 'General Manager';
    if($getCrew->level_id == 3){
        $level = 'SEM';
    }
    if($getCrew->level_id == 4){
        $level = 'EM';
    }
    if($getCrew->level_id == 5){
        $level = 'SM';
    }
    if($getCrew->level_id == 6){
        $level = 'MQB';
    }
    if($getCrew->level_id == 7){
        $level = 'Manager';
    }
    if($getCrew->level_id == 8){
        $level = 'Asmen';
    }
    if($getCrew->level_id == 9){
        $level = 'TLD';
    }
    if($getCrew->level_id == 10){
        $level = 'LD';
    }
    if($getCrew->level_id == 11){
        $level = 'TR';
    }
    if($getCrew->level_id == 12){
        $level = 'MD';
    }
    if($getCrew->level_id == 13){
        $level = 'RT';
    }
?>
<div class="content">
    <div class="row">
        <div class="col-lg-5 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-3">
                            <div class="icon-big text-center icon-warning">
                                <i class="nc-icon nc-single-02 text-primary"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-9">
                            <div class="numbers">
                                <p class="card-category">Nama</p>
                                <p class="card-title" style="font-size: 18px;">{{$getCrew->name}}</p>
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-3">
                            <div class="icon-big text-center icon-warning">
                                <i class="nc-icon nc-diamond text-success"></i>
                            </div>
                        </div>
                        <div class="col-7 col-md-9">
                            <div class="numbers">
                                <p class="card-category">Total Bonus</p>
                                <?php
                                    $totalBonus = 0;
                                    if($getBonusTotal != null){
                                        $totalBonus = number_format($getBonusTotal->total_bonus, 0, ',', '.');
                                    }
                                ?>
                                <p class="card-title">Rp. {{$totalBonus}}
                                </p>
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($getDetailBonus != null)
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <?php
                        $day = '';
                        if($date != null){
                            $day = date('d-M-Y', strtotime($date->startDay)).' - '.date('d-M-Y', strtotime($date->endDay));
                        }
                    ?>
                    <h5 class="card-title"> Detail Bonus {{$day}}</h5>
                </div>
                <div class="card-body">
                <form class="login100-form validate-form" method="get" action="/adm/bonus/{{$id}}" style="margin-bottom: 20px;">
                        {{ csrf_field() }}
                        <div id="addPenjualan">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Start Date</label>
                                        <input type="text" class="form-control datepickerStart" name="start_date">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>End Date</label>
                                        <input type="text" class="form-control datepickerEnd" name="end_date">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <button class="form-control btn btn-sm btn-primary " style="margin: 0">Cari</button>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </form>
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead class=" text-primary">
                                <tr>
                                    <th>No</th>
                                    <th>Bonus (Rp)</th>
                                    <th>Jenis</th>
                                    <th>Tgl</th>
                                    <th>Barang</th>
                                    <th>Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0; ?>
                                @foreach($getDetailBonus as $row)
                                    <?php 
                                        $no++; 
                                        $type = 'Obat';
                                        if($row->purchase_type == 1){
                                            $type = 'Alat';
                                        }
                                        if($row->purchase_type == 2){
                                            $type = 'Strip';
                                        }
                                        $jenis = 'Lainnya';
                                        if($row->bonus_type == 1){
                                            $jenis = 'Harian';
                                        }
                                        if($row->bonus_type == 2){
                                            $jenis = 'Bulanan';
                                        }
                                    ?>
                                
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{number_format($row->bonus_price, 0, ',', '.')}}</td>
                                        <td>{{$jenis}}</td>
                                        <td><?php  echo (date('d M Y', strtotime($row->bonus_date))) ?></td>
                                        <td>{{$row->purchase_name}}</td>
                                        <td>{{$type}}</td>
                                    </tr>
                                @endforeach    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@stop

@section('admin-styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@if($download == true)
    <script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
    <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
@endif
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
$('.datepickerStart').datetimepicker({
    format: 'YYYY-MM-DD',
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-chevron-up",
      down: "fa fa-chevron-down",
      previous: 'fa fa-chevron-left',
      next: 'fa fa-chevron-right',
      today: 'fa fa-screenshot',
      clear: 'fa fa-trash',
      close: 'fa fa-remove'
    }
});
$('.datepickerEnd').datetimepicker({
    format: 'YYYY-MM-DD',
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-chevron-up",
      down: "fa fa-chevron-down",
      previous: 'fa fa-chevron-left',
      next: 'fa fa-chevron-right',
      today: 'fa fa-screenshot',
      clear: 'fa fa-trash',
      close: 'fa fa-remove'
    }
});
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export Bonus {{$getCrew->name}}_{{$level}}'
                    }
                ],
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: -1 },
                 ]
        } );
    } );
</script>
@stop