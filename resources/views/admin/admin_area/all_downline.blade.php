@extends('admin.layouts.main')
@section('admin-content')
<?php //SIDEBAR KIRI ?>
@include('admin.layouts.sidebar')
<?php //MENU UTAMA ?>
<div class="main-panel">
    <?php //MENU HEADER  ?>
    <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <p class="navbar-brand">{{$dataUser->name}}</p>
            </div>
        </div>
    </nav>
    <?php //BATAS MENU HEADER  ?>
    <?php 
        $level1 = 'Manager';
        if($dataManager->level_id == 2){
            $level1 = 'General Manager';
        }
        if($dataManager->level_id == 3){
            $level1 = 'Senior Executive Manager';
        }
        if($dataManager->level_id == 4){
            $level1 = 'Executive Manager';
        }
        if($dataManager->level_id == 5){
            $level1 = 'Senior Manager';
        }
        if($dataManager->level_id == 6){
            $level1 = 'MQB';
        }
    ?>
    <div class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="card card-user">
                    <div class="image">
                        <img src="/images/mlm.jpg" alt="...">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <div>
                                <img class="avatar border-gray" src="/images/default-avatar.png" alt="...">
                                <h5 class="title">{{$dataManager->name}}</h5>
                            </div>
                            <p class="description">
                                {{$level1}}
                            </p>
                        </div>
                        <h5 class="description text-center">{{$dataManager->cabang_name}}</h5>
                    </div>
                    <div class="card-footer">
                        <hr>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title"> {{$headerTitle}}</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/add-down/{{$dataManager->id}}">Tambah</a>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/bonus/{{$dataManager->id}}">Bonus</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }} 
                            </div>
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Level</th>
                                        <th>Tgl Aktif</th>
                                        <th class="td-actions text-left">##</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $noKosong = 0;
                                        ?>
                                    
                                    @if($dataDownline != null)
                                        @foreach($dataDownline as $row)
                                        <?php 
                                            $noKosong++;
                                            $level = 'General Manager';
                                            if($row->level_id == 3){
                                                $level = 'SEM';
                                            }
                                            if($row->level_id == 4){
                                                $level = 'Executive Manager';
                                            }
                                            if($row->level_id == 5){
                                                $level = 'Senior Manager';
                                            }
                                            if($row->level_id == 6){
                                                $level = 'MQB';
                                            }
                                            if($row->level_id == 7){
                                                $level = 'Manager';
                                            }
                                            if($row->level_id == 8){
                                                $level = 'Asistance Manager';
                                            }
                                            if($row->level_id == 9){
                                                $level = 'Top Leader';
                                            }
                                            if($row->level_id == 10){
                                                $level = 'Leader';
                                            }
                                            if($row->level_id == 11){
                                                $level = 'Trainer';
                                            }
                                            if($row->level_id == 12){
                                                $level = 'Merchandiser';
                                            }
                                            if($row->level_id == 13){
                                                $level = 'Retrainer';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$noKosong}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$level}}</td>
                                                <td><?php  echo (date('d M Y', strtotime($row->active_at))) ?></td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip" title="Bonus Detail" class="text-primary" href="{{ URL::to('/') }}/adm/bonus/{{$row->id}}"><i class="nc-icon nc-diamond"></i></a>
                                                            &nbsp;&nbsp;
                                                        <a rel="tooltip"  title="Upgrade {{$row->name}}" data-toggle="modal" data-target="#upgradeModal" class="text-primary" href="{{ URL::to('/') }}/adm/add-upgrade/{{$row->id}}/{{$dataManager->id}}"><i class="nc-icon nc-trophy"></i></a>
                                                            &nbsp;&nbsp;
                                                        <a rel="tooltip"  title="Edit {{$row->name}}" data-toggle="modal" data-target="#editModal" class="text-primary" href="{{ URL::to('/') }}/adm/edit-crew/{{$row->id}}/{{$dataManager->id}}"><i class="nc-icon nc-badge"></i></a>
                                                            &nbsp;&nbsp;
                                                        <a rel="tooltip"  title="Hapus {{$row->name}}" data-toggle="modal" data-target="#deleteModal" class="text-danger" href="{{ URL::to('/') }}/adm/rm-crew/{{$row->id}}/{{$dataManager->id}}"><i class="nc-icon nc-simple-remove"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="upgradeModal" tabindex="-1" role="dialog" aria-labelledby="crewModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="crewModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="crewModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('admin-styles')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('admin-javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'export_downline_manager {{$dataManager->name}}'
                        }
                    ],
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: -1 },
                 ]
        } );
    } );
    $("#upgradeModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $("#editModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $("#deleteModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop