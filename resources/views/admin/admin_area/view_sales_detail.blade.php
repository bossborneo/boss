<?php
        $level = 'General Manager';
        if($member->level_id == 3){
            $level = 'Senior Executive Manager';
        }
        if($member->level_id == 4){
            $level = 'Executive Manager';
        }
        if($member->level_id == 5){
            $level = 'Senior Manager';
        }
        if($member->level_id == 6){
            $level = 'MQB';
        }
        if($member->level_id == 7){
            $level = 'Manager';
        }
        if($member->level_id == 8){
            $level = 'Assistant Manager';
        }
        if($member->level_id == 9){
            $level = 'Top Leader';
        }
        if($member->level_id == 10){
            $level = 'Leader';
        }
        if($member->level_id == 11){
            $level = 'Trainer';
        }
        if($member->level_id == 12){
            $level = 'Merchandiser';
        }
        if($member->level_id == 13){
            $level = 'Retrainer';
        }
    ?>    

<div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$member->name}} ({{$level}})</h5>
        <h5 class="modal-title" id="exampleModalLabel">{{date('d-M-Y', strtotime($allSales[0]->sale_date))}}</h5>
    </div>
    <div class="modal-body">
        <div>
            <table class="table" id="mytable">
                <thead class=" text-primary">
                    <tr>
                        <th>No</th>
                        <th>Barang</th>
                        <th>Type</th>
                        <th>Jumlah</th>
                        <th>Harga (Rp.)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 0;  
                        $sum = 0;
                    ?>
                    @if($allSales != null)
                        @foreach($allSales as $row)
                        <?php 
                            $no++;
                            $type = '--';
                            if($row->type == 1){
                                $type = 'Alat';
                            }
                            if($row->type == 2){
                                $type = 'Strip';
                            }
                            if($row->type == 3){
                                $type = 'Obat';
                            }
                            if($row->type == 10){
                                $type = 'Lainnya';
                            }
                            $sum+= $row->sale_price;
                        ?>
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$row->purchase_name}}</td>
                                <td>{{$type}}</td>
                                <td>{{number_format($row->amount, 1, ',', '.')}}</td>
                                <td>{{number_format($row->sale_price, 0, ',', '.')}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td>&nbsp</td>
                            <td>TOTAL</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>{{number_format($sum, 0, ',', '.')}}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal-footer" style="margin-right: 10px;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
    </div>


